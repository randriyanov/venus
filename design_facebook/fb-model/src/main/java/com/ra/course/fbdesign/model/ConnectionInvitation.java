package com.ra.course.fbdesign.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class ConnectionInvitation extends BaseEntity {
    private Member memberInvited;
    private ConnectionInvitationStatus status;
    private LocalDateTime dateCreated;
    private LocalDateTime updateDate;
    private Notification notification;

}
