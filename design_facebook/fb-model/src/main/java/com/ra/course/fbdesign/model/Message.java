package com.ra.course.fbdesign.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Blob;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    private Set<Member> sendTo;
    private String messageBody;
    private Blob[] media;
    private Set<Notification> notifications;
    private Member author;
}
