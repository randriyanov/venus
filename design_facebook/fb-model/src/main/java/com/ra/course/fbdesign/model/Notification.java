package com.ra.course.fbdesign.model;

import java.time.LocalDateTime;

public abstract class Notification {
    protected int notificationId;
    protected LocalDateTime createdOn;
    protected String content;

    public abstract boolean send();
}
