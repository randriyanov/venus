package com.ra.course.fbdesign.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageBean extends BaseEntity {

    private String pageName;
    private String description;
    private String pageType;
    private int totalMembers;
    private Set<Recommendation> recommendations;
    private List<MediaContent> mediaContents;
    private Set<Member> membersFollows;
    private Member creator;

}
