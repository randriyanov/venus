package com.ra.course.fbdesign.model;

public enum AccountStatus {

    ACTIVE, CLOSED, CANCELED, BLACKLISTED, DISABLED;

    public boolean isActive() {
        return this == ACTIVE;
    }

    public boolean isClosed() {
        return this == CLOSED;
    }

    public boolean isCanceled() {
        return this == CANCELED;
    }

    public boolean isBlackListed() {
        return this == BLACKLISTED;
    }

    public boolean isDisabled() {
        return this == DISABLED;
    }

}
