package com.ra.course.fbdesign.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EducationDetails {
    private String school;
    private String degree;
    private String fromYear;
    private String toYear;
    private String description;

}
