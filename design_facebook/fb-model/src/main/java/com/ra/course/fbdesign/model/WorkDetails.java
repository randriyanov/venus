package com.ra.course.fbdesign.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorkDetails {

    private String title;
    private String company;
    private String location;
    private Date from;
    private Date to;
    private String description;

}
