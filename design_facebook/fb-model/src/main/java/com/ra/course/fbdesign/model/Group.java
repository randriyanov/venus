package com.ra.course.fbdesign.model;

import lombok.*;

import java.util.Set;

@EqualsAndHashCode(exclude = "members", callSuper = true)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Group extends BaseEntity {

    private Member creator;
    private String groupName;
    private String description;
    private int totalMembers;
    private Set<Member> members;

}
