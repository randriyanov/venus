package com.ra.course.fbdesign.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostClass extends BaseEntity {

    private String postText;
    private int totalLikes;
    private int totalShares;
    private Member postCreator;
    private Set<Member> membersShares;
    private Set<Member> membersLikes;
    private PrivacyList privacyList;
    private List<Comment> comments;

}
