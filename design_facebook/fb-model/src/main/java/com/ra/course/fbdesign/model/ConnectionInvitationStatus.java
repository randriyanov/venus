package com.ra.course.fbdesign.model;

public enum ConnectionInvitationStatus {

    PENDING, ACCEPTED, REJECTED, CANCELED;

    public boolean isPending() {
        return this == PENDING;
    }

    public boolean isAccepted() {
        return this == ACCEPTED;
    }

    public boolean isCanceled() {
        return this == CANCELED;
    }

    public boolean isRejected() {
        return this == REJECTED;
    }

}
