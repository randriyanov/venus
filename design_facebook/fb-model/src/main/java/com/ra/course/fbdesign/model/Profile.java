package com.ra.course.fbdesign.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.sql.Blob;
import java.time.LocalDateTime;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Profile extends BaseEntity {

    private LocalDateTime dateOfMembership;
    private Blob profilePicture;
    private Blob coverPhoto;
    private String gender;
    private List<WorkDetails> workDetailsList;
    private List<EducationDetails> educationDetails;
    private List<PlacesDetails> placesDetailsList;

}