package com.ra.course.fbdesign.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@EqualsAndHashCode(exclude = {"messages", "groups"}, callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Member extends Person {
    private Profile profile;
    private Set<Message> messages;
    private List<MediaContent> mediaContents;
    private Set<Recommendation> recommendations;
    private Set<Group> groups;
    private Set<Comment> comments;
    private PrivacyList friends;
    private PrivacyList followers;
    private List<ConnectionInvitation> conInvitations;
    private List<PostClass> postClasses;

}
