package com.ra.course.fbdesign.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person extends BaseEntity {
    protected String name;
    protected Address address;
    protected String email;
    protected String phone;
    protected Account account;

}
