package com.ra.course.fbdesign.persistence.repositories.impl;

import com.ra.course.fbdesign.model.ConnectionInvitation;
import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.persistence.repositories.IConInvitationRepo;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@NoArgsConstructor
public class ConInvitationRepoImpl extends BaseRepositoryImpl<ConnectionInvitation> implements IConInvitationRepo {

    @Override
    public Optional<ConnectionInvitation> getByMemberInvited(final Member member) {
        return repository.stream()
                .filter(c -> c.getMemberInvited().getId().equals(member.getId())).findAny();
    }
}
