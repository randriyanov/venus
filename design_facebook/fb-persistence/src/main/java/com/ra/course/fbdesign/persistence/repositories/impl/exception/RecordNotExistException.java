package com.ra.course.fbdesign.persistence.repositories.impl.exception;

public class RecordNotExistException extends RuntimeException{
    private static final long serialVersionUID = 384L;
}
