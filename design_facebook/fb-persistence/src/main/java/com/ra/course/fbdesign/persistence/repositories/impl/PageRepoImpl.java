package com.ra.course.fbdesign.persistence.repositories.impl;

import com.ra.course.fbdesign.model.PageBean;
import com.ra.course.fbdesign.persistence.repositories.IPageRepo;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@NoArgsConstructor
public class PageRepoImpl extends BaseRepositoryImpl<PageBean> implements IPageRepo {

    @Override
    public Optional<PageBean> getPageByName(final String pageName) {
        return repository.stream().filter(pageBean -> pageName.equals(pageBean.getPageName())).findAny();
    }
}
