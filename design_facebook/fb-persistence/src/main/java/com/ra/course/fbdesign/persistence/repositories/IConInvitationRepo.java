package com.ra.course.fbdesign.persistence.repositories;

import com.ra.course.fbdesign.model.ConnectionInvitation;
import com.ra.course.fbdesign.model.Member;

import java.util.Optional;

public interface IConInvitationRepo extends IBaseRepository<ConnectionInvitation>{
	Optional<ConnectionInvitation> getByMemberInvited(Member member);
}
