package com.ra.course.fbdesign.persistence.repositories.impl;

import com.ra.course.fbdesign.model.Profile;
import com.ra.course.fbdesign.persistence.repositories.IProfileRepo;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@NoArgsConstructor
public class ProfileRepoImpl extends BaseRepositoryImpl<Profile> implements IProfileRepo {

}
