package com.ra.course.fbdesign.persistence.repositories.impl;

import com.ra.course.fbdesign.model.Group;
import com.ra.course.fbdesign.persistence.repositories.IGroupRepo;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@NoArgsConstructor
public class GroupRepoImpl extends BaseRepositoryImpl<Group> implements IGroupRepo {

    @Override
    public Optional<Group> getGroupByName(final String groupName) {
        return repository.stream().filter(g -> groupName.equals(g.getGroupName())).findAny();
    }
}
