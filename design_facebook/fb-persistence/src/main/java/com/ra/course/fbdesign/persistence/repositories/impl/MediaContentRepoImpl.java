package com.ra.course.fbdesign.persistence.repositories.impl;

import com.ra.course.fbdesign.model.MediaContent;
import com.ra.course.fbdesign.persistence.repositories.IMediaContentRepo;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@NoArgsConstructor
public class MediaContentRepoImpl extends BaseRepositoryImpl<MediaContent> implements IMediaContentRepo {

}
