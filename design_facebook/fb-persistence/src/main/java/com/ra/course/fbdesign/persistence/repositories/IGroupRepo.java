package com.ra.course.fbdesign.persistence.repositories;

import com.ra.course.fbdesign.model.Group;

import java.util.Optional;

public interface IGroupRepo extends IBaseRepository<Group> {

    Optional<Group> getGroupByName(final String groupName);

}
