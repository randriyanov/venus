package com.ra.course.fbdesign.persistence.repositories;

import com.ra.course.fbdesign.model.PostClass;

public interface IPostRepo extends IBaseRepository<PostClass> {

}
