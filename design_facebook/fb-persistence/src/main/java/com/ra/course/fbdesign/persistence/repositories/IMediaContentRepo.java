package com.ra.course.fbdesign.persistence.repositories;

import com.ra.course.fbdesign.model.MediaContent;

public interface IMediaContentRepo extends IBaseRepository<MediaContent> {
}
