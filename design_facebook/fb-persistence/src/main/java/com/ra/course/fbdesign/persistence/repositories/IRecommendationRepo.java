package com.ra.course.fbdesign.persistence.repositories;

import com.ra.course.fbdesign.model.Recommendation;

public interface IRecommendationRepo extends IBaseRepository<Recommendation>{
}
