package com.ra.course.fbdesign.persistence.repositories.impl;

import com.ra.course.fbdesign.model.Comment;
import com.ra.course.fbdesign.persistence.repositories.ICommentRepo;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@NoArgsConstructor
public class CommentRepoImpl extends BaseRepositoryImpl<Comment> implements ICommentRepo {

}

