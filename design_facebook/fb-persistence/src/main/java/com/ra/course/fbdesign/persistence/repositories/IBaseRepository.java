package com.ra.course.fbdesign.persistence.repositories;

import com.ra.course.fbdesign.model.BaseEntity;

import java.util.Collection;
import java.util.Optional;

public interface IBaseRepository<T extends BaseEntity> {
    T add(T t);

    T update(T t);

    Optional<T> getById(Long id);

    void deleteById(Long id);

    Collection<T> getAll();
}
