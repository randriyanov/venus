package com.ra.course.fbdesign.persistence.repositories.impl;

import com.ra.course.fbdesign.model.Recommendation;
import com.ra.course.fbdesign.persistence.repositories.IRecommendationRepo;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@NoArgsConstructor
public class RecommendationRepoImpl extends BaseRepositoryImpl<Recommendation> implements IRecommendationRepo {

}
