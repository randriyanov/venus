package com.ra.course.fbdesign.persistence.repositories;

import com.ra.course.fbdesign.model.Profile;

public interface IProfileRepo extends IBaseRepository<Profile> {

}
