package com.ra.course.fbdesign.persistence.repositories;

import java.util.Optional;

public interface ISearchByName<T> {

    Optional<T> getByName(String name);

}
