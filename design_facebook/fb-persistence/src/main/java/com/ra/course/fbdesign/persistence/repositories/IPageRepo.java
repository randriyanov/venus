package com.ra.course.fbdesign.persistence.repositories;

import com.ra.course.fbdesign.model.PageBean;

import java.util.Optional;

public interface IPageRepo extends IBaseRepository<PageBean>{

    Optional<PageBean> getPageByName(final String pageName);

}
