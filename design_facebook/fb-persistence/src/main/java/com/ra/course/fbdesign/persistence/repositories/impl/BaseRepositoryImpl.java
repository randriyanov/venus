package com.ra.course.fbdesign.persistence.repositories.impl;

import com.ra.course.fbdesign.model.BaseEntity;
import com.ra.course.fbdesign.persistence.repositories.IBaseRepository;
import com.ra.course.fbdesign.persistence.repositories.impl.exception.RecordExistException;
import com.ra.course.fbdesign.persistence.repositories.impl.exception.RecordNotExistException;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Repository
public class BaseRepositoryImpl<T extends BaseEntity> implements IBaseRepository<T> {
    protected final transient Set<T> repository = new HashSet<>();
    private static Long id = 1L;

    @Override
    public T add(final T entity) {
        if (getById(entity.getId()).isPresent()) {
            throw new RecordExistException();
        }
        entity.setId(id);
        id++;
        repository.add(entity);
        return entity;
    }

    @Override
    public T update(final T entity) {
        final Optional<T> temp = getById(entity.getId());
        if (temp.isEmpty()) {
            throw new RecordNotExistException();
        }
        entity.setId(temp.get().getId());
        repository.remove(temp.get());
        repository.add(entity);
        return entity;
    }

    @Override
    public Optional<T> getById(final Long id) {
        return repository.stream().filter(el -> el.getId().equals(id)).findAny();
    }

    @Override
    public void deleteById(final Long id) {
        final Optional<T> temp = getById(id);
        if (temp.isEmpty()) {
            throw new RecordNotExistException();
        }
        repository.remove(temp.get());
    }

    @Override
    public Collection<T> getAll() {
        return repository;
    }
}
