package com.ra.course.fbdesign.persistence.repositories;

import com.ra.course.fbdesign.model.Comment;

public interface ICommentRepo extends IBaseRepository<Comment>{

}
