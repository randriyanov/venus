package com.ra.course.fbdesign.persistence.repositories;

import com.ra.course.fbdesign.model.Member;

import java.util.Optional;

public interface IMemberRepo extends IBaseRepository<Member> {
    Optional<Member> getByName(final String memberName);
}
