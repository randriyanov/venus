package com.ra.course.fbdesign.persistence.repositories.impl;

import com.ra.course.fbdesign.model.PostClass;
import com.ra.course.fbdesign.persistence.repositories.IPostRepo;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@NoArgsConstructor
public class PostRepoImpl extends BaseRepositoryImpl<PostClass> implements IPostRepo {

}
