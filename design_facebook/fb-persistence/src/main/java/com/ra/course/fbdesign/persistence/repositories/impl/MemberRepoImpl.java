package com.ra.course.fbdesign.persistence.repositories.impl;

import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.persistence.repositories.IMemberRepo;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@NoArgsConstructor
public class MemberRepoImpl extends BaseRepositoryImpl<Member> implements IMemberRepo {

    @Override
    public Optional<Member> getByName(final String memberName) {
        return repository.stream().filter(el -> memberName.equals(el.getName())).findAny();
    }

}
