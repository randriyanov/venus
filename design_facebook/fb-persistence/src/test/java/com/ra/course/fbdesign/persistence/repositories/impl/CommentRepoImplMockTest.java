package com.ra.course.fbdesign.persistence.repositories.impl;

import com.ra.course.fbdesign.model.Comment;
import com.ra.course.fbdesign.persistence.repositories.impl.exception.RecordExistException;
import com.ra.course.fbdesign.persistence.repositories.impl.exception.RecordNotExistException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.MockitoAnnotations.initMocks;

class CommentRepoImplMockTest {
    private Comment COMMENT;
    @Mock
    private CommentRepoImpl commentRepoImpl;

    @BeforeEach
    void setUp() {
        initMocks(this);
        commentRepoImpl = new CommentRepoImpl();
        COMMENT = new Comment();
        COMMENT.setTotalLikes(1);
    }

    @Test
    @DisplayName("If Comment exists we can get by id")
    void getCommentByIdWhenExist() {
        commentRepoImpl.add(COMMENT);
        assertTrue(commentRepoImpl.getById(COMMENT.getId()).isPresent());
    }

    @Test
    @DisplayName("If Comment not exist")
    void getCommentByIdWhenNoSuchComment() {
        assertTrue(commentRepoImpl.getById(222L).isEmpty());
    }


    @Test
    @DisplayName("Save comment and try to get by id this one")
    void saveCommentWhenAllIsOk() {
        commentRepoImpl.add(COMMENT);
        assertEquals(commentRepoImpl.getById(COMMENT.getId()).get(), COMMENT);
    }

    @Test
    @DisplayName("When we delete comment he is not present in repository")
    void deleteComment() {
        commentRepoImpl.add(COMMENT);
        commentRepoImpl.deleteById(COMMENT.getId());
        assertFalse(commentRepoImpl.getAll().contains(COMMENT));
    }

    @Test
    @DisplayName("When we update comment and it is  present in repo then we add it in repo")
    void updateCommentWhenItPresentInRepo() {
        commentRepoImpl.add(COMMENT);
        commentRepoImpl.update(COMMENT);
        assertTrue(commentRepoImpl.getAll().contains(COMMENT));
    }

    @Test
    void deleteByIdWhenIsNotPresentThenExceptionThrows() {
        assertThrows(RecordNotExistException.class, () -> commentRepoImpl.deleteById(COMMENT.getId()));
    }

    @Test
    void whenWeAddCommentWhenItExistThenExceptionThrows() {
        commentRepoImpl.add(COMMENT);
        assertThrows(RecordExistException.class, () -> commentRepoImpl.add(COMMENT));
    }

    @Test
    void whenWeUpdateCommentWhichNotExistThenExceptionThrows() {
        assertThrows(RecordNotExistException.class, () -> commentRepoImpl.update(new Comment()));
    }
}