package com.ra.course.fbdesign.persistence.repositories.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class ProfileRepoImplMockTest {
    private ProfileRepoImpl profileRepo;

    @BeforeEach
    void setUp() {
        profileRepo = new ProfileRepoImpl();
    }

    @Test
    @DisplayName("When we want to save profile")
    void saveWhenNew() {
        assertNotNull(profileRepo.repository);
    }

}