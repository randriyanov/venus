package com.ra.course.fbdesign.persistence.repositories.impl;

import com.ra.course.fbdesign.model.PageBean;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PageRepoImplMockTest {
    private PageRepoImpl pageRepoImpl;
    private PageBean pageBean;

    @BeforeEach
    void setUp() {
        pageBean = new PageBean();
        pageBean.setPageName("name");
        pageRepoImpl = new PageRepoImpl();
    }

    @Test
    void getPageByIdWhenAllIsOk() {
        assertNotNull(pageRepoImpl.repository);
    }

    @Test
    void getPageByName() {
        pageRepoImpl.add(pageBean);
        assertEquals(pageBean.getPageName(), pageRepoImpl.getPageByName(pageBean.getPageName()).get().getPageName());
    }
}