package com.ra.course.fbdesign.persistence.repositories.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class RecommendationRepoImplMockTest {
    private RecommendationRepoImpl recommendationRepo;

    @BeforeEach
    void setUp() {
        recommendationRepo = new RecommendationRepoImpl();
    }

    @Test
    @DisplayName("When Rec is not present then empty optional returns")
    void getRecommendationById() {
        assertNotNull(recommendationRepo.repository);
    }

}