package com.ra.course.fbdesign.persistence.repositories.impl;

import com.ra.course.fbdesign.model.ConnectionInvitation;
import com.ra.course.fbdesign.model.Member;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ConInvitationRepoImplMockTest {
    private ConInvitationRepoImpl conInvitationRepo;
    private Member member;
    private ConnectionInvitation connectionInvitation;
    private MemberRepoImpl memberRepo;

    @BeforeEach
    void setUp() {
        memberRepo = new MemberRepoImpl();
        conInvitationRepo = new ConInvitationRepoImpl();
        member = new Member();
        memberRepo.add(member);
        connectionInvitation = new ConnectionInvitation();
        connectionInvitation.setMemberInvited(member);
    }

    @Test
    @DisplayName("When conInvitation is  present then exception  throws")
    void whenInvitationIsPresentThenExceptionThrows() {
        assertNotNull(conInvitationRepo.repository);
    }

    @Test
    @DisplayName("when invited member is not present then null returns")
    void getByMemberInvited() {

        conInvitationRepo.add(connectionInvitation);
        Member absentMember = new Member();
        assertEquals(Optional.empty(), conInvitationRepo.getByMemberInvited(absentMember));
    }
}