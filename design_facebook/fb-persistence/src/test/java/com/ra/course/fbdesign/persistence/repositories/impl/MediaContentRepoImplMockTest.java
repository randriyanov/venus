package com.ra.course.fbdesign.persistence.repositories.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class MediaContentRepoImplMockTest {
    private MediaContentRepoImpl mediaContentRepo;

    @BeforeEach
    void setUp() {
        mediaContentRepo = new MediaContentRepoImpl();
    }

    @Test
    @DisplayName("When media is not present then empty optional returns")
    void getByIdWhenNotExists() {
        assertNotNull(mediaContentRepo.repository);
    }

}