package com.ra.course.fbdesign.persistence.repositories.impl;

import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.model.Profile;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class MemberRepoImplMockTest {
    private MemberRepoImpl memberRepoImpl;
    private Member member;

    @BeforeEach
    void setUp() {

        member = new Member();
        member.setProfile(new Profile());
        member.setName("name");
        memberRepoImpl = new MemberRepoImpl();
    }

    @Test
    void getMemberByNameWhenAllIsOk() {
        assertNotNull(memberRepoImpl.repository);
    }

    @Test
    void getPageByName() {
        memberRepoImpl.add(member);
        assertEquals(member.getName(), memberRepoImpl.getByName(member.getName()).get().getName());
    }
}