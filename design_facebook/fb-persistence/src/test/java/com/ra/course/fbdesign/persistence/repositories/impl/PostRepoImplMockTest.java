package com.ra.course.fbdesign.persistence.repositories.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class PostRepoImplMockTest {
    private PostRepoImpl postRepoImpl;

    @BeforeEach
    void setUp() {
        postRepoImpl = new PostRepoImpl();
    }

    @Test
    void getPostById() {
        assertNotNull(postRepoImpl.repository);
    }

}