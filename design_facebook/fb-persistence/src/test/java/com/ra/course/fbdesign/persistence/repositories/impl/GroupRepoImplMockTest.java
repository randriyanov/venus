package com.ra.course.fbdesign.persistence.repositories.impl;

import com.ra.course.fbdesign.model.Group;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class GroupRepoImplMockTest {
    @Mock
    private GroupRepoImpl groupRepoImpl;
    private Group group;

    @BeforeEach
    void setUp() {
        group = new Group();
        group.setGroupName("name");
        groupRepoImpl = new GroupRepoImpl();
    }

    @Test
    void getGroupByIdWhenAllIsOk() {
        assertNotNull(groupRepoImpl.repository);
    }

    @Test
    void getGroupByName() {
        groupRepoImpl.add(group);
        assertEquals(group, groupRepoImpl.getGroupByName(group.getGroupName()).get());
    }
}