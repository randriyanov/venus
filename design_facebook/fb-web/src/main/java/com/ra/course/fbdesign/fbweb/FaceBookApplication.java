package com.ra.course.fbdesign.fbweb;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SuppressWarnings("PMD")
@SpringBootApplication(scanBasePackages = "com.ra.course.fbdesign")

public class FaceBookApplication {
    public static void main(String[] args) {
        SpringApplication.run(FaceBookApplication.class, args);

    }
}
