package com.ra.course.fbdesign.fbweb;

import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.model.PrivacyList;
import com.ra.course.fbdesign.services.IFollowService;
import com.ra.course.fbdesign.services.impl.MemberServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class FollowServiceImplIntegrationTest {

    @Autowired
    private IFollowService followService;

    @Autowired
    private MemberServiceImpl memberService;

    private Set<Member> members;
    private PrivacyList privacyList;
    private Member testMember;
    private Member follower;

    @BeforeEach
    void setUp() {
        testMember = new Member();
        testMember.setName("name");
        members = new HashSet<>();
        follower = new Member();
        privacyList = new PrivacyList();
        testMember.setFollowers(privacyList);
        testMember.getFollowers().setMembersList(members);
    }

    @Test
    void followMember() {
        memberService.addMember(testMember);
        followService.followMember(testMember, follower);
        assertTrue(testMember.getFollowers().getMembersList().contains(follower));
    }
}
