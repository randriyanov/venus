package com.ra.course.fbdesign.fbweb;

import com.ra.course.fbdesign.model.EducationDetails;
import com.ra.course.fbdesign.model.PlacesDetails;
import com.ra.course.fbdesign.model.Profile;
import com.ra.course.fbdesign.model.WorkDetails;
import com.ra.course.fbdesign.services.exceptions.RecordNotFoundException;
import com.ra.course.fbdesign.services.impl.ProfileServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ProfileServiceImplIntegrationTest {

    private EducationDetails education;
    private WorkDetails workDetails;
    private PlacesDetails placesDetails;

    @Autowired
    private ProfileServiceImpl profileService;
    private Profile profile;

    @BeforeEach
    void setUp() {
        profile = new Profile();
        profile.setWorkDetailsList(new LinkedList<>());
        profile.setEducationDetails(new LinkedList<>());
        profile.setPlacesDetailsList(new LinkedList<>());
        education = new EducationDetails();
        workDetails = new WorkDetails();
        placesDetails = new PlacesDetails();
        profileService.add(profile);
    }


    @Test
    void addEducation() {
        assertTrue(profileService.addEducation(profile, education).getEducationDetails().contains(education));
    }

    @Test
    void addWork() {
        assertTrue(profileService.addWork(profile, workDetails).getWorkDetailsList().contains(workDetails));
    }

    @Test
    void addPlace() {
        assertTrue(profileService.addPlace(profile, placesDetails).getPlacesDetailsList().contains(placesDetails));
    }


    @Test
    void getProfileByFakeId() {
        assertThrows(RecordNotFoundException.class, () -> profileService.getProfileById(2231L));
    }

    @Test
    void getProfileByRealId() {
        assertEquals(profileService.getProfileById(profile.getId()).getId(), profile.getId());
    }
}
