package com.ra.course.fbdesign.fbweb;

import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.model.PostClass;
import com.ra.course.fbdesign.services.PostService;
import com.ra.course.fbdesign.services.exceptions.MemberNotFoundException;
import com.ra.course.fbdesign.services.exceptions.PostClassNotFoundException;
import com.ra.course.fbdesign.services.impl.MemberServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class PostServiceImplIntegrationTest {
    @Autowired
    private MemberServiceImpl memberService;

    @Autowired
    private PostService postService;

    private final String NAME = "MemberNameForPostServiceImplIntegrationTest";

    @BeforeEach
    void setUp(){
        memberService.addMember(getMember());
    }
    @Test
    void createPostWithNoCreator() {
        assertThrows(MemberNotFoundException.class, () -> postService.createPost("", ""));
    }

    @Test
    void createPostWhenAllIsOk() {
        assertTrue(postService.createPost(NAME, "some"));
    }

    @Test
    void sharePostWithoutMember() {
        assertThrows(MemberNotFoundException.class, () -> postService.sharePost("", 2L));
    }

    @Test
    void sharePostWithoutPost() {
        assertThrows(PostClassNotFoundException.class, () -> postService.sharePost(NAME, -1L));
    }

    @Test
    void sharePostWhenAllIsOk() {
        PostClass postClass = new PostClass();
        PostClass postClassWithId = postService.add(postClass);
        System.out.println(postClassWithId.getId());
        assertTrue(postService.sharePost(NAME, postClassWithId.getId()));
    }

    private Member getMember() {
        Member member = new Member();
        member.setName(NAME);
        member.setPostClasses(new LinkedList<>());
        return member;
    }

}
