package com.ra.course.fbdesign.fbweb;

import com.ra.course.fbdesign.model.MediaContent;
import com.ra.course.fbdesign.persistence.repositories.impl.exception.RecordNotExistException;
import com.ra.course.fbdesign.services.impl.MediaContentServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class MediaContentServiceImplIntegrationTest {

    @Autowired
    private MediaContentServiceImpl mediaContentService;

    private final Long mediaId = 1L;

    private MediaContent mediaContent;

    @BeforeEach
    void setUp() {
        mediaContent = new MediaContent();
        mediaContent.setId(mediaId);
    }

    @Test
    @DisplayName("Try to get Media when id is not exist")
    void getByIdWhenNotExist() {
        assertThrows(RecordNotExistException.class, () -> mediaContentService.getById(mediaId));
    }

    @Test
    @DisplayName("Try to get Media when id exists")
    void getByIdWhenExist() {
        mediaContentService.add(mediaContent);
        assertEquals(mediaContentService.getById(mediaContent.getId()), mediaContent);
    }
}
