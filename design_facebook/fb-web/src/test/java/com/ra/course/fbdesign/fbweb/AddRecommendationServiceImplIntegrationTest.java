package com.ra.course.fbdesign.fbweb;

import com.ra.course.fbdesign.model.PageBean;
import com.ra.course.fbdesign.model.Recommendation;
import com.ra.course.fbdesign.services.AddRecommendationService;
import com.ra.course.fbdesign.services.PageService;
import com.ra.course.fbdesign.services.exceptions.PageBeanNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class AddRecommendationServiceImplIntegrationTest {

    private Recommendation recommendation;
    @Autowired
    private AddRecommendationService addRecommendationService;
    @Autowired
    private PageService pageService;

    @BeforeEach
    void setUp() {
        recommendation = new Recommendation();
    }


    @Test
    @DisplayName("When page is not found then exception throws")
    void addRecommendationWhenPageIsAbsentThenExceptionThrows() {
        //when
        var missPageId = 123L;
        //then
        assertThrows(PageBeanNotFoundException.class,
                () -> addRecommendationService.addRecommendation(missPageId, recommendation));
    }

    @Test
    @DisplayName("When page is found then recommendation adds")
    void whenPageIsFoundThenRecommendationAdds() {
        //when
        PageBean pageBean = new PageBean();
        pageBean.setRecommendations(new HashSet<>());
        PageBean pageBeanWithId = pageService.add(pageBean);
        //result
        assertTrue(addRecommendationService.addRecommendation(pageBeanWithId.getId(), recommendation).getRecommendations().contains(recommendation));
    }
}
