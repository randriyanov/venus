package com.ra.course.fbdesign.fbweb;

import com.ra.course.fbdesign.model.Comment;
import com.ra.course.fbdesign.model.EmailNotification;
import com.ra.course.fbdesign.model.PostClass;
import com.ra.course.fbdesign.services.SendPostCommentNotification;
import com.ra.course.fbdesign.services.exceptions.PostClassNotFoundException;
import com.ra.course.fbdesign.services.impl.PostServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class SendPostCommentNotificationImplIntegrationTest {
    @Autowired
    private PostServiceImpl postService;
    @Autowired
    private SendPostCommentNotification sendPostCommentNotification;
    private Comment comment;

    @BeforeEach
    void setUp() {
        comment = new Comment();
    }

    @Test
    @DisplayName("When post is not present then exceptions throws")
    void whenPostIsNotPresentThenExceptionThrows() {
        var missPostId = 1L;
        assertThrows(PostClassNotFoundException.class,
                () -> sendPostCommentNotification.sendPostCommentNotification(missPostId, comment));
    }

    @Test
    @DisplayName("When post is  present. If we have new comment then notification send")
    void sendPostCommentNotification() {
        assertEquals(EmailNotification.class, sendPostCommentNotification.sendPostCommentNotification(testPostWithNewComment().getId(), comment).getClass());
    }

    @Test
    @DisplayName("When post is  present. If we dont have new comment then null send")
    void whenPostIsPresentAndWeDontHaveNewCommentThenNullSend() {
        var id = testPostWithNewComment().getId();
        assertEquals(EmailNotification.class,
                sendPostCommentNotification.sendPostCommentNotification(id, comment).getClass());
    }


    PostClass testPostWithNewComment() {
        var testPost = new PostClass();
        postService.add(testPost);
        testPost.setComments(new LinkedList<>());
        testPost.getComments().add(comment);
        return testPost;
    }
}
