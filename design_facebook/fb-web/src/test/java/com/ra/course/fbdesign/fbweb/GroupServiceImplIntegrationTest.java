package com.ra.course.fbdesign.fbweb;

import com.ra.course.fbdesign.model.Group;
import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.persistence.repositories.impl.GroupRepoImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.MemberRepoImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.PostRepoImpl;
import com.ra.course.fbdesign.services.exceptions.GroupNotFoundException;
import com.ra.course.fbdesign.services.exceptions.MemberNotFoundException;
import com.ra.course.fbdesign.services.impl.GroupServiceImpl;
import com.ra.course.fbdesign.services.impl.MemberServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class GroupServiceImplIntegrationTest {

    @Autowired
    private MemberServiceImpl memberService;

    @Autowired
    private GroupServiceImpl groupService;

    private final Long GROUP_ID = 1L;
    private final Long MEMBER_ID = 1L;
    private final String MEMBER_NAME = "Member_Name";
    private final String GROUP_NAME = "Group_Name";
    private Group group;


    @BeforeEach
    void setUp() {
        group = new Group();
        group.setMembers(new HashSet<>());
        group.setGroupName(GROUP_NAME);
    }

    @Test
    @DisplayName("Will fail on creating group")
    void createGroupWhenNoMemberFound() {
        assertThrows(MemberNotFoundException.class, () -> groupService.createGroup(GROUP_NAME, "", MEMBER_NAME));
    }

    @Test
    @DisplayName("Search group by name when group exists")
    void searchGroupByName() {
        groupService.add(group);
        assertEquals(GROUP_NAME, groupService.searchByName(GROUP_NAME).getGroupName());
    }

    @Test
    @DisplayName("Will fail on member check")
    void createGroupWithWrongMember() {
        assertThrows(MemberNotFoundException.class, () -> groupService.createGroup("", "", "MEMBER_NAME"));
    }

    @Test
    void createGroupWhenAllIsOk() {
        memberService.addMember(getMember());
        assertTrue(groupService.createGroup("GROUP_NAME", "description", MEMBER_NAME));
    }


    @Test
    void joinGroupWithoutMember() {
        assertThrows(MemberNotFoundException.class, () -> groupService.joinGroup(GROUP_ID, MEMBER_NAME));
    }

    @Test
    void joinGroupWithoutGroup() {
        assertThrows(GroupNotFoundException.class, () -> groupService.joinGroup(123L, MEMBER_NAME));
    }

    @Test
    void joinGroupWhenAllIsOk() {
        groupService.add(group);
        Member member = new Member();
        member.setName(MEMBER_NAME);
        member.setGroups(new HashSet<>());
        memberService.addMember(member);
        assertTrue(groupService.joinGroup(group.getId(), MEMBER_NAME));
    }

    @Test
    @DisplayName("Search group by name when group does not exist")
    void searchGroupByFakeName() {
        assertThrows(GroupNotFoundException.class, () -> groupService.searchByName("Fake name"));
    }

    private Member getMember() {
        Member member = new Member();
        member.setName(MEMBER_NAME);
        member.setGroups(new HashSet<>());
        return member;
    }
}
