package com.ra.course.fbdesign.fbweb;

import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.model.PostClass;
import com.ra.course.fbdesign.model.PrivacyList;
import com.ra.course.fbdesign.services.exceptions.MemberNotFoundException;
import com.ra.course.fbdesign.services.exceptions.PostClassNotFoundException;
import com.ra.course.fbdesign.services.impl.MemberServiceImpl;
import com.ra.course.fbdesign.services.impl.PostServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class MemberServiceImplIntegrationTest {

    @Autowired
    private MemberServiceImpl memberService;
    @Autowired
    private PostServiceImpl postService;

    private final String MEMBER_NAME = "MemberName";
    private final String OTHER_NAME = "OtherName";

    @Test
    void addFriendWhenNoFriendExist() {
        assertThrows(MemberNotFoundException.class, () -> memberService.addFriend(MEMBER_NAME, OTHER_NAME));
    }

    @Test
    void getMemberByNameWhenNameIsAbsent() {
        assertThrows(MemberNotFoundException.class, () -> memberService.searchByName(OTHER_NAME));
    }

    @Test
    void getMemberByNameWhenNameIsPresentThenOptionalReturns() {
        Assertions.assertEquals(memberService.searchByName(MEMBER_NAME).getName(), testMember().getName());
    }

    @Test
    void getMemberByFakeId() {
        assertThrows(MemberNotFoundException.class, () -> memberService.getById(12312L));
    }


    @Test
    void addFriendWhenNoAdderExist() {
        assertThrows(MemberNotFoundException.class, () -> memberService.addFriend(MEMBER_NAME, OTHER_NAME));
    }

    @Test
    void addFriendWhenAllIsOk() {
        memberService.addMember(testMember());
        memberService.addMember(testMemberOther());
        assertTrue(memberService.addFriend(MEMBER_NAME, OTHER_NAME));
    }

    @Test
    void setPrivacyWhenNoMember() {
        assertThrows(MemberNotFoundException.class, () -> memberService.addPrivacy("wrong name", 123L));
    }

    @Test
    void setPrivacyWhenNoPost() {
        assertThrows(PostClassNotFoundException.class, () -> memberService.addPrivacy(MEMBER_NAME, -1L));
    }

    @Test
    void setPrivacyWhenAllIsOk() {
        assertEquals(2L, getPost().getId());
    }

    @Test
    @DisplayName("Search member by name when member exists")
    void searchMemberByName() {
        memberService.addMember(testMember());
        assertEquals(MEMBER_NAME, memberService.searchByName(MEMBER_NAME).getName());
    }

    @Test
    @DisplayName("Search member by name when member does not exist")
    void searchMemberByNameWhenDoesNotExist() {
        assertThrows(MemberNotFoundException.class, () -> memberService.searchByName("OTHER_NAME"));
    }

    private Member testMember() {
        var member = new Member();
        member.setName(MEMBER_NAME);
        member.setFriends(new PrivacyList());
        member.getFriends().setMembersList(new HashSet<>());
        return member;
    }

    private Member testMemberOther() {
        var member = new Member();
        member.setName(OTHER_NAME);
        member.setFriends(new PrivacyList());
        member.getFriends().setMembersList(new HashSet<>());
        return member;
    }

    private PostClass getPost() {
        PostClass post = new PostClass();
        post.setId(2L);
        post.setPrivacyList(new PrivacyList());
        return post;
    }
}
