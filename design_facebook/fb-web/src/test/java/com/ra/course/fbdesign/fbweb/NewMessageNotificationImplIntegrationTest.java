package com.ra.course.fbdesign.fbweb;

import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.model.Message;
import com.ra.course.fbdesign.services.NewMessageNotification;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class NewMessageNotificationImplIntegrationTest {

    @Autowired
    private NewMessageNotification notification;
    private Message message;

    @BeforeEach
    void setUp() {
        message = new Message();
        Member testMember = new Member();
        message.setSendTo(new HashSet<>());
        message.getSendTo().add(testMember);
        message.setNotifications(new HashSet<>());
    }

    @Test
    @DisplayName("We have notifications when new message have")
    void sendMessageNotification() {
        notification.sendMessageNotification(message);
        assertEquals(message.getNotifications().size(), message.getSendTo().size());
    }
}
