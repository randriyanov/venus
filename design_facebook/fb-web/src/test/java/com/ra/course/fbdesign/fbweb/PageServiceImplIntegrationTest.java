package com.ra.course.fbdesign.fbweb;

import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.model.PageBean;
import com.ra.course.fbdesign.services.exceptions.MemberNotFoundException;
import com.ra.course.fbdesign.services.exceptions.PageBeanNotFoundException;
import com.ra.course.fbdesign.services.impl.MemberServiceImpl;
import com.ra.course.fbdesign.services.impl.PageServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PageServiceImplIntegrationTest {

    @Autowired
    private MemberServiceImpl memberService;

    @Autowired
    private PageServiceImpl pageService;

    private final String PAGE_NAME = "Page Name";
    private final String MEMBER_NAME = "Member Name";
    private final String FOLLOWER_NAME = "Follower Name";
    private final String PAGE_DESCR = "Description";
    private final Long PAGE_ID = 1L;

    @Test
    void createPageWithNoCreator() {
        assertThrows(MemberNotFoundException.class, () -> pageService.createPage("WrongName", PAGE_NAME, PAGE_DESCR));
    }

    @Test
    void createPageWhenAllIsOk() {
        Member member = new Member();
        member.setName(MEMBER_NAME);
        memberService.addMember(member);
        assertTrue(pageService.createPage(MEMBER_NAME, PAGE_NAME, PAGE_DESCR));
    }

    @Test
    void followPageWhenNoMemberExist() {
        assertThrows(MemberNotFoundException.class, () -> pageService.followPage(FOLLOWER_NAME, PAGE_ID));

    }

    @Test
    void followPageWhenNoPageExist() {
        assertThrows(PageBeanNotFoundException.class, () -> pageService.followPage(FOLLOWER_NAME, 111L));
    }

    @Test
    void followPageWhenAllIsOk() {
        Member member = new Member();
        member.setName(FOLLOWER_NAME);
        memberService.addMember(member);
        PageBean page = getPage();
        pageService.add(page);
        assertTrue(pageService.followPage(FOLLOWER_NAME, page.getId()));
    }

    @Test
    @DisplayName("Search page by name when page exists")
    void searchPageByName() {
        pageService.add(getPage());
        assertEquals(PAGE_NAME, pageService.searchByName(PAGE_NAME).getPageName());
    }

    @Test
    @DisplayName("Search page by name when page does not exist")
    void searchPageByFakeName() {
        assertThrows(PageBeanNotFoundException.class, () -> pageService.searchByName("wrong name"));
    }

    private PageBean getPage() {
        PageBean page = new PageBean();
        page.setPageName(PAGE_NAME);
        page.setMembersFollows(new HashSet<>());
        return page;
    }
}
