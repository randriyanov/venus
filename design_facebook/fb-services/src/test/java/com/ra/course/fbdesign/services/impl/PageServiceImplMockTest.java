package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.model.PageBean;
import com.ra.course.fbdesign.persistence.repositories.impl.MemberRepoImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.PageRepoImpl;
import com.ra.course.fbdesign.services.exceptions.MemberNotFoundException;
import com.ra.course.fbdesign.services.exceptions.PageBeanNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.HashSet;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class PageServiceImplMockTest {
    @Mock
    private MemberRepoImpl mockMemberDAO;
    @Mock
    private PageRepoImpl mockPageDAO;
    private PageServiceImpl pageService;
    private final String PAGE_NAME = "Page Name";
    private final String MEMBER_NAME = "Member Name";
    private final String FOLLOWER_NAME = "Follower Name";
    private final String PAGE_DESCR = "Description";
    private final Long PAGE_ID = 1L;
    private PageBean pageBean;

    @BeforeEach
    void setUp() {
        initMocks(this);
        pageBean = new PageBean();
        pageService = new PageServiceImpl(mockPageDAO, mockMemberDAO);
    }

    @Test
    void createPageWithNoCreator() {
        assertThrows(MemberNotFoundException.class, () -> pageService.createPage(MEMBER_NAME, PAGE_NAME, PAGE_DESCR));
    }

    @Test
    void deletePage() {
        pageService.deletePage(getPage().getId());
        verify(mockPageDAO, times(1)).deleteById(PAGE_ID);
    }

    @Test
    void createPageWhenAllIsOk() {
        when(mockMemberDAO.getByName(MEMBER_NAME)).thenReturn(Optional.of(new Member()));
        assertTrue(pageService.createPage(MEMBER_NAME, PAGE_NAME, PAGE_DESCR));
    }

    @Test
    void followPageWhenNoMemberExist() {
        assertThrows(MemberNotFoundException.class, () -> pageService.followPage(FOLLOWER_NAME, PAGE_ID));

    }

    @Test
    void followPageWhenNoPageExist() {
        when(mockMemberDAO.getByName(FOLLOWER_NAME)).thenReturn(Optional.of(new Member()));
        assertThrows(PageBeanNotFoundException.class, () -> pageService.followPage(FOLLOWER_NAME, PAGE_ID));
    }

    @Test
    void followPageWhenAllIsOk() {
        when(mockPageDAO.getById(PAGE_ID)).thenReturn(Optional.of(getPage()));
        when(mockMemberDAO.getByName(FOLLOWER_NAME)).thenReturn(Optional.of(new Member()));
        assertTrue(pageService.followPage(FOLLOWER_NAME, PAGE_ID));
    }

    @Test
    @DisplayName("Search page by name when page exists")
    void searchPageByName() {
        when(mockPageDAO.getPageByName(PAGE_NAME)).thenReturn(Optional.of(getPage()));
        assertEquals(PAGE_NAME, pageService.searchByName(PAGE_NAME).getPageName());
    }

    @Test
    @DisplayName("Search page by name when page does not exist")
    void searchPageByFakeName() {
        assertThrows(PageBeanNotFoundException.class, () -> pageService.searchByName(PAGE_NAME));
    }

    @Test
    void addPage() {
        pageService.add(pageBean);
        verify(mockPageDAO, times(1)).add(pageBean);
    }

    private PageBean getPage() {
        PageBean page = new PageBean();
        page.setPageName(PAGE_NAME);
        page.setId(PAGE_ID);
        page.setMembersFollows(new HashSet<>());
        return page;
    }
}