package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.*;
import com.ra.course.fbdesign.persistence.repositories.impl.ProfileRepoImpl;
import com.ra.course.fbdesign.services.IProfileService;
import com.ra.course.fbdesign.services.exceptions.RecordNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.LinkedList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class ProfileServiceImplMockTest {

    private EducationDetails education;
    private WorkDetails workDetails;
    private PlacesDetails placesDetails;

    @Mock
    private ProfileRepoImpl mockProfileRepo;
    @Mock
    private MemberServiceImpl memberService;

    private IProfileService profileService;
    private final Long PROFILE_ID = 1L;
    private Profile profile;

    @BeforeEach
    void setUp() {
        initMocks(this);
        profileService = new ProfileServiceImpl(mockProfileRepo, memberService);
        profile = new Profile();
        profile.setId(PROFILE_ID);
        profile.setWorkDetailsList(new LinkedList<>());
        profile.setEducationDetails(new LinkedList<>());
        profile.setPlacesDetailsList(new LinkedList<>());
        education = new EducationDetails();
        workDetails = new WorkDetails();
        placesDetails = new PlacesDetails();
    }


    @Test
    void createProfile() {
        profileService.createProfile("male");
        verify(mockProfileRepo, times(1)).add(any());
    }

    @Test
    void deleteProfile() {
        profileService.deleteProfile(PROFILE_ID);
        verify(mockProfileRepo, times(1)).deleteById(PROFILE_ID);
    }

    @Test
    void getByMemberName() {
        when(memberService.searchByName("name")).thenReturn(new Member());
        profileService.getProfileByMemberName("name");
        verify(memberService, times(1)).searchByName("name");
    }

    @Test
    void addEducation() {
        when(mockProfileRepo.getById(PROFILE_ID)).thenReturn(Optional.of(profile));
        profileService.addEducation(profileService.getProfileById(PROFILE_ID), education);
        assertTrue(profile.getEducationDetails().contains(education));
    }

    @Test
    void addWork() {
        when(mockProfileRepo.getById(PROFILE_ID)).thenReturn(Optional.of(profile));
        profileService.addWork(profileService.getProfileById(PROFILE_ID), workDetails);
        assertTrue(profile.getWorkDetailsList().contains(workDetails));
    }

    @Test
    void addPlace() {
        when(mockProfileRepo.getById(PROFILE_ID)).thenReturn(Optional.of(profile));
        profileService.addPlace(profile, placesDetails);
        assertTrue(profile.getPlacesDetailsList().contains(placesDetails));
    }

    @Test
    void getProfileByFakeId() {
        assertThrows(RecordNotFoundException.class, () -> profileService.getProfileById(PROFILE_ID));
    }

    @Test
    void getProfileByRealId() {
        when(mockProfileRepo.getById(PROFILE_ID)).thenReturn(Optional.of(profile));
        assertEquals(profileService.getProfileById(PROFILE_ID), profile);
    }

    @Test
    void addProfile() {
        profileService.add(profile);
        verify(mockProfileRepo, times(1)).add(profile);
    }

}