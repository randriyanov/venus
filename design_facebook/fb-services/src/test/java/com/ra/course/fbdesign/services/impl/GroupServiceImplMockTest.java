package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.Group;
import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.persistence.repositories.impl.GroupRepoImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.MemberRepoImpl;
import com.ra.course.fbdesign.services.exceptions.GroupNotFoundException;
import com.ra.course.fbdesign.services.exceptions.MemberNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.HashSet;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;


class GroupServiceImplMockTest {
    @Mock
    private MemberRepoImpl mockMemberDAO;
    @Mock
    private GroupRepoImpl mockGroupDAO;
    private GroupServiceImpl groupService;
    private final Long GROUP_ID = 1L;
    private final String MEMBER_NAME = "Member Name";
    private final String GROUP_NAME = "Group Name";
    private Group group;


    @BeforeEach
    void setUp() {
        initMocks(this);
        groupService = new GroupServiceImpl(mockGroupDAO, mockMemberDAO);
        group = new Group();
        group.setMembers(new HashSet<>());
        group.setId(GROUP_ID);
        group.setGroupName(GROUP_NAME);
    }

    @Test
    void getGroupByIdWhenExist() {
        when(mockGroupDAO.getById(GROUP_ID)).thenReturn(Optional.of(group));
        assertEquals(groupService.getById(GROUP_ID), group);
    }

    @Test
    void getGroupByIdWhenNOExistThenExceptionThrows() {
        when(mockGroupDAO.getById(GROUP_ID)).thenReturn(Optional.empty());
        assertThrows(GroupNotFoundException.class, () -> groupService.getById(GROUP_ID));
    }

    @Test
    void deleteByIdWhenExistInRepo() {
        groupService.deleteById(GROUP_ID);
        verify(mockGroupDAO, times(1)).deleteById(GROUP_ID);
    }

    @Test
    void deleteWhenExistInRepo() {
        groupService.deleteGroup(group);
        verify(mockGroupDAO, times(1)).deleteById(group.getId());
    }

    @Test
    @DisplayName("Will fail on member check")
    void createGroupWithWrongMember() {
        assertThrows(MemberNotFoundException.class, () -> groupService.createGroup("", "", MEMBER_NAME));
    }

    @Test
    @DisplayName("Will fail on creating group")
    void createGroupWhenNoMemberFound() {
        when(mockMemberDAO.getByName(MEMBER_NAME)).thenReturn(Optional.empty());
        assertThrows(MemberNotFoundException.class, () -> groupService.createGroup(GROUP_NAME, "", MEMBER_NAME));
    }

    @Test
    void createGroupWhenAllIsOk() {
        when(mockMemberDAO.getByName(MEMBER_NAME)).thenReturn(Optional.of(getMember()));
        when(mockGroupDAO.getById(anyLong())).thenReturn(Optional.of(group));
        assertTrue(groupService.createGroup(GROUP_NAME, "description", MEMBER_NAME));
    }

    @Test
    void joinGroupWithoutMember() {
        assertThrows(MemberNotFoundException.class, () -> groupService.joinGroup(GROUP_ID, MEMBER_NAME));
    }

    @Test
    void joinGroupWithoutGroup() {
        when(mockMemberDAO.getByName(MEMBER_NAME)).thenReturn(Optional.of(new Member()));
        assertThrows(GroupNotFoundException.class, () -> groupService.joinGroup(GROUP_ID, MEMBER_NAME));
    }

    @Test
    void joinGroupWhenAllIsOk() {
        when(mockGroupDAO.getById(GROUP_ID)).thenReturn(Optional.of(group));
        when(mockMemberDAO.getByName(MEMBER_NAME)).thenReturn(Optional.of(getMember()));
        assertTrue(groupService.joinGroup(GROUP_ID, MEMBER_NAME));
    }

    @Test
    @DisplayName("Search group by name when group exists")
    void searchGroupByName() {
        when(mockGroupDAO.getGroupByName(GROUP_NAME)).thenReturn(Optional.of(group));
        assertEquals(GROUP_NAME, groupService.searchByName(GROUP_NAME).getGroupName());
    }

    @Test
    @DisplayName("Search group by name when group does not exist")
    void searchGroupByFakeName() {
        assertThrows(GroupNotFoundException.class, () -> groupService.searchByName(GROUP_NAME));
    }

    @Test
    void addGroup() {
        groupService.add(group);
        verify(mockGroupDAO, times(1)).add(group);
    }

    private Member getMember() {
        Member member = new Member();
        member.setGroups(new HashSet<>());
        return member;
    }
}