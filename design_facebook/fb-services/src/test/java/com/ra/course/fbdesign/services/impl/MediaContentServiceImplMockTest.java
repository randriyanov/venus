package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.MediaContent;
import com.ra.course.fbdesign.persistence.repositories.impl.MediaContentRepoImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.exception.RecordNotExistException;
import com.ra.course.fbdesign.services.IMediaContentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class MediaContentServiceImplMockTest {

    @Mock
    private MediaContentRepoImpl mockMediaContentRepo;
    @Mock
    private IMediaContentService mediaContentService;

    private final Long mediaId = 1L;

    private MediaContent mediaContent;
    private MediaContent mediaContentWithId;

    @BeforeEach
    void setUp() {
        initMocks(this);
        mediaContentService = new MediaContentServiceImpl(mockMediaContentRepo);
        mediaContent = new MediaContent();
        mediaContentWithId = new MediaContent();
        mediaContentWithId.setId(mediaId);
    }

    @Test
    @DisplayName("Try to get Media when id is not exist")
    void getByIdWhenNotExist() {
        assertThrows(RecordNotExistException.class, () -> mediaContentService.getById(mediaId));
    }

    @Test
    @DisplayName("Try to get Media when id exists")
    void getByIdWhenExist() {
        when(mockMediaContentRepo.getById(mediaId)).thenReturn(Optional.of(mediaContent));
        assertEquals(mediaContentService.getById(mediaId), mediaContent);
    }

    @Test
    void deleteByIdWhenExistInRepo() {
        mediaContentService.deleteById(mediaId);
        verify(mockMediaContentRepo, times(1)).deleteById(mediaId);
    }

    @Test
    void addContentInRepo() {
        when(mockMediaContentRepo.add(mediaContent)).thenReturn(mediaContentWithId);
        assertEquals(1L, (long) mediaContentService.add(mediaContent).getId());
    }

}