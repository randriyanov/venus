package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.model.Message;
import com.ra.course.fbdesign.services.SendMessageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertTrue;

class SendMessageServiceImplMockTest {
    private SendMessageService sendMessageService;
    private Message message;
    private Member fromMember;
    private Member toMember;


    @BeforeEach
    void setUp() {
        sendMessageService = new SendMessageServiceImpl();
        message = new Message();
        fromMember = new Member();
        fromMember.setMessages(new HashSet<>());
        toMember = new Member();
        toMember.setMessages(new HashSet<>());
        message.setSendTo(new HashSet<>());
        message.getSendTo().add(toMember);
        message.setAuthor(fromMember);
    }

    @Test
    @DisplayName("When message has sent we add it in message list both members")
    void whenMessageSentWeAddItInAuthorList() {
        //when
        sendMessageService.sendMessage(message);
        //result
        assertTrue(fromMember.getMessages().contains(message));
        assertTrue(toMember.getMessages().contains(message));
    }
}