package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.model.PostClass;
import com.ra.course.fbdesign.model.PrivacyList;
import com.ra.course.fbdesign.persistence.repositories.impl.MemberRepoImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.PostRepoImpl;
import com.ra.course.fbdesign.services.exceptions.MemberNotFoundException;
import com.ra.course.fbdesign.services.exceptions.PostClassNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class MemberServiceImplMockTest {
    @Mock
    private MemberRepoImpl mockMemberDAO;
    @Mock
    private PostRepoImpl mockPostDAO;
    private MemberServiceImpl memberService;
    private final String MEMBER_NAME = "Member Name";
    private final String OTHER_NAME = "Other Name";
    private final Long MEMBER_ID = 1L;
    private final Long FAKE_MEMBER_ID = 3L;


    @BeforeEach
    void setUp() {
        initMocks(this);
        memberService = new MemberServiceImpl(mockMemberDAO, mockPostDAO);
    }

    @Test
    void addFriendWhenNoFriendExist() {
        assertThrows(MemberNotFoundException.class, () -> memberService.addFriend(MEMBER_NAME, OTHER_NAME));
    }

    @Test
    void getMemberByNameWhenNameIsAbsent() {
        assertThrows(MemberNotFoundException.class, () -> memberService.searchByName(MEMBER_NAME));
    }

    @Test
    void getMemberByNameWhenNameIsPresentThenOptionalReturns() {
        Mockito.when(mockMemberDAO.getByName(MEMBER_NAME)).thenReturn(Optional.of(testMember()));
        Assertions.assertEquals(memberService.searchByName(MEMBER_NAME), testMember());
    }

    @Test
    void getMemberByFakeId() {
        assertThrows(MemberNotFoundException.class, () -> memberService.getById(MEMBER_ID));
    }

    @Test
    void getMemberByIdWhenAllOk() {
        when(mockMemberDAO.getById(MEMBER_ID)).thenReturn(Optional.of(testMember()));
        assertTrue(memberService.getById(MEMBER_ID).equals(testMember()));
    }

    @Test
    void addFriendWhenNoAdderExist() {
        when(mockMemberDAO.getByName(OTHER_NAME)).thenReturn(Optional.of(testMember()));
        assertThrows(MemberNotFoundException.class, () -> memberService.addFriend(MEMBER_NAME, OTHER_NAME));
    }

    @Test
    void addFriendWhenAllIsOk() {
        when(mockMemberDAO.getByName(any())).thenReturn(Optional.of(testMember())).thenReturn(Optional.of(testMember()));
        assertTrue(memberService.addFriend(MEMBER_NAME, OTHER_NAME));
    }

    @Test
    void setPrivacyWhenNoMember() {
        assertThrows(MemberNotFoundException.class, () -> memberService.addPrivacy(MEMBER_NAME, FAKE_MEMBER_ID));
    }

    @Test
    void setPrivacyWhenNoPost() {
        when(mockMemberDAO.getByName(MEMBER_NAME)).thenReturn(Optional.of(testMember()));
        assertThrows(PostClassNotFoundException.class, () -> memberService.addPrivacy(MEMBER_NAME, FAKE_MEMBER_ID));
    }

    @Test
    void setPrivacyWhenAllIsOk() {
        when(mockMemberDAO.getByName(MEMBER_NAME)).thenReturn(Optional.of(testMember()));
        when(mockPostDAO.getById(FAKE_MEMBER_ID)).thenReturn(Optional.of(getPost()));
        assertTrue(memberService.addPrivacy(MEMBER_NAME, FAKE_MEMBER_ID));
    }

    private Member testMember() {
        var member = new Member();
        member.setName(MEMBER_NAME);
        member.setId(MEMBER_ID);
        member.setFriends(new PrivacyList());
        return member;
    }

    private PostClass getPost() {
        PostClass post = new PostClass();
        post.setPrivacyList(new PrivacyList());
        return post;
    }

    @Test
    @DisplayName("Search member by name when member exists")
    void searchMemberByName() {
        when(mockMemberDAO.getByName(MEMBER_NAME)).thenReturn(Optional.of(testMember()));
        assertEquals(MEMBER_NAME, memberService.searchByName(MEMBER_NAME).getName());
    }

    @Test
    @DisplayName("Search member by name when member does not exist")
    void searchMemberByNameWhenDoesNotExist() {
        assertThrows(MemberNotFoundException.class, () -> memberService.searchByName(MEMBER_NAME));
    }

    @Test
    void addMember() {
        memberService.addMember(testMember());
        verify(mockMemberDAO, times(1)).add(testMember());
    }

    @Test
    void deleteMember() {
        memberService.deleteMember(testMember().getId());
        verify(mockMemberDAO, times(1)).deleteById(1L);
    }

}