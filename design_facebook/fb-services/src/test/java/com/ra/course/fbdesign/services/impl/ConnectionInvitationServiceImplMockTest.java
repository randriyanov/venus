package com.ra.course.fbdesign.services.impl;


import com.ra.course.fbdesign.model.ConnectionInvitation;
import com.ra.course.fbdesign.model.EmailNotification;
import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.persistence.repositories.impl.MemberRepoImpl;
import com.ra.course.fbdesign.services.ConnectionInvitationService;
import com.ra.course.fbdesign.services.exceptions.MemberNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class ConnectionInvitationServiceImplMockTest {
    @Mock
    private MemberRepoImpl memberRepository;
    private ConnectionInvitation connectionInvitation;
    private ConnectionInvitationService invitationService;

    @BeforeEach
    void setUp() {
        initMocks(this);
        invitationService = new ConnectionInvitationServiceImpl(memberRepository);
        connectionInvitation = new ConnectionInvitation();
        connectionInvitation.setMemberInvited(new Member());
    }


    @Test
    @DisplayName("When invited member  is absent " +
            "then MemberNotFoundException throws")
    void sendConnectionInvitation() {
        assertThrows(MemberNotFoundException.class,
                () -> invitationService.sendConnectionInvitation(connectionInvitation));
    }

    @Test
    @DisplayName("When invited member  is present " +
            "then email notification returns")
    void whenMemberInvitedIsPresentThenConnectionSend() {
        when(memberRepository.getByName(connectionInvitation.getMemberInvited().getName())).
                thenReturn(Optional.of(testMember()));
        invitationService.sendConnectionInvitation(connectionInvitation);
        assertEquals(EmailNotification.class, connectionInvitation.getNotification().getClass());
    }

    private Member testMember() {
        return new Member();
    }
}
