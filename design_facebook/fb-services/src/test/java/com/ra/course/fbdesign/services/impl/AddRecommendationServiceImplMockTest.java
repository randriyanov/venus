package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.PageBean;
import com.ra.course.fbdesign.model.Recommendation;
import com.ra.course.fbdesign.persistence.repositories.impl.PageRepoImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.RecommendationRepoImpl;
import com.ra.course.fbdesign.services.AddRecommendationService;
import com.ra.course.fbdesign.services.exceptions.PageBeanNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.HashSet;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class AddRecommendationServiceImplMockTest {
    private Recommendation recommendation;
    private AddRecommendationService addRecommendationService;
    @Mock
    private PageRepoImpl pageBeanRepository;
    @Mock
    private RecommendationRepoImpl recommendationRepo;

    @BeforeEach
    void setUp() {
        initMocks(this);
        recommendation = new Recommendation();
        addRecommendationService = new AddRecommendationServiceImpl(pageBeanRepository, recommendationRepo);
    }

    @Test
    @DisplayName("When page is not found then exception throws")
    void addRecommendationWhenPageIsAbsentThenExceptionThrows() {
        //when
        var missPageId = 1L;
        //then
        assertThrows(PageBeanNotFoundException.class,
                () -> addRecommendationService.addRecommendation(missPageId, recommendation));
    }

    @Test
    void createRecommendation() {
        addRecommendationService.createRecommendation(3, "description");
        verify(recommendationRepo, times(1)).add(any());
    }

    @Test
    void deleteRecommendation() {
        addRecommendationService.deleteRecommendation(1L);
        verify(recommendationRepo, times(1)).deleteById(1L);
    }

    @Test
    @DisplayName("When page is found then recommendation adds")
    void whenPageIsFoundThenRecommendationAdds() {
        //when
        var goodId = 1L;
        //then
        when(pageBeanRepository.getById(goodId)).thenReturn(Optional.of(testPage()));
        //result
        assertTrue(addRecommendationService.addRecommendation(goodId, recommendation).getRecommendations().contains(recommendation));
    }

    PageBean testPage() {
        var page = new PageBean();
        page.setRecommendations(new HashSet<>());
        return page;
    }
}