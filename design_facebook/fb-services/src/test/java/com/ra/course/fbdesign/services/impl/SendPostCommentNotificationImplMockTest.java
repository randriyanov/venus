package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.Comment;
import com.ra.course.fbdesign.model.EmailNotification;
import com.ra.course.fbdesign.model.PostClass;
import com.ra.course.fbdesign.persistence.repositories.IPostRepo;
import com.ra.course.fbdesign.services.SendPostCommentNotification;
import com.ra.course.fbdesign.services.exceptions.PostClassNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.LinkedList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class SendPostCommentNotificationImplMockTest {
    @Mock
    private IPostRepo postClassRepository;
    private SendPostCommentNotification sendPostCommentNotification;
    private Comment comment;

    @BeforeEach
    void setUp() {
        initMocks(this);
        comment = new Comment();
        sendPostCommentNotification = new SendPostCommentNotificationImpl(postClassRepository);
    }

    @Test
    @DisplayName("When post is not present then exceptions throws")
    void whenPostIsNotPresentThenExceptionThrows() {
        var missPostId = 1L;

        assertThrows(PostClassNotFoundException.class,
                () -> sendPostCommentNotification.sendPostCommentNotification(missPostId, comment));
    }

    @Test
    @DisplayName("When post is  present. If we have new comment then notification send")
    void sendPostCommentNotification() {
        var goodPostId = 1L;

        when(postClassRepository.getById(goodPostId))
                .thenReturn(Optional.of(testPostWithoutNewComment()));

        assertNull(sendPostCommentNotification.sendPostCommentNotification(goodPostId, comment));
    }

    @Test
    @DisplayName("When post is  present. If we dont have new comment then null send")
    void whenPostIsPresentAndWeDontHaveNewCommentThenNullSend() {
        var goodPostId = 1L;
        when(postClassRepository.getById(goodPostId)).
                thenReturn(Optional.of(testPostWithNewComment()));
        assertEquals(EmailNotification.class,
                sendPostCommentNotification.sendPostCommentNotification(goodPostId, comment).getClass());
    }

    PostClass testPostWithoutNewComment() {
        var testPost = new PostClass();
        testPost.setComments(new LinkedList<>());
        return testPost;
    }

    PostClass testPostWithNewComment() {
        var testPost = new PostClass();
        testPost.setComments(new LinkedList<>());
        testPost.getComments().add(comment);
        return testPost;
    }

}