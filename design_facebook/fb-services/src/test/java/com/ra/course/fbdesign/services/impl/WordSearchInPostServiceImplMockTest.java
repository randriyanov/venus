package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.PostClass;
import com.ra.course.fbdesign.persistence.repositories.impl.PostRepoImpl;
import com.ra.course.fbdesign.services.WordSearchInPostService;
import com.ra.course.fbdesign.services.exceptions.EmptySearchWordException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class WordSearchInPostServiceImplMockTest {
    @Mock
    private PostRepoImpl postClassRepository;
    private WordSearchInPostService wordSearchInPostService;

    @BeforeEach
    void setUp() {
        initMocks(this);
        wordSearchInPostService = new WordSearchInPostServiceImpl(postClassRepository);
    }

    @Test
    @DisplayName("When search word is EMPTY then EmptySearchWordException throws")
    void findEmptyWord() {
        String emptyWord = "";
        assertThrows(EmptySearchWordException.class,
                () -> wordSearchInPostService.findWord(emptyWord));
    }

    @Test
    @DisplayName("When search word is NOT EMPTY then we search word")
    void findWordWhenPostIsPresent() {
        //when
        String word = "word";
        //then
        when(postClassRepository.getAll()).thenReturn(set());
        assertFalse(wordSearchInPostService.findWord(word).contains(testPostClass()));
        //result
    }

    PostClass testPostClass() {
        var post = new PostClass();
        post.setPostText("test");
        return post;
    }

    Set<PostClass> set() {
        Set<PostClass> postClasses = new HashSet<>();
        postClasses.add(testPostClass());
        return postClasses;
    }
}