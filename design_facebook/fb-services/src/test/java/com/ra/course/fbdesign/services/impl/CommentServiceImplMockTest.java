package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.Comment;
import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.model.PostClass;
import com.ra.course.fbdesign.persistence.repositories.impl.CommentRepoImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.MemberRepoImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.PostRepoImpl;
import com.ra.course.fbdesign.services.CommentService;
import com.ra.course.fbdesign.services.exceptions.APIException;
import com.ra.course.fbdesign.services.exceptions.CommentNotFoundException;
import com.ra.course.fbdesign.services.exceptions.MemberNotFoundException;
import com.ra.course.fbdesign.services.exceptions.PostClassNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class CommentServiceImplMockTest {
    @Mock
    private MemberRepoImpl mockMemberDAO;
    @Mock
    private PostRepoImpl mockPostDAO;
    @Mock
    private CommentRepoImpl mockCommentDAO;
    private CommentService commentService;
    private Comment comment;
    private Long id = 1L;

    @BeforeEach
    void setUp() {
        initMocks(this);
        commentService = new CommentServiceImpl(mockMemberDAO, mockCommentDAO, mockPostDAO);
        comment = new Comment();
    }

    @Test
    @DisplayName("Try to get Comment when id is not exist")
    void getByIdWhenNotExist() {
        assertThrows(CommentNotFoundException.class, () -> commentService.getById(id));
    }

    @Test
    @DisplayName("Try to get Comment when id exists")
    void getByIdWhenExist() {
        when(mockCommentDAO.getById(id)).thenReturn(Optional.of(comment));
        assertEquals(commentService.getById(id), comment);
    }

    @Test
    void deleteByIdWhenExistInRepo() {
        commentService.deleteById(id);
        verify(mockCommentDAO, times(1)).deleteById(id);
    }

    @Test
    void deleteWhenExistInRepo() {
        commentService.deleteComment(comment);
        verify(mockCommentDAO, times(1)).deleteById(comment.getId());
    }

    @Test
    void addCommentWithNoMember() {
        assertThrows(MemberNotFoundException.class, () -> commentService.addComment("", "", 2L));
    }

    @Test
    void addCommentWithNoPost() {
        when(mockMemberDAO.getByName("")).thenReturn(Optional.of(new Member()));
        assertThrows(PostClassNotFoundException.class, () -> commentService.addComment("", "", 2L));
    }

    @Test
    void addCommentWithEmptyText() {
        when(mockMemberDAO.getByName("")).thenReturn(Optional.of(new Member()));
        when(mockPostDAO.getById(2L)).thenReturn(Optional.of(new PostClass()));
        APIException exception = assertThrows(APIException.class, () -> commentService.addComment("", "", 2L));
        assertEquals("Invalid data", exception.getMessage());
    }

    @Test
    void addCommentWhenAllIsOk() {
        when(mockMemberDAO.getByName("name")).thenReturn(Optional.of(new Member()));
        when(mockPostDAO.getById(2L)).thenReturn(Optional.of(getPost()));
        assertTrue(commentService.addComment("name", "text", 2L));
    }

    private PostClass getPost() {
        PostClass post = new PostClass();
        post.setComments(new ArrayList<>());
        return post;
    }


    @Test
    void likeCommentWithNoMember() {
        assertThrows(MemberNotFoundException.class, () -> commentService.likeComment("", 2L));
    }

    @Test
    void likeCommentWithNoComment() {
        when(mockMemberDAO.getByName("")).thenReturn(Optional.of(new Member()));
        assertThrows(CommentNotFoundException.class, () -> commentService.likeComment("", 2L));
    }

    @Test
    void likeCommentWhenAllIsOk() {
        when(mockMemberDAO.getByName("name")).thenReturn(Optional.of(new Member()));
        when(mockCommentDAO.getById(3L)).thenReturn(Optional.of(new Comment()));
        assertTrue(commentService.likeComment("name", 3L));
    }
}