package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.model.PostClass;
import com.ra.course.fbdesign.persistence.repositories.impl.MemberRepoImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.PostRepoImpl;
import com.ra.course.fbdesign.services.PostService;
import com.ra.course.fbdesign.services.exceptions.MemberNotFoundException;
import com.ra.course.fbdesign.services.exceptions.PostClassNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class PostServiceImplMockTest {
    @Mock
    private MemberRepoImpl mockMemberDAO;
    @Mock
    private PostRepoImpl mockPostDAO;
    private PostService postService;

    private PostClass postClass;

    @BeforeEach
    void setUp() {
        initMocks(this);
        postClass = new PostClass();
        postService = new PostServiceImpl(mockMemberDAO, mockPostDAO);
    }

    @Test
    void createPostWithNoCreator() {
        assertThrows(MemberNotFoundException.class, () -> postService.createPost("", ""));
    }

    @Test
    void createPostWhenAllIsOk() {
        when(mockMemberDAO.getByName("name")).thenReturn(Optional.of(getMember()));
        assertTrue(postService.createPost("name", "some"));
    }

    @Test
    void sharePostWithoutMember() {
        assertThrows(MemberNotFoundException.class, () -> postService.sharePost("", 2L));
    }

    @Test
    void sharePostWithoutPost() {
        when(mockMemberDAO.getByName("")).thenReturn(Optional.of(getMember()));
        assertThrows(PostClassNotFoundException.class, () -> postService.sharePost("", 2L));
    }

    @Test
    void deletePost() {
        postService.deletePost(1L);
        verify(mockPostDAO, times(1)).deleteById(1L);
    }

    @Test
    void sharePostWhenAllIsOk() {
        when(mockPostDAO.getById(2L)).thenReturn(Optional.of(new PostClass()));
        when(mockMemberDAO.getByName("name")).thenReturn(Optional.of(getMember()));
        assertTrue(postService.sharePost("name", 2L));
    }

    @Test
    void addPost() {
        postService.add(postClass);
        verify(mockPostDAO, times(1)).add(postClass);
    }

    private Member getMember() {
        Member member = new Member();
        member.setPostClasses(new ArrayList<>());
        return member;
    }
}