package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.model.PrivacyList;
import com.ra.course.fbdesign.services.IFollowService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class FollowServiceImplMockTest {

    private IFollowService followService;
    private Set<Member> members;
    private PrivacyList privacyList;
    private Member testMember;
    private Member follower;
    @Mock
    private MemberServiceImpl searchService;

    @BeforeEach
    void setUp() {
        initMocks(this);
        followService = new FollowServiceImpl(searchService);
        testMember = new Member();
        members = new HashSet<>();
        follower = new Member();
        privacyList = new PrivacyList();
        testMember.setFollowers(privacyList);
        testMember.getFollowers().setMembersList(members);
    }

    @Test
    void followMember() {
        when(searchService.searchByName(testMember.getName())).thenReturn(Optional.of(testMember).get());
        followService.followMember(testMember, follower);
        assertTrue(testMember.getFollowers().getMembersList().contains(follower));
    }
}