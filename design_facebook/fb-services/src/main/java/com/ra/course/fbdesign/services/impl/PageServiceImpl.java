package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.model.PageBean;
import com.ra.course.fbdesign.persistence.repositories.impl.MemberRepoImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.PageRepoImpl;
import com.ra.course.fbdesign.services.ISearchService;
import com.ra.course.fbdesign.services.PageService;
import com.ra.course.fbdesign.services.exceptions.MemberNotFoundException;
import com.ra.course.fbdesign.services.exceptions.PageBeanNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class PageServiceImpl implements PageService, ISearchService<PageBean> {

    private final PageRepoImpl pageRepo;
    private final MemberRepoImpl memberRepo;

    @Override
    public PageBean add(final PageBean pageBean) {
        return pageRepo.add(pageBean);
    }

    @Override
    public boolean createPage(final String creatorName, final String pageName, final String pageDescription) {
        final Optional<Member> creator = memberRepo.getByName(creatorName);
        if (creator.isEmpty()) {
            throw new MemberNotFoundException();
        }
        final PageBean newPage = new PageBean();
        newPage.setCreator(creator.get());
        newPage.setDescription(pageDescription);
        newPage.setPageName(pageName);
        pageRepo.add(newPage);
        return true;
    }

    public void deletePage(final Long id) {
        pageRepo.deleteById(id);
    }

    @Override
    public boolean followPage(final String followerName, final long pageId) {
        final Optional<Member> follower = memberRepo.getByName(followerName);
        if (follower.isEmpty()) {
            throw new MemberNotFoundException();
        }
        final Optional<PageBean> page = pageRepo.getById(pageId);
        if (page.isEmpty()) {
            throw new PageBeanNotFoundException();
        }
        return page.get().getMembersFollows().add(follower.get());
    }

    @Override
    public PageBean searchByName(final String pageName) {
        final Optional<PageBean> pageBean = pageRepo.getPageByName(pageName);
        if (pageBean.isEmpty()) {
            throw new PageBeanNotFoundException();
        }
        return pageBean.get();
    }
}
