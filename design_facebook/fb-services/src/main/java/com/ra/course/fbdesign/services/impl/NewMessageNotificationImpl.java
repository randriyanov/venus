package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.EmailNotification;
import com.ra.course.fbdesign.model.Message;
import com.ra.course.fbdesign.services.NewMessageNotification;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class NewMessageNotificationImpl implements NewMessageNotification {
    /**
     * The system should send a notification to a member whenever there is a new message
     */
    @Override
    public void sendMessageNotification(final Message message) {
        message.getSendTo().
                forEach(member -> message.getNotifications().add(new EmailNotification()));
    }
}
