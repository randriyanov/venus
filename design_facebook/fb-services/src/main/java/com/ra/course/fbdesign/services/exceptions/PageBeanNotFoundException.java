package com.ra.course.fbdesign.services.exceptions;

public class PageBeanNotFoundException extends RuntimeException{
    private static final long serialVersionUID = 1L;
}
