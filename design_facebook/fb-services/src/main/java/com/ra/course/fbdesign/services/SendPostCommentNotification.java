package com.ra.course.fbdesign.services;

import com.ra.course.fbdesign.model.Comment;
import com.ra.course.fbdesign.model.Notification;

public interface SendPostCommentNotification {
    Notification sendPostCommentNotification(final Long postId, final Comment newComment);
}
