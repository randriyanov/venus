package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.services.IFollowService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class FollowServiceImpl implements IFollowService {

    private final MemberServiceImpl searchService;

    @Override
    public void followMember(final Member member, final Member follower) {
        final Member temp = searchService.searchByName(member.getName());
        temp.getFollowers().getMembersList().add(follower);
    }


}
