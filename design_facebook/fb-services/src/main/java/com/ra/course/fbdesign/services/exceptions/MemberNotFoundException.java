package com.ra.course.fbdesign.services.exceptions;

public class MemberNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1L;
}
