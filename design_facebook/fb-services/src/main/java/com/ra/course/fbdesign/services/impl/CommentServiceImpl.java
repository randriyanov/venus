package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.Comment;
import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.model.PostClass;
import com.ra.course.fbdesign.persistence.repositories.impl.CommentRepoImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.MemberRepoImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.PostRepoImpl;
import com.ra.course.fbdesign.services.CommentService;
import com.ra.course.fbdesign.services.exceptions.APIException;
import com.ra.course.fbdesign.services.exceptions.CommentNotFoundException;
import com.ra.course.fbdesign.services.exceptions.MemberNotFoundException;
import com.ra.course.fbdesign.services.exceptions.PostClassNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
@AllArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final MemberRepoImpl memberDAO;

    private final CommentRepoImpl commentDAO;

    private final PostRepoImpl postDAO;

    @Override
    public Comment getById(final Long id) {
        return commentDAO.getById(id).orElseThrow(CommentNotFoundException::new);
    }

    @Override
    public void deleteById(final Long id) {
        commentDAO.deleteById(id);
    }

    @Override
    public boolean addComment(final String memberName, final String text, final long postId) {
        final Optional<Member> member = memberDAO.getByName(memberName);
        checkMember(member);
        final Optional<PostClass> post = postDAO.getById(postId);
        if (post.isEmpty()) {
            throw new PostClassNotFoundException();
        }
        if (text.isEmpty()) {
            throw new APIException("Invalid data");
        }
        final Comment comment = new Comment();
        comment.setText(text);
        commentDAO.add(comment);
        return post.get().getComments().add(comment);
    }

    public void deleteComment(final Comment comment) {
        commentDAO.deleteById(comment.getId());
    }

    @Override
    public boolean likeComment(final String memberName, final long commentId) {
        final Optional<Member> member = memberDAO.getByName(memberName);
        checkMember(member);
        final Optional<Comment> comment = commentDAO.getById(commentId);
        if (comment.isEmpty()) {
            throw new CommentNotFoundException();
        }
        comment.get().setTotalLikes(comment.get().getTotalLikes() + 1);
        return true;
    }

    private void checkMember(final Optional<Member> member) {
        if (member.isEmpty()) {
            throw new MemberNotFoundException();
        }
    }
}
