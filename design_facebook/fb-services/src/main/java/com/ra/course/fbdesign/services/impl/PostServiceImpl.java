package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.model.PostClass;
import com.ra.course.fbdesign.persistence.repositories.impl.MemberRepoImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.PostRepoImpl;
import com.ra.course.fbdesign.services.PostService;
import com.ra.course.fbdesign.services.exceptions.MemberNotFoundException;
import com.ra.course.fbdesign.services.exceptions.PostClassNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
@AllArgsConstructor
public class PostServiceImpl implements PostService {

    private final MemberRepoImpl memberDAO;
    private final PostRepoImpl postDAO;

    @Override
    public PostClass add(final PostClass postClass) {
        return postDAO.add(postClass);
    }

    @Override
    public boolean createPost(final String creatorName, final String postText) {
        final Optional<Member> creator = memberDAO.getByName(creatorName);
        if (creator.isEmpty()) {
            throw new MemberNotFoundException();
        }
        final PostClass newPost = new PostClass();
        newPost.setPostCreator(creator.get());
        newPost.setPostText(postText);
        postDAO.add(newPost);
        creator.get().getPostClasses().add(newPost);
        return true;
    }

    public void deletePost(final Long id) {
        postDAO.deleteById(id);
    }

    @Override
    public boolean sharePost(final String memberName, final long postIdToShare) {
        final Optional<Member> member = memberDAO.getByName(memberName);
        if (member.isEmpty()) {
            throw new MemberNotFoundException();
        }
        final Optional<PostClass> post = postDAO.getById(postIdToShare);
        if (post.isEmpty()) {
            throw new PostClassNotFoundException();
        }
        return member.get().getPostClasses().add(post.get());
    }

}
