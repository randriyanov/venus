package com.ra.course.fbdesign.services.exceptions;

public class CommentNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 3464L;
}
