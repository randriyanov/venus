package com.ra.course.fbdesign.services;

import com.ra.course.fbdesign.model.Message;

public interface SendMessageService {
    void sendMessage( final Message message);
}
