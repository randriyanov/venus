package com.ra.course.fbdesign.services;

import com.ra.course.fbdesign.model.PageBean;
import com.ra.course.fbdesign.model.Recommendation;

public interface AddRecommendationService {
    PageBean addRecommendation(final Long pageId, Recommendation recommendation);

    void createRecommendation(final int rating, final String description);

    void deleteRecommendation(final Long id);
}
