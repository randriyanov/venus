package com.ra.course.fbdesign.services;

import com.ra.course.fbdesign.model.ConnectionInvitation;
import com.ra.course.fbdesign.model.EmailNotification;

public interface ConnectionInvitationService {
    EmailNotification sendConnectionInvitation(ConnectionInvitation connection);
}
