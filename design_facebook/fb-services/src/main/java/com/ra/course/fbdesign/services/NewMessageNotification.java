package com.ra.course.fbdesign.services;

import com.ra.course.fbdesign.model.Message;

public interface NewMessageNotification {
    void sendMessageNotification(Message message);
}
