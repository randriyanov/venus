package com.ra.course.fbdesign.services;


import com.ra.course.fbdesign.model.PostClass;

public interface PostService {

    PostClass add(PostClass postClass);

    boolean createPost(final String creatorName, String postText);

    boolean sharePost(final String memberName, long postIdToShare);

    void deletePost(final Long id);
}
