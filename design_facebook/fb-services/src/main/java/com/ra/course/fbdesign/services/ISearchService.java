package com.ra.course.fbdesign.services;

public interface ISearchService<T> {
    T searchByName(final String objectName);
}
