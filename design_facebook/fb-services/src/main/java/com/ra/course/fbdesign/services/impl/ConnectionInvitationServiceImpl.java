package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.ConnectionInvitation;
import com.ra.course.fbdesign.model.EmailNotification;
import com.ra.course.fbdesign.persistence.repositories.impl.MemberRepoImpl;
import com.ra.course.fbdesign.services.ConnectionInvitationService;
import com.ra.course.fbdesign.services.exceptions.MemberNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ConnectionInvitationServiceImpl implements ConnectionInvitationService {

    private final transient MemberRepoImpl memberRepo;

    /**
     * The system should send a notification to a member whenever
     * there is a new  friend request(ConnectionInvitation) .
     */

    @Override
    public EmailNotification sendConnectionInvitation(final ConnectionInvitation connection) {
        if (memberRepo.getByName(connection.getMemberInvited().getName()).isEmpty()) {
            throw new MemberNotFoundException();
        }
        final EmailNotification emailNotification = new EmailNotification();
        connection.setNotification(emailNotification);
        return emailNotification;
    }
}
