package com.ra.course.fbdesign.services;

import com.ra.course.fbdesign.model.*;

public interface IProfileService {
    Profile add(Profile profile);

    Profile getProfileById(Long profileId);

    Profile getProfileByMemberName(String memberName);

    Profile addEducation(Profile profile, EducationDetails education);

    Profile addWork(Profile profile, WorkDetails workDetails);

    Profile addPlace(Profile profile, PlacesDetails placesDetails);

    void createProfile(final String gender);

    void deleteProfile(final Long id);
}
