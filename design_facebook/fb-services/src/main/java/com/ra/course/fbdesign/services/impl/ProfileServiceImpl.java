package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.EducationDetails;
import com.ra.course.fbdesign.model.PlacesDetails;
import com.ra.course.fbdesign.model.Profile;
import com.ra.course.fbdesign.model.WorkDetails;
import com.ra.course.fbdesign.persistence.repositories.impl.ProfileRepoImpl;
import com.ra.course.fbdesign.services.IProfileService;
import com.ra.course.fbdesign.services.exceptions.RecordNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
@Service
@AllArgsConstructor
public class ProfileServiceImpl implements IProfileService {

    private final ProfileRepoImpl profileRepo;
    private final MemberServiceImpl searchService;

    @Override
    public Profile add(final Profile profile) {
        return profileRepo.add(profile);
    }

    @Override
    public Profile getProfileByMemberName(final String memberName) {
        return searchService.searchByName(memberName).getProfile();
    }

    public void createProfile(final String gender) {
        final Profile profile = new Profile();
        profile.setDateOfMembership(LocalDateTime.now());
        profile.setGender(gender);
        profileRepo.add(profile);
    }

    public void deleteProfile(final Long id) {
        profileRepo.deleteById(id);
    }

    @Override
    public Profile addEducation(final Profile profile, final EducationDetails education) {
        final Profile profileTemp = getProfileById(profile.getId());
        profileTemp.getEducationDetails().add(education);
        profileRepo.update(profileTemp);
        return profileTemp;
    }

    @Override
    public Profile addWork(final Profile profile, final WorkDetails workDetails) {
        final Profile profileTemp = getProfileById(profile.getId());
        profileTemp.getWorkDetailsList().add(workDetails);
        profileRepo.update(profileTemp);
        return profileTemp;
    }

    @Override
    public Profile addPlace(final Profile profile, final PlacesDetails placesDetails) {
        final Profile profileTemp = getProfileById(profile.getId());
        profileTemp.getPlacesDetailsList().add(placesDetails);
        profileRepo.update(profileTemp);
        return profileTemp;
    }

    @Override
    public Profile getProfileById(final Long profileId) {
        final Optional<Profile> profileOptional = profileRepo.getById(profileId);
        if (profileOptional.isEmpty()) {
            throw new RecordNotFoundException("Profile does not exists.");
        }
        return profileOptional.get();
    }
}
