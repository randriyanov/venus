package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.Comment;
import com.ra.course.fbdesign.model.EmailNotification;
import com.ra.course.fbdesign.model.Notification;
import com.ra.course.fbdesign.model.PostClass;
import com.ra.course.fbdesign.persistence.repositories.IBaseRepository;
import com.ra.course.fbdesign.services.SendPostCommentNotification;
import com.ra.course.fbdesign.services.exceptions.PostClassNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class SendPostCommentNotificationImpl implements SendPostCommentNotification {

    private final transient IBaseRepository<PostClass> postClassRepo;

    @Override
    public Notification sendPostCommentNotification(final Long postId, final Comment newComment) {
        final Optional<PostClass> post = postClassRepo.getById(postId);
        if (post.isEmpty()) {
            throw new PostClassNotFoundException();
        }
        if (post.get().getComments().contains(newComment)) {
            final EmailNotification emailNotification = new EmailNotification();
            newComment.setNotification(emailNotification);
            return emailNotification;
        }
        return null;
    }
}