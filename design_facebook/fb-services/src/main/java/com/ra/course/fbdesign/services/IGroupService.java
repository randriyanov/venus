package com.ra.course.fbdesign.services;


import com.ra.course.fbdesign.model.Group;

public interface IGroupService {
    Group add(Group group);

    Group getById(final Long id);

    void deleteById(final Long id);

    boolean createGroup(final String groupName, final String groupDescription, final String creatorName);

    boolean joinGroup(final long groupId, final String memberName);
}
