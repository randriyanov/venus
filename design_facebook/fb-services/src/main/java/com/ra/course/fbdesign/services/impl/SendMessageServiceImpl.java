package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.Message;
import com.ra.course.fbdesign.services.SendMessageService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SendMessageServiceImpl implements SendMessageService {

    /**
     * Any member should be able to send messages to other members.
     */
    @Override
    public void sendMessage(final Message message) {
        message.getAuthor().getMessages().add(message);
        message.getSendTo().forEach(member -> member.getMessages().add(message));
    }
}
