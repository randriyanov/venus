package com.ra.course.fbdesign.services.exceptions;

public class RecordNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 42L;

	public RecordNotFoundException(final String message) {
		super(message);
	}
}
