package com.ra.course.fbdesign.services;

import com.ra.course.fbdesign.model.PageBean;

public interface PageService {

    PageBean add(PageBean pageBean);

    boolean createPage(final String creatorName, String pageName, String pageDescription);

    boolean followPage(final String followerName, long pageId);
}
