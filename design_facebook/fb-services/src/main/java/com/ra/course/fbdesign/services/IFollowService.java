package com.ra.course.fbdesign.services;

import com.ra.course.fbdesign.model.Member;

public interface IFollowService {
	void followMember(Member member, Member follower);
}
