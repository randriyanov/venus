package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.PostClass;
import com.ra.course.fbdesign.persistence.repositories.IBaseRepository;
import com.ra.course.fbdesign.services.WordSearchInPostService;
import com.ra.course.fbdesign.services.exceptions.EmptySearchWordException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;
@Service
@AllArgsConstructor
public class WordSearchInPostServiceImpl implements WordSearchInPostService {

    private final transient IBaseRepository<PostClass> postClassRepo;

    /**
     * Members should be able to search through posts for a word
     */
    @Override
    public Set<PostClass> findWord(final String findWord) {
        if (findWord.trim().equals("")) {
            throw new EmptySearchWordException();
        }
        return postClassRepo.getAll().stream()
                .filter(post -> post.getPostText().contains(findWord))
                .collect(Collectors.toSet());
    }
}
