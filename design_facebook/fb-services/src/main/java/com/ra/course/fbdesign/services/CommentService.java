package com.ra.course.fbdesign.services;


import com.ra.course.fbdesign.model.Comment;

public interface CommentService {
    Comment getById(final Long id);

    void deleteById(final Long id);

    boolean addComment(final String memberName, String text, final long postId);

    boolean likeComment(final String memberName, final long commentId);

    void deleteComment(final Comment comment);
}
