package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.PageBean;
import com.ra.course.fbdesign.model.Recommendation;
import com.ra.course.fbdesign.persistence.repositories.impl.BaseRepositoryImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.RecommendationRepoImpl;
import com.ra.course.fbdesign.services.AddRecommendationService;
import com.ra.course.fbdesign.services.exceptions.PageBeanNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AddRecommendationServiceImpl implements AddRecommendationService {

    private final transient BaseRepositoryImpl<PageBean> pageBeanRepo;
    private final transient RecommendationRepoImpl recommRepo;

    /**
     * Any member should be able to add a recommendation for any page.
     */
    @Override
    public PageBean addRecommendation(final Long pageId, final Recommendation recommendation) {
        final Optional<PageBean> pageBean = pageBeanRepo.getById(pageId);
        if (pageBean.isEmpty()) {
            throw new PageBeanNotFoundException();
        }
        pageBean.get().getRecommendations().add(recommendation);
        pageBeanRepo.update(pageBean.get());
        return pageBean.get();
    }

    public void createRecommendation(final int rating, final String description) {
        final Recommendation recommendation = new Recommendation();
        recommendation.setRating(rating);
        recommendation.setDescription(description);
        recommendation.setCreatedAt(LocalDateTime.now());
        recommRepo.add(recommendation);
    }

    public void deleteRecommendation(final Long id) {
        recommRepo.deleteById(id);
    }
}
