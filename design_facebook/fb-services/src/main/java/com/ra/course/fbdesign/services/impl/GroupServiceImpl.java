package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.Group;
import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.persistence.repositories.impl.GroupRepoImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.MemberRepoImpl;
import com.ra.course.fbdesign.services.IGroupService;
import com.ra.course.fbdesign.services.ISearchService;
import com.ra.course.fbdesign.services.exceptions.GroupNotFoundException;
import com.ra.course.fbdesign.services.exceptions.MemberNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
@AllArgsConstructor
public class GroupServiceImpl implements IGroupService, ISearchService<Group> {
    private final GroupRepoImpl groupRepo;
    private final MemberRepoImpl memberRepo;

    @Override
    public Group add(final Group group) {
        return groupRepo.add(group);
    }

    @Override
    public Group getById(final Long id) {
        return groupRepo.getById(id).orElseThrow(GroupNotFoundException::new);
    }

    @Override
    public void deleteById(final Long id) {
        groupRepo.deleteById(id);
    }

    @Override
    public boolean createGroup(final String groupName, final String groupDescription, final String creatorName) {
        final Optional<Member> creator = memberRepo.getByName(creatorName);
        if (creator.isEmpty()) {
            throw new MemberNotFoundException();
        }
        final Group group = new Group();
        group.setGroupName(groupName);
        group.setDescription(groupDescription);
        group.setCreator(creator.get());
        creator.get().getGroups().add(group);
        groupRepo.add(group);
        return true;
    }

    public void deleteGroup(final Group group) {
        groupRepo.deleteById(group.getId());
    }

    @Override
    public boolean joinGroup(final long groupId, final String memberName) {
        final Optional<Member> member = memberRepo.getByName(memberName);
        if (member.isEmpty()) {
            throw new MemberNotFoundException();
        }
        final Optional<Group> group = groupRepo.getById(groupId);
        if (group.isEmpty()) {
            throw new GroupNotFoundException();
        }
        group.get().getMembers().add(member.get());
        member.get().getGroups().add(group.get());
        return true;
    }

    @Override
    public Group searchByName(final String groupName) {
        final Optional<Group> group = groupRepo.getGroupByName(groupName);
        if (group.isEmpty()) {
            throw new GroupNotFoundException();
        }
        return group.get();
    }
}
