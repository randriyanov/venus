package com.ra.course.fbdesign.services;

import com.ra.course.fbdesign.model.Member;

public interface IMemberService {

    Member getById(Long memberId);

    boolean addFriend(final String member, final String friend);

    boolean addPrivacy(final String memberName, final Long postId);
}
