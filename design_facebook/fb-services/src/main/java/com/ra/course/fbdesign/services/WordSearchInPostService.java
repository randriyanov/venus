package com.ra.course.fbdesign.services;


import com.ra.course.fbdesign.model.PostClass;

import java.util.Set;

public interface WordSearchInPostService {

    Set<PostClass> findWord(String findWord);
}
