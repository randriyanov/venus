package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.MediaContent;
import com.ra.course.fbdesign.persistence.repositories.IBaseRepository;
import com.ra.course.fbdesign.persistence.repositories.impl.exception.RecordNotExistException;
import com.ra.course.fbdesign.services.IMediaContentService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MediaContentServiceImpl implements IMediaContentService {
    private final IBaseRepository<MediaContent> mediaContentRepo;

    @Override
    public MediaContent getById(final Long mediaContentId) {
        return mediaContentRepo.getById(mediaContentId).orElseThrow(RecordNotExistException::new);
    }

    @Override
    public MediaContent add(final MediaContent mediaContent) {
        return mediaContentRepo.add(mediaContent);
    }

    @Override
    public void deleteById(final Long id) {
        mediaContentRepo.deleteById(id);
    }
}
