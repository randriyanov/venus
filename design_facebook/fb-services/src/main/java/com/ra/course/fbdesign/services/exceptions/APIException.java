package com.ra.course.fbdesign.services.exceptions;


public class APIException extends RuntimeException {
    private static final long serialVersionUID = 461L;

    public APIException(final String text) {
        super(text);
    }
}
