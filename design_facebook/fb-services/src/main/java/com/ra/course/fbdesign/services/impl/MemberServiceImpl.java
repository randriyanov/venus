package com.ra.course.fbdesign.services.impl;

import com.ra.course.fbdesign.model.Member;
import com.ra.course.fbdesign.model.PostClass;
import com.ra.course.fbdesign.persistence.repositories.impl.MemberRepoImpl;
import com.ra.course.fbdesign.persistence.repositories.impl.PostRepoImpl;
import com.ra.course.fbdesign.services.IMemberService;
import com.ra.course.fbdesign.services.ISearchService;
import com.ra.course.fbdesign.services.exceptions.MemberNotFoundException;
import com.ra.course.fbdesign.services.exceptions.PostClassNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
@AllArgsConstructor
public class MemberServiceImpl implements IMemberService, ISearchService<Member> {

    private final transient MemberRepoImpl memberRepo;
    private final transient PostRepoImpl postRepo;

    @Override
    public Member getById(final Long memberId) {
        return memberRepo.getById(memberId).orElseThrow(MemberNotFoundException::new);
    }

    public Member addMember(final Member member) {
        return memberRepo.add(member);
    }

    public void deleteMember(final Long id) {
        memberRepo.deleteById(id);
    }

    @Override
    public boolean addFriend(final String member, final String friend) {
        final Optional<Member> memberWhoAdd = memberRepo.getByName(member);
        checkMember(memberWhoAdd);
        final Optional<Member> memberFriend = memberRepo.getByName(friend);
        checkMember(memberFriend);
        return memberWhoAdd.get().getFriends().getMembersList().add(memberFriend.get());
    }

    @Override
    public boolean addPrivacy(final String memberName, final Long postId) {
        final Optional<Member> member = memberRepo.getByName(memberName);
        checkMember(member);
        final Optional<PostClass> post = postRepo.getById(postId);
        if (post.isEmpty()) {
            throw new PostClassNotFoundException();
        }
        post.get().getPrivacyList().setMembersList(member.get().getFriends().getMembersList());
        return true;
    }

    @Override
    public Member searchByName(final String memberName) {
        final Optional<Member> member = memberRepo.getByName(memberName);
        checkMember(member);
        return member.get();
    }

    private void checkMember(final Optional<Member> member) {
        if (member.isEmpty()) {
            throw new MemberNotFoundException();
        }
    }
}
