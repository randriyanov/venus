package com.ra.course.fbdesign.services.exceptions;

public class GroupNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 3564L;
}
