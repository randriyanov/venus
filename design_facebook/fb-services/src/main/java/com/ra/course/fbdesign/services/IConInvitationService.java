package com.ra.course.fbdesign.services;

import com.ra.course.fbdesign.model.ConnectionInvitation;
import com.ra.course.fbdesign.model.Member;

public interface IConInvitationService {
	ConnectionInvitation getById(Long conInvitationId);
	boolean save(ConnectionInvitation conInvitation);
	boolean send(Member member, ConnectionInvitation conInvitation);
	boolean acceptFriend(Member member, ConnectionInvitation conInvitation);
	boolean rejectFriend(Member member, ConnectionInvitation conInvitation);
}
