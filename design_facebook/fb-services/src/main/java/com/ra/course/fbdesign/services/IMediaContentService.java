package com.ra.course.fbdesign.services;

import com.ra.course.fbdesign.model.MediaContent;

public interface IMediaContentService {
    MediaContent getById(final Long mediaContentId);

    void deleteById(final Long id);

    MediaContent add(MediaContent mediaContent);
}
