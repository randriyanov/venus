package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.TableSeat;
import com.ra.course.restaurant.persistence.CRUDRepository;

import java.util.List;

public interface TableSeatRepository extends CRUDRepository<TableSeat> {
    List<TableSeat> getTableSeatsByTableID(int tableiD);
}
