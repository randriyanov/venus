package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.TableSeat;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class TableSeatRepositoryImpl extends CRUDRepositoryImpl<TableSeat> implements TableSeatRepository {
    @Override
    public List<TableSeat> getTableSeatsByTableID(final int tableiD) {
        return repository.stream().filter(tableSeat -> tableSeat.getTableID() == tableiD).collect(Collectors.toList());
    }

}
