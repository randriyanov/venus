package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class BranchRepositoryImpl extends CRUDRepositoryImpl<Branch> implements BranchRepository {
    @Override
    public Optional<Branch> getBranchByName(final String name) {
        return repository.stream().filter(item -> item.getName().equals(name)).findAny();
    }
}
