package com.ra.course.restaurant.persistence.menu;

import com.ra.course.restaurant.model.entity.menu.Menu;
import com.ra.course.restaurant.persistence.CRUDRepository;

public interface MenuRepository extends CRUDRepository<Menu> {
}
