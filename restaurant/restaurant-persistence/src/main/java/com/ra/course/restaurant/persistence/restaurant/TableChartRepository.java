package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.TableChart;
import com.ra.course.restaurant.persistence.CRUDRepository;

import java.util.Optional;

public interface TableChartRepository extends CRUDRepository<TableChart> {
    Optional<TableChart> getTableChartByBranchName (final String name);
}
