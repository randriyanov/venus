package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.Kitchen;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class KitchenRepositoryImpl extends CRUDRepositoryImpl<Kitchen> implements KitchenRepository {
    @Override
    public Optional<Kitchen> getKitchenByBranchName(final String name) {
        return repository.stream().filter(item -> item.getBranchName().equals(name)).findAny();
    }
}
