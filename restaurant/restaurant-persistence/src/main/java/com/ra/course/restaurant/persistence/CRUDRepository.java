package com.ra.course.restaurant.persistence;

import com.ra.course.restaurant.model.entity.BaseEntity;

import java.util.Collection;
import java.util.Optional;

public interface CRUDRepository<T extends BaseEntity> {

    void add(T entity);

    Optional<T> findById(int id);

    void update(T entity);

    void delete(T entity);

    Collection<T> getAll();

}
