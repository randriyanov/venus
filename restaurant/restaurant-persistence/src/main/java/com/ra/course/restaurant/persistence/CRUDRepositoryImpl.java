package com.ra.course.restaurant.persistence;

import com.ra.course.restaurant.model.entity.BaseEntity;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

@Repository
public class CRUDRepositoryImpl<T extends BaseEntity> implements CRUDRepository<T> {

    protected final List<T> repository = new CopyOnWriteArrayList<>();

    @Override
    public void add(final T entity) {
        entity.setId(repository.size() + 1);
        repository.add(entity);
    }

    @Override
    public Optional<T> findById(final int id) {
        return repository.stream().filter(entity->entity.getId() == id).findAny();
    }

    @Override
    public void update(final T entity) {
        repository.set(entity.getId() - 1, entity);
    }

    @Override
    public void delete(final T entity) {
        repository.remove(entity);
    }

    @Override
    public Collection<T> getAll() {
        return repository;
    }

    public List<T> getRepository() {
        return repository;
    }

}
