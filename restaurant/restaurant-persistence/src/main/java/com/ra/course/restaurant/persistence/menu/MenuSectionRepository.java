package com.ra.course.restaurant.persistence.menu;

import com.ra.course.restaurant.model.entity.menu.MenuSection;
import com.ra.course.restaurant.persistence.CRUDRepository;

import java.util.List;

public interface MenuSectionRepository extends CRUDRepository<MenuSection> {
    List<MenuSection> getSectionsByMenuID (final int menuID);
}
