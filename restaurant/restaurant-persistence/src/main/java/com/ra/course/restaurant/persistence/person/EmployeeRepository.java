package com.ra.course.restaurant.persistence.person;

import com.ra.course.restaurant.model.entity.enumerations.EmployeeRole;
import com.ra.course.restaurant.model.entity.person.Employee;
import com.ra.course.restaurant.persistence.CRUDRepository;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository extends CRUDRepository<Employee> {

    Optional<Employee> getEmployeeByEmail(String email);

    Optional<Employee> getEmployeeByPhone(String phone);

    List<Employee> getEmployeeByRole(EmployeeRole employeeRole);
}
