package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.Restaurant;
import org.springframework.stereotype.Repository;

@Repository
public class RestaurantRepositoryImpl implements RestaurantRepository {

    final private Restaurant restaurant = new Restaurant("No name");

    @Override
    public void updateName(final String name) {
        restaurant.setName(name);
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }
}
