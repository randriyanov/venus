package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.TableChart;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class TableChartRepositoryImpl extends CRUDRepositoryImpl<TableChart> implements TableChartRepository {

    @Override
    public Optional<TableChart> getTableChartByBranchName(final String name) {
        return repository.stream().filter(item -> item.getBranchName().equals(name)).findAny();
    }
}
