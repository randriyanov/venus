package com.ra.course.restaurant.persistence.order;

import com.ra.course.restaurant.model.entity.menu.MealItem;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class MealItemRepositoryImpl extends CRUDRepositoryImpl<MealItem> implements MealItemRepository {

    @Override
    public List<MealItem> getMealItemsByMealId(final int mealId) {
        return repository.stream().filter(mealItem -> mealItem.getMealId() == mealId).collect(Collectors.toList());
    }
}
