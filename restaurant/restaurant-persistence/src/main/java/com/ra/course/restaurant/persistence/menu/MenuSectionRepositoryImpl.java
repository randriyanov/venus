package com.ra.course.restaurant.persistence.menu;

import com.ra.course.restaurant.model.entity.menu.MenuSection;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class MenuSectionRepositoryImpl extends CRUDRepositoryImpl<MenuSection> implements MenuSectionRepository {
    @Override
    public List<MenuSection> getSectionsByMenuID(final int menuID) {
        return repository.stream().filter(item -> item.getMenuID() == menuID).collect(Collectors.toList());
    }
}
