package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.persistence.CRUDRepository;

import java.util.Optional;

public interface BranchRepository extends CRUDRepository<Branch> {
    Optional<Branch> getBranchByName(final String name);
}
