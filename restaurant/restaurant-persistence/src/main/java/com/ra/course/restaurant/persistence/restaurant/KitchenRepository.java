package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.Kitchen;
import com.ra.course.restaurant.persistence.CRUDRepository;

import java.util.Optional;

public interface KitchenRepository extends CRUDRepository<Kitchen> {
    Optional<Kitchen> getKitchenByBranchName (final String name);
}
