package com.ra.course.restaurant.persistence.menu;

import com.ra.course.restaurant.model.entity.menu.MenuItem;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class MenuItemRepositoryImpl extends CRUDRepositoryImpl<MenuItem> implements MenuItemRepository{

    @Override
    public List<MenuItem> getItemsBySectionID(final int sectionID) {
        return repository.stream().filter(item -> item.getSectionID() == sectionID).collect(Collectors.toList());
    }
}
