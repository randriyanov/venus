package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.Restaurant;

public interface RestaurantRepository {
    void updateName (String name);
    Restaurant getRestaurant();
}
