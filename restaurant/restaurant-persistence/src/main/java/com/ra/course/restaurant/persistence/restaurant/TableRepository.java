package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Table;
import com.ra.course.restaurant.persistence.CRUDRepository;

import java.util.List;

public interface TableRepository extends CRUDRepository<Table> {
    List<Table> getTablesByBranch(Branch branch);
}
