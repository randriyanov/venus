package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.Reservation;
import com.ra.course.restaurant.persistence.CRUDRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ReservationRepository extends CRUDRepository<Reservation> {
    List<Reservation> getReservationsByCustomerID(int customerID);
    Optional<Reservation> getReservationByTime(LocalDateTime datetime);
    List<Reservation> getReservationsToNotify(int minutes);
    List<Reservation> getFutureReservations();
}
