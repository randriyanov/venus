package com.ra.course.restaurant.persistence.order;

import com.ra.course.restaurant.model.entity.menu.Meal;
import com.ra.course.restaurant.persistence.CRUDRepository;

public interface MealRepository extends CRUDRepository<Meal> {
}
