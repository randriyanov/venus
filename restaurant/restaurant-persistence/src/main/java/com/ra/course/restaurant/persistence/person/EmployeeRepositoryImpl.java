package com.ra.course.restaurant.persistence.person;

import com.ra.course.restaurant.model.entity.enumerations.EmployeeRole;
import com.ra.course.restaurant.model.entity.person.Employee;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepositoryImpl extends CRUDRepositoryImpl<Employee> implements EmployeeRepository {

    @Override
    public Optional<Employee> getEmployeeByEmail(final String email) {
        return repository.stream().filter(empl -> empl.getEmail().equals(email)).findFirst();
    }

    @Override
    public Optional<Employee> getEmployeeByPhone(final String phone) {
        return repository.stream().filter(empl -> empl.getPhone().equals(phone)).findFirst();
    }


    @Override
    public List<Employee> getEmployeeByRole(final EmployeeRole employeeRole) {
        return repository.stream().filter(empl->empl.getEmployeeRole().equals(employeeRole)).collect(Collectors.toList());
    }


}
