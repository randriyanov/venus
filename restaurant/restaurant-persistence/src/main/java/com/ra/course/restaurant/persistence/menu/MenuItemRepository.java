package com.ra.course.restaurant.persistence.menu;

import com.ra.course.restaurant.model.entity.menu.MenuItem;
import com.ra.course.restaurant.persistence.CRUDRepository;

import java.util.List;

public interface MenuItemRepository extends CRUDRepository<MenuItem> {
    List<MenuItem> getItemsBySectionID (final int sectionID);
}
