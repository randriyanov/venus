package com.ra.course.restaurant.persistence.payment;

import com.ra.course.restaurant.model.entity.payment.CheckBill;
import com.ra.course.restaurant.persistence.CRUDRepository;

public interface CheckBillRepository extends CRUDRepository<CheckBill> {
}
