package com.ra.course.restaurant.persistence.order;

import com.ra.course.restaurant.model.entity.order.Order;
import com.ra.course.restaurant.persistence.CRUDRepository;

public interface OrderRepository extends CRUDRepository<Order> {
}
