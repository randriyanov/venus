package com.ra.course.restaurant.persistence.order;

import com.ra.course.restaurant.model.entity.order.Order;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

@Repository
public class OrderRepositoryImpl extends CRUDRepositoryImpl<Order> implements OrderRepository {
}
