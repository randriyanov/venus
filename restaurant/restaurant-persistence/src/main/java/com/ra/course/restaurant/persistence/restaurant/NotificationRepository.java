package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.notification.Notification;
import com.ra.course.restaurant.persistence.CRUDRepository;

import java.util.List;

public interface NotificationRepository extends CRUDRepository<Notification> {
    List<Notification> getNotificationsByCustomerID(int customer_id);
}
