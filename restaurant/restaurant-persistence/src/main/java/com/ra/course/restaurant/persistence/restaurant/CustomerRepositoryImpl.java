package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.person.Customer;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class CustomerRepositoryImpl extends CRUDRepositoryImpl<Customer> implements CustomerRepository {

    @Override
    public Optional<Customer> getCustomerByPhone(final String phone) {
        return repository.stream().filter(customer -> customer.getPhone().equals(phone)).findAny();
    }
}
