package com.ra.course.restaurant.persistence.payment;

import com.ra.course.restaurant.model.entity.payment.CheckBill;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

@Repository
public class CheckBillRepositoryImpl extends CRUDRepositoryImpl<CheckBill> implements CheckBillRepository {
}
