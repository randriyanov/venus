package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.person.Customer;
import com.ra.course.restaurant.persistence.CRUDRepository;

import java.util.Optional;

public interface CustomerRepository extends CRUDRepository<Customer> {
    Optional<Customer> getCustomerByPhone(String phone);
}
