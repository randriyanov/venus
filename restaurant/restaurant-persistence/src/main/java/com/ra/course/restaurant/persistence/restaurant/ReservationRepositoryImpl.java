package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.Reservation;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class ReservationRepositoryImpl extends CRUDRepositoryImpl<Reservation> implements ReservationRepository {

    @Override
    public List<Reservation> getReservationsByCustomerID(final int customerID) {
        return repository.stream().filter(reservation -> reservation.getCustomer().getId() == customerID).collect(Collectors.toList());
    }

    @Override
    public Optional<Reservation> getReservationByTime(final LocalDateTime datetime) {
        return repository.stream().filter(reservation -> reservation.getTimeOfReservation() == datetime).findAny();
    }

    @Override
    public List<Reservation> getReservationsToNotify(final int minutes) {
        return repository.stream().filter(reservation -> reservation.getTimeOfReservation().isAfter(LocalDateTime.now().plusHours(1)) && reservation.getTimeOfReservation().isBefore(LocalDateTime.now().plusHours(1).plusMinutes(minutes))).collect(Collectors.toList());
    }

    @Override
    public List<Reservation> getFutureReservations() {
        return repository.stream().filter(reservation -> reservation.getTimeOfReservation().isAfter(LocalDateTime.now())).collect(Collectors.toList());
    }
}
