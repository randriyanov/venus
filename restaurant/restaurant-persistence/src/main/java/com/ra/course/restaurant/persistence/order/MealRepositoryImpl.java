package com.ra.course.restaurant.persistence.order;

import com.ra.course.restaurant.model.entity.menu.Meal;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

@Repository
public class MealRepositoryImpl extends CRUDRepositoryImpl<Meal> implements MealRepository {
}
