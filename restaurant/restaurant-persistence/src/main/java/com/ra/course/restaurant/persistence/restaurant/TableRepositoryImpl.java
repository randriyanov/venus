package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Table;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class TableRepositoryImpl extends CRUDRepositoryImpl<Table> implements TableRepository {
    @Override
    public List<Table> getTablesByBranch(final Branch branch) {
        return repository.stream().filter(table -> table.getTableChart().getBranchName().equals(branch.getName())).collect(Collectors.toList());
    }
}
