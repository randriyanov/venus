package com.ra.course.restaurant.persistence.person;

import com.ra.course.restaurant.model.entity.person.Account;
import com.ra.course.restaurant.persistence.CRUDRepository;

import java.util.Optional;

public interface AccountRepository extends CRUDRepository<Account> {
    Optional<Account> getAccountByEmployeeId(int employeeId);
}
