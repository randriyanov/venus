package com.ra.course.restaurant.persistence.menu;

import com.ra.course.restaurant.model.entity.menu.Menu;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

@Repository
public class MenuRepositoryImpl extends CRUDRepositoryImpl<Menu> implements MenuRepository {
}
