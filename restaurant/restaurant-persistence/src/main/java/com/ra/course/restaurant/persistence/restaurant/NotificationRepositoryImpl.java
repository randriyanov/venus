package com.ra.course.restaurant.persistence.restaurant;

import com.ra.course.restaurant.model.entity.notification.Notification;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class NotificationRepositoryImpl extends CRUDRepositoryImpl<Notification> implements NotificationRepository {
    @Override
    public List<Notification> getNotificationsByCustomerID(final int customer_id) {
        return repository.stream().filter(notification -> notification.getCustomerId() == customer_id).collect(Collectors.toList());
    }
}
