package com.ra.course.restaurant.persistence.order;

import com.ra.course.restaurant.model.entity.menu.MealItem;
import com.ra.course.restaurant.persistence.CRUDRepository;

import java.util.List;

public interface MealItemRepository extends CRUDRepository<MealItem> {
    List<MealItem> getMealItemsByMealId(int mealId);
}
