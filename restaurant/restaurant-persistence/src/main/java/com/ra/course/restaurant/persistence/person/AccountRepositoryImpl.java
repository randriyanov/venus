package com.ra.course.restaurant.persistence.person;

import com.ra.course.restaurant.model.entity.person.Account;
import com.ra.course.restaurant.persistence.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class AccountRepositoryImpl extends CRUDRepositoryImpl<Account> implements AccountRepository {
    @Override
    public Optional<Account> getAccountByEmployeeId(final int employeeId) {
        return repository.stream().filter(account->account.getEmployeeId() == employeeId).findAny();
    }

}
