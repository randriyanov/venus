package com.ra.course.restaurant.persistence.menu.exceptions;

import java.util.UUID;

public class NonPositivePriceException extends RuntimeException {
    private static final UUID serialVersionUID = UUID.randomUUID();
    public NonPositivePriceException(final String message) {
        super(message);
    }
}