package com.ra.course.restaurant.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SuppressWarnings("PMD")
@SpringBootApplication(scanBasePackages = "com.ra.course.restaurant")
public class RestaurantApplication {
    public static void main(final String[] args) {
        SpringApplication.run(RestaurantApplication.class, args);
    }
}
