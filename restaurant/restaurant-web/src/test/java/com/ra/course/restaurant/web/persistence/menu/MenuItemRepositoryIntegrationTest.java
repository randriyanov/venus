package com.ra.course.restaurant.web.persistence.menu;

import com.ra.course.restaurant.model.entity.menu.MenuItem;
import com.ra.course.restaurant.persistence.menu.MenuItemRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@SpringBootTest
class MenuItemRepositoryIntegrationTest extends TestDataUtil {

    @Autowired
    private MenuItemRepository itemRepository;

    private static final int MENU_SECTION_ID = 1;
    private static final int MENU_SECTION_ANOTHER_ID = 2;
    private static final BigDecimal DEFAULT_PRICE = new BigDecimal(100);
    private final List<MenuItem> menuItems = List.of(getMenuItem(MENU_SECTION_ID, DEFAULT_PRICE), getMenuItem(MENU_SECTION_ID, DEFAULT_PRICE), getMenuItem(MENU_SECTION_ANOTHER_ID, DEFAULT_PRICE));

    @BeforeEach
    void before() {
        menuItems.forEach(itemRepository::add);
        followTestData(itemRepository, List.copyOf(menuItems));
    }

    @AfterEach
    void after() {
        removeTestData();
    }

    @Test
    @DisplayName("When getting list of items, which are in the repository, return these items")
    void getItemsBySectionIDWhenItemsAreInRepo() {
        Assertions.assertEquals(itemRepository.getItemsBySectionID(1), List.of(menuItems.get(0), menuItems.get(1)));
    }

    @Test
    @DisplayName("When getting list of items, which are not in the repository, return empty list")
    void getItemsBySectionIDWhenItemsAreNotInRepo() {
        Assertions.assertEquals(itemRepository.getItemsBySectionID(5), Collections.emptyList());
    }

    private MenuItem getMenuItem(int sectionID, BigDecimal price) {
        return new MenuItem("title1", "description1", price, sectionID);
    }
}