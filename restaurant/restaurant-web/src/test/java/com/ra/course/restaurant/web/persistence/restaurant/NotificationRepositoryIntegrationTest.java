package com.ra.course.restaurant.web.persistence.restaurant;

import com.ra.course.restaurant.model.entity.notification.EmailNotification;
import com.ra.course.restaurant.model.entity.notification.Notification;
import com.ra.course.restaurant.persistence.restaurant.NotificationRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class NotificationRepositoryIntegrationTest extends TestDataUtil {

    private static final int EXISTS_CUSTOMER_ID = 1;
    public static final int ANOTHER_CUSTOMER_ID = 2;
    private final LocalDateTime nowTime = LocalDateTime.now();
    private List<Notification> notifications;

    @Autowired
    private NotificationRepository notificationRepository;

    @BeforeEach
    void before() {
        notifications = List.of(getNotification(nowTime));
        notifications.forEach(notificationRepository::add);
        followTestData(notificationRepository, List.copyOf(notifications));
    }

    @AfterEach
    void after() {
        removeTestData();
    }

    @Test
    @DisplayName("If notifications exist then return list of notifications")
    void getNotificationsByCustomerID() {
        //then
        assertTrue(notificationRepository.getNotificationsByCustomerID(EXISTS_CUSTOMER_ID).size() > 0);
    }

    @Test
    @DisplayName("If notifications not exist then return empty list")
    void getNotificationsByCustomerIDWhenNotificationsNotExist() {
        //then
        assertEquals(Collections.emptyList(), notificationRepository.getNotificationsByCustomerID(ANOTHER_CUSTOMER_ID));
    }

    private Notification getNotification(LocalDateTime date) {
        return new EmailNotification(date, "Some message", "mail.ru", EXISTS_CUSTOMER_ID);
    }
}