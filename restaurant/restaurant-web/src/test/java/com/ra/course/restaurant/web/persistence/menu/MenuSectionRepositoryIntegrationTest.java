package com.ra.course.restaurant.web.persistence.menu;

import com.ra.course.restaurant.model.entity.menu.MenuSection;
import com.ra.course.restaurant.persistence.menu.MenuSectionRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class MenuSectionRepositoryIntegrationTest extends TestDataUtil {

    @Autowired
    private MenuSectionRepository menuSectionRepository;
    private static final int DEFAULT_MENU_ID = 1;
    private final List<MenuSection> menuSections = List.of(getSection(DEFAULT_MENU_ID), getSection(DEFAULT_MENU_ID), getSection(DEFAULT_MENU_ID));

    @BeforeEach
    void before() {
        menuSections.forEach(menuSectionRepository::add);
        followTestData(menuSectionRepository, List.copyOf(menuSections));
    }

    @AfterEach
    void after() {
        removeTestData();
    }

    @Test
    @DisplayName("When getting a section, which is in the menu, the section is provided")
    void getSectionsByMenuID() {
        assertEquals(menuSectionRepository.getSectionsByMenuID(DEFAULT_MENU_ID), menuSections);
    }

    private MenuSection getSection(int menuId) {
        return new MenuSection(menuId, "title", "desc");
    }
}