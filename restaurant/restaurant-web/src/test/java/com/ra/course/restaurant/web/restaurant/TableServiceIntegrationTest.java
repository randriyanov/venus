package com.ra.course.restaurant.web.restaurant;

import com.ra.course.restaurant.model.entity.basic.Address;
import com.ra.course.restaurant.model.entity.enumerations.SeatType;
import com.ra.course.restaurant.model.entity.enumerations.TableStatus;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Table;
import com.ra.course.restaurant.model.entity.restaurant.TableChart;
import com.ra.course.restaurant.model.entity.restaurant.TableSeat;
import com.ra.course.restaurant.persistence.restaurant.BranchRepository;
import com.ra.course.restaurant.persistence.restaurant.TableChartRepository;
import com.ra.course.restaurant.persistence.restaurant.TableRepository;
import com.ra.course.restaurant.persistence.restaurant.TableSeatRepository;
import com.ra.course.restaurant.service.exception.TableException;
import com.ra.course.restaurant.service.restaurant.BranchService;
import com.ra.course.restaurant.service.restaurant.TableService;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class TableServiceIntegrationTest extends TestDataUtil {

    public static final int TABLE_CAPACITY = 3;
    public static final int MAX_CAPACITY = 3;
    public static final int LOCATION_IDENTIFIER = 1;
    public static final int TABLE_ID = 1;
    public static final int TABLE_QUANTITY = 3;
    public static final AtomicInteger EMPTY_TABLE_QUANTITY = new AtomicInteger(0);
    public static final int EMPTY_TABLE_CAPACITY = 0;
    public static final int NON_EXISTENT_TABLE_ID = -1;

    @Autowired
    private TableService tableService;
    @Autowired
    private BranchService branchService;
    @Autowired
    private BranchRepository branchRepository;
    @Autowired
    private TableRepository tableRepository;
    @Autowired
    private TableChartRepository tableChartRepository;
    @Autowired
    private TableSeatRepository tableSeatRepository;

    private TableChart tableChart;
    private Branch branch;
    private Table table;
    private List<TableSeat> tableSeats;

    @BeforeEach
    void setUp() {
        branch = getBranch("Branch");
        branchService.addBranch(branch);
        tableChart = getTableChart();
        tableChart.setTableQuantity(new AtomicInteger(TABLE_QUANTITY));
        branchService.addTableChart(tableChart);
        table = getTable();
        table.setTableChart(tableChart);
        tableSeats = List.of(getTableSeats(table.getId()), getTableSeats(table.getId()));
        tableSeats.forEach(tableSeatRepository::add);
        table.setTableSeats(tableSeats);
        tableRepository.add(table);
        // save test data to map
        followTestData(branchRepository, branch);
        followTestData(tableChartRepository, tableChart);
        followTestData(tableRepository, table);
        followTestData(tableSeatRepository, List.copyOf(tableSeats));
    }

    @AfterEach
    void tearDown() {
        removeTestData();
    }


    @Test
    @DisplayName("Add table when already filled")
    void addTableWhenAlreadyFilled() {
        //then
        assertThrows(TableException.class, () -> tableService.addTable(getTable(), tableChart.getId()));

    }

    @Test
    @DisplayName("If table already in chart then throw exception")
    void addTableWhenNotFilledYetButTableAlreadyInChart() {
        //given
        tableChart.setTableQuantity(EMPTY_TABLE_QUANTITY);
        //then
        assertThrows(TableException.class, () -> tableService.addTable(table, tableChart.getId()));
    }

    @Test
    @DisplayName("If table not in chart and we have enough space then add table")
    void addTableWhenNotFilledYet() {
        //given
        tableChart.setTableQuantity(EMPTY_TABLE_QUANTITY);
        table.setTableChart(null);
        followTestData(tableRepository, table);
        //then
        assertDoesNotThrow(() -> tableService.addTable(table, tableChart.getId()));
    }

    @Test
    @DisplayName("Update table if table in repo")
    void updateTable() {
        //then
        assertDoesNotThrow(() -> tableService.updateTable(table));
    }

    @Test
    @DisplayName("If table not exists then throw exception")
    void updateTableWhenNotExists() {
        //given
        tableRepository.delete(table);
        //then
        assertThrows(TableException.class, () -> tableService.updateTable(table));
    }

    @Test
    @DisplayName("If table not exists then throw exception")
    void removeTableWhenNotExists() {
        //given
        tableRepository.delete(table);
        //then
        assertThrows(TableException.class, () -> tableService.removeTable(table.getId()));
    }

    @Test
    @DisplayName("Remove table, standard behaviour")
    void removeTable() {
        //when
        tableService.removeTable(TABLE_ID);
        //then
        assertEquals(0, tableRepository.getAll().size());
    }

    @Test
    @DisplayName("Add tableSeat, standard behaviour")
    void addTableSeat() {
        //given
        TableSeat tableSeat = getTableSeats(table.getId());
        followTestData(tableSeatRepository, tableSeat);
        //when
        tableService.addTableSeat(tableSeat);
        //then
        assertEquals(3, tableSeatRepository.getAll().size());
    }

    @Test
    @DisplayName("Add tableSeat when table not exists throw exception")
    void addTableSeatWhenTableNotExists() {
        //given
        TableSeat tableSeat = getTableSeats(NON_EXISTENT_TABLE_ID);
        //then
        assertThrows(TableException.class, () -> tableService.addTableSeat(tableSeat));
    }

    @Test
    @DisplayName("When reached maximum capacity then throw exception")
    void addTableSeatWhenReachedMaxCapacity() {
        //given
        table.setMaxCapacity(EMPTY_TABLE_CAPACITY);
        TableSeat tableSeat = getTableSeats(table.getId());
        //then
        assertThrows(TableException.class, () -> tableService.addTableSeat(tableSeat));
    }

    @Test
    @DisplayName("If tableseat already exists then throw exception")
    void addTableSeatWhenAlreadyInBase() {
        //then
        assertThrows(TableException.class, () -> tableService.addTableSeat(tableSeats.get(0)));
    }

    @Test
    @DisplayName("If tableseat not in base then throw exception")
    void updateTableSeatWhenSeatNotExists() {
        //given
        TableSeat tableSeat = getTableSeats(table.getId());
        //then
        assertThrows(TableException.class, () -> tableService.updateTableSeat(tableSeat));
    }

    @Test
    @DisplayName("If tableseat in base then update record")
    void updateTableSeat() {
        //given
        TableSeat tableSeat = tableSeats.get(0);
        tableSeat.setType(SeatType.KID);
        //when
        tableService.updateTableSeat(tableSeat);
        //then
        assertEquals(SeatType.KID, tableSeatRepository.findById(tableSeat.getId()).get().getType());
    }

    private Table getTable() {
        Table table = new Table(TableStatus.FREE, MAX_CAPACITY, LOCATION_IDENTIFIER);
        table.setMaxCapacity(TableServiceIntegrationTest.TABLE_CAPACITY);
        return table;
    }

    private TableChart getTableChart() {
        return new TableChart("some/path", branch.getName(), TABLE_CAPACITY);
    }

    private Branch getBranch(String name) {
        Address location = new Address("Street", "Kharkov", "Babai", "61003", "Ukraine");
        return new Branch(name, location);
    }

    private TableSeat getTableSeats(int tabid) {
        return new TableSeat(SeatType.REGULAR, tabid);
    }
}
