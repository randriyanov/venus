package com.ra.course.restaurant.web.persistence.restaurant;

import com.ra.course.restaurant.model.entity.person.Customer;
import com.ra.course.restaurant.persistence.restaurant.CustomerRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class CustomerRepositoryIntegrationTest extends TestDataUtil {

    @Autowired
    private CustomerRepository customerRepository;
    private static final String NON_EXISTENT_PHONE = "+380999999999";
    private Customer customer;

    @BeforeEach
    void before() {
        customer = new Customer("Egor", "+380506666666");
        customerRepository.add(customer);
        followTestData(customerRepository, customer);
    }

    @AfterEach
    void after() {
        removeTestData();
    }

    @Test
    @DisplayName("when getting a customer from the repository by phone, the customer is provided")
    void getCustomerByPhone() {
        //given
        String customerPhone = customer.getPhone();
        //then
        assertEquals(customerRepository.getCustomerByPhone(customerPhone), Optional.of(customer));
    }

    @Test
    @DisplayName("when getting a customer, which is not in the repository, by phone, an empty object is provided")
    void getCustomerByPhoneWhenCustomerNotInRepo() {
        //then
        assertEquals(customerRepository.getCustomerByPhone(NON_EXISTENT_PHONE), Optional.empty());
    }
}