package com.ra.course.restaurant.web.persistence.order;

import com.ra.course.restaurant.model.entity.menu.MealItem;
import com.ra.course.restaurant.model.entity.menu.MenuItem;
import com.ra.course.restaurant.persistence.order.MealItemRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class MealItemRepositoryIntegrationTest extends TestDataUtil {

    private static final int DEFAULT_MEAL_ID = 1;
    private static final int DEFAULT_MEAL_SECOND_ID = 2;
    private static final int DEFAULT_MEAL_ITEM_QUANTITY = 1;
    private static final BigDecimal DEFAULT_PRICE = new BigDecimal(50);

    private static final String DEFAULT_TITLE = "title";
    private static final String DEFAULT_DESCRIPTION = "description";

    private final List<MealItem> mealItems = List.of(getItem(), getItem(), getItem());

    @Autowired
    private MealItemRepository mealItemRepository;

    @BeforeEach
    void before() {
        mealItems.forEach(mealItemRepository::add);
        followTestData(mealItemRepository, List.copyOf(mealItems));
    }

    @AfterEach
    void after() {
        removeTestData();
    }

    @Test
    @DisplayName("When getting meal items by menu item ID, all meal items provided")
    void getMealItemsByMealId() {
        assertEquals(mealItemRepository.getMealItemsByMealId(DEFAULT_MEAL_ID), mealItems);
    }

    @Test
    @DisplayName("When getting a list of meal items, which is not in the repo, get empty list")
    void getMealItemsByMealIdWhenItemsNotExist() {
        assertEquals(mealItemRepository.getMealItemsByMealId(DEFAULT_MEAL_SECOND_ID), Collections.emptyList());
    }

    private MealItem getItem() {
        MenuItem menuItem = new MenuItem(DEFAULT_TITLE, DEFAULT_DESCRIPTION, DEFAULT_PRICE, DEFAULT_MEAL_ID);
        return new MealItem(DEFAULT_MEAL_ID, DEFAULT_MEAL_ITEM_QUANTITY, menuItem);
    }
}