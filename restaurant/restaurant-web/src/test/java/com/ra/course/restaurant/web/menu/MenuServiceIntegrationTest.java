package com.ra.course.restaurant.web.menu;

import com.ra.course.restaurant.model.entity.basic.Address;
import com.ra.course.restaurant.model.entity.menu.Menu;
import com.ra.course.restaurant.model.entity.menu.MenuItem;
import com.ra.course.restaurant.model.entity.menu.MenuSection;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.persistence.menu.exceptions.NonPositivePriceException;
import com.ra.course.restaurant.service.exception.MenuException;
import com.ra.course.restaurant.service.menu.MenuService;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;


@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MenuServiceIntegrationTest extends TestDataUtil {

    private static final int DEFAULT_ENTITY_ID = 1;
    private static final BigDecimal DEFAULT_PRICE_VALUE = new BigDecimal(100);
    private static final int DEFAULT_ENTITY_SECOND_ID = 2;
    private static final int UNIDENTIFIED_ENTITY_ID = Integer.MAX_VALUE;
    private static final BigDecimal NEGATIVE_PRICE = new BigDecimal(-3);
    private static final String STREET = "street";
    private static final String CITY = "city";
    private static final String STATE = "state";
    private static final String ZIP_CODE = "16565";
    private static final String COUNTRY = "country";
    private static final String BRANCH_NAME = "branch name";
    private static final String MENU_NAME = "Our menu";
    private static final String DESCRIPTION = "description";
    private static final String MENU_SECTION_TITLE = "1st menu section";
    private static final String MENU_SECTION_SECOND_TITLE = "2nd menu section";
    private static final String MENU_ITEM_TITLE = "I am a menu item";
    private static final String MENU_ITEM_SECOND_TITLE = "I am another menu item";

    @Autowired
    private MenuService menuService;

    @BeforeEach
    void before () {
        Address address = new Address(STREET, CITY, STATE, ZIP_CODE, COUNTRY);
        Branch branch = new Branch(BRANCH_NAME,address);
        Menu menu = new Menu(MENU_NAME, DESCRIPTION,branch);
        menuService.addMenu(menu);
        followTestData(menuService, menu);
        MenuSection menuSection = new MenuSection(menu.getId(), MENU_SECTION_TITLE, DESCRIPTION);
        menuService.addMenuSectionToMenu(menuSection);
        MenuItem menuItem = new MenuItem(MENU_ITEM_TITLE, DESCRIPTION,DEFAULT_PRICE_VALUE,menuSection.getId());
        menuService.addMenuItemToSection(menuItem);
    }

    @AfterEach
    void after () {
        removeTestData();
    }

    @Test
    @DisplayName("When price is negative then throw exception")
    void updatePriceWhenPriceIsNegative() {
        //Given negative price
        assertThrows(NonPositivePriceException.class, ()->menuService.updatePrice(DEFAULT_ENTITY_ID, NEGATIVE_PRICE));
    }

    @Test
    @DisplayName("When price is positive but item not exists, throw exception")
    void updatePriceWhenPriceIsPositiveButItemNotExists() {
        //Given positive price
        assertThrows(MenuException.class, ()->menuService.updatePrice(UNIDENTIFIED_ENTITY_ID, DEFAULT_PRICE_VALUE));
    }

    @Test
    @DisplayName("When price is positive and item exists then return true")
    void updatePriceWhenPriceIsPositiveAndItemExists() {
        //Given positive price and item
        BigDecimal newPrice = DEFAULT_PRICE_VALUE;
        assertTrue(menuService.updatePrice(DEFAULT_ENTITY_ID, newPrice));
        assertEquals(menuService.getMenuItemRep().findById(DEFAULT_ENTITY_ID).get().getPrice(), newPrice);
    }

    @Test
    @DisplayName("When item already in section then throw exception")
    void addMenuItemToSectionWhenItemAlreadyThere() {
        MenuItem menuItem = menuService.getMenuItemRep().findById(DEFAULT_ENTITY_ID).get();
        assertThrows(MenuException.class, ()->menuService.addMenuItemToSection(menuItem));
    }

    @Test
    @DisplayName("When section not exists then throw exception")
    void addMenuItemToSectionWhenSectionNotExists() {
        MenuItem menuItem = menuService.getMenuItemRep().findById(DEFAULT_ENTITY_ID).get();
        assertThrows(MenuException.class, ()->menuService.addMenuItemToSection(menuItem));
    }

    @Test
    @DisplayName("When section exists and item not in section then return true")
    void addMenuItemToSectionStandardBehaviour() {
        MenuItem menuItem = new MenuItem(MENU_ITEM_SECOND_TITLE,DESCRIPTION,new BigDecimal(12),DEFAULT_ENTITY_ID);
        assertTrue(menuService.addMenuItemToSection(menuItem));
    }

    @Test
    @DisplayName("When adding a new section to a menu, standard behavior expected")
    void addMenuSectionToMenuWhenSectionNotYetInRepo() {
        MenuSection menuSection = new MenuSection(DEFAULT_ENTITY_ID,MENU_SECTION_SECOND_TITLE,DESCRIPTION);
        assertDoesNotThrow(()->menuService.addMenuSectionToMenu(menuSection));
    }

    @Test
    @DisplayName("When adding a section to a menu, when it is already in the menu, throw an exception")
    void addMenuSectionToMenuWhenSectionAlreadyExists() {
        MenuSection menuSection = getDefaultMenuSection();
        assertThrows(MenuException.class, ()->menuService.addMenuSectionToMenu(menuSection));
    }

    @Test
    @DisplayName("When adding a section to a menu, which is not exist, throw an exception")
    void addMenuSectionToMenuWhenMenuNotExists() {
        MenuSection menuSection = new MenuSection(DEFAULT_ENTITY_SECOND_ID,MENU_SECTION_TITLE,DESCRIPTION);
        assertThrows(MenuException.class, ()->menuService.addMenuSectionToMenu(menuSection));
    }

    @Test
    @DisplayName("If menu is in the repo, do not throw an exception")
    void updateMenu() {
        Menu menu = getDefaultMenu();
        assertDoesNotThrow(()->menuService.updateMenu(menu));
    }

    @Test
    @DisplayName("If menu is not in the repo, then throw an exception")
    void updateMenuWhenMenuNotExists() {
        Menu menu = new Menu(MENU_NAME, DESCRIPTION, any(Branch.class));
        assertThrows(MenuException.class, ()->menuService.updateMenu(menu));
    }

    @Test
    @DisplayName("When a section is in the repo, no exception expected")
    void updateMenuSection() {
        MenuSection menuSection = getDefaultMenuSection();
        assertDoesNotThrow(()->menuService.updateMenuSection(menuSection));
    }

    @Test
    @DisplayName("When section is not in the repo, throw an exception")
    void updateMenuSectionWhenSectionNotExists() {
        MenuSection menuSection = new MenuSection(DEFAULT_ENTITY_ID, MENU_SECTION_SECOND_TITLE, DESCRIPTION);
        assertThrows(MenuException.class, ()->menuService.updateMenuSection(menuSection));
    }

    @Test
    @DisplayName("When menu item is in the repo, no exception thrown")
    void updateMenuItem() {
        MenuItem menuItem = getDefaultMenuItem();
        assertDoesNotThrow(()->menuService.updateMenuItem(menuItem));
    }

    @Test
    @DisplayName("When menu item is not in the repo, throw an exception")
    void updateMenuItemWhenItemNotExists() {
        MenuItem menuItem = new MenuItem(MENU_ITEM_SECOND_TITLE,DESCRIPTION, DEFAULT_PRICE_VALUE, DEFAULT_ENTITY_ID);
        assertThrows(MenuException.class, ()->menuService.updateMenuItem(menuItem));
    }

    @Test
    @DisplayName("If menu is not in the repo, then it is successfully added")
    void addNewMenuInRepo() {
        Menu menu = new Menu(MENU_NAME,DESCRIPTION,new Branch(BRANCH_NAME,new Address(STREET,CITY,STATE,ZIP_CODE,COUNTRY)));
        menuService.addMenu(menu);
        assertTrue(menuService.getMenuRep().getAll().contains(menu));
    }

    @Test
    @DisplayName("If menu is already in the repo, then throw an exception")
    void addMenuWhenAlreadyExists() {
        Menu menu = getDefaultMenu();
        assertThrows(MenuException.class, ()->menuService.addMenu(menu));
    }

    @Test
    @DisplayName("When menu item is in the repo, no exception thrown")
    void removeMenuItem() {
        MenuItem menuItem = getDefaultMenuItem();
        assertDoesNotThrow(()->menuService.removeMenuItem(DEFAULT_ENTITY_ID));
    }

    @Test
    @DisplayName("When menu item is not in the repo, throw an exception")
    void removeMenuItemWhenItemNotExists() {
        assertThrows(MenuException.class, ()->menuService.removeMenuItem(DEFAULT_ENTITY_SECOND_ID));
    }

    @Test
    @DisplayName("When menu section is deleted, then menu items are also deleted")
    void removeSection() {
        menuService.removeSection(DEFAULT_ENTITY_ID);
        assertEquals(0,menuService.getMenuItemRep().getAll().size());
    }


    private MenuSection getDefaultMenuSection () {
        return menuService.getMenuSectionRep().findById(DEFAULT_ENTITY_ID).get();
    }

    private Menu getDefaultMenu () {
        return menuService.getMenuRep().findById(DEFAULT_ENTITY_ID).get();
    }

    private MenuItem getDefaultMenuItem () {
        return menuService.getMenuItemRep().findById(DEFAULT_ENTITY_ID).get();
    }
}
