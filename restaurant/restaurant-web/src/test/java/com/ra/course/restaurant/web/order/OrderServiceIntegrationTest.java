package com.ra.course.restaurant.web.order;

import com.ra.course.restaurant.model.entity.basic.Address;
import com.ra.course.restaurant.model.entity.enumerations.OrderStatus;
import com.ra.course.restaurant.model.entity.order.Order;
import com.ra.course.restaurant.model.entity.person.Chef;
import com.ra.course.restaurant.model.entity.person.Employee;
import com.ra.course.restaurant.model.entity.person.Manager;
import com.ra.course.restaurant.model.entity.person.Waiter;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.persistence.order.OrderRepository;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;
import com.ra.course.restaurant.service.exception.OrderException;
import com.ra.course.restaurant.service.order.OrderService;
import com.ra.course.restaurant.service.person.EmployeeService;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class OrderServiceIntegrationTest extends TestDataUtil {


    public static final String DEFAULT_NAME = "name";
    public static final String DEFAULT_EMAIL = "email";
    public static final String DEFAULT_PHONE = "12345";
    public static final LocalDate DEFAULT_DATE = LocalDate.now();
    private static final int DEFAULT_ENTITY_ID = 1;
    private static final String STREET = "street";
    private static final String CITY = "city";
    private static final String STATE = "state";
    private static final String ZIP_CODE = "16565";
    private static final String COUNTRY = "country";
    private static final String BRANCH_NAME = "branch name";


    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmployeeRepository employeeRepository;
    private Order order;
    private Branch branch;
    private Manager manager;
    private Chef chef;
    private Waiter waiter;

    @BeforeEach
    void before() {
        Address address = new Address(STREET, CITY, STATE, ZIP_CODE, COUNTRY);
        branch = new Branch(BRANCH_NAME, address);
        chef = getChef();
        waiter = getWaiter();
        manager = getManager();
        employeeService.addEmployee(chef);
        employeeService.addEmployee(waiter);
        employeeService.addEmployee(manager);
        followTestData(employeeRepository, List.of(chef, waiter, manager));
        order = getDefaultOrder();
        orderRepository.add(order);
        followTestData(orderRepository, order);
    }

    @AfterEach
    void after() {
        removeTestData();
    }

    @Test
    @DisplayName("When a chef is assigned to an order, standard behavior")
    void takeOrder() {
        orderService.takeOrder(chef.getId(), order);
        //then
        assertTrue(order.getStaff().contains(chef));
    }

    @Test
    @DisplayName("When taking an order with an inappropriate status, throw an exception")
    void takeOrderWhenOrderStatusIsNotUnsuitable() {
        order.setOrderStatus(OrderStatus.COMPLETE);
        //then
        assertThrows(OrderException.class, () -> orderService.takeOrder(DEFAULT_ENTITY_ID, order));
    }

    @Test
    @DisplayName("When taking an order by an employee, who is not a chef, throw an exception")
    void takeOrderWhenEmployeeNotChef() {
        assertThrows(OrderException.class, () -> orderService.takeOrder(waiter.getId(), order));
    }

    @Test
    @DisplayName("When taking an order by an employee, which is not in the repo, throw an exception")
    void takeOrderWhenEmployeeNotExists() {
        Employee employee = new Manager(DEFAULT_NAME, DEFAULT_EMAIL, DEFAULT_PHONE, DEFAULT_DATE, null);
        assertThrows(OrderException.class, () -> orderService.takeOrder(employee.getId(), order));
    }

    @Test
    @DisplayName("When a manager creates an order, they are added to the employee staff")
    void createOrder() {
        orderService.createOrder(manager.getId(), order);
        assertTrue(order.getStaff().contains(manager));
    }

    @Test
    @DisplayName("When a waiter creates an order, they are added to the employee staff")
    void createOrderWhenEmployeeIsWaiter() {
        orderService.createOrder(waiter.getId(), order);
        assertTrue(order.getStaff().contains(waiter));
    }

    @Test
    @DisplayName("When an employee, who is not a waiter or a manager creates an order, throw an exception")
    void createOrderWhenEmployeeHasNotRights() {
        assertThrows(OrderException.class, () -> orderService.createOrder(chef.getId(), order));
    }

    @Test
    @DisplayName("When changing a status of an order, it is being modified")
    void setOrderStatus() {
        order.setOrderStatus(OrderStatus.COMPLETE);
        assertEquals(order.getOrderStatus(), OrderStatus.COMPLETE);
    }

    @Test
    @DisplayName("When changing a status of an order, which is not in the repo, throw an exception")
    void setOrderStatusWhenOrderNotExists() {
        Order order = new Order(OrderStatus.NONE,DEFAULT_ENTITY_ID);
        assertThrows(OrderException.class, ()->orderService.setOrderStatus(order.getId(), OrderStatus.COMPLETE));
    }

    @Test
    @DisplayName("When updating an order, which is in the repo, standard behavior")
    void updateOrderWhenOrderExists(){
        List<Employee> employees = List.of(getWaiter(),getChef(),getManager());
        order.setStaff(employees);
        orderService.getOrderRepository().update(order);
        assertEquals(order.getStaff(), employees);
    }

    @Test
    @DisplayName("When updating an order, which is not in the repo, throws and exception")
    void updateOrderWhenOrderNotExists(){
        Order order = new Order(OrderStatus.NONE,DEFAULT_ENTITY_ID);
        assertThrows(OrderException.class, ()->orderService.updateOrder(order));
    }


    private Order getDefaultOrder() {
        return new Order(OrderStatus.NONE, DEFAULT_ENTITY_ID);
    }

    private Chef getChef() {
        return new Chef(DEFAULT_NAME, DEFAULT_EMAIL, DEFAULT_PHONE, DEFAULT_DATE, branch);
    }

    private Waiter getWaiter() {
        return new Waiter(DEFAULT_NAME, DEFAULT_EMAIL, DEFAULT_PHONE, DEFAULT_DATE, branch);
    }

    private Manager getManager() {
        return new Manager(DEFAULT_NAME, DEFAULT_EMAIL, DEFAULT_PHONE, DEFAULT_DATE, branch);
    }
}
