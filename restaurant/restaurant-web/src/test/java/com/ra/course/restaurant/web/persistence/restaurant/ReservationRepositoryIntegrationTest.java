package com.ra.course.restaurant.web.persistence.restaurant;

import com.ra.course.restaurant.model.entity.enumerations.ReservationStatus;
import com.ra.course.restaurant.model.entity.person.Customer;
import com.ra.course.restaurant.model.entity.restaurant.Reservation;
import com.ra.course.restaurant.persistence.restaurant.ReservationRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ReservationRepositoryIntegrationTest extends TestDataUtil {

    @Autowired
    private ReservationRepository reservationRepository;
    private List<Reservation> reservations;
    private final LocalDateTime reservTime = LocalDateTime.now();
    public static final int PEOPLE_COUNT = 4;
    private static final int EXISTS_CUSTOMER_ID = 1;
    private static final int NOT_EXISTS_CUSTOMER_ID = 2;
    private static final int PLUS_MINUS_HOURS_VALUE = 1;
    private static final int PLUS_MINUTES_VALUE = 1;
    private static final int PLUS_MINUTES_VALUE_FOR_GETTING_EMPTY = 2;
    private static final int NOTIFIER_DEFAULT_INTERVAL = 1;
    private static final int NOTIFIER_INTERVAL_20 = 20;

    @BeforeEach
    void before() {
        reservations = List.of(getReservation(reservTime), getReservation(reservTime.plusHours(PLUS_MINUS_HOURS_VALUE).plusMinutes(PLUS_MINUTES_VALUE)));
        reservations.forEach(reservationRepository::add);
        followTestData(reservationRepository, List.copyOf(reservations));
    }

    @AfterEach
    void after() {
        removeTestData();
    }

    @Test
    @DisplayName("when getting a reservation from the repository by customer ID, the reservation is provided")
    void getReservationsByCustomerID() {
        //then
        assertEquals(reservationRepository.getReservationsByCustomerID(EXISTS_CUSTOMER_ID), reservations);
    }

    @Test
    @DisplayName("when getting a reservation, which is not in the repository, by customer ID, an empty collection is provided")
    void getReservationsByCustomerIDWhenReservationsNotExist() {
        //given
        int customerId = 2;
        //then
        assertEquals(reservationRepository.getReservationsByCustomerID(NOT_EXISTS_CUSTOMER_ID), Collections.emptyList());
    }

    @Test
    @DisplayName("when getting a reservation from the repository by time, the reservation is provided")
    void getReservationByTime() {
        //then
        assertEquals(reservationRepository.getReservationByTime(reservTime), Optional.of(reservations.get(0)));
    }

    @Test
    @DisplayName("when getting a reservation, which is not in the repository, by time, an empty object is provided")
    void getReservationByTimeWhenSuchReservationNotExists() {
        //then
        assertEquals(reservationRepository.getReservationByTime(reservTime.minusHours(1)), Optional.empty());
    }

    @Test
    @DisplayName("when getting a reservation from the repository by time to notify, the list of reservations is provided")
    void getReservationsToNotify() {
        //given
        reservations.get(1).setTimeOfReservation(reservTime.plusHours(PLUS_MINUS_HOURS_VALUE).plusMinutes(PLUS_MINUTES_VALUE));
        assertEquals(reservationRepository.getReservationsToNotify(NOTIFIER_INTERVAL_20), List.of(reservations.get(1)));
    }

    @Test
    @DisplayName("when there are no reservations by this time, an empty list is provided")
    void getReservationsToNotifyWhenNothingToNotify() {
        //given
        reservations.forEach(reservationRepository::delete);
        assertEquals(reservationRepository.getReservationsToNotify(NOTIFIER_DEFAULT_INTERVAL), Collections.emptyList());
    }

    @Test
    @DisplayName("When there are no reservations to notify, returns empty list")
    void getReservationsToNotifyWhenTimeNotEqualsByOneInterval() {
        //given
        reservations.get(1).setTimeOfReservation(reservTime.plusHours(PLUS_MINUS_HOURS_VALUE).plusMinutes(PLUS_MINUTES_VALUE_FOR_GETTING_EMPTY));
        assertEquals(reservationRepository.getReservationsToNotify(NOTIFIER_DEFAULT_INTERVAL), Collections.emptyList());
    }

    @Test
    @DisplayName("When getting all reservations from the current moment of time, a list of all future reservations is provided")
    void getFutureReservations() {
        assertEquals(reservationRepository.getFutureReservations(), List.of(reservations.get(1)));
    }

    @Test
    @DisplayName("When getting all reservations from the current moment of time, and there are no such reservations, an empty list is provided")
    void getFutureReservationsWhenFutureReservationsNotExist() {
        reservations.get(1).setTimeOfReservation(LocalDateTime.now().minusHours(PLUS_MINUS_HOURS_VALUE));
        assertEquals(reservationRepository.getFutureReservations(), Collections.emptyList());
    }

    private Reservation getReservation(LocalDateTime date) {
        return new Reservation(date, PEOPLE_COUNT, ReservationStatus.PENDING, "notes", LocalDateTime.now(), getCustomer());
    }

    private Customer getCustomer() {
        Customer customer = new Customer("Petya", "+380905555555");
        customer.setId(EXISTS_CUSTOMER_ID);
        return customer;
    }
}