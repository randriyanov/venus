package com.ra.course.restaurant.web.persistence.restaurant;

import com.ra.course.restaurant.model.entity.enumerations.SeatType;
import com.ra.course.restaurant.model.entity.restaurant.TableSeat;
import com.ra.course.restaurant.persistence.restaurant.TableSeatRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class TableSeatRepositoryIntegrationTest extends TestDataUtil {

    @Autowired
    private TableSeatRepository tableSeatRepository;
    private List<TableSeat> tableSeats;
    private final static int DEFAULT_TABLE_ID = 1;
    private final static int TABLE_SEAT_ID_ONE = 1;
    private final static int TABLE_SEAT_ID_TWO = 1;
    private final static int TABLE_SEAT_NON_EXISTENT_ID = -1;

    @BeforeEach
    void before() {
        tableSeats = List.of(getTableSeat(TABLE_SEAT_ID_ONE), getTableSeat(TABLE_SEAT_ID_TWO));
        tableSeats.forEach(tableSeatRepository::add);
        followTestData(tableSeatRepository, List.copyOf(tableSeats));
    }

    @AfterEach
    void after() {
        removeTestData();
    }

    @Test
    @DisplayName("When getting a list of table seats of a table, which is in the repository, the list of table seats is provided")
    void getTableSeatsByTableID() {
        //then
        assertEquals(tableSeatRepository.getTableSeatsByTableID(TABLE_SEAT_ID_ONE), tableSeats);
    }

    @Test
    @DisplayName("When getting a list of table seats of a table, which is not in the repository, an empty list is provided")
    void getTableSeatsByTableIDWhenTableSeatsNotExist() {
        //then
        assertEquals(tableSeatRepository.getTableSeatsByTableID(TABLE_SEAT_NON_EXISTENT_ID), Collections.emptyList());
    }

    private TableSeat getTableSeat(int id) {
        TableSeat tableSeat = new TableSeat(SeatType.REGULAR, DEFAULT_TABLE_ID);
        tableSeat.setId(id);
        return tableSeat;
    }
}