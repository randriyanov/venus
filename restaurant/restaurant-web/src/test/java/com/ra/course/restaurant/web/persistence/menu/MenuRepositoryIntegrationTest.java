package com.ra.course.restaurant.web.persistence.menu;

import com.ra.course.restaurant.persistence.menu.MenuRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MenuRepositoryIntegrationTest extends TestDataUtil {

    @Autowired
    private MenuRepository menuRepository;

    @Test
    @DisplayName("check if repository exists")
    void checkIfInstanceOfCRUDRepo() {
        Assertions.assertNotNull(menuRepository);
    }
}
