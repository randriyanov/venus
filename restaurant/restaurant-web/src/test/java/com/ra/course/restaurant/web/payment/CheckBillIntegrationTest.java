package com.ra.course.restaurant.web.payment;

import com.ra.course.restaurant.model.entity.basic.Address;
import com.ra.course.restaurant.model.entity.enumerations.OrderStatus;
import com.ra.course.restaurant.model.entity.enumerations.PaymentType;
import com.ra.course.restaurant.model.entity.menu.*;
import com.ra.course.restaurant.model.entity.order.Order;
import com.ra.course.restaurant.model.entity.payment.CheckBill;
import com.ra.course.restaurant.model.entity.person.Chef;
import com.ra.course.restaurant.model.entity.person.Employee;
import com.ra.course.restaurant.model.entity.person.Manager;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;
import com.ra.course.restaurant.service.exception.BranchException;
import com.ra.course.restaurant.service.exception.CheckBillException;
import com.ra.course.restaurant.service.menu.MenuService;
import com.ra.course.restaurant.service.order.MealService;
import com.ra.course.restaurant.service.order.OrderService;
import com.ra.course.restaurant.service.payment.CheckBillService;
import com.ra.course.restaurant.service.person.EmployeeService;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class CheckBillIntegrationTest extends TestDataUtil {

    public static final String DEFAULT_NAME = "name";
    public static final String DEFAULT_EMAIL = "email";
    public static final String DEFAULT_PHONE = "12345";
    public static final LocalDate DEFAULT_DATE = LocalDate.now();
    private static final int DEFAULT_ENTITY_ID = 1;
    private static final int DEFAULT_ENTITY_SECOND_ID = 2;
    private static final int DEFAULT_QUANTITY = 5;
    private static final BigDecimal DEFAULT_PRICE_VALUE = new BigDecimal(100);
    private static final String DESCRIPTION = "description";
    private static final String MENU_ITEM_TITLE = "I am a menu item";
    private static final String STREET = "street";
    private static final String CITY = "city";
    private static final String STATE = "state";
    private static final String ZIP_CODE = "16565";
    private static final String COUNTRY = "country";
    private static final String BRANCH_NAME = "branch name";
    private static final String MENU_SECTION_TITLE = "1st menu section";
    private static final String MENU_NAME = "Our menu";

    @Autowired
    private CheckBillService checkBillService;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private OrderService orderService;
    @Autowired
    private MenuService menuService;
    @Autowired
    private MealService mealService;

    private Branch branch;
    private Employee manager;


    @BeforeEach
    void before() {
        Address address = new Address(STREET, CITY, STATE, ZIP_CODE, COUNTRY);
        branch = new Branch(BRANCH_NAME, address);
        CheckBill checkBill = new CheckBill(DEFAULT_ENTITY_ID, PaymentType.CASH);
        manager = getDefaultEmployee();
        employeeService.addEmployee(manager);
        checkBillService.createBill(manager.getId(), checkBill);
        Menu menu = new Menu(MENU_NAME, DESCRIPTION, branch);
        menuService.addMenu(menu);
        MenuSection menuSection = new MenuSection(menu.getId(), MENU_SECTION_TITLE, DESCRIPTION);
        menuService.addMenuSectionToMenu(menuSection);
        MenuItem menuItem = new MenuItem(MENU_ITEM_TITLE, DESCRIPTION, DEFAULT_PRICE_VALUE, menuSection.getId());
        menuService.addMenuItemToSection(menuItem);
        Order order = new Order(OrderStatus.NONE, DEFAULT_ENTITY_ID);
        orderService.getOrderRepository().add(order);
        Meal meal = new Meal(DEFAULT_ENTITY_ID, DEFAULT_ENTITY_ID);
        mealService.addMeal(meal);
        MealItem mealItem = new MealItem(DEFAULT_ENTITY_ID, DEFAULT_QUANTITY, menuItem);
        mealService.addMealItem(mealItem);
        meal.setMealItems(List.of(mealItem));
        order.setMeals(List.of(meal));

        followTestData(employeeRepository, manager);
        followTestData(menuService, menu);
        followTestData(mealService, meal);
        followTestData(orderService.getOrderRepository(), order);
        followTestData(checkBillService, checkBill);

    }

    @AfterEach
    void after() {
        removeTestData();
    }

    @Test
    @DisplayName("If employee does not exist, throw an exception")
    void createBillWhenEmployeeDoesNotExist() {
        assertThrows(CheckBillException.class, () -> checkBillService.createBill(DEFAULT_ENTITY_SECOND_ID, getDefaultCheckBill()));
    }

    @Test
    @DisplayName("If employee has no rights, throw an exception")
    void createBillWhenEmployeeHasNoRights() {
        assertThrows(CheckBillException.class, () -> checkBillService.createBill(getUnsuitableEmployee().getId(), getDefaultCheckBill()));
    }

    @Test
    @DisplayName("If bill is already in the repo, throw an exception")
    void createBillWhenItAlreadyExists() {
        assertThrows(CheckBillException.class, () -> checkBillService.createBill(manager.getId(), getDefaultCheckBill()));
    }

    @Test
    @DisplayName("When creating a bill, which is not in the repo, and employee has rights, then create a bill")
    void createBillWhenItDoesNotExistAndEmployeeHasRights() {
        CheckBill checkBill = new CheckBill(DEFAULT_ENTITY_SECOND_ID, PaymentType.CHECK);
        checkBillService.createBill(manager.getId(), checkBill);
        Assertions.assertTrue(checkBillService.getCheckBillRep().getAll().contains(checkBill));
    }

    @Test
    @DisplayName("When an order is in the repo, calculate the corresponding bill")
    void calculateBillWhenOrderIsPresent() {
        CheckBill checkBill = getDefaultCheckBill();
        checkBillService.getCheckBillRep().delete(getDefaultCheckBill());
        checkBillService.createBill(manager.getId(), checkBill);
        Assertions.assertEquals(new BigDecimal(500), checkBill.getAmount());
    }

    @Test
    @DisplayName("If updating a checkbill that is not in the repository, throw an exception")
    void updateCheckBillWhenItIsNotInRepository() {
        CheckBill checkBill = new CheckBill(DEFAULT_ENTITY_SECOND_ID, PaymentType.CHECK);
        assertThrows(BranchException.class, () -> checkBillService.updateCheckBill(checkBill));
    }

    @Test
    @DisplayName("If updating a checkbill that is in the repository, then the bill is updated")
    void updateCheckBillWhenItIsInRepository() {
        CheckBill checkBill = getDefaultCheckBill();
        checkBill.setPaid(true);
        checkBillService.updateCheckBill(checkBill);
        assertTrue(getDefaultCheckBill().isPaid());
    }

    @Test
    @DisplayName("If printing a bill that is in the repository, a corresponding file is written")
    void printBillWhenItIsInRepository() throws IOException {
        CheckBill checkBill = getDefaultCheckBill();
        checkBillService.printBill(checkBill.getId());
        assertTrue((new File("./bill" + checkBill.getId() + ".txt")).exists());
    }


    private CheckBill getDefaultCheckBill() {
        return checkBillService.getCheckBillRep().findById(DEFAULT_ENTITY_ID).get();
    }

    private Employee getDefaultEmployee() {
        return new Manager(DEFAULT_NAME, DEFAULT_EMAIL, DEFAULT_PHONE, DEFAULT_DATE, branch);
    }

    private Employee getUnsuitableEmployee() {
        Address address = new Address(STREET, CITY, STATE, ZIP_CODE, COUNTRY);
        Branch branch = new Branch(BRANCH_NAME, address);
        return new Chef(DEFAULT_NAME, DEFAULT_EMAIL, DEFAULT_PHONE, DEFAULT_DATE, branch);
    }
}
