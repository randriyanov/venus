package com.ra.course.restaurant.web.persistence.person;

import com.ra.course.restaurant.model.entity.basic.Address;
import com.ra.course.restaurant.model.entity.enumerations.EmployeeRole;
import com.ra.course.restaurant.model.entity.person.Employee;
import com.ra.course.restaurant.model.entity.person.Manager;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class EmployeeRepositoryIntegrationTest extends TestDataUtil {

    @Autowired
    private EmployeeRepository employeeRepository;

    private static Address address = new Address("street", "city", "state", "zipCode", "country");
    private static final Branch branch = new Branch("name", address);
    private static final Map<String, String> DEFAULT_EMPLOYEE_PARAMS = Map.of("name", "Vasya", "email", "vasya@mail.ru", "phone", "+380506666666", "not_exists_phone", "+380999999999");
    private static final Employee employee = new Manager(DEFAULT_EMPLOYEE_PARAMS.get("name"), DEFAULT_EMPLOYEE_PARAMS.get("email"), DEFAULT_EMPLOYEE_PARAMS.get("phone"), LocalDate.now(), branch);

    @BeforeEach
    void before() {
        employeeRepository.add(employee);
        followTestData(employeeRepository, employee);
    }

    @AfterEach
    void after() {
        removeTestData();
    }

    @Test
    @DisplayName("when getting an employee from the repository by e-mail, the employee is provided")
    void getEmployeeByEmail() {
        //given
        String email = DEFAULT_EMPLOYEE_PARAMS.get("email");
        //then
        assertEquals(employeeRepository.getEmployeeByEmail(email), Optional.of(employee));
    }

    @Test
    @DisplayName("when getting an employee, which is not in the repository, by e-mail, an empty object is provided")
    void getEmployeeByEmailWhenSuchEmployeeNotExists() {
        //given
        String email = "different@mail.ru";
        //then
        assertEquals(employeeRepository.getEmployeeByEmail(email), Optional.empty());
    }

    @Test
    @DisplayName("when getting an employee from the repository by phone, the employee is provided")
    void getEmployeeByPhone() {
        //given
        String phone = DEFAULT_EMPLOYEE_PARAMS.get("phone");
        //then
        assertEquals(employeeRepository.getEmployeeByPhone(phone), Optional.of(employee));
    }

    @Test
    @DisplayName("when getting an employee, which is not in the repository, by phone, an empty object is provided")
    void getEmployeeByPhoneWhenSuchEmployeeNotExists() {
        //given
        String phone = DEFAULT_EMPLOYEE_PARAMS.get("not_exists_phone");
        //then
        assertEquals(employeeRepository.getEmployeeByPhone(phone), Optional.empty());
    }

    @Test
    @DisplayName("when getting an employee from the repository by role, the employee is provided")
    void getEmployeeByRole() {
        //given
        EmployeeRole role = EmployeeRole.MANAGER;
        //then
        assertEquals(employeeRepository.getEmployeeByRole(role), List.of(employee));
    }

    @Test
    @DisplayName("when getting an employee from the repository by role, which is not represented in this repository, an empty object is provided")
    void getEmployeeByRoleWhenNoSuchRoles() {
        //given
        EmployeeRole role = EmployeeRole.WAITER;
        //then
        assertEquals(employeeRepository.getEmployeeByRole(role), Collections.emptyList());
    }
}