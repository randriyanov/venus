package com.ra.course.restaurant.web.restaurant;

import com.ra.course.restaurant.model.entity.basic.Address;
import com.ra.course.restaurant.model.entity.enumerations.ReservationStatus;
import com.ra.course.restaurant.model.entity.enumerations.TableStatus;
import com.ra.course.restaurant.model.entity.person.Customer;
import com.ra.course.restaurant.model.entity.person.Employee;
import com.ra.course.restaurant.model.entity.person.Manager;
import com.ra.course.restaurant.model.entity.person.Waiter;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Reservation;
import com.ra.course.restaurant.model.entity.restaurant.Table;
import com.ra.course.restaurant.model.entity.restaurant.TableChart;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;
import com.ra.course.restaurant.persistence.restaurant.*;
import com.ra.course.restaurant.service.exception.ReservationException;
import com.ra.course.restaurant.service.person.EmployeeService;
import com.ra.course.restaurant.service.restaurant.*;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ReservationServiceIntegrationTest extends TestDataUtil {
    private static final int PEOPLE_COUNT_10 = 10;
    private static final int PEOPLE_COUNT_20 = 20;
    private static final int PEOPLE_COUNT_5 = 5;
    private static final int LOCATION_IDENTIFIER = 1;
    private static final int MAX_CAPACITY = 22;
    private static final int PEOPLE_COUNT_2 = 2;
    private static final int TABLE_CAPACITY = 1;
    private static final int NOTIFY_INTERVAL_MINUTE = 1;
    private static final int NOTIFY_INTERVAL_MINUTE_5 = 5;
    private static final int HOURS_INTERVAL_2 = 2;
    private static final int HOURS_INTERVAL_1 = 1;
    private static final int HOURS_INTERVAL_3 = 3;
    private static final int DAYS_INTERVAL_1 = 1;
    public static final String DEFAULT_PATH = "default/path";
    public static final int DEFAULT_TABLE_CAPACITY = 5;
    public static final String DEFAULT_BRANCH_NAME = "br1";
    private final List<Table> tables = List.of(getTable(TABLE_CAPACITY), getTable(TABLE_CAPACITY));

    @Autowired
    private ReservationService reservationService;
    @Autowired
    private ReservationRepository reservationRepository;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private TableService tableService;
    @Autowired
    private TableRepository tableRepository;
    @Autowired
    private BranchService branchService;
    @Autowired
    private TableChartRepository tableChartRepository;
    @Autowired
    private BranchRepository branchRepository;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private NotificationService notificationService;

    private Manager manager;
    private Employee waiter;
    private Customer customer;
    private Branch branch;

    private Map<String, Reservation> reservations;

    @BeforeEach
    void setUp() {
        customer = getCustomer();
        customerService.addCustomer(customer);
        TableChart tableChart = getTableChart();
        reservationService.setTableService(tableService);
        branch = getBranch();
        branchService.addBranch(branch);
        branchService.addTableChart(tableChart);
        tables.forEach(table -> {
            tableService.addTable(table, 1);
        });
        reservations = Map.of(
                "res_10_2", getReservation(PEOPLE_COUNT_10, LocalDateTime.now().plusHours(HOURS_INTERVAL_2)),
                "res_5_2", getReservation(PEOPLE_COUNT_5, LocalDateTime.now().plusHours(HOURS_INTERVAL_2)),
                "res_2_2", getReservation(PEOPLE_COUNT_2, LocalDateTime.now().plusHours(HOURS_INTERVAL_2)),
                "res_5_3_1", getReservation(PEOPLE_COUNT_5, LocalDateTime.now().plusHours(HOURS_INTERVAL_3).minusDays(DAYS_INTERVAL_1)),
                "res_5_3", getReservation(PEOPLE_COUNT_5, LocalDateTime.now().plusHours(HOURS_INTERVAL_3)),
                "res_5_1", getReservation(PEOPLE_COUNT_5, LocalDateTime.now().plusHours(HOURS_INTERVAL_1).plusMinutes(NOTIFY_INTERVAL_MINUTE)));

        reservationService.setCustomerService(customerService);
        reservationService.setNotifService(notificationService);
        reservations.forEach((key, value) -> {
            reservationRepository.add(value);
        });
        manager = getManager();
        waiter = getEmployee();
        employeeService.addEmployee(manager);
        employeeService.addEmployee(waiter);
        followTestData(tableRepository, List.copyOf(tables));
        followTestData(reservationRepository, List.copyOf(reservations.values()));
        followTestData(employeeRepository, manager);
        followTestData(employeeRepository, waiter);
        followTestData(customerRepository, customer);
        followTestData(branchRepository, branch);
        followTestData(tableChartRepository, tableChart);

    }

    @AfterEach
    void tearDown() {
        removeTestData();
    }

    @Test
    @DisplayName("When not enough seats then throw exception")
    void createReservationWhenNotEnoughSeats() {
        //then
        assertThrows(ReservationException.class, () -> reservationService.createReservation(manager.getId(), reservations.get("res_10_2"), tables));
    }

    @Test
    @DisplayName("Try to create reservation when employee has no rights")
    void createReservationWhenEmployeeHasNoRights() {
        //then
        assertThrows(ReservationException.class, () -> reservationService.createReservation(waiter.getId(), reservations.get("res_5_2"), tables));
    }

    @Test
    @DisplayName("Create reservation when already exists")
    void createReservationWhenEmployeeHasRightsAndReservationAlreadyExists() {
        //then
        assertThrows(ReservationException.class, () -> reservationService.createReservation(manager.getId(), reservations.get("res_5_2"), tables));
    }

    @Test
    @DisplayName("Create reservation")
    void createReservationWhenEmployeeHasRightsAndReservationNotExists() {
        //given
        reservationRepository.delete(reservations.get("res_2_2"));
        Reservation reservation = reservations.get("res_2_2");
        //then
        assertAll(() -> {
            assertDoesNotThrow(() -> reservationService.createReservation(manager.getId(), reservation, tables));
            assertEquals(reservation.getTableList(), tables);
        });
    }

    @Test
    @DisplayName("Update reservation")
    void updateReservation() {
        //given
        Reservation reservation = reservations.get("res_5_2");
        reservation.setPeopleCount(100);
        //then
        assertAll(() -> {
            assertDoesNotThrow(() -> reservationService.updateReservation(reservation));
            assertTrue(reservationRepository.getReservationsByCustomerID(reservation.getCustomer().getId()).contains(reservation));
        });
    }

    @Test
    @DisplayName("Update reservation when already exists, throw exception")
    void updateReservationWhenNotExists() {
        //given
        reservationRepository.delete(reservations.get("res_5_2"));
        Reservation reservation = reservations.get("res_5_2");
        //then
        assertThrows(ReservationException.class, () -> reservationService.updateReservation(reservation));
    }

    @Test
    @DisplayName("When reservation not exists, throw exception")
    void updatePeopleCountWhenReservationNotExists() {
        //given
        reservationRepository.delete(reservations.get("res_2_2"));
        Reservation reservation = reservations.get("res_2_2");
        //then
        assertThrows(ReservationException.class, () -> reservationService.updatePeopleCount(reservation.getId(), PEOPLE_COUNT_5, reservation.getTableList()));
    }

    @Test
    @DisplayName("Update people count when there are more people than there were")
    void updatePeopleCountWhenPeopleMoreThenBefore() {
        //given
        Reservation reservation = reservations.get("res_10_2");
        //when
//        when(reservationRepository.findById(anyInt())).thenReturn(Optional.of(reservation));

        //then
        assertDoesNotThrow(() -> reservationService.updatePeopleCount(reservation.getId(), PEOPLE_COUNT_20, reservation.getTableList()));

    }

    @Test
    @DisplayName("Update people count when there are less people than there were")
    void updatePeopleCountWhenPeopleLessThenBefore() {
        //given
        Reservation reservation = reservations.get("res_10_2");
        //then
        assertDoesNotThrow(() -> reservationService.updatePeopleCount(reservation.getId(), PEOPLE_COUNT_5, reservation.getTableList()));
    }

    @Test
    @DisplayName("Checkin when customer has not reservations, throw exception")
    void checkInWhenCustomerHasNotReservations() {
        //given
        Customer customer = getCustomer();
        //then
        assertThrows(ReservationException.class, () -> reservationService.checkIn(customer, LocalDateTime.now()));
    }

    @Test
    @DisplayName("Checkin when customer has reservations")
    void checkInWhenCustomerHasReservations() {
        //given
        Reservation reservation = reservations.get("res_10_2");
        //when
        reservationService.checkIn(customer, reservation.getCheckinTime());
        //then
        assertEquals(customer.getLastVisited(), reservation.getCheckinTime());
    }

    @Test
    @DisplayName("If no available tables then size 0")
    void getAvailableTablesWhenHasNotAvailableTables() {
        //then
        assertEquals(0, reservationService.getAvailableTables(LocalDateTime.now(), branch).size());
    }


    @Test
    void getAvailableTablesWhenHasAvailableTables() {
        //given
        getReservation(PEOPLE_COUNT_5, LocalDateTime.now().plusHours(HOURS_INTERVAL_3));
        assertEquals(2, reservationService.getAvailableTables(LocalDateTime.now(), branch).size());
    }

    @Test
    @DisplayName("If future reservation exists then equals size")
    void getFutureReservations() {
        //then
        assertEquals(5, reservationService.getFutureReservations().size());
    }

    @Test
    @DisplayName("Get all reservations which ready to send")
    void getReservationsToNotify() {
        //then
        assertEquals(1, reservationService.getReservationsToNotify(NOTIFY_INTERVAL_MINUTE_5).size());
    }

    private Reservation getReservation(int peopleCount, LocalDateTime reservTime) {
        Reservation reservation = new Reservation(LocalDateTime.now(), PEOPLE_COUNT_5, ReservationStatus.PENDING, "notes", null, customer);
        reservation.setPeopleCount(peopleCount);
        reservation.setCheckinTime(LocalDateTime.now());
        reservation.setTimeOfReservation(reservTime);
        tables.get(0).setReservations(List.of(reservation));
        tables.get(1).setReservations(List.of(reservation));
        reservation.setTableList(tables);
        return reservation;
    }

    private TableChart getTableChart() {
        TableChart tableChart = new TableChart(DEFAULT_PATH, DEFAULT_BRANCH_NAME, DEFAULT_TABLE_CAPACITY);
        tableChart.setTableQuantity(new AtomicInteger(0));
        return tableChart;
    }

    private Customer getCustomer() {
        return new Customer("Vaasya", "+380506137777");
    }

    private Table getTable(int capacity) {
        Table table = new Table(TableStatus.FREE, MAX_CAPACITY, LOCATION_IDENTIFIER);
        table.setMaxCapacity(capacity);
        return table;
    }

    private Manager getManager() {
        return new Manager("Vanya", "email", "phone", LocalDate.now(), getBranch());
    }

    private Employee getEmployee() {
        return new Waiter("Volodya", "volodya@mail.ru", "+380506666666", LocalDate.now(), getBranch());
    }

    private Branch getBranch() {
        Address location = new Address("Street", "Kharkov", "Babai", "61003", "Ukraine");
        return new Branch(DEFAULT_BRANCH_NAME, location);
    }
}
