package com.ra.course.restaurant.web;

import com.ra.course.restaurant.model.entity.BaseEntity;
import com.ra.course.restaurant.persistence.CRUDRepository;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestDataUtil {

    private Map<Object, List<BaseEntity>> testValues = new HashMap<>();
    private final List<String> allowedPossibleMethods = List.of("removeMenu", "removeMeal", "removeMealItem");

    protected void followTestData(Object repoKey, BaseEntity repoValue) {
        if (testValues.containsKey(repoKey)) {
            testValues.get(repoKey).add(repoValue);
        } else {
            testValues.put(repoKey, new ArrayList<>(List.of(repoValue)));
        }
    }

    protected void followTestData(Object repoKey, List<BaseEntity> repoValues) {
        repoValues.forEach(value -> followTestData(repoKey, value));
    }

    protected void removeTestData() {
        testValues.forEach((k, v) -> v.forEach(entity -> {
            if (k instanceof CRUDRepository) {
                tryToDelete((CRUDRepository) k, entity);
            } else {
                allowedPossibleMethods.forEach(method -> {
                    tryToDelete(k, entity, method);
                });
            }
        }));
    }

    private boolean tryToDelete(CRUDRepository repository, BaseEntity entity) {
        try {
            repository.delete(entity);
            return true;
        } catch (Exception ignored) {
            //ignored
            return false;
        }
    }

    private boolean tryToDelete(Object anyService, BaseEntity entity, String methodType) {
        try {
            ReflectionTestUtils.invokeMethod(anyService, methodType, entity.getId());
            return true;
        } catch (Exception ignored) {
            //ignored
            return false;
        }
    }
}
