package com.ra.course.restaurant.web.persistence.restaurant;

import com.ra.course.restaurant.model.entity.basic.Address;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.persistence.restaurant.BranchRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class BranchRepositoryIntegrationTest extends TestDataUtil {

    @Autowired
    private BranchRepository branchRepository;
    private Branch branch;

    @BeforeEach
    void before() {
        Address address = new Address("Street", "City", "State", "12345", "Country");
        branch = new Branch("Branch1", address);
        branchRepository.add(branch);
        followTestData(branchRepository, branch);
    }

    @AfterEach
    void after() {
        removeTestData();
    }

    @Test
    @DisplayName("If branch exists in repository then return this branch")
    void getBranchByName() {
        assertEquals(branchRepository.getBranchByName(branch.getName()), Optional.of(branch));
    }

    @Test
    @DisplayName("If branch not exists in repository then return Optional.empty")
    void getBranchByNameWhenNotExists() {
        assertEquals(branchRepository.getBranchByName("Different branch"), Optional.empty());
    }
}