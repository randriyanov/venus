package com.ra.course.restaurant.web.restaurant;

import com.ra.course.restaurant.model.entity.basic.Address;
import com.ra.course.restaurant.model.entity.person.Chef;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Kitchen;
import com.ra.course.restaurant.model.entity.restaurant.TableChart;
import com.ra.course.restaurant.persistence.restaurant.BranchRepository;
import com.ra.course.restaurant.persistence.restaurant.KitchenRepository;
import com.ra.course.restaurant.persistence.restaurant.TableChartRepository;
import com.ra.course.restaurant.service.exception.BranchException;
import com.ra.course.restaurant.service.person.EmployeeService;
import com.ra.course.restaurant.service.restaurant.BranchService;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
public class BranchServiceIntegrationTest extends TestDataUtil {
    public static final String DEFAULT_PATH = "default/path";
    public static final int DEFAULT_TABLE_CAPACITY = 3;
    public static final int NON_EXISTENT_TABLE_CHART_ID = -1;
    public static final String DEFAULT_BRANCH_NAME = "br1";
    public static final String DIFFERENT_BRANCH_NAME = "diffBranch";
    public static final String NON_EXISTENT_BRANCH = "non_existent_branch";
    private final Chef DEFAULT_CHEF = new Chef("Ektor", "ektor@mail.ru", "+380995556677", LocalDate.now(), getBranch(DEFAULT_BRANCH_NAME));
    @Autowired
    private BranchService branchService;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private BranchRepository branchRepository;
    @Autowired
    private TableChartRepository tableChartRepository;
    @Autowired
    private KitchenRepository kitchenRepository;
    private TableChart tableChart;
    private TableChart tableChartNotInBase;
    private TableChart tableChartWithoutBranch;
    private final Kitchen kitchenInDB = new Kitchen("kitchen", List.of(DEFAULT_CHEF), DEFAULT_BRANCH_NAME);
    private Kitchen kitchenNotInDB;
    private Branch branch;

    @BeforeEach
    void setUp() {
        branchService.setEmployeeService(employeeService);
        tableChartWithoutBranch = getTableChartNoBranch();
        branch = getBranch(DEFAULT_BRANCH_NAME);
        Branch anotherBranch = getBranch(DIFFERENT_BRANCH_NAME);
        branchService.addBranch(branch);
        branchService.addBranch(anotherBranch);
        tableChart = getTableChartByBranch();
        branchService.addTableChart(tableChart);
        branchService.addKitchen(kitchenInDB);

        followTestData(branchRepository, List.of(branch, anotherBranch));
        followTestData(tableChartRepository, tableChart);
        followTestData(kitchenRepository, kitchenInDB);
    }

    @AfterEach
    void tearDown() {
        removeTestData();
    }

    @Test
    @DisplayName("If chart exists then does not throw exception")
    void getTableChartByIDWhenAlreadyExists() {
        //then
        assertDoesNotThrow(() -> branchService.getTableChartByID(tableChart.getId()));
    }

    @Test
    @DisplayName("If chart not exists then throw exception")
    void getTableChartByIDWhenNotExists() {
        //then
        assertThrows(BranchException.class, () -> branchService.getTableChartByID(NON_EXISTENT_TABLE_CHART_ID));
    }

    @Test
    @DisplayName("If chart already exists then throw exception")
    void addTableChartWhenAlreadyExists() {
        //then
        assertThrows(BranchException.class, () -> branchService.addTableChart(tableChart));
    }

    @Test
    @DisplayName("If chart not exists then add")
    void addTableChartWhenNotExists() {
        //given
        tableChartNotInBase = getTableChart();
        followTestData(tableChartRepository, tableChartNotInBase);
        //then
        assertDoesNotThrow(() -> branchService.addTableChart(tableChartNotInBase));
    }

    @Test
    @DisplayName("If branch not exists then throw exception")
    void addTableChartWhenBranchNotExists() {
        //then
        assertThrows(BranchException.class, () -> branchService.addTableChart(tableChartWithoutBranch));
    }

    @Test
    @DisplayName("Add kitchen if not already exists")
    void addKitchen() {
        //given
        kitchenNotInDB = new Kitchen("kitchen", List.of(DEFAULT_CHEF), DIFFERENT_BRANCH_NAME);
        followTestData(kitchenRepository, kitchenNotInDB);
        //when
        branchService.addKitchen(kitchenNotInDB);
    }

    @Test
    @DisplayName("Add kitchen, if already exists then throw exception")
    void addKitchenWhenAlreadyExists() {
        //then
        assertThrows(BranchException.class, () -> branchService.addKitchen(kitchenInDB));
    }

    @Test
    @DisplayName("Add kitchen, if branch not exists then throw exception")
    void addKitchenWhenBranchNotExists() {
        //given
        Kitchen kitchen = new Kitchen("kitchen", List.of(DEFAULT_CHEF), NON_EXISTENT_BRANCH);
        ;
        //then
        assertThrows(BranchException.class, () -> branchService.addKitchen(kitchen));
    }

    @Test
    @DisplayName("If branch exists then update branch")
    void updateBranch() {
        //when
        assertDoesNotThrow(() -> branchService.updateBranch(branch));
    }

    @Test
    @DisplayName("If branch not exists then throw exception")
    void updateBranchWhenNotExists() {
        branchRepository.delete(branch);
        //then
        assertThrows(BranchException.class, () -> branchService.updateBranch(branch));
    }

    @Test
    @DisplayName("If kitchen exists then update kitchen")
    void updateKitchen() {
        //then
        assertDoesNotThrow(() -> branchService.updateKitchen(kitchenInDB));
    }

    @Test
    @DisplayName("If kitchen not exists then throw exception")
    void updateKitchenWhenNotExists() {
        //given
        kitchenNotInDB = new Kitchen("kitchen", List.of(DEFAULT_CHEF), DIFFERENT_BRANCH_NAME);
        //then
        assertThrows(BranchException.class, () -> branchService.updateKitchen(kitchenNotInDB));
    }

    @Test
    @DisplayName("If chart already exists then update chart")
    void updateTableChart() {
        //then
        assertDoesNotThrow(() -> branchService.updateTableChart(tableChart));
    }

    @Test
    @DisplayName("If chart not exists then throw exception")
    void updateTableChartWhenChartNotExists() {
        //given
        tableChartNotInBase = getTableChartNoBranch();
        //then
        assertThrows(BranchException.class, () -> branchService.updateTableChart(tableChartNotInBase));
    }

    @Test
    @DisplayName("when table chart does not exist, throw an exception")
    void printTableChartWhenNotExists() {
        //then
        assertThrows(BranchException.class, () -> branchService.printTableChart(NON_EXISTENT_TABLE_CHART_ID));
    }

    @Test
    @DisplayName("when table chart exists, print table chart")
    void printTableChartWhenExists() {
        //then
        assertDoesNotThrow(() -> branchService.printTableChart(tableChart.getId()));
    }

    @Test
    @DisplayName("If chef and branch exist then set branch to employee and update employee")
    void assignChef() {
        employeeService.addEmployee(DEFAULT_CHEF);
        //when
        branchService.assignChef(branch.getName(), DEFAULT_CHEF);
        //then
        assertEquals(DEFAULT_CHEF.getBranch(), branch);
    }

    @Test
    @DisplayName("when assigning chef to a branch that not exist, throw an exception")
    void assignChefWhenBranchNotExists() {
        //given
        branchRepository.delete(branch);
        //then
        assertThrows(BranchException.class, () -> branchService.assignChef(branch.getName(), DEFAULT_CHEF));
    }

    @Test
    @DisplayName("When branch already exists then throw exception")
    void addBranchWhenAlreadyExists() {
        //then
        assertThrows(BranchException.class, () -> branchService.addBranch(branch));
    }

    @Test
    @DisplayName("When branch not exists then add branch")
    void addBranch() {
        branchRepository.delete(branch);
        //then
        assertDoesNotThrow(() -> branchService.addBranch(branch));
    }

    private TableChart getTableChart() {
        return new TableChart(DEFAULT_PATH, DEFAULT_BRANCH_NAME, DEFAULT_TABLE_CAPACITY);
    }

    private TableChart getTableChartByBranch() {
        return new TableChart(DEFAULT_PATH, DEFAULT_BRANCH_NAME, DEFAULT_TABLE_CAPACITY);
    }

    private TableChart getTableChartNoBranch() {
        return new TableChart(DEFAULT_PATH, NON_EXISTENT_BRANCH, DEFAULT_TABLE_CAPACITY);
    }

    private Branch getBranch(String name) {
        Address location = new Address("Street", "Kharkov", "Babai", "61003", "Ukraine");
        return new Branch(name, location);
    }

    private Kitchen getKitchen(String name) {
        Branch branch = getBranch(DEFAULT_BRANCH_NAME);
        return new Kitchen(name, new ArrayList<>(), branch.getName());
    }
}
