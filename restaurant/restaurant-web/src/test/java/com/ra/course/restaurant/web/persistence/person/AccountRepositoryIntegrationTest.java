package com.ra.course.restaurant.web.persistence.person;

import com.ra.course.restaurant.model.entity.basic.Address;
import com.ra.course.restaurant.model.entity.enumerations.AccountStatus;
import com.ra.course.restaurant.model.entity.person.Account;
import com.ra.course.restaurant.persistence.person.AccountRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class AccountRepositoryIntegrationTest extends TestDataUtil {

    public static final int UNSOUGHT_ID = -1;
    @Autowired
    private AccountRepository accountRepository;
    private static final int EMPLOYEE_ID = 1;
    private static Address address = new Address("street", "city", "state", "zipCode", "country");
    private static final Account account = new Account("pass", address, AccountStatus.ACTIVE, EMPLOYEE_ID);

    @BeforeEach
    void before() {
        accountRepository.add(account);
        followTestData(accountRepository, account);
    }

    @AfterEach
    void after() {
        removeTestData();
    }

    @Test
    @DisplayName("When getting an account by employee ID, which is in the repository, the account is provided")
    void getAccountByEmployeeId() {
        assertEquals(accountRepository.getAccountByEmployeeId(EMPLOYEE_ID), Optional.of(account));
    }

    @Test
    @DisplayName("When getting an account by employee ID, which is not in the repository, then return optional empty")
    void getAccountByEmployeeIdWhenEmployeeNotExists() {
        assertEquals(accountRepository.getAccountByEmployeeId(UNSOUGHT_ID), Optional.empty());
    }
}