package com.ra.course.restaurant.web.persistence.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.Restaurant;
import com.ra.course.restaurant.persistence.restaurant.RestaurantRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class RestaurantRepositoryIntegrationTest extends TestDataUtil {

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Test
    @DisplayName("When updating name of a restaurant, it is successfully updating")
    void updateName() {
        Restaurant restaurant = restaurantRepository.getRestaurant();
        assertEquals(restaurant.getName(), "No name");
        restaurantRepository.updateName("Yaposhka");
        assertEquals(restaurantRepository.getRestaurant().getName(), "Yaposhka");
    }

    @Test
    void getRestaurant() {
        assertNotNull(restaurantRepository.getRestaurant());
    }
}