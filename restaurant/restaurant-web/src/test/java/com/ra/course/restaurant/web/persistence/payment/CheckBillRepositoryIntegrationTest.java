package com.ra.course.restaurant.web.persistence.payment;

import com.ra.course.restaurant.persistence.payment.CheckBillRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
public class CheckBillRepositoryIntegrationTest extends TestDataUtil {

    @Autowired
    private CheckBillRepository checkBillRepository;

    @Test
    @DisplayName("check if repository exists")
    void checkIfInstanceOfCRUDRepo() {
        assertNotNull(checkBillRepository);
    }
}
