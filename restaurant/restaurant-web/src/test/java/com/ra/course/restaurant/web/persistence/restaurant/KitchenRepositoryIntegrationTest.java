package com.ra.course.restaurant.web.persistence.restaurant;

import com.ra.course.restaurant.model.entity.basic.Address;
import com.ra.course.restaurant.model.entity.person.Chef;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Kitchen;
import com.ra.course.restaurant.persistence.restaurant.KitchenRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class KitchenRepositoryIntegrationTest extends TestDataUtil {

    @Autowired
    private KitchenRepository kitchenRepository;
    private Kitchen kitchen;

    @BeforeEach
    void before() {
        Address address = new Address("street", "city", "state", "zipCode", "country");
        Branch branch = new Branch("name", address);
        Chef chef = new Chef("name", "email", "1234", LocalDate.now(), branch);
        kitchen = new Kitchen("Ramzy", List.of(chef), "Branch1");
        kitchenRepository.add(kitchen);
        followTestData(kitchenRepository, kitchen);
    }

    @AfterEach
    void after() {
        removeTestData();
    }

    @Test
    @DisplayName("when getting a kitchen from the repository by branch name, the kitchen is provided")
    void getKitchenByBranchName() {
        //given
        String branchName = "Branch1";
        //then
        assertEquals(kitchenRepository.getKitchenByBranchName(branchName), Optional.of(kitchen));
    }

    @Test
    @DisplayName("when getting a kitchen, which is not in the repository, by branch name, an empty object is provided")
    void getKitchenByBranchNameWhenNoSuchKitchen() {
        //given
        String branchName = "BranchZ";
        //then
        assertEquals(kitchenRepository.getKitchenByBranchName(branchName), Optional.empty());
    }
}