package com.ra.course.restaurant.web.persistence.restaurant;

import com.ra.course.restaurant.model.entity.basic.Address;
import com.ra.course.restaurant.model.entity.enumerations.SeatType;
import com.ra.course.restaurant.model.entity.enumerations.TableStatus;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Table;
import com.ra.course.restaurant.model.entity.restaurant.TableChart;
import com.ra.course.restaurant.model.entity.restaurant.TableSeat;
import com.ra.course.restaurant.persistence.restaurant.TableRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class TableRepositoryIntegrationTest extends TestDataUtil {

    @Autowired
    private TableRepository tableRepository;
    private Branch branch;
    private static final int DEFAULT_ENTITY_ID = 1;
    private static Address address = new Address("street", "city", "state", "zipCode", "country");
    private Table table;
    private static final int DEFAULT_CAPACITY = 3;
    private static final int LOCATION_IDENTIFIER_DEFAULT = 1;

    @BeforeEach
    void before() {
        branch = getBranch();
        table = getTable();
        tableRepository.add(table);
        followTestData(tableRepository, table);
    }

    @AfterEach
    void after() {
        removeTestData();
    }

    @Test
    @DisplayName("When getting a list of tables from a branch, which is in the repository, the table list is provided")
    void getTablesByBranch() {
        assertEquals(tableRepository.getTablesByBranch(branch), List.of(table));
    }

    @Test
    @DisplayName("When getting a list of tables from a branch, which is not in the repository, an empty list is provided")
    void getTablesByBranchWhenTablesByThisBranchNotExist() {
        branch.setName("Different Name");
        assertEquals(tableRepository.getTablesByBranch(branch), Collections.emptyList());
    }

    private Table getTable() {
        TableChart tableChart = new TableChart("path", "Branch1", DEFAULT_CAPACITY);
        Table table = new Table(TableStatus.FREE, DEFAULT_CAPACITY, LOCATION_IDENTIFIER_DEFAULT);
        table.setTableSeats(List.of(new TableSeat(SeatType.REGULAR, DEFAULT_ENTITY_ID)));
        table.setTableChart(tableChart);
        return table;
    }

    private Branch getBranch() {
        return new Branch("Branch1", address);
    }
}