package com.ra.course.restaurant.web.order;

import com.ra.course.restaurant.model.entity.menu.Meal;
import com.ra.course.restaurant.model.entity.menu.MealItem;
import com.ra.course.restaurant.model.entity.menu.MenuItem;
import com.ra.course.restaurant.service.exception.MealException;
import com.ra.course.restaurant.service.order.MealService;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class MealServiceIntegrationTest extends TestDataUtil {

    private static final int DEFAULT_ENTITY_ID = 1;
    private static final int DEFAULT_QUANTITY = 5;
    private static final BigDecimal DEFAULT_PRICE_VALUE = new BigDecimal(100);
    private static final int DEFAULT_ENTITY_SECOND_ID = 2;
    private static final String DESCRIPTION = "description";
    private static final String MENU_ITEM_TITLE = "I am a menu item";
    public static final int POSITIVE_QUANTITY = 150;
    public static final int NEGATIVE_QUANTITY = -5;


    @Autowired
    private MealService mealService;

    @BeforeEach
    void before () {
        Meal meal = new Meal(DEFAULT_ENTITY_ID,DEFAULT_ENTITY_ID);
        mealService.addMeal(meal);
        MenuItem menuItem = new MenuItem(MENU_ITEM_TITLE, DESCRIPTION,DEFAULT_PRICE_VALUE,DEFAULT_ENTITY_ID);
        MealItem mealItem = new MealItem(DEFAULT_ENTITY_ID,DEFAULT_QUANTITY,menuItem);
        mealService.addMealItem(mealItem);
        followTestData(mealService, meal);
    }

    @AfterEach
    void after () {
        removeTestData();
    }

    @Test
    @DisplayName("When adding a meal, which is not in the repo, no exception thrown")
    void addMeal() {
        Meal meal = getNotExistingMeal();
        followTestData(mealService, meal);
        assertDoesNotThrow(() -> mealService.addMeal(meal));
    }

    @Test
    @DisplayName("When updating meal, which is not in the repo, throw an exception")
    void updateMealWhenNotExists() {
        assertThrows(MealException.class, () -> mealService.updateMeal(getNotExistingMeal()));
    }

    @Test
    @DisplayName("When updating meal, which is in the repo, no exception thrown")
    void updateMealWhenMealExists() {
        assertDoesNotThrow(() -> mealService.updateMeal(getDefaultMeal()));
    }

    @Test
    @DisplayName("When adding a meal, which is in the repo, throw an exception")
    void addMealWhichAlreadyExists() {
        assertThrows(MealException.class, () -> mealService.addMeal(getDefaultMeal()));
    }

    @Test
    @DisplayName("When removing meal, which is not in the repo, throw an exception")
    void removeMealWhenMealNotExists() {
        assertThrows(MealException.class, () -> mealService.removeMeal(DEFAULT_ENTITY_SECOND_ID));
    }

    @Test
    @DisplayName("When removing meal, which is in the repo, no exception thrown")
    void removeMealWhenMealExists() {
        assertDoesNotThrow(() -> mealService.removeMeal(DEFAULT_ENTITY_ID));
    }

    @Test
    @DisplayName("When adding a meal item, which is in the repo, throw an exception")
    void addMealItemWhenAlreadyExists() {
        assertThrows(MealException.class, () -> mealService.addMealItem(getDefaultMealItem()));
    }

    @Test
    @DisplayName("When adding a meal item, which is not in the repo, no exception thrown")
    void addMealItemWhenMealNotExists() {
        assertDoesNotThrow(() -> mealService.addMealItem(getNotExistingMealItem()));
    }

    @Test
    @DisplayName("When updating a meal item, which is not in the repo, throw an exception")
    void updateMealItemWhenNotExists() {
        assertThrows(MealException.class, () -> mealService.updateMealItem(getNotExistingMealItem()));
    }

    @Test
    @DisplayName("When updating a meal item, which is in the repo, no exception thrown")
    void updateMealItemWhenExists() {
        assertDoesNotThrow(() -> mealService.updateMealItem(getDefaultMealItem()));
    }

    @Test
    @DisplayName("When removing a meal item, which is not in the repo, throw an exception")
    void removeMealItemWhenItNotExists() {
        Assertions.assertThrows(MealException.class, () -> mealService.removeMealItem(DEFAULT_ENTITY_SECOND_ID));
    }

    @Test
    @DisplayName("When removing a meal item, which is in the repo, no exception thrown")
    void removeMealItem() {
        Assertions.assertDoesNotThrow(() -> mealService.removeMealItem(DEFAULT_ENTITY_ID));
    }

    @Test
    @DisplayName("When updating a quantity of a meal item, which is in the repo, standard behavior")
    void updateQuantityWhenMealItemExists() {
        assertDoesNotThrow(() -> mealService.updateQuantity(DEFAULT_ENTITY_ID, POSITIVE_QUANTITY));
    }

    @Test
    @DisplayName("When updating a quantity of a meal item with a negative value, throw an exception")
    void updateQuantityWhenPriceNegative() {
        assertThrows(MealException.class, () -> mealService.updateQuantity(DEFAULT_ENTITY_ID, NEGATIVE_QUANTITY));
    }



    private Meal getDefaultMeal () {
        return mealService.getMealRepository().findById(DEFAULT_ENTITY_ID).get();
    }

    private Meal getNotExistingMeal () {
        return new Meal(DEFAULT_ENTITY_ID,DEFAULT_ENTITY_ID);
    }

    private MealItem getDefaultMealItem () {
        return mealService.getMealItemRepo().findById(DEFAULT_ENTITY_ID).get();
    }

    private MealItem getNotExistingMealItem () {
        final MenuItem menuItem = new MenuItem(MENU_ITEM_TITLE, DESCRIPTION,DEFAULT_PRICE_VALUE,DEFAULT_ENTITY_ID);
        return new MealItem(DEFAULT_ENTITY_ID,DEFAULT_QUANTITY,menuItem);
    }

}
