package com.ra.course.restaurant.web.persistence.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.TableChart;
import com.ra.course.restaurant.persistence.restaurant.TableChartRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class TableChartRepositoryIntegrationTest extends TestDataUtil {

    public static final int TABLE_CAPACITY = 4;

    @Autowired
    private TableChartRepository tableChartRepository;

    private TableChart tableChart;

    @BeforeEach
    void before() {
        tableChart = getTableChart();
        tableChartRepository.add(tableChart);
        followTestData(tableChartRepository, tableChart);
    }

    @AfterEach
    void after() {
        removeTestData();
    }

    @Test
    @DisplayName("When getting a table chart from the repository by branch name, the table chart is provided")
    void getTableChartByBranchName() {
        //given
        String branchName = "branch1";
        //then
        assertEquals(tableChartRepository.getTableChartByBranchName(branchName), Optional.of(tableChart));
    }

    @Test
    @DisplayName("When getting a table chart, which is not in the repository, by branch name, an empty object is provided")
    void getTableChartByBranchNameWhenSuchTableChartNotExists() {
        //given
        String branchName = "another";
        //then
        assertEquals(tableChartRepository.getTableChartByBranchName(branchName), Optional.empty());
    }

    private TableChart getTableChart() {
        return new TableChart("any/path", "branch1", TABLE_CAPACITY);
    }
}