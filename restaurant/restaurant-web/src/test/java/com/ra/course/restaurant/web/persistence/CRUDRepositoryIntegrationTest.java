package com.ra.course.restaurant.web.persistence;

import com.ra.course.restaurant.model.entity.BaseEntity;
import com.ra.course.restaurant.persistence.CRUDRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
class CRUDRepositoryIntegrationTest extends TestDataUtil {

    @Autowired
    private CRUDRepository<BaseEntity> crudRepository;
    private final BaseEntityWithString entity = new BaseEntityWithString();
    private static final int NON_EXISTENT_ID = Integer.MAX_VALUE;

    @BeforeEach
    void before() {
        crudRepository.add(entity);
        followTestData(crudRepository, entity);
    }

    @AfterEach
    void after() {
        removeTestData();
    }

    @Test
    @DisplayName("when adding an element to this repository, the element is in the repository")
    void addElementExpectItInsideRepo() {
        Assertions.assertTrue(crudRepository.getAll().contains(entity));
    }

    @Test
    @DisplayName("when finding an element by ID, which is in the repository, then returns this element ")
    void findByIdWhenItIsInRepo() {
        assertEquals(crudRepository.findById(entity.getId()).get(), entity);
    }

    @Test
    @DisplayName("When finding an element by ID, which is not in the repository, then return empty element")
    void findByIdWhenItIsNotInRepo() {
        assertEquals(crudRepository.findById(NON_EXISTENT_ID), Optional.empty());
    }

    @Test
    @DisplayName("When updating an element, which is in the repository, then the element is changed")
    void update() {
        entity.setString("updated string");
        crudRepository.update(entity);
        assertEquals(crudRepository.findById(entity.getId()).get(), entity);
    }

    @Test
    @DisplayName("When an element is in the repository, successfully delete")
    void delete() {
        crudRepository.delete(entity);
        assertEquals(crudRepository.getAll().size(), 0);
    }

    @Test
    @DisplayName("When getting all elements of the repository, receiving a list or these elements")
    void getAll() {
        assertEquals(crudRepository.getAll(), List.of(entity));
    }

    private class BaseEntityWithString extends BaseEntity {
        private String string = "test string";

        public String getString() {
            return string;
        }

        public void setString(String string) {
            this.string = string;
        }
    }
}