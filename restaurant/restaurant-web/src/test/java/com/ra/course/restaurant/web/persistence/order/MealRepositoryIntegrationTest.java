package com.ra.course.restaurant.web.persistence.order;

import com.ra.course.restaurant.persistence.order.MealRepository;
import com.ra.course.restaurant.web.TestDataUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class MealRepositoryIntegrationTest extends TestDataUtil {

    @Autowired
    private MealRepository mealRepository;

    @Test
    @DisplayName("check if repository exists")
    void checkIfInstanceOfCRUDRepo() {
        assertNotNull(mealRepository);
    }
}
