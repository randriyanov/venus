package com.ra.course.restaurant.model.entity.payment;

import com.ra.course.restaurant.model.entity.BaseEntity;
import com.ra.course.restaurant.model.entity.enumerations.PaymentStatus;

import java.time.LocalDateTime;

public abstract class Payment extends BaseEntity {
    private double amount;
    private final LocalDateTime creationDate;
    private final CheckBill checkBill;
    private PaymentStatus paymentStatus;

    public Payment(int amount, LocalDateTime creationDate, CheckBill checkBill) {
        this.amount = amount;
        this.creationDate = creationDate;
        this.checkBill = checkBill;
        this.paymentStatus = PaymentStatus.UNPAID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public CheckBill getCheckBill() {
        return checkBill;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }
}
