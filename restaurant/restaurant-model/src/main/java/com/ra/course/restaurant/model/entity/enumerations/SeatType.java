package com.ra.course.restaurant.model.entity.enumerations;

public enum SeatType {
    REGULAR, KID, ACCESSIBLE, OTHER
}
