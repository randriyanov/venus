package com.ra.course.restaurant.model.entity.order;

import com.ra.course.restaurant.model.entity.BaseEntity;
import com.ra.course.restaurant.model.entity.enumerations.OrderStatus;
import com.ra.course.restaurant.model.entity.menu.Meal;
import com.ra.course.restaurant.model.entity.payment.CheckBill;
import com.ra.course.restaurant.model.entity.person.Employee;

import java.util.ArrayList;
import java.util.List;

public class Order extends BaseEntity {
    private OrderStatus orderStatus;
    private final int tableID;
    private List<Employee> staff = new ArrayList<>();
    private CheckBill checkBill;
    private List<Meal> meals;

    public Order(OrderStatus orderStatus, int tableID) {
        this.orderStatus = orderStatus;
        this.tableID = tableID;
    }

    public List<Employee> getStaff() {
        return staff;
    }

    public void setStaff(List<Employee> staff) {
        this.staff = staff;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public int getTableID() {
        return tableID;
    }

    public CheckBill getCheckBill() {
        return checkBill;
    }

    public void setCheckBill(CheckBill checkBill) {
        this.checkBill = checkBill;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }
}
