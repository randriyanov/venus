package com.ra.course.restaurant.model.entity.notification;

import java.time.LocalDateTime;

public class EmailNotification extends Notification {
    private String email;

    public EmailNotification(LocalDateTime date, String content, String email, int customerId) {
        super(date, content, customerId);
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
