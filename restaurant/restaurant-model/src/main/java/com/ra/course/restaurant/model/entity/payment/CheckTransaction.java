package com.ra.course.restaurant.model.entity.payment;

import java.time.LocalDateTime;

public class CheckTransaction extends Payment {

    private final String bankName;
    private final String checkNumber;

    public CheckTransaction(int amount, LocalDateTime creationDate, CheckBill checkBill, String bankName, String checkNumber) {
        super(amount, creationDate, checkBill);
        this.bankName = bankName;
        this.checkNumber = checkNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public String getCheckNumber() {
        return checkNumber;
    }
}
