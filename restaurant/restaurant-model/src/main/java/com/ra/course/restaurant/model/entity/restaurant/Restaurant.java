package com.ra.course.restaurant.model.entity.restaurant;

import com.ra.course.restaurant.model.entity.BaseEntity;

import java.util.List;

public class Restaurant extends BaseEntity {
    private String name;
    private List<Branch> branches;

    public Restaurant(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Branch> getBranches() {
        return branches;
    }

    public void setBranches(List<Branch> branches) {
        this.branches = branches;
    }
}
