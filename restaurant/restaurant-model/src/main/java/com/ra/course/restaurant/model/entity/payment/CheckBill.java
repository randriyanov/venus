package com.ra.course.restaurant.model.entity.payment;

import com.ra.course.restaurant.model.entity.BaseEntity;
import com.ra.course.restaurant.model.entity.enumerations.PaymentType;

import java.math.BigDecimal;

public class CheckBill extends BaseEntity {
    private BigDecimal amount;
    private BigDecimal tip;
    private BigDecimal tax;
    private boolean paid;
    private int paymentID;
    private int employeeID;
    private final int orderID;
    private final PaymentType paymentType;


    public CheckBill(int orderID, PaymentType paymentType) {
        this.paid = false;
        this.orderID = orderID;
        this.paymentType = paymentType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getTip() {
        return tip;
    }

    public void setTip(BigDecimal tip) {
        this.tip = tip;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }

    public void setPaymentID(int paymentID) {
        this.paymentID = paymentID;
    }

    public int getPaymentID() {
        return paymentID;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(int employeeID) {
        this.employeeID = employeeID;
    }

    public int getOrderID() {
        return orderID;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }
}
