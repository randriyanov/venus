package com.ra.course.restaurant.model.entity.restaurant;

import com.ra.course.restaurant.model.entity.BaseEntity;
import com.ra.course.restaurant.model.entity.enumerations.SeatType;
import com.ra.course.restaurant.model.entity.menu.Meal;

import java.util.List;

public class TableSeat extends BaseEntity {
    private final int tableID;
    private SeatType type;
    private List<Meal>meals;

    public TableSeat(SeatType type, int tableID) {
        this.type = type;
        this.tableID = tableID;
    }

    public int getTableID() {
        return tableID;
    }

    public SeatType getType() {
        return type;
    }

    public void setType(SeatType type) {
        this.type = type;
    }

    public List<Meal> getMeals() {
        return meals;
    }

    public void setMeals(List<Meal> meals) {
        this.meals = meals;
    }
}
