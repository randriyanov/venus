package com.ra.course.restaurant.model.entity.person;

import com.ra.course.restaurant.model.entity.restaurant.Reservation;

import java.time.LocalDateTime;

public class Customer extends Person {
    private LocalDateTime lastVisited;
    private Reservation reservation;

    public Customer(String name, String phone) {
        super(name, phone);
    }

    public LocalDateTime getLastVisited() {
        return lastVisited;
    }

    public void setLastVisited(LocalDateTime lastVisited) {
        this.lastVisited = lastVisited;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }
}
