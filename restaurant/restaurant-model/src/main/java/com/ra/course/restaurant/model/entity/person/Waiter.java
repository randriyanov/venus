package com.ra.course.restaurant.model.entity.person;

import com.ra.course.restaurant.model.entity.enumerations.EmployeeRole;
import com.ra.course.restaurant.model.entity.order.Order;
import com.ra.course.restaurant.model.entity.restaurant.Branch;

import java.time.LocalDate;
import java.util.List;

public class Waiter extends Employee {
    private List<Order> orders;

    public Waiter(String name, String email, String phone, LocalDate dateJoined, Branch branch) {
        super(name, email, phone, dateJoined, branch, EmployeeRole.WAITER);
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
