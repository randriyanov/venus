package com.ra.course.restaurant.model.entity.enumerations;

public enum PaymentType {
    CASH, CREDIT_CARD, CHECK
}
