package com.ra.course.restaurant.model.entity.person;

import com.ra.course.restaurant.model.entity.enumerations.EmployeeRole;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Reservation;

import java.time.LocalDate;
import java.util.List;

public class Receptionist extends Employee {
    private List<Reservation> reservations;

    public Receptionist(String name, String email, String phone, LocalDate dateJoined, Branch branch) {
        super(name, email, phone, dateJoined, branch, EmployeeRole.RECEPTIONIST);
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }
}
