package com.ra.course.restaurant.model.entity.menu;

import com.ra.course.restaurant.model.entity.BaseEntity;

import java.math.BigDecimal;

public class MenuItem extends BaseEntity {
    private String title;
    private String description;
    private BigDecimal price;
    private final int sectionID;

    public MenuItem(String title, String description, BigDecimal price, int sectionID) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.sectionID = sectionID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getSectionID() {
        return sectionID;
    }
}
