package com.ra.course.restaurant.model.entity.enumerations;

public enum TableStatus {
    FREE, RESERVED, OCCUPIED, OTHER
}
