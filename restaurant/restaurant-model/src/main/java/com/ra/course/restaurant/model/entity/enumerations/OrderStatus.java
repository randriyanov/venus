package com.ra.course.restaurant.model.entity.enumerations;

public enum OrderStatus {
    RECEIVED, PREPARING, COMPLETE, CANCELED, NONE
}
