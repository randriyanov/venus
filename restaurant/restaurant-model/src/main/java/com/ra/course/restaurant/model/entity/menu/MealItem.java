package com.ra.course.restaurant.model.entity.menu;

import com.ra.course.restaurant.model.entity.BaseEntity;

public class MealItem extends BaseEntity {
    private int mealId;
    private int quantity;
    private final MenuItem menuItem;

    public MealItem(int mealId, int quantity, MenuItem menuItem) {
        this.mealId = mealId;
        this.quantity = quantity;
        this.menuItem = menuItem;
    }

    public int getMealId() {
        return mealId;
    }

    public void setMealId(int mealId) {
        this.mealId = mealId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public MenuItem getMenuItem() {
        return menuItem;
    }
}
