package com.ra.course.restaurant.model.entity.notification;

import com.ra.course.restaurant.model.entity.BaseEntity;

import java.time.LocalDateTime;

public abstract class Notification extends BaseEntity {
    private LocalDateTime date;
    private String content;
    private final int customerId;

    public Notification(LocalDateTime date, String content, int customerId) {
        this.date = date;
        this.content = content;
        this.customerId = customerId;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
