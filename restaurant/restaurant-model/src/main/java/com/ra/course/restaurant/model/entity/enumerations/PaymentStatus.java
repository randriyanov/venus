package com.ra.course.restaurant.model.entity.enumerations;

public enum PaymentStatus {
    UNPAID, PENDING, COMPLETED, FAILED, DECLINED, CANCELLED, ABANDONED, SETTLING, SETTLED, REFUNDED
}
