package com.ra.course.restaurant.model.entity.menu;

import com.ra.course.restaurant.model.entity.BaseEntity;
import com.ra.course.restaurant.model.entity.restaurant.Branch;

import java.util.ArrayList;
import java.util.List;

public class Menu extends BaseEntity {
    private String title;
    private String description;

    private List<MenuSection> menuSections;
    private Branch branch;

    public Menu(String title, String description, Branch branch) {
        this.title = title;
        this.description = description;
        this.branch = branch;
        this.menuSections = new ArrayList<>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public List<MenuSection> getMenuSections() {
        return menuSections;
    }

    public void setMenuSections(List<MenuSection> menuSections) {
        this.menuSections = menuSections;
    }

}
