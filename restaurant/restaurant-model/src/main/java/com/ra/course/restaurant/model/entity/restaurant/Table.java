package com.ra.course.restaurant.model.entity.restaurant;

import com.ra.course.restaurant.model.entity.BaseEntity;
import com.ra.course.restaurant.model.entity.enumerations.TableStatus;

import java.util.List;

public class Table extends BaseEntity {
    private TableStatus status;
    private int maxCapacity;
    private int locationIdentifier;
    private List<TableSeat> tableSeats;
    private List<Reservation> reservations;
    private TableChart tableChart;

    public Table(TableStatus status, int maxCapacity, int locationIdentifier) {
        this.status = status;
        this.maxCapacity = maxCapacity;
        this.locationIdentifier = locationIdentifier;
    }

    public TableStatus getStatus() {
        return status;
    }

    public void setStatus(TableStatus status) {
        this.status = status;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public int getLocationIdentifier() {
        return locationIdentifier;
    }

    public void setLocationIdentifier(int locationIdentifier) {
        this.locationIdentifier = locationIdentifier;
    }

    public List<TableSeat> getTableSeats() {
        return tableSeats;
    }

    public void setTableSeats(List<TableSeat> tableSeats) {
        this.tableSeats = tableSeats;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public TableChart getTableChart() {
        return tableChart;
    }

    public void setTableChart(TableChart tableChart) {
        this.tableChart = tableChart;
    }
}
