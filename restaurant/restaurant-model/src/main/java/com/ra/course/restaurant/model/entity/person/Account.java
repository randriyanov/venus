package com.ra.course.restaurant.model.entity.person;

import com.ra.course.restaurant.model.entity.BaseEntity;
import com.ra.course.restaurant.model.entity.basic.Address;
import com.ra.course.restaurant.model.entity.enumerations.AccountStatus;

public class Account extends BaseEntity {
    private int employeeId;
    private String password;
    private Address address;
    private AccountStatus accountStatus;

    public Account(String password, Address address, AccountStatus accountStatus, int employeeId) {
        this.password = password;
        this.address = address;
        this.accountStatus = accountStatus;
        this.employeeId = employeeId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }

}
