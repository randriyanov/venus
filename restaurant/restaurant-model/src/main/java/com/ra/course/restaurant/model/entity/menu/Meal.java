package com.ra.course.restaurant.model.entity.menu;

import com.ra.course.restaurant.model.entity.BaseEntity;

import java.util.List;

public class Meal extends BaseEntity {
    private final int tableSeatID;
    private List<MealItem> mealItems;
    private final int orderID;

    public Meal(int tableSeatID, int orderID) {
        this.tableSeatID = tableSeatID;
        this.orderID = orderID;
    }

    public int getTableSeatID() {
        return tableSeatID;
    }

    public List<MealItem> getMealItems() {
        return mealItems;
    }

    public void setMealItems(List<MealItem> mealItems) {
        this.mealItems = mealItems;
    }

    public int getOrderID() {
        return orderID;
    }
}
