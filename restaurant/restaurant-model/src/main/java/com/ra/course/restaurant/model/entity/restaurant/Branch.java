package com.ra.course.restaurant.model.entity.restaurant;

import com.ra.course.restaurant.model.entity.BaseEntity;
import com.ra.course.restaurant.model.entity.basic.Address;
import com.ra.course.restaurant.model.entity.menu.Menu;
import com.ra.course.restaurant.model.entity.person.Employee;

import java.util.ArrayList;
import java.util.List;

public class Branch extends BaseEntity {
    private String name;
    private Address location;
    private Kitchen kitchen;
    private List<TableChart> tableCharts;
    private Menu menu;
    private List<Employee> employees;

    public Branch(String name, Address location) {
        this.name = name;
        this.location = location;
        this.employees = new ArrayList<>();
        this.tableCharts = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getLocation() {
        return location;
    }

    public void setLocation(Address location) {
        this.location = location;
    }

    public Kitchen getKitchen() {
        return kitchen;
    }

    public void setKitchen(Kitchen kitchen) {
        this.kitchen = kitchen;
    }

    public List<TableChart> getTableCharts() {
        return tableCharts;
    }

    public void setTableCharts(List<TableChart> tableCharts) {
        this.tableCharts = tableCharts;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }


}
