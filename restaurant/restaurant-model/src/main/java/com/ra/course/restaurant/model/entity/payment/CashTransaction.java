package com.ra.course.restaurant.model.entity.payment;

import java.time.LocalDateTime;

public class CashTransaction extends Payment{
    private double cashTendered;

    public CashTransaction(int amount, LocalDateTime creationDate, CheckBill checkBill, double cashTendered) {
        super(amount, creationDate, checkBill);
        this.cashTendered = cashTendered;
    }

    public double getCashTendered() {
        return cashTendered;
    }

    public void setCashTendered(double cashTendered) {
        this.cashTendered = cashTendered;
    }
}
