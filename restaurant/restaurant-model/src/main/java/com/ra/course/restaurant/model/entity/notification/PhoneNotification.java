package com.ra.course.restaurant.model.entity.notification;

import java.time.LocalDateTime;

public class PhoneNotification extends Notification {
    private String phone;

    public PhoneNotification(LocalDateTime date, String content, String phone, int customerId) {
        super(date, content, customerId);
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
