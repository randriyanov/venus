package com.ra.course.restaurant.model.entity.enumerations;

public enum EmployeeRole {
    CHEF, WAITER, RECEPTIONIST, MANAGER
}
