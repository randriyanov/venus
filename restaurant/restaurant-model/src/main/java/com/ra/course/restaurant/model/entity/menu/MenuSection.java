package com.ra.course.restaurant.model.entity.menu;

import com.ra.course.restaurant.model.entity.BaseEntity;

import java.util.ArrayList;
import java.util.List;

public class MenuSection extends BaseEntity {
    private String title;
    private String description;
    private final int menuID;

    private List<MenuItem> menuItems;

    public MenuSection(int menuID, String title, String description) {
        this.title = title;
        this.description = description;
        this.menuItems = new ArrayList<>();
        this.menuID = menuID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public int getMenuID() {
        return menuID;
    }
}

