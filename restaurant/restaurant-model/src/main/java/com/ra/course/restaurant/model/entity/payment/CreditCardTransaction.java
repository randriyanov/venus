package com.ra.course.restaurant.model.entity.payment;

import java.time.LocalDateTime;

public class CreditCardTransaction extends Payment {
    private final String nameOnCard;

    public CreditCardTransaction(int amount, LocalDateTime creationDate, CheckBill checkBill, String nameOnCard) {
        super(amount, creationDate, checkBill);
        this.nameOnCard = nameOnCard;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }
}
