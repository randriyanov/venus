package com.ra.course.restaurant.model.entity.person;

import com.ra.course.restaurant.model.entity.BaseEntity;

public abstract class Person extends BaseEntity {
    private String name;
    protected String email;
    private String phone;

    public Person(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
