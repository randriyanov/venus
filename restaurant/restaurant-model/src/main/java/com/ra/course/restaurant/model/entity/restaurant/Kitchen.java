package com.ra.course.restaurant.model.entity.restaurant;

import com.ra.course.restaurant.model.entity.BaseEntity;
import com.ra.course.restaurant.model.entity.person.Chef;

import java.util.List;

public class Kitchen extends BaseEntity {
    private String name;
    private List<Chef> chefs;
    private String BranchName;

    public Kitchen(String name, List<Chef> chefs, String branchName) {
        this.name = name;
        this.chefs = chefs;
        BranchName = branchName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Chef> getChefs() {
        return chefs;
    }

    public void setChefs(List<Chef> chefs) {
        this.chefs = chefs;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }


}
