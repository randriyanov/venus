package com.ra.course.restaurant.model.entity.enumerations;

public enum AccountStatus {
    ACTIVE, OFFLINE, CLOSED, CANCELED, BLACKLISTED
}
