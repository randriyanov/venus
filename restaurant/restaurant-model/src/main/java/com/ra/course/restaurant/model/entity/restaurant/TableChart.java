package com.ra.course.restaurant.model.entity.restaurant;

import com.ra.course.restaurant.model.entity.BaseEntity;

import java.util.concurrent.atomic.AtomicInteger;

public class TableChart extends BaseEntity {
    private String tableChartImagePath;
    private String branchName;
    private AtomicInteger tableQuantity;
    private final int tableCapacity;

    public TableChart(String tableChartImagePath, String branchName, int tableCapacity) {
        this.tableChartImagePath = tableChartImagePath;
        this.branchName = branchName;
        this.tableCapacity = tableCapacity;
    }

    public String getTableChartImagePath() {
        return tableChartImagePath;
    }

    public void setTableChartImagePath(String tableChartImagePath) {
        this.tableChartImagePath = tableChartImagePath;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public AtomicInteger getTableQuantity() {
        return tableQuantity;
    }

    public void setTableQuantity(AtomicInteger tableQuantity) {
        this.tableQuantity = tableQuantity;
    }

    public int getTableCapacity() {
        return tableCapacity;
    }
}
