package com.ra.course.restaurant.model.entity.enumerations;

public enum ReservationStatus {
    REQUESTED, PENDING, CONFIRMED, CHECKEDIN, CANCELED, ABANDONED
}
