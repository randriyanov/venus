package com.ra.course.restaurant.model.entity.person;

import com.ra.course.restaurant.model.entity.enumerations.EmployeeRole;
import com.ra.course.restaurant.model.entity.restaurant.Branch;

import java.time.LocalDate;

public abstract class Employee extends Person {
    private LocalDate dateJoined;
    private Branch branch;
    private EmployeeRole employeeRole;
    private Account account;

    public Employee(String name, String email, String phone, LocalDate dateJoined, Branch branch, EmployeeRole employeeRole) {
        super(name, phone);
        this.email = email;
        this.dateJoined = dateJoined;
        this.branch = branch;
        this.employeeRole = employeeRole;
    }

    public EmployeeRole getEmployeeRole() {
        return employeeRole;
    }

    public void setEmployeeRole(EmployeeRole employeeRole) {
        this.employeeRole = employeeRole;
    }

    public LocalDate getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(LocalDate dateJoined) {
        this.dateJoined = dateJoined;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
