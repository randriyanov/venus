package com.ra.course.restaurant.service.exception;

import java.util.UUID;

public class EntityNotFoundException extends RuntimeException{
    private static final UUID serialVersionUID = UUID.randomUUID();
    public EntityNotFoundException(final String message) {
        super(message);
    }
}
