package com.ra.course.restaurant.service.exception;

import java.util.UUID;

public class OrderException extends RuntimeException {
    private static final UUID serialVersionUID = UUID.randomUUID();

    public OrderException(final String message) {
        super(message);
    }
}
