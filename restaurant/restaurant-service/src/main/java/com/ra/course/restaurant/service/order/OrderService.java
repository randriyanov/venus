package com.ra.course.restaurant.service.order;

import com.ra.course.restaurant.model.entity.enumerations.OrderStatus;
import com.ra.course.restaurant.model.entity.order.Order;
import com.ra.course.restaurant.persistence.order.OrderRepository;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;

public interface OrderService {
    EmployeeRepository getEmployeeRepo();

    OrderRepository getOrderRepository();

    void takeOrder(int employeeId, Order order);
    void createOrder(int employeeId, Order order);
    void setOrderStatus(int orderId, OrderStatus status);
    void updateOrder (Order order);
}
