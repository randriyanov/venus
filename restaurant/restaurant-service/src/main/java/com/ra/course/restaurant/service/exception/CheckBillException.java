package com.ra.course.restaurant.service.exception;

import java.util.UUID;

public class CheckBillException extends RuntimeException{
    private static final UUID serialVersionUID = UUID.randomUUID();

    public CheckBillException(final String message) {
        super(message);
    }
}
