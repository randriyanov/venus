package com.ra.course.restaurant.service.person;

import com.ra.course.restaurant.model.entity.enumerations.AccountStatus;
import com.ra.course.restaurant.model.entity.person.Account;
import com.ra.course.restaurant.model.entity.person.Employee;
import com.ra.course.restaurant.persistence.person.AccountRepository;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;
import com.ra.course.restaurant.service.exception.EntityNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    private final EmployeeRepository employeeData;
    private final AccountRepository accountRepository;

    public AccountServiceImpl(final EmployeeRepository employeeData, final AccountRepository accountRepository) {
        this.employeeData = employeeData;
        this.accountRepository = accountRepository;
    }

    public EmployeeRepository getEmployeeData() {
        return employeeData;
    }

    public AccountRepository getAccountRepository() {
        return accountRepository;
    }

    @Override
    public void registerAccount(final Account account) {
        accountRepository.getAccountByEmployeeId(account.getEmployeeId()).ifPresentOrElse((Account addAcc) -> {
            throw new EntityNotFoundException("Account already exists");
        },()->{accountRepository.add(account);});
    }

    @Override
    public void cancelAccount(final int employeeId){
        accountRepository.getAccountByEmployeeId(employeeId).ifPresentOrElse((Account account) -> {
            account.setAccountStatus(AccountStatus.CANCELED);
            accountRepository.update(account);
        },()->{throw new EntityNotFoundException("Couldn't find an account");});
    }

    @Override
    public void login(final String email, final String password) {
        employeeData.getEmployeeByEmail(email).ifPresentOrElse((Employee employee) -> {
            accountRepository.getAccountByEmployeeId(employee.getId()).ifPresentOrElse((Account account) -> {
                if( !account.getPassword().equals(password) ){
                    throw new EntityNotFoundException("Wrong password");
                }
                account.setAccountStatus(AccountStatus.ACTIVE);
                accountRepository.update(account);
            },()->{ throw new EntityNotFoundException("Account not registered yet");});
        },()-> {throw new EntityNotFoundException("Wrong email or password");});

    }

    @Override
    public void logout(final int employeeId) {
        accountRepository.getAccountByEmployeeId(employeeId).ifPresentOrElse((Account logoutAcc) -> {
            logoutAcc.setAccountStatus(AccountStatus.OFFLINE);
            accountRepository.update(logoutAcc);
        },()->{throw new EntityNotFoundException("Couldn't find an account by employeeId");});
    }

    @Override
    public void resetPassword(final int employeeId, final String newPassword) {
        accountRepository.getAccountByEmployeeId(employeeId).ifPresentOrElse((Account resetAcc)->{
            resetAcc.setPassword(newPassword);
            accountRepository.update(resetAcc);
        },()->{throw new EntityNotFoundException("Account not exists");});
    }
}
