package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.persistence.restaurant.RestaurantRepository;
import org.springframework.stereotype.Service;

@Service
public class RestaurantServiceImpl implements RestaurantService {

    private final RestaurantRepository restRep;

    public RestaurantServiceImpl(final RestaurantRepository restRep) {
        this.restRep = restRep;
    }

    @Override
    public void updateName(final String name) {
        restRep.updateName(name);
    }

    public RestaurantRepository getRestRep() {
        return restRep;
    }
}
