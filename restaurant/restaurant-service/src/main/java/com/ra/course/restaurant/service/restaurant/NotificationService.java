package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.model.entity.person.Customer;
import com.ra.course.restaurant.model.entity.restaurant.Reservation;

public interface NotificationService {
    void sendNotification(String notification, Customer customer);
    String createReservationNotificationContent (Reservation reservation, boolean isUpdated);
    void reservationReminder (int minutes);
}
