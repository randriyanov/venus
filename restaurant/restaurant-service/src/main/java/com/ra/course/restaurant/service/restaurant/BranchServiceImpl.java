package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.model.entity.person.Chef;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Kitchen;
import com.ra.course.restaurant.model.entity.restaurant.TableChart;
import com.ra.course.restaurant.persistence.restaurant.BranchRepository;
import com.ra.course.restaurant.persistence.restaurant.KitchenRepository;
import com.ra.course.restaurant.persistence.restaurant.TableChartRepository;
import com.ra.course.restaurant.service.exception.BranchException;
import com.ra.course.restaurant.service.person.EmployeeService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BranchServiceImpl implements BranchService {
    private final BranchRepository branchRepository;
    private final TableChartRepository tableChartRep;
    private final KitchenRepository kitchenRepository;
    private EmployeeService employeeService;
    private static final String BRANCH_NOT_EXIST = "This branch does not exist";

    public BranchServiceImpl(final BranchRepository branchRepository, final TableChartRepository tableChartRep, final KitchenRepository kitchenRepository) {
        this.branchRepository = branchRepository;
        this.tableChartRep = tableChartRep;
        this.kitchenRepository = kitchenRepository;
    }

    public void setEmployeeService(final EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @Override
    public void addTableChart(final TableChart tableChart) {
        final Optional<Branch> branch = branchRepository.getBranchByName(tableChart.getBranchName());
        branch.ifPresentOrElse((Branch item) -> {
            final boolean isDuplicated = tableChartRep.getAll().stream().anyMatch((TableChart tChart) -> tChart.getId() == tableChart.getId());
            if (isDuplicated) {
                throw new BranchException("This table chart is already in the branch");
            }
            tableChartRep.add(tableChart);
        }, () -> {
            throw new BranchException(BRANCH_NOT_EXIST);
        });
    }

    @Override
    public void addKitchen(final Kitchen kitchen) {
        final Optional<Branch> branch = branchRepository.getBranchByName(kitchen.getBranchName());
        branch.ifPresentOrElse((Branch item) -> {
            final Optional<Kitchen> dbKitchen = kitchenRepository.getKitchenByBranchName(kitchen.getBranchName());
            if (dbKitchen.isPresent()) {
                throw new BranchException("This kitchen is already in the branch");
            }
            kitchenRepository.add(kitchen);
        }, () -> {
            throw new BranchException(BRANCH_NOT_EXIST);
        });
    }

    @Override
    public void updateBranch(final Branch branch) {
        final Optional<Branch> branchToUpdate = branchRepository.getBranchByName(branch.getName());
        if (branchToUpdate.isEmpty()) {
            throw new BranchException(BRANCH_NOT_EXIST);
        }
        branchRepository.update(branch);
    }

    @Override
    public void updateKitchen(final Kitchen kitchen) {
        final Optional<Kitchen> kitchenToUpdate = kitchenRepository.getKitchenByBranchName(kitchen.getBranchName());
        if (kitchenToUpdate.isEmpty()) {
            throw new BranchException("This kitchen does not exist");
        }
        kitchenRepository.update(kitchen);
    }

    @Override
    public void updateTableChart(final TableChart tableChart) {
        final Optional<TableChart> tChartToUpdate = tableChartRep.getTableChartByBranchName(tableChart.getBranchName());
        if (tChartToUpdate.isEmpty()) {
            throw new BranchException("This table chart does not exist");
        }
        tableChartRep.update(tableChart);
    }

    @Override
    public TableChart printTableChart(final int id) {
        final Optional<TableChart> chart = tableChartRep.findById(id);
        chart.ifPresentOrElse((TableChart tableChart) -> {
        }, () -> {
            throw new BranchException("There is no table chart with such ID");
        });
        return chart.get();
    }

    @Override
    public void assignChef(final String branchName, final Chef chef) {
        branchRepository.getBranchByName(branchName).ifPresentOrElse((Branch branch) -> {
            chef.setBranch(branch);
            employeeService.updateEmployee(chef);
        }, () -> {
            throw new BranchException("No such branch exist");
        });
    }

    @Override
    public void addBranch(final Branch branch) {
        branchRepository.getBranchByName(branch.getName()).ifPresentOrElse((Branch br) -> {
            throw new BranchException("branch named " + br.getName() + " is already in branch repository");
        }, () -> branchRepository.add(branch));
    }

    @Override
    public TableChart getTableChartByID(final int id) {
        final Optional<TableChart> tableChart = tableChartRep.findById(id);
        if (tableChart.isEmpty()) {
            throw new BranchException("There is no such table chart in the repository");
        }
        return tableChart.get();
    }

    public EmployeeService getEmployeeService() {
        return employeeService;
    }

    public BranchRepository getBranchRepository() {
        return branchRepository;
    }

    public TableChartRepository getTableChartRep() {
        return tableChartRep;
    }

    public KitchenRepository getKitchenRepository() {
        return kitchenRepository;
    }
}
