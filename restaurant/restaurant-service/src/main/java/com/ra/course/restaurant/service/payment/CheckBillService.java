package com.ra.course.restaurant.service.payment;

import com.ra.course.restaurant.model.entity.payment.CheckBill;
import com.ra.course.restaurant.model.entity.payment.Payment;
import com.ra.course.restaurant.persistence.payment.CheckBillRepository;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;

import java.io.IOException;

public interface CheckBillService {
    void createBill(int employeeID, CheckBill bill);

    void initiateTransaction(Payment payment);

    void printBill(int billID) throws IOException;

    void updateCheckBill(CheckBill checkBill);

    CheckBillRepository getCheckBillRep();

    EmployeeRepository getEmployeeRep();
}
