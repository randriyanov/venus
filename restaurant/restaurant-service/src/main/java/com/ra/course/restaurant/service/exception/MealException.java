package com.ra.course.restaurant.service.exception;

import java.util.UUID;

public class MealException extends RuntimeException {
    private static final UUID serialVersionUID = UUID.randomUUID();

    public MealException(final String message) {
        super(message);
    }
}
