package com.ra.course.restaurant.service.menu;

import com.ra.course.restaurant.model.entity.menu.Menu;
import com.ra.course.restaurant.model.entity.menu.MenuItem;
import com.ra.course.restaurant.model.entity.menu.MenuSection;
import com.ra.course.restaurant.persistence.CRUDRepository;
import com.ra.course.restaurant.persistence.menu.MenuItemRepository;
import com.ra.course.restaurant.persistence.menu.MenuSectionRepository;

import java.math.BigDecimal;

public interface MenuService {
    boolean updatePrice (int menuItemID, BigDecimal newPrice);
    boolean addMenuItemToSection (MenuItem menuItem);
    boolean addMenuSectionToMenu (MenuSection menuSection);
    MenuItemRepository getMenuItemRep();
    MenuSectionRepository getMenuSectionRep();
    CRUDRepository<Menu> getMenuRep();
    boolean addMenu (Menu menu);
    void updateMenu(Menu menu);
    void updateMenuSection (MenuSection menuSection);
    void updateMenuItem (MenuItem menuItem);
    void removeMenuItem (final int menuItemID);
    void removeSection (final int sectionID);
    void removeMenu (final int menuID);
}
