package com.ra.course.restaurant.service.exception;

import java.util.UUID;

public class TableException extends RuntimeException {
    private static final UUID serialVersionUID = UUID.randomUUID();
    public TableException(final String message) {
        super(message);
    }
}
