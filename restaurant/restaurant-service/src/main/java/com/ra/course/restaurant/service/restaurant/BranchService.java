package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.model.entity.person.Chef;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Kitchen;
import com.ra.course.restaurant.model.entity.restaurant.TableChart;
import com.ra.course.restaurant.persistence.restaurant.BranchRepository;
import com.ra.course.restaurant.persistence.restaurant.KitchenRepository;
import com.ra.course.restaurant.persistence.restaurant.TableChartRepository;
import com.ra.course.restaurant.service.person.EmployeeService;

public interface BranchService {
    BranchRepository getBranchRepository();

    TableChartRepository getTableChartRep();

    KitchenRepository getKitchenRepository();

    void addTableChart(TableChart tableChart);

    void addKitchen(Kitchen kitchen);

    void addBranch(Branch branch);

    void updateBranch(Branch branch);

    void updateKitchen(Kitchen kitchen);

    void updateTableChart(TableChart tableChart);

    TableChart printTableChart(int id);

    void assignChef(String branchName, Chef chef);

    TableChart getTableChartByID(int id);

    void setEmployeeService(final EmployeeService employeeService);
}
