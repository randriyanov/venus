package com.ra.course.restaurant.service.exception;

import java.util.UUID;

public class RestaurantException extends RuntimeException {
    private static final UUID serialVersionUID = UUID.randomUUID();
    public RestaurantException(final String message) {
        super(message);
    }
}
