package com.ra.course.restaurant.service.person;


import com.ra.course.restaurant.model.entity.enumerations.EmployeeRole;
import com.ra.course.restaurant.model.entity.person.Employee;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;

import java.util.List;
import java.util.Optional;


public interface EmployeeService {
    EmployeeRepository getPersonRepo();
    void addEmployee(Employee employee);
    List<Employee> getEmployeesByRole(EmployeeRole role);
    Optional<Employee> getEmployeeByID(int employeeID);
    boolean checkAccess(int employeeID, List<EmployeeRole> accessRoles);
    void updateEmployee(Employee employee);
}
