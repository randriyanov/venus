package com.ra.course.restaurant.service.order;

import com.ra.course.restaurant.model.entity.menu.Meal;
import com.ra.course.restaurant.model.entity.menu.MealItem;
import com.ra.course.restaurant.persistence.order.MealItemRepository;
import com.ra.course.restaurant.persistence.order.MealRepository;

public interface MealService {
    MealRepository getMealRepository();

    MealItemRepository getMealItemRepo();
    void addMeal(Meal meal);
    void updateMeal(Meal meal);
    void removeMeal(int mealId);
    void addMealItem(MealItem mealItem);
    void updateMealItem(MealItem mealItem);
    void removeMealItem(int mealItemId);
    void updateQuantity(int mealItemId, int newQuantity);
}
