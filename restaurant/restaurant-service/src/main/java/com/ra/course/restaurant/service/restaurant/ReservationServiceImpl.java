package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.model.entity.enumerations.EmployeeRole;
import com.ra.course.restaurant.model.entity.enumerations.ReservationStatus;
import com.ra.course.restaurant.model.entity.enumerations.TableStatus;
import com.ra.course.restaurant.model.entity.person.Customer;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Reservation;
import com.ra.course.restaurant.model.entity.restaurant.Table;
import com.ra.course.restaurant.persistence.restaurant.ReservationRepository;
import com.ra.course.restaurant.service.exception.ReservationException;
import com.ra.course.restaurant.service.person.EmployeeService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReservationServiceImpl implements ReservationService {

    private static final int TIME_FRAME_HOURS = 2;

    private final ReservationRepository reservationRep;
    private final EmployeeService employeeService;
    private TableService tableService;
    private CustomerService customerService;
    private NotificationService notifService;

    public ReservationServiceImpl(final ReservationRepository reservationRep, final EmployeeService employeeService) {
        this.reservationRep = reservationRep;
        this.employeeService = employeeService;
    }

    public ReservationRepository getReservationRep() {
        return reservationRep;
    }

    public EmployeeService getEmployeeService() {
        return employeeService;
    }

    public void setTableService(final TableService tableService) {
        this.tableService = tableService;
    }

    public void setCustomerService(final CustomerService customerService) {
        this.customerService = customerService;
    }

    public void setNotifService(final NotificationService notifService) {
        this.notifService = notifService;
    }

    @Override
    public void createReservation(final int employeeID, final Reservation reservation, final List<Table> tables) {
        final int numberOfSeats = tables.stream().mapToInt(Table::getMaxCapacity).sum();
        if (numberOfSeats < reservation.getPeopleCount()) {
            throw new ReservationException("No enough free seats to place the clients");
        }
        //Check employee has rights
        if (employeeService.checkAccess(employeeID, List.of(EmployeeRole.MANAGER, EmployeeRole.RECEPTIONIST))) {
            //Check if reservation does not exists
            reservationRep.findById(reservation.getId()).ifPresentOrElse((Reservation reserv) -> {
                throw new ReservationException("Reservation with id " + reserv.getId() + " already exists");
            }, () -> {
                reservation.setStaff_id(employeeID);
                tables.forEach(table -> {
                    table.setStatus(TableStatus.RESERVED);
                    tableService.updateTable(table);
                });
                reservation.setTableList(tables);
                reservationRep.add(reservation);
                final String notifyContent = notifService.createReservationNotificationContent(reservation, false);
                notifService.sendNotification(notifyContent, reservation.getCustomer());
            });
        } else {
            throw new ReservationException("You have no rights for this operation");
        }

    }

    @Override
    public void updateReservation(final Reservation reservation) {
        reservationRep.findById(reservation.getId()).ifPresentOrElse((Reservation reserv) -> {
            reservationRep.update(reservation);
            notifService.createReservationNotificationContent(reserv, true);
        }, () -> {
            throw new ReservationException("Reservation does not exists");
        });
    }

    @Override
    public void updatePeopleCount(final int reservationID, final int newPeopleCount, final List<Table> tablesToUpdate) {
        reservationRep.findById(reservationID).ifPresentOrElse((Reservation reservation) -> {
            if (reservation.getPeopleCount() < newPeopleCount) {
                final List<Table> newTables = new ArrayList<>();
                newTables.addAll(reservation.getTableList());
                newTables.addAll(tablesToUpdate);
                reservation.setTableList(newTables);
                reservation.getTableList().forEach((Table table) -> {
                    table.setStatus(TableStatus.RESERVED);
                    tableService.updateTable(table);
                });
            } else {
                tablesToUpdate.forEach((Table table) -> {
                    table.setStatus(TableStatus.FREE);
                    tableService.updateTable(table);
                });
            }
            reservation.setPeopleCount(newPeopleCount);
            reservationRep.update(reservation);
        }, () -> {
            throw new ReservationException("Could'nt find such reservation");
        });
    }

    @Override
    public void checkIn(final Customer customer, final LocalDateTime time) {
        reservationRep.getReservationsByCustomerID(customer.getId()).stream().filter(reservation -> reservation.getCheckinTime().equals(time)).findAny().ifPresentOrElse((Reservation reservation) -> {
            reservation.setCheckinTime(time);
            reservation.setStatus(ReservationStatus.CHECKEDIN);
            updateReservation(reservation);
            customer.setLastVisited(time);
            customerService.updateCustomer(customer);
        }, () -> {
            throw new ReservationException("You have not reservations for this time");
        });
    }

    @Override
    public List<Table> getAvailableTables(final LocalDateTime time, final Branch branch) {
        return tableService.getAllTables(branch).stream().filter((Table table) -> table.getReservations().stream()
                .filter((Reservation reserv)->{
                    final LocalDateTime rTime = reserv.getTimeOfReservation();
                    return rTime.isAfter(time.minusHours(TIME_FRAME_HOURS)) &&
                            rTime.isBefore(time.plusHours(TIME_FRAME_HOURS));
                }).findAny().isEmpty()).collect(Collectors.toList());
    }

    @Override
    public List<Reservation> getFutureReservations() {
        return reservationRep.getFutureReservations();
    }

    @Override
    public List<Reservation> getReservationsToNotify(final int minutes) {
        return reservationRep.getReservationsToNotify(minutes);
    }

    public TableService getTableService() {
        return tableService;
    }

    public CustomerService getCustomerService() {
        return customerService;
    }

    public NotificationService getNotifService() {
        return notifService;
    }

    public int getTimeFrameHours() {
        return TIME_FRAME_HOURS;
    }
}
