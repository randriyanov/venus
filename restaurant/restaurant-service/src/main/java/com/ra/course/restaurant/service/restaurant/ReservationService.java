package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.model.entity.person.Customer;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Reservation;
import com.ra.course.restaurant.model.entity.restaurant.Table;

import java.time.LocalDateTime;
import java.util.List;

public interface ReservationService {
    void createReservation(int employeeID, Reservation reservation, List<Table> tables);

    void updateReservation(Reservation reservation);

    void updatePeopleCount(int reservationID, int newPeopleCount, List<Table> tableList);

    void checkIn(Customer customer, LocalDateTime time);

    List<Table> getAvailableTables(LocalDateTime time, Branch branch);

    List<Reservation> getFutureReservations();

    List<Reservation> getReservationsToNotify(int minutes);

    void setTableService(final TableService tableService);

    void setCustomerService(final CustomerService customerService);

    void setNotifService(final NotificationService notifService);
}
