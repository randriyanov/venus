package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.model.entity.notification.EmailNotification;
import com.ra.course.restaurant.model.entity.notification.Notification;
import com.ra.course.restaurant.model.entity.notification.PhoneNotification;
import com.ra.course.restaurant.model.entity.person.Customer;
import com.ra.course.restaurant.model.entity.restaurant.Reservation;
import com.ra.course.restaurant.model.entity.restaurant.Table;
import com.ra.course.restaurant.persistence.CRUDRepository;
import com.ra.course.restaurant.persistence.restaurant.NotificationRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class NotificationServiceImpl implements NotificationService {

    private final NotificationRepository notificationRep;
    private final ReservationService reservService;

    public NotificationServiceImpl(final NotificationRepository notiRepository, final ReservationService reservService) {
        this.notificationRep = notiRepository;
        this.reservService = reservService;
    }

    @Override
    public void sendNotification(final String content, final Customer customer) {
        Notification notification;
        if (customer.getEmail() != null && !customer.getEmail().isEmpty()) {
            notification = new EmailNotification(LocalDateTime.now(), content, customer.getEmail(), customer.getId());
            sendEmailNotification(notification);
            notificationRep.add(notification);
        }
        notification = new PhoneNotification(LocalDateTime.now(), content, customer.getPhone(), customer.getId());
        sendPhoneNotification(notification);
        notificationRep.add(notification);
    }

    private void sendPhoneNotification (final Notification notification) {
        //todo
        notification.toString();
    }

    private void sendEmailNotification (final Notification notification) {
        //todo
        notification.toString();
    }

    @Override
    public String createReservationNotificationContent(final Reservation reservation,final boolean isUpdated) {
        final StringBuffer str = new StringBuffer(100);
        str.append(String.format("Dear %20s!\n",reservation.getCustomer().getName()))
                .append(String.format("You reservation #%4d has been ",reservation.getId()));
        if (isUpdated) {
            str.append("updated!\n");
        } else {
            str.append("confirmed!\n");
        }
        str.append(String.format("You have reserved seats for %3d customers.\n",reservation.getPeopleCount()))
                .append("Your table numbers are as follows:\n");
        reservation.getTableList().forEach((Table table) -> {
            str.append(String.format("%2d\n",table.getLocationIdentifier()));
        });
        return str.toString();
    }

    public CRUDRepository<Notification> getNotificationRep() {
        return notificationRep;
    }

    @Override
    public void reservationReminder(final int minutes) {
        reservService.getReservationsToNotify(minutes).forEach((Reservation reservation) -> {
            final String content = String.format("Dear %20s!\nYour reservation #%3d is due one hour!",reservation.getCustomer().getName(), reservation.getId());
            sendNotification(content,reservation.getCustomer());
        });
    }

    public ReservationService getReservService() {
        return reservService;
    }
}
