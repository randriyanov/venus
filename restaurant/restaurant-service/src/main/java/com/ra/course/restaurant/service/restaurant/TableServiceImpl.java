package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Table;
import com.ra.course.restaurant.model.entity.restaurant.TableChart;
import com.ra.course.restaurant.model.entity.restaurant.TableSeat;
import com.ra.course.restaurant.persistence.restaurant.TableRepository;
import com.ra.course.restaurant.persistence.restaurant.TableSeatRepository;
import com.ra.course.restaurant.service.exception.TableException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TableServiceImpl implements TableService {

    private final TableRepository tableRepository;
    private final TableSeatRepository tableSeatRep;
    private final BranchService branchService;

    public TableServiceImpl(final TableRepository tableRepository, final TableSeatRepository tableSeatRep, final BranchService branchService) {
        this.tableRepository = tableRepository;
        this.tableSeatRep = tableSeatRep;
        this.branchService = branchService;
    }

    public TableRepository getTableRepository() {
        return tableRepository;
    }

    public TableSeatRepository getTableSeatRep() {
        return tableSeatRep;
    }

    @Override
    public void addTable(final Table table, final int tableChartID) {
        final TableChart tableChart = branchService.getTableChartByID(tableChartID);
        if (tableChart.getTableQuantity().get() >= tableChart.getTableCapacity()) {
            throw new TableException("The table chart is filled, no new tables can be added");
        }
        if (table.getTableChart() != null && table.getTableChart().equals(tableChart)) {
            throw new TableException("This table is already in this table chart");
        }
        table.setTableChart(tableChart);
        table.setLocationIdentifier(tableChart.getTableQuantity().addAndGet(1));
        tableRepository.add(table);
        branchService.updateTableChart(tableChart);
    }

    @Override
    public void updateTable(final Table table) {
        tableRepository.findById(table.getId()).ifPresentOrElse(tableRepository::update, () -> {
            throw new TableException("No such table exists");
        });
    }

    @Override
    public void removeTable(final int tableID) {
        tableRepository.findById(tableID).ifPresentOrElse((Table table) -> {
                    tableSeatRep.getTableSeatsByTableID(table.getId()).forEach(tableSeatRep::delete);
                    tableRepository.delete(table);
                    final TableChart tableChart = branchService.getTableChartByID(table.getTableChart().getId());
                    tableChart.getTableQuantity().decrementAndGet();
                    branchService.updateTableChart(tableChart);
                },
                () -> {
                    throw new TableException("Table does not exists");
                });
    }

    @Override
    public void addTableSeat(final TableSeat tableSeat) {
        final Optional<Table> table = tableRepository.findById(tableSeat.getTableID());
        final int maxCapacity;
        if (table.isPresent()) {
            maxCapacity = table.get().getMaxCapacity();
        } else {
            throw new TableException("Table not exists");
        }
        if (tableSeatRep.getTableSeatsByTableID(table.get().getId()).size() < maxCapacity) {
            tableSeatRep.findById(tableSeat.getId()).ifPresentOrElse((TableSeat tabSeat) -> {
                throw new TableException("TableSeat with id " + tabSeat.getId() + " already exists");
            }, () -> {
                tableSeatRep.add(tableSeat);
            });
        } else {
            throw new TableException("Can not add any seat, maximum capacity is reached");
        }
    }

    @Override
    public void updateTableSeat(final TableSeat tableSeat) {
        tableSeatRep.findById(tableSeat.getId()).ifPresentOrElse(tabSeat -> {
            tableSeatRep.update(tableSeat);
        }, () -> {
            throw new TableException("TableSeat does not exists");
        });
    }

    @Override
    public List<Table> getAllTables(final Branch branch) {
        return tableRepository.getTablesByBranch(branch);
    }

    public BranchService getBranchService() {
        return branchService;
    }
}
