package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Table;
import com.ra.course.restaurant.model.entity.restaurant.TableSeat;

import java.util.List;

public interface TableService {
    void addTable(Table table, int tableChartID);
    void updateTable(Table table);
    void removeTable(int tableID);
    void addTableSeat(TableSeat tableSeat);
    void updateTableSeat(TableSeat tableSeat);
    List<Table> getAllTables(Branch branch);
}
