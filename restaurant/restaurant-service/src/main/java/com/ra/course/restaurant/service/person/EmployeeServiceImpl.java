package com.ra.course.restaurant.service.person;

import com.ra.course.restaurant.model.entity.enumerations.EmployeeRole;
import com.ra.course.restaurant.model.entity.person.Employee;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;
import com.ra.course.restaurant.service.exception.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository personRepo;

    public EmployeeServiceImpl(final EmployeeRepository personRepo) {
        this.personRepo = personRepo;
    }

    public EmployeeRepository getPersonRepo() {
        return personRepo;
    }

    @Override
    public void addEmployee(final Employee employee) {
        personRepo.findById(employee.getId()).ifPresent((Employee empl)->{
            throw new EntityNotFoundException("Employee already exists");
        });
        personRepo.add(employee);
    }

    @Override
    public List<Employee> getEmployeesByRole(final EmployeeRole role) {
        return personRepo.getEmployeeByRole(role);
    }

    @Override
    public Optional<Employee> getEmployeeByID(final int employeeID) {
        return personRepo.findById(employeeID);
    }

    @Override
    public boolean checkAccess(final int employeeID, final List<EmployeeRole> accessRoles) {
        final Optional<Employee> employee = personRepo.findById(employeeID);
        return employee.isPresent() && accessRoles.contains(employee.get().getEmployeeRole());
    }

    @Override
    public void updateEmployee(final Employee employee) {
        personRepo.findById(employee.getId()).ifPresentOrElse((Employee empl) -> {
            personRepo.update(employee);
        }, ()->{throw new EntityNotFoundException("Employee does not exists");});
    }


}
