package com.ra.course.restaurant.service.person;

import com.ra.course.restaurant.model.entity.person.Account;
import com.ra.course.restaurant.persistence.person.AccountRepository;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;

public interface AccountService {
    EmployeeRepository getEmployeeData();
    AccountRepository getAccountRepository();
    void registerAccount(Account account);
    void cancelAccount(int employeeId);
    void login(String email, String password);
    void logout(int employeeId);
    void resetPassword(int employeeId, String newPassword);
}
