package com.ra.course.restaurant.service.exception;

import java.util.UUID;

public class ReservationException extends RuntimeException {
    private static final UUID serialVersionUID = UUID.randomUUID();
    public ReservationException(final String message) {
        super(message);
    }
}
