package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.model.entity.person.Customer;
import com.ra.course.restaurant.persistence.restaurant.CustomerRepository;
import com.ra.course.restaurant.service.exception.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRep;

    public CustomerServiceImpl(final CustomerRepository customerRep) {
        this.customerRep = customerRep;
    }

    public CustomerRepository getCustomerRep() {
        return customerRep;
    }

    @Override
    public void addCustomer(final Customer customer) {
        final Optional<Customer> cust = customerRep.getCustomerByPhone(customer.getPhone());
        if(cust.isPresent()){
            throw new EntityNotFoundException("Customer already exists");
        }
        customerRep.add(customer);
    }

    @Override
    public void updateCustomer(final Customer customer) {
        customerRep.getCustomerByPhone(customer.getPhone()).ifPresentOrElse(cust ->{
            customerRep.update(customer);
        }, () -> {throw new EntityNotFoundException("Customer not exists");
        });
    }
}
