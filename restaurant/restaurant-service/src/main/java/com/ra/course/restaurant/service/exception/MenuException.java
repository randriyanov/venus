package com.ra.course.restaurant.service.exception;

import java.util.UUID;

public class MenuException extends RuntimeException {
    private static final UUID serialVersionUID = UUID.randomUUID();
    public MenuException(final String message) {
        super(message);
    }
}
