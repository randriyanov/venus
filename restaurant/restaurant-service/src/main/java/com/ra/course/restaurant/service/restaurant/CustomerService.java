package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.model.entity.person.Customer;


public interface CustomerService {
    void addCustomer(Customer customer);
    void updateCustomer(Customer customer);
}
