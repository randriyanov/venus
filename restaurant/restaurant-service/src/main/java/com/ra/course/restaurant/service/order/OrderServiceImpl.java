package com.ra.course.restaurant.service.order;

import com.ra.course.restaurant.model.entity.enumerations.EmployeeRole;
import com.ra.course.restaurant.model.entity.enumerations.OrderStatus;
import com.ra.course.restaurant.model.entity.order.Order;
import com.ra.course.restaurant.model.entity.person.Employee;
import com.ra.course.restaurant.persistence.order.OrderRepository;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;
import com.ra.course.restaurant.service.exception.OrderException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    private final EmployeeRepository employeeRepo;
    private final OrderRepository orderRepository;
    private static final String PERMISSION_DENIED = "You have no rights to the current operation";
    private static final String ITEM_NOT_EXISTS = "No order with such ID exists";

    public OrderServiceImpl(final EmployeeRepository employeeRepo, final OrderRepository orderRepository) {
        this.employeeRepo = employeeRepo;
        this.orderRepository = orderRepository;
    }

    public EmployeeRepository getEmployeeRepo() {
        return employeeRepo;
    }

    public OrderRepository getOrderRepository() {
        return orderRepository;
    }

    public String getPermissionDenied() {
        return PERMISSION_DENIED;
    }

    @Override
    public void takeOrder(final int employeeId, final Order order) {
        if (List.of(OrderStatus.NONE,OrderStatus.RECEIVED).contains(order.getOrderStatus())) {
            employeeRepo.findById(employeeId).ifPresentOrElse((Employee employee) -> {
                if (employee.getEmployeeRole().equals(EmployeeRole.CHEF)) {
                    // who prepare order
                    final List<Employee> staffs = order.getStaff();
                    staffs.add(employee);
                    order.setStaff(staffs);
                    setOrderStatus(order.getId(), OrderStatus.PREPARING);
                    orderRepository.update(order);
                } else {
                    throw new OrderException(PERMISSION_DENIED);
                }
            }, () -> {
                throw new OrderException("No such employee");
            });
        } else {
            throw new OrderException("This order cannot be taken in progress");
        }
    }

    @Override
    public void createOrder(final int employeeId, final Order order) {
        final Optional<Employee> employee = employeeRepo.findById(employeeId);
        employee.ifPresent((Employee empl) -> {
            if( empl.getEmployeeRole().equals(EmployeeRole.MANAGER) || empl.getEmployeeRole().equals(EmployeeRole.WAITER) ){
                // add manager to staff list
                setOrderStatus(order.getId(), OrderStatus.RECEIVED);
                final List<Employee> staffs = order.getStaff();
                staffs.add(empl);
                order.setStaff(staffs);
                orderRepository.update(order);
            }else{
                throw new OrderException(PERMISSION_DENIED);
            }
        });
    }

    @Override
    public void setOrderStatus(final int orderId, final OrderStatus status) {
        orderRepository.findById(orderId).ifPresentOrElse((Order order) -> {
            order.setOrderStatus(status);
            orderRepository.update(order);
        }, () -> {
            throw new OrderException(ITEM_NOT_EXISTS);
        });
    }

    @Override
    public void updateOrder(final Order order) {
        orderRepository.findById(order.getId())
                .ifPresentOrElse(orderRepository::update,()->{throw new OrderException(ITEM_NOT_EXISTS);});
    }

}
