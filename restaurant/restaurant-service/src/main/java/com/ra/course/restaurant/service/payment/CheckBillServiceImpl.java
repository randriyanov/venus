package com.ra.course.restaurant.service.payment;

import com.ra.course.restaurant.model.entity.enumerations.EmployeeRole;
import com.ra.course.restaurant.model.entity.menu.Meal;
import com.ra.course.restaurant.model.entity.menu.MealItem;
import com.ra.course.restaurant.model.entity.menu.MenuItem;
import com.ra.course.restaurant.model.entity.order.Order;
import com.ra.course.restaurant.model.entity.payment.CheckBill;
import com.ra.course.restaurant.model.entity.payment.Payment;
import com.ra.course.restaurant.model.entity.person.Employee;
import com.ra.course.restaurant.persistence.order.OrderRepository;
import com.ra.course.restaurant.persistence.payment.CheckBillRepository;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;
import com.ra.course.restaurant.service.exception.BranchException;
import com.ra.course.restaurant.service.exception.CheckBillException;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CheckBillServiceImpl implements CheckBillService {

    private final CheckBillRepository checkBillRep;
    private final EmployeeRepository employeeRep;
    private final OrderRepository orderRepository;
    private Map<MealItem, MenuItem> mealToMenu;
    private static int indexer;
    private static BigDecimal amount;
    private static final BigDecimal TAX_RATE = new BigDecimal("0.2");
    private static final BigDecimal TIP_RATE = new BigDecimal("0.1");


    public CheckBillServiceImpl(final CheckBillRepository checkBillRep, final EmployeeRepository employeeRep, final OrderRepository orderRep) {
        this.checkBillRep = checkBillRep;
        this.employeeRep = employeeRep;
        this.orderRepository = orderRep;
    }

    @Override
    public void createBill(final int employeeID, final CheckBill bill) {
        employeeRep.findById(employeeID).ifPresentOrElse((Employee emp) -> {
            if (emp.getEmployeeRole().equals(EmployeeRole.MANAGER) || emp.getEmployeeRole().equals(EmployeeRole.WAITER)) {
                bill.setEmployeeID(employeeID);
                checkBillRep.findById(bill.getId()).ifPresentOrElse((CheckBill checkBill) -> {
                    throw new CheckBillException("This bill with ID " + checkBill.getId() + "is already in the system");
                }, () -> checkBillRep.add(calculateBill(bill)));
            } else {
                throw new CheckBillException("This employee has no right to create bills");
            }
        }, () -> {
            throw new CheckBillException("This employee does not exist");
        });
    }

    @Override
    public void initiateTransaction(final Payment payment) {
        //check payment type
        //call method that will tell us whether the payment is successful and return new payment status
        //if payment is completed, we change isPaid variable in checkBill to True
        //todo
    }

    private CheckBill calculateBill(final CheckBill checkBill) {
        final Optional<Order> order = orderRepository.findById(checkBill.getOrderID());
        if (order.isPresent()) {
            final List<MealItem> mealItems = new ArrayList<>();
            order.get()
                    .getMeals()
                    .stream()
                    .filter((Meal meal) -> meal.getOrderID() == order.get().getId())
                    .collect(Collectors.toList())
                    .forEach((Meal meal) -> {
                        mealItems.addAll(meal.getMealItems());
                    });
            mealToMenu = new HashMap<>();
            amount = BigDecimal.ZERO;
            mealItems.forEach((MealItem item) ->{
                amount = amount.add(item.getMenuItem().getPrice().multiply(BigDecimal.valueOf(item.getQuantity())));
                mealToMenu.put(item, item.getMenuItem());
            });
            checkBill.setAmount(amount);
            checkBill.setTax(amount.multiply(TAX_RATE));
            checkBill.setTip(amount.multiply(TIP_RATE));
        }
        return checkBill;
    }

    @Override
    public void updateCheckBill(final CheckBill checkBill) {
        final Optional<CheckBill> checkToUpdate = checkBillRep.findById(checkBill.getId());
        if (checkToUpdate.isEmpty()) {
            throw new BranchException("This check does not exist");
        }
        checkBillRep.update(calculateBill(checkBill));
    }

    @Override
    public void printBill(final int billID) throws IOException {
        final Optional<CheckBill> checkBill = checkBillRep.findById(billID);
        checkBill.ifPresent(this::calculateBill);
        try (BufferedWriter os = Files.newBufferedWriter(Paths.get("./bill" + billID + ".txt"), StandardCharsets.UTF_8)) {
            final StringBuilder stringBuffer = new StringBuilder();
            stringBuffer.append(String.format("%-5s%-30s%-15s%-10s\n", "ID", "Title", "Quantity", "Price"));
            mealToMenu.forEach((mealItem, menuItem) ->
                    stringBuffer.append(String.format("%-5s%-30s%-15s%-4.2f", indexer++, menuItem.getTitle(), mealItem.getQuantity(), menuItem.getPrice()))
            );
            os.write(stringBuffer.toString());
            indexer = 0;
        }
    }

    @Override
    public CheckBillRepository getCheckBillRep() {
        return checkBillRep;
    }

    @Override
    public EmployeeRepository getEmployeeRep() {
        return employeeRep;
    }

    public OrderRepository getOrderRepository() {
        return orderRepository;
    }

    public Map<MealItem, MenuItem> getMealToMenu() {
        return mealToMenu;
    }

    public void setMealToMenu(final Map<MealItem, MenuItem> mealToMenu) {
        this.mealToMenu = mealToMenu;
    }
}
