package com.ra.course.restaurant.service.menu;

import com.ra.course.restaurant.model.entity.menu.Menu;
import com.ra.course.restaurant.model.entity.menu.MenuItem;
import com.ra.course.restaurant.model.entity.menu.MenuSection;
import com.ra.course.restaurant.persistence.CRUDRepository;
import com.ra.course.restaurant.persistence.menu.MenuItemRepository;
import com.ra.course.restaurant.persistence.menu.MenuRepository;
import com.ra.course.restaurant.persistence.menu.MenuSectionRepository;
import com.ra.course.restaurant.persistence.menu.exceptions.NonPositivePriceException;
import com.ra.course.restaurant.service.exception.MenuException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class MenuServiceImpl implements MenuService {


    private final MenuRepository menuRep;
    private final MenuItemRepository menuItemRep;
    private final MenuSectionRepository menuSectionRep;
    private static final String ITEM_NOT_EXISTS = "MenuItem not exists";

    public MenuServiceImpl(final MenuItemRepository menuItemRep, final MenuSectionRepository menuSectionRep, final MenuRepository menuRep) {
        this.menuItemRep = menuItemRep;
        this.menuSectionRep = menuSectionRep;
        this.menuRep = menuRep;
    }

    @Override
    public boolean updatePrice(final int menuItemID, final BigDecimal newPrice) {
        if (newPrice.compareTo(BigDecimal.ZERO) <= -1) {
            throw new NonPositivePriceException("Cannot update by a non-positive price");
        }
        menuItemRep.findById(menuItemID).ifPresentOrElse((MenuItem item) -> {
            item.setPrice(newPrice);
            menuItemRep.update(item);
        }, () -> {
            throw new MenuException(ITEM_NOT_EXISTS);
        });

        return true;
    }

    @Override
    public boolean addMenuItemToSection(final MenuItem menuItem) {
        final Optional<MenuSection> menuSection = menuSectionRep.findById(menuItem.getSectionID());
        menuSection.ifPresentOrElse((MenuSection item) -> {
            final boolean isDuplicated = menuItemRep.getItemsBySectionID(menuItem.getSectionID()).stream().anyMatch(mItem -> mItem.getTitle().equals(menuItem.getTitle()));
            if (isDuplicated) {
                throw new MenuException("This item is already in the section");
            }
            menuItemRep.add(menuItem);
        }, () -> {
            throw new MenuException("This section does not exist");
        });
        return true;
    }

    @Override
    public boolean addMenuSectionToMenu(final MenuSection menuSection) {
        menuRep.findById(menuSection.getMenuID()).ifPresentOrElse((Menu item) -> {
            final boolean isDuplicated = menuSectionRep.getSectionsByMenuID(menuSection.getMenuID()).stream().anyMatch((MenuSection mSection) -> mSection.getTitle().equals(menuSection.getTitle()));
            if (isDuplicated) {
                throw new MenuException("This section is already in the menu");
            }
            menuSectionRep.add(menuSection);
        }, () -> {
            throw new MenuException("This menu does not exist");
        });
        return true;
    }

    public MenuItemRepository getMenuItemRep() {
        return menuItemRep;
    }

    public MenuSectionRepository getMenuSectionRep() {
        return menuSectionRep;
    }

    @Override
    public void updateMenu(final Menu menu) {
        menuRep.findById(menu.getId()).ifPresentOrElse(m -> menuRep.update(menu),
                () -> {
                    throw new MenuException("Menu does not exists");
                });
    }

    @Override
    public void updateMenuSection(final MenuSection menuSection) {
        menuSectionRep.findById(menuSection.getId()).ifPresentOrElse(menuSectionRep::update, () -> {
            throw new MenuException("MenuSection not exists");
        });
    }

    @Override
    public void updateMenuItem(final MenuItem menuItem) {
        menuItemRep.findById(menuItem.getId()).ifPresentOrElse(menuItemRep::update, () -> {
            throw new MenuException(ITEM_NOT_EXISTS);
        });
    }

    @Override
    public CRUDRepository<Menu> getMenuRep() {
        return menuRep;
    }

    @Override
    public boolean addMenu(final Menu menu) {
        menuRep.findById(menu.getId()).ifPresentOrElse((Menu m) -> {
            throw new MenuException("Menu already exists");
        }, () -> {
            menuRep.add(menu);
        });
        return true;
    }

    @Override
    public void removeMenuItem(final int menuItemID) {
        menuItemRep.findById(menuItemID).ifPresentOrElse(menuItemRep::delete,
                () -> {
                    throw new MenuException(ITEM_NOT_EXISTS);
                });
    }

    @Override
    public void removeSection(final int sectionID) {
        menuItemRep.getItemsBySectionID(sectionID).forEach(item -> removeMenuItem(item.getId()));
        menuSectionRep.delete(menuSectionRep.findById(sectionID).get());
    }

    @Override
    public void removeMenu(final int menuID) {
        final Optional<Menu> menu = menuRep.findById(menuID);
        menu.ifPresentOrElse((Menu mItem) -> {
            menuSectionRep.getSectionsByMenuID(mItem.getId()).forEach((MenuSection item) -> removeSection(item.getId()));
            menuRep.delete(menu.get());
        }, () -> {
            throw new MenuException("There is no such menu to delete");
        });
    }
}
