package com.ra.course.restaurant.service.order;

import com.ra.course.restaurant.model.entity.menu.Meal;
import com.ra.course.restaurant.model.entity.menu.MealItem;
import com.ra.course.restaurant.persistence.order.MealItemRepository;
import com.ra.course.restaurant.persistence.order.MealRepository;
import com.ra.course.restaurant.service.exception.MealException;
import org.springframework.stereotype.Service;

@Service
public class MealServiceImpl implements MealService {
    public static final String MEAL_ALR_EXISTS = "Meal already exists";
    private final MealRepository mealRepository;
    private final MealItemRepository mealItemRepo;
    private static final String ITEM_NOT_EXISTS = "MealItem not exists";

    public MealServiceImpl(final MealRepository mealRepository, final MealItemRepository mealItemRepo) {
        this.mealRepository = mealRepository;
        this.mealItemRepo = mealItemRepo;
    }

    public MealRepository getMealRepository() {
        return mealRepository;
    }

    public MealItemRepository getMealItemRepo() {
        return mealItemRepo;
    }

    @Override
    public void addMeal(final Meal meal) {
        mealRepository.findById(meal.getId()).ifPresentOrElse((Meal thisMeal) -> {
            throw new MealException(MEAL_ALR_EXISTS);
        }, () -> mealRepository.add(meal));
    }

    @Override
    public void updateMeal(final Meal meal) {
        mealRepository.findById(meal.getId()).ifPresentOrElse(mealRepository::update,()->{throw new MealException(ITEM_NOT_EXISTS);});
    }

    @Override
    public void removeMeal(final int mealId) {
        mealRepository.findById(mealId).ifPresentOrElse((Meal meal) -> {
            mealItemRepo.getMealItemsByMealId(mealId).forEach((MealItem mealItem) -> removeMealItem(mealItem.getId()));
            mealRepository.delete(meal);
        },()->{throw new MealException("Couldn't find this meal");});
    }

    @Override
    public void addMealItem(final MealItem mealItem) {
        mealItemRepo.findById(mealItem.getId()).ifPresentOrElse((MealItem mealIt) -> {
            throw new MealException(MEAL_ALR_EXISTS);
        }, () -> mealItemRepo.add(mealItem));
    }

    @Override
    public void updateMealItem(final MealItem mealItem) {
        mealItemRepo.findById(mealItem.getId()).ifPresentOrElse(mealItemRepo::update,()->{throw new MealException(ITEM_NOT_EXISTS);});
    }

    @Override
    public void removeMealItem(final int mealItemId) {
        mealItemRepo.findById(mealItemId).ifPresentOrElse(mealItemRepo::delete, () -> {
            throw new MealException("MealItem with id "+ mealItemId + " is not in the meal item repository");
        });
    }

    @Override
    public void updateQuantity(final int mealItemId, final int newQuantity) {
        if (newQuantity <= 0) {
            throw new MealException("Cannot create an order with a non-positive meal quantity");
        }
        mealItemRepo.findById(mealItemId).ifPresentOrElse((MealItem item) -> {
            item.setQuantity(newQuantity);
            mealItemRepo.update(item);
        }, () -> {throw new MealException("Couldn't find this meal item");});
    }


}
