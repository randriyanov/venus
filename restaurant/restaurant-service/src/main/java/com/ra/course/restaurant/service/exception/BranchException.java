package com.ra.course.restaurant.service.exception;

import java.util.UUID;

public class BranchException extends RuntimeException {
    private static final UUID serialVersionUID = UUID.randomUUID();

    public BranchException(final String message) {
        super(message);
    }
}
