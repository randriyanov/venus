package com.ra.course.restaurant.service.order;

import com.ra.course.restaurant.model.entity.enumerations.OrderStatus;
import com.ra.course.restaurant.model.entity.order.Order;
import com.ra.course.restaurant.model.entity.person.Chef;
import com.ra.course.restaurant.model.entity.person.Employee;
import com.ra.course.restaurant.model.entity.person.Manager;
import com.ra.course.restaurant.model.entity.person.Waiter;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.persistence.order.OrderRepository;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;
import com.ra.course.restaurant.service.exception.OrderException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class OrderServiceImplMockTest {
    public static final int DEFAULT_ENTITY_ID = 1;

    private OrderService orderService;
    @Mock
    private EmployeeRepository employeeRepo;
    @Mock
    private OrderRepository orderRepository;


    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
        this.orderService = new OrderServiceImpl(employeeRepo, orderRepository);
    }

    @Test
    @DisplayName("Will return not null")
    void getEmployeeRepo() {
        assertNotNull(orderService.getEmployeeRepo());
    }

    @Test
    @DisplayName("Will return not null")
    void getOrderRepository() {
        assertNotNull(orderService.getOrderRepository());
    }

    @Test
    @DisplayName("Will return String Permission denied")
    void getPermissionDenied() {
        assertEquals(new OrderServiceImpl(employeeRepo, orderRepository).getPermissionDenied(), "You have no rights to the current operation");
    }

    @Test
    @DisplayName("When employee is chef then add employee id to order staff")
    void takeOrder() {
        when(employeeRepo.findById(any(Integer.class))).thenReturn(getChef());
        Employee employee = employeeRepo.findById(DEFAULT_ENTITY_ID).get();
        Order order = getOrder();
        orderService.takeOrder(employee.getId(), order);
        assertEquals(order.getStaff().get(0), employee);
    }

    @Test
    @DisplayName("When employee is chef then add employee id to order staff")
    void takeOrderWhenOrderStatusIsNotUnsuitable() {
        Order order = getOrder();
        order.setOrderStatus(OrderStatus.COMPLETE);
        assertThrows(OrderException.class, ()->orderService.takeOrder(DEFAULT_ENTITY_ID, order));
    }

    @Test
    @DisplayName("When employee is not chef then throw exception")
    void takeOrderWhenEmployeeNotChef() {
        when(employeeRepo.findById(any(Integer.class))).thenReturn(getWaiter());
        Employee employee = employeeRepo.findById(DEFAULT_ENTITY_ID).get();
        assertThrows(OrderException.class, () -> orderService.takeOrder(employee.getId(), getOrder()));
    }

    @Test
    @DisplayName("When employee is empty then throw exception 'No such employee'")
    void takeOrderWhenEmployeeNotExists() {
        when(employeeRepo.findById(any(Integer.class))).thenReturn(Optional.empty());
        assertThrows(OrderException.class, () -> orderService.takeOrder(DEFAULT_ENTITY_ID, getOrder()));
    }

    @Test
    @DisplayName("If employee is Manager then add employee id to order staff")
    void createOrder() {
        when(employeeRepo.findById(any(Integer.class))).thenReturn(getManager());
        Employee employee = employeeRepo.findById(DEFAULT_ENTITY_ID).get();
        employee.setId(DEFAULT_ENTITY_ID);
        Order order = getOrder();
        orderService.createOrder(DEFAULT_ENTITY_ID, order);
        assertTrue(order.getStaff().get(0).getId() == employee.getId());
    }

    @Test
    @DisplayName("If employee is Waiter then add employee id to order staff")
    void createOrderWhenEmployeeIsWaiter() {
        when(employeeRepo.findById(any(Integer.class))).thenReturn(getWaiter());
        Employee employee = employeeRepo.findById(DEFAULT_ENTITY_ID).get();
        employee.setId(DEFAULT_ENTITY_ID);
        Order order = getOrder();
        orderService.createOrder(DEFAULT_ENTITY_ID, order);
        assertTrue(order.getStaff().get(0).getId() == employee.getId());
    }

    @Test
    @DisplayName("If employee is not Manager or Waiter then throw exception Permission denied")
    void createOrderWhenEmployeeHasNotRights() {
        when(employeeRepo.findById(any(Integer.class))).thenReturn(getChef());
        Employee employee = employeeRepo.findById(DEFAULT_ENTITY_ID).get();
        Order order = getOrder();
        assertThrows(OrderException.class, () -> orderService.createOrder(DEFAULT_ENTITY_ID, order) );
    }

    @Test
    @DisplayName("Should change status")
    void setOrderStatus() {
        when(orderRepository.findById(DEFAULT_ENTITY_ID)).thenReturn(Optional.of(new Order(OrderStatus.NONE, DEFAULT_ENTITY_ID)));
        Order order = orderRepository.findById(DEFAULT_ENTITY_ID).get();
        orderService.setOrderStatus(DEFAULT_ENTITY_ID, OrderStatus.COMPLETE);
        assertEquals(order.getOrderStatus(), OrderStatus.COMPLETE);
    }

    @Test
    @DisplayName("Should thrown exception")
    void setOrderStatusWhenOrderNotExists() {
        when(orderRepository.findById(DEFAULT_ENTITY_ID)).thenReturn(Optional.empty());
        assertThrows(OrderException.class, ()->orderService.setOrderStatus(DEFAULT_ENTITY_ID, OrderStatus.COMPLETE));
    }

    @Test
    @DisplayName("Update order when order exists")
    void updateOrderWhenOrderExists(){
        Order order = getOrder();
        when(orderRepository.findById(any(Integer.class))).thenReturn(Optional.of(order));
        orderService.updateOrder(order);
        verify(orderRepository).update(order);
    }

    @Test
    @DisplayName("Update order when order not exists, will throw exception")
    void updateOrderWhenOrderNotExists(){
        Order order = getOrder();
        when(orderRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        assertThrows(OrderException.class, ()->orderService.updateOrder(order));
    }

    private Order getOrder(){
        when(orderRepository.findById(any(Integer.class))).thenReturn(Optional.of(new Order( OrderStatus.NONE, DEFAULT_ENTITY_ID)));
        return new Order(OrderStatus.NONE, DEFAULT_ENTITY_ID);
    }

    private Optional<Employee> getChef(){
        return Optional.of(new Chef( "Chef", "chef@mail.ru", "+380973333333", LocalDate.now(), any(Branch.class)));
    }

    private Optional<Employee> getWaiter(){
        return Optional.of(new Waiter( "Waiter", "waiter@mail.ru", "+380973333333", LocalDate.now(), any(Branch.class)));
    }

    private Optional<Employee> getManager(){
        return Optional.of(new Manager("Manager", "manag@mail.ru", "+380973333333", LocalDate.now(), any(Branch.class)));
    }
}