package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.model.entity.enumerations.SeatType;
import com.ra.course.restaurant.model.entity.enumerations.TableStatus;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Table;
import com.ra.course.restaurant.model.entity.restaurant.TableChart;
import com.ra.course.restaurant.model.entity.restaurant.TableSeat;
import com.ra.course.restaurant.persistence.restaurant.TableRepository;
import com.ra.course.restaurant.persistence.restaurant.TableSeatRepository;
import com.ra.course.restaurant.service.exception.TableException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class TableServiceImplMockTest {

    public static final int DEFAULT_TABLE_CAPACITY = 8;
    public static final int TABLE_CAPACITY = 3;
    public static final int MAX_CAPACITY = 3;
    public static final int LOCATION_IDENTIFIER = 1;
    public static final int TABLE_ID = 1;
    public static final int TABLE_CHART_ID = 1;
    public static final int TABLE_QUANTITY = 3;
    public static final int SECOND_TABLE_CHART_ID = 2;
    public static final int EMPTY_TABLE_QUANTITY = 0;
    public static final int EMPTY_TABLE_CAPACITY = 0;
    @InjectMocks
    private TableServiceImpl tableService;
    @Mock
    private BranchServiceImpl branchService;
    @Mock
    private TableRepository tableRepository;
    @Mock
    private TableSeatRepository tableSeatRepository;


    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @DisplayName("Will return not null")
    void getTableRepository() {
        assertNotNull(tableService.getTableRepository());
    }

    @Test
    @DisplayName("Will return not null")
    void getTableSeatRep() {
        assertNotNull(tableService.getTableSeatRep());
    }

    @Test
    @DisplayName("Add table when already filled")
    void addTableWhenAlreadyFilled() {
        //given
        TableChart tableChart = getTableChart(TABLE_CHART_ID);
        tableChart.setTableQuantity(new AtomicInteger(TABLE_QUANTITY));
        //when
        when(branchService.getTableChartByID(TABLE_ID)).thenReturn(tableChart);
        //then
        assertThrows(TableException.class, ()->tableService.addTable(getTable(TABLE_ID, TABLE_ID), tableChart.getId()));

    }

    @Test
    @DisplayName("If table already in chart then throw exception")
    void addTableWhenNotFilledYetButTableAlreadyInChart() {
        //given
        TableChart tableChart = getTableChart(TABLE_CHART_ID);
        tableChart.setTableQuantity(new AtomicInteger(TABLE_QUANTITY));
        Table table = getTable(TABLE_ID, DEFAULT_TABLE_CAPACITY);
        table.setTableChart(tableChart);
        //when
        when(branchService.getTableChartByID(TABLE_ID)).thenReturn(tableChart);
        //then
        assertThrows(TableException.class, () -> tableService.addTable(table, tableChart.getId()));
    }

    @Test
    @DisplayName("If table not in chart and we have enough space then add table")
    void addTableWhenNotFilledYet() {
        //given
        TableChart tableChart = getTableChart(TABLE_CHART_ID);
        tableChart.setTableQuantity(new AtomicInteger(EMPTY_TABLE_QUANTITY));
        Table table = getTable(TABLE_ID, EMPTY_TABLE_CAPACITY);
        table.setTableChart(getTableChart(SECOND_TABLE_CHART_ID));
        //when
        when(branchService.getTableChartByID(TABLE_ID)).thenReturn(tableChart);
        tableService.addTable(table, tableChart.getId());
        //then
        verify(tableRepository).add(table);
    }

    @Test
    @DisplayName("Remove table from repo if table exists")
    void updateTable() {
        //given
        Table table = getTable(TABLE_ID, DEFAULT_TABLE_CAPACITY);
        //when
        when(tableRepository.findById(anyInt())).thenReturn(Optional.of(table));
        tableService.updateTable(table);
        //then
        verify(tableRepository).update(table);
    }

    @Test
    @DisplayName("If table not exists then throw exception")
    void updateTableWhenNotExists() {
        //given
        Table table = getTable(TABLE_ID, DEFAULT_TABLE_CAPACITY);
        //when
        when(tableRepository.findById(anyInt())).thenReturn(Optional.empty());
        //then
        assertThrows(TableException.class, ()->tableService.updateTable(table));
    }

    @Test
    @DisplayName("If table not exists then throw exception")
    void removeTableWhenNotExists() {
        //given
        Table table = getTable(TABLE_ID, DEFAULT_TABLE_CAPACITY);
        //when
        when(tableRepository.findById(anyInt())).thenReturn(Optional.empty());
        //then
        assertThrows(TableException.class, ()->tableService.removeTable(table.getId()));
    }

    @Test
    @DisplayName("Remove table, standard behaviour")
    void removeTable() {
        //given
        TableChart tableChart = getTableChart(TABLE_CHART_ID);
        tableChart.setTableQuantity(new AtomicInteger(TABLE_QUANTITY));
        Table table = getTable(TABLE_ID, DEFAULT_TABLE_CAPACITY);
        table.setTableChart(tableChart);
        //when
        when(tableRepository.findById(anyInt())).thenReturn(Optional.of(table));
        tableService.removeTable(TABLE_ID);
        //then
        verify(tableRepository).delete(table);
    }

    @Test
    @DisplayName("Add tableSeat, standard behaviour")
    void addTableSeat() {
        //given
        int tableID = TABLE_ID;
        Table table = getTable(tableID, DEFAULT_TABLE_CAPACITY);
        TableSeat tableSeat = getTableSeats(tableID);
        //when
        tableService.addTableSeat(tableSeat);
        //then
        verify(tableSeatRepository).add(tableSeat);
    }

    @Test
    @DisplayName("Add tableSeat when table not exists throw exception")
    void addTableSeatWhenTableNotExists() {
        //given
        int tableID = TABLE_ID;
        TableSeat tableSeat = getTableSeats(tableID);
        //when
        when(tableRepository.findById(tableSeat.getId())).thenReturn(Optional.empty());
        //then
        assertThrows(TableException.class, ()->tableService.addTableSeat(tableSeat));
    }

    @Test
    @DisplayName("When reached maximum capacity then throw exception")
    void addTableSeatWhenReachedMaxCapacity() {
        //given
        int tableID = TABLE_ID;
        Table table = getTable(tableID, EMPTY_TABLE_CAPACITY);
        TableSeat tableSeat = getTableSeats(tableID);
        //when
        when(tableSeatRepository.getTableSeatsByTableID(anyInt())).thenReturn(List.of(getTableSeats(TABLE_ID), getTableSeats(TABLE_ID)));
        //then
        assertThrows(TableException.class, ()->tableService.addTableSeat(tableSeat));
    }

    @Test
    @DisplayName("If tableseat already exists then throw exception")
    void addTableSeatWhenAlreadyInBase() {
        //given
        int tableID = TABLE_ID;
        Table table = getTable(tableID, DEFAULT_TABLE_CAPACITY);
        TableSeat tableSeat = getTableSeats(tableID);
        //when
        when(tableSeatRepository.getTableSeatsByTableID(anyInt())).thenReturn(List.of(tableSeat, tableSeat));
        when(tableSeatRepository.findById(anyInt())).thenReturn(Optional.of(tableSeat));
        //then
        assertThrows(TableException.class, ()->tableService.addTableSeat(tableSeat));
    }

    @Test
    @DisplayName("If tableseat not in base then add tableSeat")
    void updateTableSeatWhenSeatNotExists() {
        //given
        TableSeat tableSeat = getTableSeats(TABLE_ID);
        //when
        when(tableSeatRepository.findById(anyInt())).thenReturn(Optional.empty());
        //then
        assertThrows(TableException.class, ()->tableService.updateTableSeat(tableSeat));
    }

    @Test
    @DisplayName("If tableseat in base then update record")
    void updateTableSeat() {
        //given
        TableSeat tableSeat = getTableSeats(TABLE_ID);
        //when
        when(tableSeatRepository.findById(anyInt())).thenReturn(Optional.of(tableSeat));
        tableService.updateTableSeat(tableSeat);
        //then
        verify(tableSeatRepository).update(tableSeat);
    }

    @Test
    @DisplayName("Will return not null")
    void getAllTables() {
        assertNotNull(tableService.getAllTables(any(Branch.class)));
    }

    @Test
    @DisplayName("Will return not null")
    void getBranchService() {
        assertNotNull(tableService.getBranchService());
    }

    private Table getTable(int tabid, int capacity){
        Table table = new Table(TableStatus.FREE, MAX_CAPACITY, LOCATION_IDENTIFIER);
        table.setMaxCapacity(capacity);
        table.setTableSeats(List.of(getTableSeats(tabid), getTableSeats(tabid)));
        when(tableRepository.findById(tabid)).thenReturn(Optional.of(table));
        return table;
    }

    private TableChart getTableChart(int id){
        TableChart tableChart = new TableChart("some/path", "br1", TABLE_CAPACITY);
        tableChart.setId(id);
        when(branchService.getTableChartByID(id)).thenReturn(tableChart);
        return tableChart;
    }

    private TableSeat getTableSeats(int tabid){
        return new TableSeat(SeatType.REGULAR, tabid);
    }
}