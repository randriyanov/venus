package com.ra.course.restaurant.service.person;

import com.ra.course.restaurant.model.entity.enumerations.EmployeeRole;
import com.ra.course.restaurant.model.entity.person.Employee;
import com.ra.course.restaurant.model.entity.person.Manager;
import com.ra.course.restaurant.model.entity.person.Waiter;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;
import com.ra.course.restaurant.service.exception.EntityNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class EmployeeServiceImplMockTest {

    public static final int DEFAULT_EMPLOYEE_ID = 1;
    @InjectMocks
    private EmployeeServiceImpl employeeService;

    @Mock
    private EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @DisplayName("Will return not null")
    void getPersonRepo(){
        assertNotNull(employeeService.getPersonRepo());
    }

    @Test
    @DisplayName("Will return needed employee")
    void getEmployeeByID(){
        //given
        Employee manager = getManager();
        //when
        when(employeeRepository.findById(any(Integer.class))).thenReturn(Optional.of(manager));
        employeeService.getEmployeeByID(manager.getId());
        //then
        verify(employeeRepository).findById(manager.getId());
    }

    @Test
    @DisplayName("If employee has rights then return true")
    void checkAccessWhenHasNotRights(){
        //given
        Employee manager = getManager();
        List<EmployeeRole> roles = List.of(EmployeeRole.MANAGER, EmployeeRole.WAITER);
        //when
        when(employeeRepository.findById(anyInt())).thenReturn(Optional.of(manager));
        boolean result = employeeService.checkAccess(manager.getId(), roles);
        //then
        assertTrue(result);
    }

    @Test
    @DisplayName("If employee has not rights then return false")
    void checkAccessWhenHasRights(){
        //given
        Employee manager = getManager();
        List<EmployeeRole> roles = List.of(EmployeeRole.RECEPTIONIST, EmployeeRole.WAITER);
        //when
        when(employeeRepository.findById(anyInt())).thenReturn(Optional.of(manager));
        boolean result = employeeService.checkAccess(manager.getId(), roles);
        //then
        assertFalse(result);
    }

    @Test
    @DisplayName("If employee not exists then return false")
    void checkAccessWhenEmployeeNotPresent(){
        //given
        final int emplId = DEFAULT_EMPLOYEE_ID;
        List<EmployeeRole> roles = List.of(EmployeeRole.RECEPTIONIST, EmployeeRole.WAITER);
        //when
        when(employeeRepository.findById(anyInt())).thenReturn(Optional.empty());
        boolean result = employeeService.checkAccess(emplId, roles);
        //then
        assertFalse(result);
    }

    @Test
    @DisplayName("If employee already in base than throw Exception(already exists)")
    void addEmployeeWhenAlreadyExists() {
        //given
        Employee manager = getManager();
        //when
        when(employeeRepository.findById(anyInt())).thenReturn(Optional.of(manager));
        //then
        assertThrows(EntityNotFoundException.class, ()->{
            employeeService.addEmployee(manager);});
    }

    @Test
    @DisplayName("If employee not in base then it is successfully added)")
    void addEmployeeShouldBeAdded() {
        //given
        Employee manager = getManager();
        //when
        employeeService.addEmployee(manager);
        //then
        verify(employeeRepository).add(manager);
    }

    @Test
    @DisplayName("Will return list by role type")
    void getEmployeesByRole(){
        //Given role
        EmployeeRole role = EmployeeRole.MANAGER;
        //when
        when(employeeRepository.getEmployeeByRole(role)).thenReturn(getManagersList());
        List<Employee> employees = employeeService.getEmployeesByRole(role);
        //then
        Assertions.assertEquals(employees.size(), 2);
    }

    @Test
    @DisplayName("If employee exists then not exception")
    void updateEmployeeIfEmployeeExists(){
        //given
        Optional<Employee> employee = getEmployee();
        //when
        when(employeeRepository.findById(any(Integer.class))).thenReturn(employee);
        //then
        Assertions.assertDoesNotThrow(() -> employeeService.updateEmployee(employee.get()));
    }

    @Test
    @DisplayName("If employee does not exists then throw exception")
    void updateEmployeeIfEmployeeNotExists(){
        assertThrows(EntityNotFoundException.class, () -> employeeService.updateEmployee(getEmployee().get()));
    }

    private Manager getManager(){
        return new Manager( "Vanya", "email", "phone" , LocalDate.now(), Mockito.mock(Branch.class));
    }

    private List<Employee> getManagersList(){
        Branch branch = mock(Branch.class);
        return List.of(new Manager( "Manager1", "manag1@email.ru", "+38050", LocalDate.now(), branch), new Manager( "Manager2", "manag2@email.ru", "+38098", LocalDate.now(), branch));
    }

    private Optional<Employee> getEmployee(){
        Branch branch = mock(Branch.class);
        return Optional.of(new Waiter("Volodya", "volodya@mail.ru", "+380506666666", LocalDate.now(), branch));
    }
}