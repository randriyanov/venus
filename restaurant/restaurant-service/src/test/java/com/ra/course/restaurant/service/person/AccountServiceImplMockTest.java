package com.ra.course.restaurant.service.person;

import com.ra.course.restaurant.model.entity.basic.Address;
import com.ra.course.restaurant.model.entity.enumerations.AccountStatus;
import com.ra.course.restaurant.model.entity.person.Account;
import com.ra.course.restaurant.model.entity.person.Employee;
import com.ra.course.restaurant.model.entity.person.Manager;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.persistence.person.AccountRepository;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;
import com.ra.course.restaurant.service.exception.EntityNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


class AccountServiceImplMockTest {

    public static final String DEFAULT_EMAIL = "test@mail.ru";
    public static final String DEFAULT_PASSWORD = "password";
    public static final int DEFAULT_EMPLOYEE_ID = 1;
    @InjectMocks
    private AccountServiceImpl accountService;

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @DisplayName("Will return not null")
    void getAccountRepository(){
        Assertions.assertTrue(accountService.getAccountRepository() != null);
    }

    @Test
    @DisplayName("Will return not null")
    void getEmployeeRepository(){
        Assertions.assertTrue(accountService.getEmployeeData() != null);
    }


    @Test
    @DisplayName("If account and employee exists than account status will be changed to Active")
    void loginToAccountWillChangeStatusToActive(){
        // Given employee which exists
        Employee manager = getEmployee(DEFAULT_EMAIL);
        //Given an account which already exists
        Account account = getAccountByEmployeeId(DEFAULT_EMAIL, DEFAULT_PASSWORD, AccountStatus.OFFLINE);
        // Set missing attributes
        manager.setAccount(account);

        // Before login status should be OFFLINE
        Assertions.assertSame(account.getAccountStatus(), AccountStatus.OFFLINE);
        accountService.login(DEFAULT_EMAIL, DEFAULT_PASSWORD);

        // Affter login status should be ACTIVE
        Assertions.assertSame(account.getAccountStatus(), AccountStatus.ACTIVE);
    }

    @Test
    @DisplayName("If employee or account not exists than thrown Exception wrong email or pass")
    void loginToAccountWhenEmployeeNotExist(){
        Assertions.assertThrows(EntityNotFoundException.class, ()->accountService.login(DEFAULT_EMAIL, DEFAULT_PASSWORD));
    }

    @Test
    @DisplayName("If employee exists but account not exists than thrown Exception")
    void loginToAccountWhenAccountNotExistsYet(){
        //given
        Employee manager = getEmployee(DEFAULT_EMAIL);
        manager.setAccount(any(Account.class));
        //then
        Assertions.assertThrows(EntityNotFoundException.class, ()->accountService.login(DEFAULT_EMAIL, DEFAULT_PASSWORD));
    }

    @Test
    @DisplayName("If password not equals than thrown Exception wrong email or pass")
    void loginToAccountWhenPasswordIsWrong(){
        //given
        Employee manager = getEmployee(DEFAULT_EMAIL);
        Account account = getAccountByEmployeeId(DEFAULT_EMAIL, "another pass", AccountStatus.OFFLINE);
        // Set missing attributes
        manager.setAccount(account);
        //then
        Assertions.assertThrows(EntityNotFoundException.class, ()->accountService.login(DEFAULT_EMAIL, DEFAULT_PASSWORD));
    }

    @Test
    @DisplayName("Thrown exception if account already exists")
    void registerAccountWhenAccountAlreadyExists(){
        //given an existent Account
        Account account = getAccountByEmployeeId(DEFAULT_EMAIL, DEFAULT_PASSWORD, AccountStatus.ACTIVE);
        //Should throw Exception
        Assertions.assertThrows(EntityNotFoundException.class,()->accountService.registerAccount(account));
    }

    @Test
    @DisplayName("If exception does not thrown than account normally added")
    void registerAccountNormalBehavior(){
        //given an Account which not exist in db
        Account account = Optional.of(new Account( "pass1", any(Address.class), AccountStatus.ACTIVE, DEFAULT_EMPLOYEE_ID)).get();
        //Than service will not throw any exception
        Assertions.assertDoesNotThrow(()-> accountService.registerAccount(account));
    }

    @Test
    @DisplayName("If account exists than account status will be changed to Cancel")
    void cancelAccountWillChangeStatusToCancel(){
        //Given an account which already exists
        Account account = getAccountByEmployeeId(DEFAULT_EMAIL, DEFAULT_PASSWORD, AccountStatus.ACTIVE);
        //when
        accountService.cancelAccount(account.getId());
        //then
        Assertions.assertSame(account.getAccountStatus(), AccountStatus.CANCELED);
    }

    @Test
    @DisplayName("If we try to cancel Account but it not exists yet, should thrown exception")
    void cancelAccountWhenAccountNotExists(){
        Assertions.assertThrows(EntityNotFoundException.class,()->accountService.cancelAccount(12));
    }

    @Test
    @DisplayName("If account logged out than change Status to OFFLINE")
    void logoutWillChangeStatusToOffline(){
        //given
        Employee manager = getEmployee(DEFAULT_EMAIL);
        Account account = getAccountByEmployeeId(DEFAULT_EMAIL, DEFAULT_PASSWORD, AccountStatus.ACTIVE);
        manager.setAccount(account);
        //when
        accountService.logout(DEFAULT_EMPLOYEE_ID);
        //then
        Assertions.assertSame(account.getAccountStatus(), AccountStatus.OFFLINE);
    }

    @Test
    @DisplayName("If account try to logout than throw exception")
    void logoutWillThrowException(){
        Assertions.assertThrows(EntityNotFoundException.class, ()->{accountService.logout(DEFAULT_EMPLOYEE_ID);});
    }

    @Test
    @DisplayName("If reset password than in account we should see new password")
    void resetPasswordWillChangePasswordInAccount(){
        //given
        String newPass = "newPass";
        Employee manager = getEmployee(DEFAULT_EMAIL);
        Account account = getAccountByEmployeeId(DEFAULT_EMAIL, DEFAULT_PASSWORD, AccountStatus.ACTIVE);
        manager.setAccount(account);
        //when
        accountService.resetPassword(DEFAULT_EMPLOYEE_ID, newPass);
        //then
        Assertions.assertSame(account.getPassword(), newPass);
    }

    @Test
    @DisplayName("If reset password and account not exists than throw exception")
    void resetPasswordWhenAccountNotExists(){
        //given
        String newPass = "newPass";
        //then
        Assertions.assertThrows(EntityNotFoundException.class, ()->accountService.resetPassword(DEFAULT_EMPLOYEE_ID, newPass));
    }

    private Employee getEmployee(String email){
        when(employeeRepository.getEmployeeByEmail(email)).thenReturn(Optional.of(new Manager("Stas", email, "38050", LocalDate.now(), any(Branch.class))));
        return employeeRepository.getEmployeeByEmail(email).get();
    }

    private Account getAccountByEmployeeId(String email, String password, AccountStatus status){
        when(accountRepository.getAccountByEmployeeId(any(Integer.class))).thenReturn(Optional.of(new Account( password, any(Address.class), status, DEFAULT_EMPLOYEE_ID)));
        return accountRepository.getAccountByEmployeeId(DEFAULT_EMPLOYEE_ID).get();
    }

    private Account getAccountById(String email, String password){
        when(accountRepository.findById(any(Integer.class))).thenReturn(Optional.of(new Account( password, any(Address.class), AccountStatus.OFFLINE, DEFAULT_EMPLOYEE_ID)));
        return accountRepository.findById(DEFAULT_EMPLOYEE_ID).get();
    }


}