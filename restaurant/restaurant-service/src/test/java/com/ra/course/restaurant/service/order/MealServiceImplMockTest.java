package com.ra.course.restaurant.service.order;

import com.ra.course.restaurant.model.entity.menu.Meal;
import com.ra.course.restaurant.model.entity.menu.MealItem;
import com.ra.course.restaurant.model.entity.menu.MenuItem;
import com.ra.course.restaurant.persistence.order.MealItemRepository;
import com.ra.course.restaurant.persistence.order.MealRepository;
import com.ra.course.restaurant.service.exception.MealException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class MealServiceImplMockTest {
    public static final int DEFAULT_MEAL_ITEM_QUANTITY = 1;
    public static final int DEFAULT_ENTITY_ID = 1;
    public static final int POSITIVE_QUANTITY = 150;
    public static final int NEGATIVE_QUANTITY = -5;
    public static final BigDecimal DEFAULT_PRICE_VALUE = new BigDecimal(100);

    private MealService mealService;
    @Mock
    private MealRepository mealRepository;
    @Mock
    private MealItemRepository mealItemRepository;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
        mealService = new MealServiceImpl(mealRepository, mealItemRepository);
    }

    @Test
    void getMealRepository() {
        assertNotNull(mealService.getMealRepository());
    }

    @Test
    void getMealItemRepo() {
        assertNotNull(mealService.getMealItemRepo());
    }

    @Test
    @DisplayName("If meal not exists then no exception")
    void addMeal() {
        when(mealRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        assertDoesNotThrow(() -> mealService.addMeal(getMeal().get()));
    }

    @Test
    @DisplayName("Update meal when meal not exists")
    void updateMealWhenNotExists() {
        assertThrows(MealException.class, () -> mealService.updateMeal(getMeal().get()));
    }

    @Test
    @DisplayName("Update meal when meal exists")
    void updateMealWhenMealExists() {
        when(mealRepository.findById(any(Integer.class))).thenReturn(getMeal());
        assertDoesNotThrow(() -> mealService.updateMeal(getMeal().get()));
    }

    @Test
    @DisplayName("If meal already exists then throw exception")
    void addMealWhichAlreadyExists() {
        when(mealRepository.findById(any(Integer.class))).thenReturn(getMeal());
        assertThrows(MealException.class, () -> mealService.addMeal(getMeal().get()));
    }

    @Test
    @DisplayName("If meal not exists then throw exception")
    void removeMealWhenMealNotExists() {
        when(mealRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        assertThrows(MealException.class, () -> {
            mealService.removeMeal(DEFAULT_ENTITY_ID);
        });
    }

    @Test
    @DisplayName("If meal exists then doesn't throw exception")
    void removeMealWhenMealExists() {
        when(mealRepository.findById(any(Integer.class))).thenReturn(getMeal());
        when(mealItemRepository.findById(any(Integer.class))).thenReturn(getMealItem());
        when(mealItemRepository.getMealItemsByMealId(DEFAULT_ENTITY_ID)).thenReturn(List.of(getMealItem().get()));
        assertDoesNotThrow(() -> {
            mealService.removeMeal(DEFAULT_ENTITY_ID);
        });
    }

    @Test
    @DisplayName("When mealItem already exists then throw exception")
    void addMealItemWhenAlreadyExists() {
        when(mealItemRepository.findById(any(Integer.class))).thenReturn(getMealItem());
        assertThrows(MealException.class, () -> mealService.addMealItem(getMealItem().get()));
    }

    @Test
    @DisplayName("When mealItem not exists then doesn't throw exception")
    void addMealItemWhenMealNotExists() {
        when(mealItemRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        assertDoesNotThrow(() -> {
            mealService.addMealItem(getMealItem().get());
        });
    }

    @Test
    @DisplayName("Update mealItem when mealItem not exists")
    void updateMealItemWhenNotExists() {
        assertThrows(MealException.class, () -> mealService.updateMealItem(getMealItem().get()));
    }

    @Test
    @DisplayName("Update meal when mealItem exists")
    void updateMealItemWhenExists() {
        when(mealItemRepository.findById(any(Integer.class))).thenReturn(getMealItem());
        assertDoesNotThrow(() -> mealService.updateMealItem(getMealItem().get()));
    }

    @Test
    @DisplayName("remove mealItem standard behaviour")
    void removeMealItemWhenItNotExists() {
        when(mealItemRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        Assertions.assertThrows(MealException.class, () -> mealService.removeMealItem(DEFAULT_ENTITY_ID));
    }

    @Test
    @DisplayName("remove mealItem when it not exists")
    void removeMealItem() {
        when(mealItemRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        Assertions.assertThrows(MealException.class, () -> mealService.removeMealItem(DEFAULT_ENTITY_ID));
    }

    @Test
    @DisplayName("When mealItem not exists then throw exception")
    void updateQuantityWhenMealItemNotExists() {
        when(mealItemRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        assertThrows(MealException.class, () -> mealService.updateQuantity(DEFAULT_ENTITY_ID, POSITIVE_QUANTITY));
    }

    @Test
    @DisplayName("When mealItem exists then quantity will be changed")
    void updateQuantityWhenMealItemExists() {
        when(mealItemRepository.findById(any(Integer.class))).thenReturn(getMealItem());
        assertDoesNotThrow(() -> {
            mealService.updateQuantity(DEFAULT_ENTITY_ID, POSITIVE_QUANTITY);
        });
    }

    @Test
    @DisplayName("When new price lower then 0")
    void updateQuantityWhenPriceNegative() {
        when(mealItemRepository.findById(any(Integer.class))).thenReturn(getMealItem());
        assertThrows(MealException.class, () -> {
            mealService.updateQuantity(DEFAULT_ENTITY_ID, NEGATIVE_QUANTITY);
        });
    }

    private Optional<Meal> getMeal() {
        return Optional.of(new Meal(DEFAULT_ENTITY_ID, DEFAULT_ENTITY_ID));
    }

    private Optional<MealItem> getMealItem() {
        return Optional.of(new MealItem(DEFAULT_ENTITY_ID, DEFAULT_MEAL_ITEM_QUANTITY, Mockito.mock(MenuItem.class)));
    }

    private MenuItem getMenuItem() {
        return new MenuItem("Item1", "Desc1", DEFAULT_PRICE_VALUE, DEFAULT_ENTITY_ID);
    }
}