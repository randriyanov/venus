package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.model.entity.enumerations.EmployeeRole;
import com.ra.course.restaurant.model.entity.enumerations.ReservationStatus;
import com.ra.course.restaurant.model.entity.enumerations.TableStatus;
import com.ra.course.restaurant.model.entity.person.Customer;
import com.ra.course.restaurant.model.entity.person.Employee;
import com.ra.course.restaurant.model.entity.person.Manager;
import com.ra.course.restaurant.model.entity.person.Waiter;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Reservation;
import com.ra.course.restaurant.model.entity.restaurant.Table;
import com.ra.course.restaurant.persistence.restaurant.ReservationRepository;
import com.ra.course.restaurant.service.exception.ReservationException;
import com.ra.course.restaurant.service.person.EmployeeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class ReservationServiceImplMockTest {

    private static final int PEOPLE_COUNT_10 = 10;
    private static final int PEOPLE_COUNT_20 = 20;
    private static final int PEOPLE_COUNT_5 = 5;
    private static final int LOCATION_IDENTIFIER = 1;
    private static final int MAX_CAPACITY = 22;
    private static final int PEOPLE_COUNT_2 = 2;
    private static final int TABLE_CAPACITY = 1;
    private static final int TABLE_CAPACITY_3 = 3;
    private static final int TABLE_CAPACITY_4 = 4;
    private static final int NOTIFY_INTERVAL_MINUTE = 1;
    private static final int HOURS_INTERVAL_2 = 2;
    private static final int HOURS_INTERVAL_1 = 1;
    private static final int HOURS_INTERVAL_3 = 3;
    private static final int DAYS_INTERVAL_1 = 1;
    private final List<Table> tables = List.of(getTable(TABLE_CAPACITY), getTable(TABLE_CAPACITY));
    @InjectMocks
    private ReservationServiceImpl reservationService;
    @Mock
    private ReservationRepository reservationRepository;
    @Mock
    private EmployeeService employeeService;
    private final TableService tableService = Mockito.mock(TableServiceImpl.class);

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        reservationService.setTableService(tableService);
        CustomerService customerService = Mockito.mock(CustomerServiceImpl.class);
        reservationService.setCustomerService(customerService);
        NotificationService notificationService = Mockito.mock(NotificationServiceImpl.class);
        reservationService.setNotifService(notificationService);
    }

    @Test
    void getReservationRep() {
        assertNotNull(reservationService.getReservationRep());
    }

    @Test
    void getEmployeeService() {
        assertNotNull(reservationService.getEmployeeService());
    }

    @Test
    void createReservationWhenNotEnoughSeats() {
        //given
        Reservation reservation = getReservation(PEOPLE_COUNT_10, LocalDateTime.now().plusHours(HOURS_INTERVAL_2));
        int employeeID = 1;
        //then
        assertThrows(ReservationException.class, ()->reservationService.createReservation(employeeID, reservation, tables));
    }

    @Test
    @DisplayName("Try to create reservation when employee has no rights")
    void createReservationWhenEmployeeHasNoRights() {
        //given
        Reservation reservation = getReservation(PEOPLE_COUNT_5, LocalDateTime.now().plusHours(HOURS_INTERVAL_2));
        Employee employee = getEmployee();
        //when
        when(employeeService.checkAccess(employee.getId(), List.of(EmployeeRole.MANAGER, EmployeeRole.RECEPTIONIST))).thenReturn(false);
        //then
        assertThrows(ReservationException.class, ()->reservationService.createReservation(employee.getId(), reservation, tables));
    }

    @Test
    @DisplayName("Create reservation when already exists")
    void createReservationWhenEmployeeHasRightsAndReservationAlreadyExists() {
        //given
        Reservation reservation = getReservation(PEOPLE_COUNT_5, LocalDateTime.now().plusHours(HOURS_INTERVAL_2));
        Employee employee = getEmployee();
        //when
        when(employeeService.checkAccess(employee.getId(), List.of(EmployeeRole.MANAGER, EmployeeRole.RECEPTIONIST))).thenReturn(true);
        when(reservationRepository.findById(anyInt())).thenReturn(Optional.of(reservation));
        //then
        assertThrows(ReservationException.class, ()->reservationService.createReservation(employee.getId(), reservation, tables));
    }

    @Test
    @DisplayName("Create reservation")
    void createReservationWhenEmployeeHasRightsAndReservationNotExists() {
        //given
        Reservation reservation = getReservation(PEOPLE_COUNT_2, LocalDateTime.now().plusHours(HOURS_INTERVAL_2));
        Employee employee = getEmployee();
        //when
        when(employeeService.checkAccess(employee.getId(), List.of(EmployeeRole.MANAGER, EmployeeRole.RECEPTIONIST))).thenReturn(true);
        when(reservationRepository.findById(anyInt())).thenReturn(Optional.empty());
        reservationService.createReservation(employee.getId(), reservation, tables);
        //then
        verify(reservationRepository).add(reservation);
    }

    @Test
    @DisplayName("Update reservation")
    void updateReservation() {
        //given
        Reservation reservation = getReservation(PEOPLE_COUNT_5, LocalDateTime.now().plusHours(HOURS_INTERVAL_2));
        //when
        when(reservationRepository.findById(anyInt())).thenReturn(java.util.Optional.of(reservation));
        reservationService.updateReservation(reservation);
        //then
        verify(reservationRepository).update(reservation);
    }

    @Test
    @DisplayName("Update reservation when already exists, throw exception")
    void updateReservationWhenNotExists() {
        //given
        Reservation reservation = getReservation(PEOPLE_COUNT_5, LocalDateTime.now().plusHours(HOURS_INTERVAL_2));
        //when
        when(reservationRepository.findById(anyInt())).thenReturn(java.util.Optional.empty());
        //then
        assertThrows(ReservationException.class, ()->reservationService.updateReservation(reservation));
    }

    @Test
    @DisplayName("When reservation not exists, throw exception")
    void updatePeopleCountWhenReservationNotExists() {
        //given
        Reservation reservation = getReservation(PEOPLE_COUNT_2, LocalDateTime.now().plusHours(HOURS_INTERVAL_2));
        //when
        when(reservationRepository.findById(anyInt())).thenReturn(Optional.empty());
        //then
        assertThrows(ReservationException.class, ()->reservationService.updatePeopleCount(reservation.getId(), PEOPLE_COUNT_5, reservation.getTableList()));
    }

    @Test
    @DisplayName("Update people count when there are more people than there were")
    void updatePeopleCountWhenPeopleMoreThenBefore() {
        //given
        Reservation reservation = getReservation(PEOPLE_COUNT_10, LocalDateTime.now().plusHours(HOURS_INTERVAL_2));
        //when
        when(reservationRepository.findById(anyInt())).thenReturn(Optional.of(reservation));
        reservationService.updatePeopleCount(reservation.getId(), PEOPLE_COUNT_20, reservation.getTableList());
        //then
        verify(reservationRepository).update(reservation);
    }

    @Test
    @DisplayName("Update people count when there are less people than there were")
    void updatePeopleCountWhenPeopleLessThenBefore() {
        //given
        Reservation reservation = getReservation(PEOPLE_COUNT_10, LocalDateTime.now().plusHours(HOURS_INTERVAL_2));
        //when
        when(reservationRepository.findById(anyInt())).thenReturn(Optional.of(reservation));
        reservationService.updatePeopleCount(reservation.getId(), PEOPLE_COUNT_5, reservation.getTableList());
        //then
        verify(reservationRepository).update(reservation);
    }

    @Test
    @DisplayName("Checkin when customer has not reservations, throw exception")
    void checkInWhenCustomerHasNotReservations() {
        //given
        Customer customer = getCustomer();
        Reservation reservation = getReservation(PEOPLE_COUNT_10, LocalDateTime.now().plusHours(HOURS_INTERVAL_2));
        //when
        when(reservationRepository.getReservationsByCustomerID(anyInt())).thenReturn(List.of(reservation));
        //then
        assertThrows(ReservationException.class, ()->reservationService.checkIn(customer, LocalDateTime.now()));
    }

    @Test
    @DisplayName("Checkin when customer has reservations")
    void checkInWhenCustomerHasReservations() {
        //given
        Customer customer = getCustomer();
        Reservation reservation = getReservation(PEOPLE_COUNT_10, LocalDateTime.now().plusHours(HOURS_INTERVAL_2));
        //when
        when(reservationRepository.getReservationsByCustomerID(anyInt())).thenReturn(List.of(reservation));
        when(reservationRepository.findById(anyInt())).thenReturn(Optional.of(reservation));
        reservationService.checkIn(customer, reservation.getCheckinTime());
        //then
        verify(reservationRepository).update(reservation);
    }

    @Test
    @DisplayName("If no available tables then size 0")
    void getAvailableTablesWhenHasNotAvailableTables() {
        //given
        Reservation reservation = getReservation(PEOPLE_COUNT_5, LocalDateTime.now().plusHours(HOURS_INTERVAL_1));
        List<Table> tables = List.of(reservation.getTableList().get(0), reservation.getTableList().get(1));
        //when
        when(tableService.getAllTables(any(Branch.class))).thenReturn(tables);
        //then
        assertEquals(0, reservationService.getAvailableTables(LocalDateTime.now(), mock(Branch.class)).size());
    }

    @Test
    void getAvailableTablesWhenHasNotAvailableTablesAndDayNotEqual() {
        //given
        Reservation reservation = getReservation(PEOPLE_COUNT_5, LocalDateTime.now().plusHours(HOURS_INTERVAL_3).minusDays(DAYS_INTERVAL_1));
        List<Table> tables = List.of(reservation.getTableList().get(0), reservation.getTableList().get(1));
        //when
        when(tableService.getAllTables(any(Branch.class))).thenReturn(tables);
        //then
        assertEquals(2, reservationService.getAvailableTables(LocalDateTime.now(), mock(Branch.class)).size());
    }

    @Test
    void getAvailableTablesWhenHasAvailableTables() {
        //given
        Reservation reservation = getReservation(PEOPLE_COUNT_5, LocalDateTime.now().plusHours(HOURS_INTERVAL_3));
        List<Table> tables = List.of(reservation.getTableList().get(0), reservation.getTableList().get(1));
        //when
        when(tableService.getAllTables(any(Branch.class))).thenReturn(tables);
        assertEquals(2, reservationService.getAvailableTables(LocalDateTime.now(), mock(Branch.class)).size());
    }

    @Test
    @DisplayName("If future reservation exists then equals size")
    void getFutureReservations() {
        //given
        Reservation reservation2 = getReservation(PEOPLE_COUNT_5, LocalDateTime.now().plusHours(HOURS_INTERVAL_3));
        //when
        when(reservationRepository.getFutureReservations()).thenReturn(List.of(reservation2));
        //then
        assertEquals(1, reservationService.getFutureReservations().size());
    }

    @Test
    @DisplayName("Get all reservations which ready to send")
    void getReservationsToNotify() {
        //given
        Reservation reservation1 = getReservation(PEOPLE_COUNT_5, LocalDateTime.now().plusHours(HOURS_INTERVAL_1));
        Reservation reservation2 = getReservation(PEOPLE_COUNT_5, LocalDateTime.now().plusHours(HOURS_INTERVAL_1));
        //when
        when(reservationRepository.getReservationsToNotify(NOTIFY_INTERVAL_MINUTE)).thenReturn(List.of(reservation1, reservation2));
        assertEquals(2, reservationService.getReservationsToNotify(NOTIFY_INTERVAL_MINUTE).size());
    }

    @Test
    @DisplayName("Will return not null")
    void getTableService() {
        assertNotNull(reservationService.getTableService());
    }

    @Test
    @DisplayName("Will return not null")
    void getCustomerService() {
        assertNotNull(reservationService.getCustomerService());
    }

    @Test
    @DisplayName("Will return not null")
    void getNotifService() {
        assertNotNull(reservationService.getNotifService());
    }

    private Reservation getReservation(int peopleCount, LocalDateTime reservTime){
        Customer customer = getCustomer();
        Reservation reservation = new Reservation( LocalDateTime.now(), PEOPLE_COUNT_5, ReservationStatus.PENDING, "notes", null, customer);
        reservation.setPeopleCount(peopleCount);
        reservation.setCheckinTime(LocalDateTime.now());
        reservation.setTimeOfReservation(reservTime);
        Table table1 = getTable(TABLE_CAPACITY_3);
        Table table2 = getTable(TABLE_CAPACITY_4);
        table1.setReservations(List.of(reservation));
        table2.setReservations(List.of(reservation));
        reservation.setTableList(List.of(table1, table2));
        return reservation;
    }

    private Customer getCustomer(){
        return new Customer( "Vaasya", "+380506137777");
    }

    private Table getTable( int capacity){
        Table table = new Table(TableStatus.FREE, MAX_CAPACITY, LOCATION_IDENTIFIER);
        table.setMaxCapacity(capacity);
        return table;
    }

    private Manager getManager(){
        return new Manager( "Vanya", "email", "phone" , LocalDate.now(), Mockito.mock(Branch.class));
    }
    private Employee getEmployee(){
        Branch branch = mock(Branch.class);
        return new Waiter("Volodya", "volodya@mail.ru", "+380506666666", LocalDate.now(), branch);
    }
}