package com.ra.course.restaurant.service.menu;

import com.ra.course.restaurant.model.entity.menu.Menu;
import com.ra.course.restaurant.model.entity.menu.MenuItem;
import com.ra.course.restaurant.model.entity.menu.MenuSection;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.persistence.menu.MenuItemRepository;
import com.ra.course.restaurant.persistence.menu.MenuRepository;
import com.ra.course.restaurant.persistence.menu.MenuSectionRepository;
import com.ra.course.restaurant.persistence.menu.exceptions.NonPositivePriceException;
import com.ra.course.restaurant.service.exception.MenuException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class MenuServiceImplMockTest {

    private static final int DEFAULT_ENTITY_ID = 1;
    private static final BigDecimal DEFAULT_PRICE_VALUE = new BigDecimal(100);
    private static final int DEFAULT_ENTITY_SECOND_ID = 2;
    private static final BigDecimal NEGATIVE_PRICE = new BigDecimal(-3);
    private MenuService menuService;
    @Mock
    private MenuItemRepository menuItemRepository;
    @Mock
    private MenuSectionRepository menuSectionRepository;
    @Mock
    private MenuRepository menuRepository;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
        menuService = new MenuServiceImpl(menuItemRepository, menuSectionRepository, menuRepository);
    }

    @Test
    @DisplayName("When price is negative then throw exception")
    void updatePriceWhenPriceIsNegative() {
        //Given negative price
        assertThrows(NonPositivePriceException.class, ()->menuService.updatePrice(DEFAULT_ENTITY_ID, NEGATIVE_PRICE));
    }

    @Test
    @DisplayName("When price is positive but item not exists, throw exception")
    void updatePriceWhenPriceIsPositiveButItemNotExists() {
        //Given positive price
        when(menuItemRepository.findById(DEFAULT_ENTITY_ID)).thenReturn(Optional.empty());
        assertThrows(MenuException.class, ()->menuService.updatePrice(DEFAULT_ENTITY_ID, DEFAULT_PRICE_VALUE));
    }

    @Test
    @DisplayName("When price is positive and item exists then return true")
    void updatePriceWhenPriceIsPositiveAndItemExists() {
        //Given positive price and item
        BigDecimal newPrice = DEFAULT_PRICE_VALUE;
        MenuItem menuItem = getMenuItemByID(DEFAULT_ENTITY_ID);
        assertTrue(menuService.updatePrice(DEFAULT_ENTITY_ID, newPrice));
        assertEquals(menuItem.getPrice(), newPrice);
    }

    @Test
    @DisplayName("When item already in section then throw exception")
    void addMenuItemToSectionWhenItemAlreadyThere() {
        MenuSection menuSection = getMenuSectionByID(DEFAULT_ENTITY_ID);
        MenuItem menuItem = getMenuItemByID(DEFAULT_ENTITY_ID);
        List<MenuItem> menuItems = getItemsBySectionID(DEFAULT_ENTITY_ID);
        assertThrows(MenuException.class, ()->menuService.addMenuItemToSection(menuItem));
    }

    @Test
    @DisplayName("When section not exists then throw exception")
    void addMenuItemToSectionWhenSectionNotExists() {
        when(menuSectionRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        MenuItem menuItem = getMenuItemByID(DEFAULT_ENTITY_ID);
        assertThrows(MenuException.class, ()->menuService.addMenuItemToSection(menuItem));
    }

    @Test
    @DisplayName("When section exists and item not in section then return true")
    void addMenuItemToSectionStandardBehaviour() {
        MenuSection menuSection = getMenuSectionByID(DEFAULT_ENTITY_ID);
        MenuItem menuItem = getMenuItemByID(DEFAULT_ENTITY_ID);
        assertTrue(menuService.addMenuItemToSection(menuItem));
    }

    @Test
    void addMenuSectionToMenuWhenSectionNotExists() {
        Menu menu = getMenuByID();
        MenuSection menuSection = getMenuSectionByID(DEFAULT_ENTITY_ID);
        List<MenuSection> sections = List.of(getMenuSectionByID(DEFAULT_ENTITY_SECOND_ID));
        assertDoesNotThrow(()->menuService.addMenuSectionToMenu(menuSection));
    }

    @Test
    void addMenuSectionToMenuWhenSectionAlreadyExists() {
        Menu menu = getMenuByID();
        MenuSection menuSection = getMenuSectionByID(DEFAULT_ENTITY_ID);
        List<MenuSection> sections = getSectionsByMenuID();
        assertThrows(MenuException.class, ()->menuService.addMenuSectionToMenu(menuSection));
    }

    @Test
    void addMenuSectionToMenuWhenMenuNotExists() {
        when(menuSectionRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        MenuSection menuSection = getMenuSectionByID(DEFAULT_ENTITY_ID);
        assertThrows(MenuException.class, ()->menuService.addMenuSectionToMenu(menuSection));
    }

    @Test
    void getMenuItemRep() {
        assertNotNull(menuService.getMenuItemRep());
    }

    @Test
    void getMenuSectionRep() {
        assertNotNull(menuService.getMenuSectionRep());
    }

    @Test
    @DisplayName("If menu exists then not exception will be thrown")
    void updateMenu() {
        Menu menu = getMenuByID();
        assertDoesNotThrow(()->menuService.updateMenu(menu));
    }

    @Test
    @DisplayName("If menu not exists then throw exception")
    void updateMenuWhenMenuNotExists() {
        Menu menu = new Menu("Menu", "desc", any(Branch.class));
        assertThrows(MenuException.class, ()->menuService.updateMenu(menu));
    }

    @Test
    @DisplayName("When section exists then no exception")
    void updateMenuSection() {
        MenuSection menuSection = getMenuSectionByID(DEFAULT_ENTITY_ID);
        assertDoesNotThrow(()->menuService.updateMenuSection(menuSection));
    }

    @Test
    @DisplayName("When section not exists then throw exception")
    void updateMenuSectionWhenSectionNotExists() {
        MenuSection menuSection = new MenuSection(DEFAULT_ENTITY_ID, "Title", "desc");
        assertThrows(MenuException.class, ()->menuService.updateMenuSection(menuSection));
    }

    @Test
    @DisplayName("When menuItem exists then no exception")
    void updateMenuItem() {
        MenuItem menuItem = getMenuItemByID(DEFAULT_ENTITY_ID);
        assertDoesNotThrow(()->menuService.updateMenuItem(menuItem));
    }

    @Test
    @DisplayName("When menuItem not exists then throw exception")
    void updateMenuItemWhenItemNotExists() {
        MenuItem menuItem = new MenuItem("Title", "Description", DEFAULT_PRICE_VALUE, DEFAULT_ENTITY_ID);
        assertThrows(MenuException.class, ()->menuService.updateMenuItem(menuItem));
    }

    @Test
    void getMenuRep() {
    }

    @Test
    @DisplayName("If menu not exists then return true")
    void addMenu() {
        //given
        Menu menu = new Menu( "Menu1", "Description", Mockito.mock(Branch.class));
        menu.setId(DEFAULT_ENTITY_ID);
        //when
        when(menuRepository.findById(anyInt())).thenReturn(Optional.empty());
        menuService.addMenu(menu);
        //then
        verify(menuRepository).add(menu);
    }

    @Test
    @DisplayName("If menu already exists then throw exception")
    void addMenuWhenAlreadyExists() {
        Menu menu = getMenuByID();
        assertThrows(MenuException.class, ()->menuService.addMenu(menu));
    }

    @Test
    @DisplayName("When menuItem exists then no exception")
    void removeMenuItem() {
        MenuItem menuItem = getMenuItemByID(DEFAULT_ENTITY_ID);
        assertDoesNotThrow(()->menuService.removeMenuItem(DEFAULT_ENTITY_ID));
    }

    @Test
    @DisplayName("When menuItem not exists then throw exception")
    void removeMenuItemWhenItemNotExists() {
        assertThrows(MenuException.class, ()->menuService.removeMenuItem(DEFAULT_ENTITY_ID));
    }

    @Test
    @DisplayName("When menu section is deleted, then menu items are also deleted")
    void removeSection() {
        MenuSection menuSection = getMenuSectionByID(DEFAULT_ENTITY_ID);
        List<MenuItem> items = getItemsBySectionID(menuSection.getId());
        menuService.removeSection(DEFAULT_ENTITY_ID);
        Mockito.verify(menuItemRepository, times(DEFAULT_ENTITY_SECOND_ID)).delete(any(MenuItem.class));
        Mockito.verify(menuSectionRepository).delete(any(MenuSection.class));
    }

    private MenuItem getMenuItemByID(int id){
        MenuItem menuItem = new MenuItem("item1", "desc1", DEFAULT_PRICE_VALUE, id);
        menuItem.setId(id);
        when(menuItemRepository.findById(id)).thenReturn(Optional.of(menuItem));
        return menuItem;
    }

    private MenuSection getMenuSectionByID(int id){
        MenuSection menuSection = new MenuSection(id, "section1", "desc1");
        menuSection.setId(id);
        when(menuSectionRepository.findById(id)).thenReturn(Optional.of(menuSection));
        return menuSection;
    }

    private Menu getMenuByID(){
        Menu menu = new Menu("Menu", "Menu desc", Mockito.mock(Branch.class));
        menu.setId(DEFAULT_ENTITY_ID);
        when(menuRepository.findById(anyInt())).thenReturn(Optional.of(menu));
        return menu;
    }

    private List<MenuItem> getItemsBySectionID(int id){
        MenuItem menuItem1 = getMenuItemByID(DEFAULT_ENTITY_ID);
        MenuItem menuItem2 = getMenuItemByID(DEFAULT_ENTITY_SECOND_ID);
        when(menuItemRepository.getItemsBySectionID(id)).thenReturn(List.of(menuItem1, menuItem2));
        return List.of(menuItem1, menuItem2);
    }

    private List<MenuSection> getSectionsByMenuID(){
        MenuSection section1 = getMenuSectionByID(DEFAULT_ENTITY_ID);
        MenuSection section2 = getMenuSectionByID(DEFAULT_ENTITY_SECOND_ID);
        when(menuSectionRepository.getSectionsByMenuID(DEFAULT_ENTITY_ID)).thenReturn(List.of(section1, section2));
        return List.of(section1, section2);
    }
}