package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.model.entity.enumerations.ReservationStatus;
import com.ra.course.restaurant.model.entity.notification.Notification;
import com.ra.course.restaurant.model.entity.person.Customer;
import com.ra.course.restaurant.model.entity.restaurant.Reservation;
import com.ra.course.restaurant.model.entity.restaurant.Table;
import com.ra.course.restaurant.persistence.restaurant.NotificationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class NotificationServiceImplMockTest {

    public static final int DEFAULT_PEOPLE_COUNT = 3;
    public static final int DEFAULT_MINUTES_INTERVAL = 1;
    @InjectMocks
    private NotificationServiceImpl notificationService;

    @Mock
    private NotificationRepository notificationRepository;
    @Mock
    private ReservationService reservationService;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @DisplayName("When customer has only phone, send a notification via phone")
    void sendNotificationWhenCustomerHasOnlyPhone() {
        //given
        Customer customer = getCustomer();
        customer.setEmail("");
        String content = "any message";
        //when
        notificationService.sendNotification(content, customer);
        //then
        verify(notificationRepository).add(any(Notification.class));
    }

    @Test
    @DisplayName("When customer has both phone and e-mail, send notifications by all means")
    void sendNotificationWhenCustomerHasPhoneAndEmail() {
        //given
        Customer customer = getCustomer();
        customer.setEmail("any@mail.ru");
        String content = "any message";
        //when
        notificationService.sendNotification(content, customer);
        //then
        verify(notificationRepository, times(2)).add(any(Notification.class));
    }

    @Test
    @DisplayName("When creating a notification for a not updated reservation, the content should include word 'confirmed'")
    void createReservationNotificationContentWhenUpdateFalse() {
        //given
        Reservation reservation = getReservation(DEFAULT_PEOPLE_COUNT);
        //then
        assertTrue(notificationService.createReservationNotificationContent(reservation, false).contains("confirmed"));
    }

    @Test
    @DisplayName("When creating a notification for an updated reservation, the content should include word 'updated'")
    void createReservationNotificationContentWhenUpdateTrue() {
        //given
        Reservation reservation = getReservation(DEFAULT_PEOPLE_COUNT);
        reservation.setTableList(List.of(mock(Table.class)));
        //then
        assertTrue(notificationService.createReservationNotificationContent(reservation, true).contains("updated"));
    }

    @Test
    void getNotificationRep() {
        assertNotNull(notificationService.getNotificationRep());
    }

    @Test
    @DisplayName("When it is time to notify about a reservation, the notification is added to the repository")
    void reservationReminder() {
        //given
        Reservation reservation = getReservation(DEFAULT_PEOPLE_COUNT);
        reservation.setTableList(List.of(mock(Table.class)));
        //when
        when(reservationService.getReservationsToNotify(DEFAULT_MINUTES_INTERVAL)).thenReturn(List.of(reservation));
        notificationService.reservationReminder(DEFAULT_MINUTES_INTERVAL);
        //then
        verify(notificationRepository).add(any(Notification.class));
    }

    @Test
    void getReservService() {
        assertNotNull(notificationService.getReservService());
    }

    private Reservation getReservation(int peopleCount){
        Customer customer = getCustomer();
        return new Reservation( LocalDateTime.now(), DEFAULT_PEOPLE_COUNT, ReservationStatus.PENDING, "notes", null, customer);
    }

    private Customer getCustomer(){
        return new Customer( "Vasya", "+380506666666");
    }
}