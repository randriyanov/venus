package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.persistence.restaurant.RestaurantRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

class RestaurantServiceImplMockTest {

    @InjectMocks
    private RestaurantServiceImpl restaurantService;
    @Mock
    private RestaurantRepository restaurantRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @DisplayName("Update restaurant name")
    void updateName() {
        //given
        String restaurantName = "Restaurant";
        //when
        restaurantService.updateName(restaurantName);
        //then
        verify(restaurantRepository).updateName(restaurantName);
    }

}