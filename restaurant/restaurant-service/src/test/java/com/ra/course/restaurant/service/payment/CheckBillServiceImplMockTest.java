package com.ra.course.restaurant.service.payment;

import com.ra.course.restaurant.model.entity.enumerations.OrderStatus;
import com.ra.course.restaurant.model.entity.enumerations.PaymentType;
import com.ra.course.restaurant.model.entity.menu.Meal;
import com.ra.course.restaurant.model.entity.menu.MealItem;
import com.ra.course.restaurant.model.entity.menu.MenuItem;
import com.ra.course.restaurant.model.entity.order.Order;
import com.ra.course.restaurant.model.entity.payment.CheckBill;
import com.ra.course.restaurant.model.entity.person.Chef;
import com.ra.course.restaurant.model.entity.person.Employee;
import com.ra.course.restaurant.model.entity.person.Manager;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.persistence.order.OrderRepository;
import com.ra.course.restaurant.persistence.payment.CheckBillRepository;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;
import com.ra.course.restaurant.service.exception.BranchException;
import com.ra.course.restaurant.service.exception.CheckBillException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CheckBillServiceImplMockTest {

    private CheckBillService checkBillService;
    @Mock
    private CheckBillRepository checkBillRep;
    @Mock
    private EmployeeRepository employeeRep;
    @Mock
    private OrderRepository orderRepository;

    private static final int BILL_EMPLOYEE_ID = 1;
    private static final int DEFAULT_ID_FOR_ALL_ENTITIES = 1;
    private static final int DEFAULT_QUANTITY = 1;
    public static final int DEFAULT_ORDER_ID = 1;
    private static final Map<String, String> DEFAULT_EMPLOYEE_PARAMS = Map.of("name", "Vanya", "phone", "+380506666666", "email", "vano@mail.ru");
    private static final BigDecimal MENU_ITEM_PRICE = new BigDecimal(50);
    public static final BigDecimal EXPECTED_AMOUNT = new BigDecimal(150);

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
        checkBillService = new CheckBillServiceImpl(checkBillRep, employeeRep, orderRepository);
    }

    @Test
    @DisplayName("If employee does not exist, throw an exception")
    void createBillWhenEmployeeDoesNotExist() {
        CheckBill checkBill = getCheckBill();
        assertThrows(CheckBillException.class, () -> checkBillService.createBill(BILL_EMPLOYEE_ID, checkBill));
    }

    @Test
    @DisplayName("If employee has no rights, throw Exception")
    void createBillWhenEmployeeHasNoRights() {
        CheckBill checkBill = getCheckBill();
        assertThrows(CheckBillException.class, () -> checkBillService.createBill(getUnsuitableEmployee().getId(), checkBill));
    }

    @Test
    @DisplayName("If bill already exists, throw Exception")
    void createBillWhenItAlreadyExists() {
        CheckBill checkBill = getCheckBill();
        assertThrows(CheckBillException.class, () -> checkBillService.createBill(getSuitableEmployee().getId(), checkBill));
    }

    @Test
    @DisplayName("If bill does not exist and employee has rights, then create bill")
    void createBillWhenItDoesNotExistAndEmployeeHasRights() {
        CheckBill checkBill = new CheckBill(DEFAULT_ID_FOR_ALL_ENTITIES, PaymentType.CASH);
        checkBillService.createBill(getSuitableEmployee().getId(), checkBill);
        verify(checkBillRep).add(checkBill);
    }

    @Test
    @DisplayName("Calculate bill when order is present")
    void calculateBillWhenOrderIsPresent() {
        CheckBill checkBill = getCheckBill();
        Order order = getOrder();
        when(checkBillRep.findById(any(Integer.class))).thenReturn(Optional.empty());
        checkBillService.createBill(getSuitableEmployee().getId(), checkBill);
        Assertions.assertEquals(0, checkBill.getAmount().compareTo(EXPECTED_AMOUNT));
    }

    @Test
    @DisplayName("If updating a checkbill that is not in the repository, throw an exception")
    void updateCheckBillWhenItIsNotInRepository() {
        CheckBill checkBill = getCheckBill();
        when(checkBillRep.findById(any(Integer.class))).thenReturn(Optional.empty());
        assertThrows(BranchException.class, () -> checkBillService.updateCheckBill(checkBill));
    }

    @Test
    @DisplayName("If updating a checkbill that is in the repository, then the bill is updated")
    void updateCheckBillWhenItIsInRepository() {
        CheckBill checkBill = getCheckBill();
        Assertions.assertDoesNotThrow(() -> checkBillService.updateCheckBill(checkBill));
    }

    @Test
    @DisplayName("If printing a bill that is in the repository, a corresponding file is written")
    void printBillWhenItIsInRepository() throws IOException {
        CheckBill checkBill = getCheckBill();
        getOrder();
        checkBillService.printBill(checkBill.getId());
        assertTrue((new File("./bill" + checkBill.getId() + ".txt")).exists());
    }

    private Order getOrder() {
        Order order = new Order(OrderStatus.NONE, DEFAULT_ID_FOR_ALL_ENTITIES);
        Meal meal = new Meal(DEFAULT_ID_FOR_ALL_ENTITIES, DEFAULT_ID_FOR_ALL_ENTITIES);
        MenuItem menuItem = new MenuItem("Duck", "This is a duck", MENU_ITEM_PRICE, DEFAULT_ID_FOR_ALL_ENTITIES);
        MealItem mealItem = new MealItem(DEFAULT_ID_FOR_ALL_ENTITIES, DEFAULT_QUANTITY, menuItem);
        meal.setMealItems(List.of(mealItem));
        order.setMeals(List.of(meal, meal, meal));
        order.setId(DEFAULT_ID_FOR_ALL_ENTITIES);
        when(orderRepository.findById(any(Integer.class))).thenReturn(Optional.of(order));
        return order;
    }

    private CheckBill getCheckBill() {
        CheckBill checkBill = new CheckBill(DEFAULT_ORDER_ID, PaymentType.CASH);
        checkBill.setId(DEFAULT_ID_FOR_ALL_ENTITIES);
        when(checkBillRep.findById(any(Integer.class))).thenReturn(Optional.of(checkBill));
        return checkBill;
    }

    private Employee getSuitableEmployee() {
        Employee manager = new Manager(DEFAULT_EMPLOYEE_PARAMS.get("name"), DEFAULT_EMPLOYEE_PARAMS.get("email"), DEFAULT_EMPLOYEE_PARAMS.get("phone"), LocalDate.now(), Mockito.mock(Branch.class));
        when(employeeRep.findById(any(Integer.class))).thenReturn(Optional.of(manager));
        return manager;
    }

    private Employee getUnsuitableEmployee() {
        Employee chef = new Chef(DEFAULT_EMPLOYEE_PARAMS.get("name"), DEFAULT_EMPLOYEE_PARAMS.get("email"), DEFAULT_EMPLOYEE_PARAMS.get("phone"), LocalDate.now(), Mockito.mock(Branch.class));
        when(employeeRep.findById(any(Integer.class))).thenReturn(Optional.of(chef));
        return chef;
    }
}
