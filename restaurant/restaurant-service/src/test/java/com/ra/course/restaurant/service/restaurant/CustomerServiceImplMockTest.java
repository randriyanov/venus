package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.model.entity.person.Customer;
import com.ra.course.restaurant.persistence.restaurant.CustomerRepository;
import com.ra.course.restaurant.service.exception.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class CustomerServiceImplMockTest {

    @InjectMocks
    private CustomerServiceImpl customerService;

    @Mock
    private CustomerRepository customerRepository;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    @DisplayName("When adding a customer that is already in the repository, throw an exception")
    void addCustomerWhenAlreadyExists() {
        //given customer which exists
        Customer customer = getRealCustomer();
        //then
        assertThrows(EntityNotFoundException.class, ()->customerService.addCustomer(customer));

    }

    @Test
    @DisplayName("When adding a customer that is not in the repository, add this customer to the repository")
    void addCustomer() {
        //given customer which exists
        Customer customer = getEmptyCustomer();
        //when
        customerService.addCustomer(customer);
        //then
        verify(customerRepository).add(customer);

    }

    @Test
    @DisplayName("When updating a customer that does not exist, throw an exception")
    void updateCustomerWhenCustomerNotExists() {
        //given customer which not exists
        Customer customer = getEmptyCustomer();
        //then
        assertThrows(EntityNotFoundException.class, ()->customerService.updateCustomer(customer));
    }

    @Test
    @DisplayName("When the customer exists, update the customer")
    void updateCustomer() {
        //given customer which exists
        Customer customer = getRealCustomer();
        //when
        customerService.updateCustomer(customer);
        //then
        verify(customerRepository).update(customer);
    }

    private Customer getRealCustomer(){
        Customer customer = getCustomer();
        when(customerRepository.getCustomerByPhone(any(String.class))).thenReturn(Optional.of(customer));
        return customer;
    }

    private Customer getEmptyCustomer(){
        Customer customer = getCustomer();
        when(customerRepository.getCustomerByPhone(any(String.class))).thenReturn(Optional.empty());
        return customer;

    }

    private Customer getCustomer(){
        return new Customer( "Vasya", "+380506666666");
    }
}