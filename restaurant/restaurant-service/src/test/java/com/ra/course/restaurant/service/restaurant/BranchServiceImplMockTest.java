package com.ra.course.restaurant.service.restaurant;

import com.ra.course.restaurant.model.entity.basic.Address;
import com.ra.course.restaurant.model.entity.person.Chef;
import com.ra.course.restaurant.model.entity.restaurant.Branch;
import com.ra.course.restaurant.model.entity.restaurant.Kitchen;
import com.ra.course.restaurant.model.entity.restaurant.TableChart;
import com.ra.course.restaurant.persistence.person.EmployeeRepository;
import com.ra.course.restaurant.persistence.restaurant.BranchRepository;
import com.ra.course.restaurant.persistence.restaurant.KitchenRepository;
import com.ra.course.restaurant.persistence.restaurant.TableChartRepository;
import com.ra.course.restaurant.service.exception.BranchException;
import com.ra.course.restaurant.service.person.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class BranchServiceImplMockTest {
    public static final int DEFAULT_TABLE_CHART_ID = 1;
    public static final int DEFAULT_TABLE_CAPACITY = 3;
    public static final int NON_EXISTENT_TABLE_CHART_ID = 2;
    @InjectMocks
    private BranchServiceImpl branchService;

    @Mock
    private BranchRepository branchRepository;
    @Mock
    private TableChartRepository tableChartRepository;
    @Mock
    private KitchenRepository kitchenRepository;
    @Mock
    private EmployeeRepository employeeRepository;
    @Mock
    private EmployeeService employeeService;


    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
        branchService.setEmployeeService(employeeService);
    }

    @Test
    @DisplayName("Will return not null")
    void getBranchRepository(){
        Assertions.assertNotNull(branchService.getBranchRepository());
    }

    @Test
    @DisplayName("Will return not null")
    void getTableChartRep(){
        Assertions.assertNotNull(branchService.getTableChartRep());
    }

    @Test
    @DisplayName("Will return not null")
    void getKitchenRepository(){
        Assertions.assertNotNull(branchService.getKitchenRepository());
    }


    @Test
    @DisplayName("If chart exists then does not throw exception")
    void getTableChartByIDWhenAlreadyExists() {
        //given
        TableChart tableChart = getTableChart();
        //when
        when(tableChartRepository.findById(DEFAULT_TABLE_CHART_ID)).thenReturn(Optional.of(tableChart));
        //then
        assertDoesNotThrow(()->branchService.getTableChartByID(tableChart.getId()));
    }

    @Test
    @DisplayName("If chart not exists then throw exception")
    void getTableChartByIDWhenNotExists() {
        //given
        //when
        when(tableChartRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        //then
        assertThrows(BranchException.class, ()->branchService.getTableChartByID(NON_EXISTENT_TABLE_CHART_ID));
    }

    @Test
    @DisplayName("If chart already exists then throw exception")
    void addTableChartWhenAlreadyExists() {
        //given
        TableChart tableChart = getTableChart();
        //when
        when(tableChartRepository.getAll()).thenReturn(List.of(tableChart));
        //then
        assertThrows(BranchException.class, ()->branchService.addTableChart(tableChart));
    }

    @Test
    @DisplayName("If chart not exists then add")
    void addTableChartWhenNotExists() {
        //given
        TableChart tableChart = getTableChart();
        //when
        branchService.addTableChart(tableChart);
        //then
        verify(tableChartRepository, times(DEFAULT_TABLE_CHART_ID)).add(tableChart);
    }

    @Test
    @DisplayName("If branch not exists then throw exception")
    void addTableChartWhenBranchNotExists() {
        //given
        TableChart tableChart = getTableChartNoBranch();
        //then
        assertThrows(BranchException.class, ()->branchService.addTableChart(tableChart));
    }

    @Test
    @DisplayName("Add kitchen if not already exists")
    void addKitchen() {
        //given
        Branch branch = getBranch("br1");
        branch.setKitchen(new Kitchen( "kitchen", List.of(Mockito.mock(Chef.class)), "br1"));
        //when
        branchService.addKitchen(branch.getKitchen());
        //then
        verify(kitchenRepository, times(DEFAULT_TABLE_CHART_ID)).add(branch.getKitchen());
    }

    @Test
    @DisplayName("Add kitchen, if already exists then throw exception")
    void addKitchenWhenAlreadyExists() {
        //given
        Kitchen kitchen = getKitchen("Kitchen");
        //then
        assertThrows(BranchException.class, ()->branchService.addKitchen(kitchen));
    }

    @Test
    @DisplayName("Add kitchen, if branch not exists then throw exception")
    void addKitchenWhenBranchNotExists() {
        //given
        Kitchen kitchen = Mockito.mock(Kitchen.class);
        //then
        assertThrows(BranchException.class, ()->branchService.addKitchen(kitchen));
    }

    @Test
    @DisplayName("If branch exists then update branch")
    void updateBranch() {
        //given
        Branch branch = getBranch("br1");
        //when
        branchService.updateBranch(branch);
        //then
        verify(branchRepository).update(branch);
    }

    @Test
    @DisplayName("If branch not exists then throw exception")
    void updateBranchWhenNotExists() {
        //given
        Branch branch = Mockito.mock(Branch.class);
        //then
        assertThrows(BranchException.class, ()->branchService.updateBranch(branch));
    }

    @Test
    @DisplayName("If kitchen exists then update kitchen")
    void updateKitchen() {
        //given
        Kitchen kitchen = getKitchen("kitchen");
        //when
        branchService.updateKitchen(kitchen);
        //then
        verify(kitchenRepository).update(kitchen);
    }

    @Test
    @DisplayName("If kitchen not exists then throw exception")
    void updateKitchenWhenNotExists() {
        //given
        Kitchen kitchen = Mockito.mock(Kitchen.class);
        //then
        assertThrows(BranchException.class, ()->branchService.updateKitchen(kitchen));
    }

    @Test
    @DisplayName("If chart already exists then update chart")
    void updateTableChart() {
        //given
        TableChart tableChart = getTableChartByBranch();
        //when
        branchService.updateTableChart(tableChart);
        //then
        verify(tableChartRepository).update(tableChart);
    }

    @Test
    @DisplayName("If chart not exists then throw exception")
    void updateTableChartWhenChartNotExists() {
        //given
        TableChart tableChart = Mockito.mock(TableChart.class);
        //then
        assertThrows(BranchException.class, ()->branchService.updateTableChart(tableChart));
    }

    @Test
    @DisplayName("when table chart does not exist, throw an exception")
    void printTableChartWhenNotExists() {
        //then
        assertThrows(BranchException.class, ()->branchService.printTableChart(DEFAULT_TABLE_CHART_ID));
    }

    @Test
    @DisplayName("when table chart exists, print table chart")
    void printTableChartWhenExists() {
        //given
        TableChart tableChart = getTableChart();
        //then
        assertDoesNotThrow(()->branchService.printTableChart(tableChart.getId()));
    }

    @Test
    @DisplayName("If chef and branch exist then set branch to employee and update employee")
    void assignChef() {
        //given
        Branch branch = getBranch("br1");
        Chef chef = new Chef( "Chef", "@", "phone", LocalDate.now(), branch);
        //when
        branchService.assignChef(branch.getName(), chef);
        when(employeeRepository.findById(any(Integer.class))).thenReturn(Optional.of(chef));
        //then
        verify(employeeService).updateEmployee(chef);
    }

    @Test
    @DisplayName("when assigning chef to a branch that not exist, throw an exception")
    void assignChefWhenBranchNotExists() {
        //given
        Branch branch = Mockito.mock(Branch.class);
        Chef chef = new Chef( "Chef", "@", "phone", LocalDate.now(), branch);
        //then
        assertThrows(BranchException.class, ()->branchService.assignChef(branch.getName(),chef));
    }

    @Test
    @DisplayName("When branch already exists then throw exception")
    void addBranchWhenAlreadyExists() {
        //given
        Branch branch = getBranch("br1");
        //then
        assertThrows(BranchException.class, ()->branchService.addBranch(branch));
    }

    @Test
    @DisplayName("When branch not exists then add branch")
    void addBranch() {
        //given
        Branch branch = Mockito.mock(Branch.class);
        //when
        branchService.addBranch(branch);
        //then
        verify(branchRepository).add(branch);
    }

    private TableChart getTableChart(){
        String image = "some path";
        TableChart tableChart = new TableChart( image, getBranch("br1").getName(), DEFAULT_TABLE_CAPACITY);
        tableChart.setId(DEFAULT_TABLE_CHART_ID);
        when(tableChartRepository.findById(DEFAULT_TABLE_CHART_ID)).thenReturn(Optional.of(tableChart));
        return tableChart;
    }

    private TableChart getTableChartByBranch(){
        String image = "some path";
        when(tableChartRepository.getTableChartByBranchName(any(String.class))).thenReturn(Optional.of(new TableChart(image, "br1", DEFAULT_TABLE_CAPACITY)));
        return new TableChart( image, getBranch("br1").getName(), DEFAULT_TABLE_CAPACITY);
    }

    private TableChart getTableChartNoBranch(){
        String image = "some path";
        when(tableChartRepository.findById(DEFAULT_TABLE_CHART_ID)).thenReturn(Optional.of(new TableChart(image, "branch", DEFAULT_TABLE_CAPACITY)));
        getBranch("anotherBr");
        return new TableChart(image, "diffBranch", DEFAULT_TABLE_CAPACITY);
    }

    private Branch getBranch(String name){
        Address location = Mockito.mock(Address.class);
        Kitchen kitchen = Mockito.mock(Kitchen.class);
        when(branchRepository.getBranchByName(name)).thenReturn(Optional.of(new Branch(name, location)));
        return new Branch( name, location);
    }

    private Kitchen getKitchen(String name){
        Branch branch = getBranch("br1");
        when(kitchenRepository.getKitchenByBranchName(any(String.class))).thenReturn(Optional.of(new Kitchen(name, new ArrayList<>(), branch.getName())));
        return new Kitchen(name, new ArrayList<>(), branch.getName());
    }

}