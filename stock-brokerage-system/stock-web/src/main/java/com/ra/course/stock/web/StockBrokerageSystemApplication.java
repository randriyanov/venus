package com.ra.course.stock.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.ra.course")
@SuppressWarnings("PMD")
public class StockBrokerageSystemApplication {

    public static void main(final String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(StockBrokerageSystemApplication.class, args);
    }

}
