package com.ra.course.stock.web;

import com.ra.course.model.entity.user.Account;
import com.ra.course.repository.account.AccountRepository;

import java.util.ArrayList;
import java.util.List;

public class IntegrationTestUtils {
    public static void cleanAccountRepository(AccountRepository repo) {
        List<Account> all = new ArrayList(repo.getAll());
        all.forEach(repo::delete);
    }
}
