package com.ra.course.stock.web;

import com.ra.course.model.entity.order.OrderPart;
import com.ra.course.model.entity.stock.BuyStocksRequest;
import com.ra.course.model.entity.stock.SellStocksRequest;
import com.ra.course.model.entity.stock.Stock;
import com.ra.course.model.entity.user.Account;

import java.math.BigDecimal;
import java.util.Arrays;

public class OrderServiceIntegrationTestUtils {
    public static SellStocksRequest sellStocksRequestInstance(final Account account, final Stock stock) {
        SellStocksRequest sellReq = new SellStocksRequest();
        sellReq.setBuyingRequest(false);
        sellReq.setMember(account);
        sellReq.setStockLot(account.getStockPositions()
                .stream()
                .filter( sp -> sp.getStock().equals(stock))
                .findAny()
                .get().getLots()
                .stream()
                .findAny().get());
        sellReq.setStock(stock);
        OrderPart orderPart = new OrderPart();
        orderPart.setQuantity(5);
        sellReq.setParts(Arrays.asList(orderPart));
        return sellReq;
    }
    public static SellStocksRequest sellStocksRequestInstance(final Account account, final Stock stock, final BigDecimal limit) {
        SellStocksRequest sellStocksRequest = sellStocksRequestInstance(account, stock);
        sellStocksRequest.setPriceLimit(limit);
        return sellStocksRequest;
    }
    public static BuyStocksRequest buyStocksRequestInstance(final Account account, final Stock stock) {
        BuyStocksRequest buyReq = new BuyStocksRequest();
        buyReq.setMember(account);
        buyReq.setBuyingRequest(true);
        buyReq.setMember(account);
        OrderPart orderPart = new OrderPart();
        orderPart.setQuantity(5);
        buyReq.setParts(Arrays.asList(orderPart));
        buyReq.setStock(stock);
        return buyReq;
    }
    public static BuyStocksRequest buyStocksRequestInstance(final Account account, final Stock stock, final BigDecimal limit){
        BuyStocksRequest buyStocksRequest = buyStocksRequestInstance(account, stock);
        buyStocksRequest.setPriceLimit(limit);
        return buyStocksRequest;
    }
}
