package com.ra.course.stock.web;

import com.ra.course.model.entity.enums.AccountStatus;
import com.ra.course.model.entity.enums.OrderStatus;
import com.ra.course.model.entity.enums.Role;
import com.ra.course.model.entity.order.MarketOrder;
import com.ra.course.model.entity.order.OrderPart;
import com.ra.course.model.entity.stock.*;
import com.ra.course.model.entity.user.Account;
import com.ra.course.repository.account.AccountRepository;
import com.ra.course.repository.order.OrderRepository;
import com.ra.course.services.order.market.MarketOrderService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;

import static com.ra.course.stock.web.IntegrationTestUtils.cleanAccountRepository;
import static com.ra.course.stock.web.OrderServiceIntegrationTestUtils.buyStocksRequestInstance;
import static com.ra.course.stock.web.OrderServiceIntegrationTestUtils.sellStocksRequestInstance;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = StockBrokerageSystemApplication.class)
class OrderServicesIntegrationTest {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private MarketOrderService marketOrderService;

    @Autowired
    private StockInventory stockInventory;

    // Test data
    private Account account;

    private BuyStocksRequest buyReq;

    private SellStocksRequest sellReq;

    private Stock aapl;

    @BeforeEach
    void beforeEach() {
        cleanAccountRepository(accountRepository);
        final StockPosition stockPosition = new StockPosition();
        this.aapl = new Stock("AAPL", BigDecimal.valueOf(10));
        stockPosition.setStock(aapl);
        stockInventory.setStocks( new ArrayList<>());
        stockInventory.getStocks().add(aapl);
        stockPosition.setLots(Collections.singletonList(
                new StockLot(aapl, 10, null, new OrderPart())));

        this.account = Account.builder()
                .availableFundsForTrading(BigDecimal.valueOf(1000))
                .email("tishakinn@gmail.com")
                .name("Nikolay")
                .role(Role.MEMBER)
                .stockPositions(Collections.singletonList(stockPosition))
                .status(AccountStatus.ACTIVE)
                .build();

        this.accountRepository.insert(account);
        sellReq = sellStocksRequestInstance(account,aapl);
        buyReq = buyStocksRequestInstance(account,aapl);
    }
    @AfterEach
    public void afterEach() {
        cleanAccountRepository(accountRepository);
    }
    @Test
    @DisplayName("When invoke cancelOrder for sell order - given order status is set to CANCELED")
    public void whenCancelOrderIsInvokedForSellOrderGivenOrderStatusChangedToCANCELED() {
        //given
        MarketOrder marketOrder = marketOrderService.placeOrder(sellReq);
        //invoked
        marketOrderService.cancelOrder(marketOrder.getId());
        //expected
        assertEquals(OrderStatus.CANCELLED, orderRepository.findById(marketOrder.getId()).get()
                .getOrderStatus());
    }
    @Test
    @DisplayName("When invoke cancelOrder for buy order - given order status is set to CANCELED")
    public void whenCancelOrderIsInvokedForBuyOrderGivenOrderStatusChangedToCANCELED() {
        //given
        MarketOrder marketOrder = marketOrderService.placeOrder(buyReq);
        //invoked
        marketOrderService.cancelOrder(marketOrder.getId());
        //expected
        assertEquals(OrderStatus.CANCELLED,orderRepository.findById(marketOrder.getId()).get()
                .getOrderStatus());
    }

}
