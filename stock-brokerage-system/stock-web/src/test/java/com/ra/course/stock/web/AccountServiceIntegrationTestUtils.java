package com.ra.course.stock.web;

import com.ra.course.model.entity.enums.AccountStatus;
import com.ra.course.model.entity.enums.Role;
import com.ra.course.model.entity.stock.Stock;
import com.ra.course.model.entity.stock.StockLot;
import com.ra.course.model.entity.stock.StockPosition;
import com.ra.course.model.entity.stock.WatchList;
import com.ra.course.model.entity.user.Account;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AccountServiceIntegrationTestUtils {


    public static Stock getStock() {
        Stock st = new Stock();
        st.setPrice(new BigDecimal(10));
        st.setSymbol("stock_symbol");
        return st;
    }

    public static WatchList getWatchlist() {
        WatchList wl = new WatchList();
        wl.setName("wl_name");
        return wl;
    }

    public static Account getMemberAccount() {
        Account account = new Account();
        account.setStatus(AccountStatus.ACTIVE);
        account.setPhone("phone");
        account.setName("Ivan");
        account.setEmail("some_email");
        account.setPassword("some_password");
        account.setAvailableFundsForTrading(new BigDecimal(100));
        account.setDateOfMembership(System.currentTimeMillis());
        account.setRole(Role.MEMBER);
        account.setWatchLists(new ArrayList<>());
        return account;
    }

    public static Account getAdminAccount() {
        Account account = new Account();
        account.setStatus(AccountStatus.ACTIVE);
        account.setPhone("admin_phone");
        account.setName("John");
        account.setEmail("some_admin_email");
        account.setPassword("some_admin_password");
        account.setDateOfMembership(System.currentTimeMillis());
        account.setRole(Role.ADMIN);
        return account;
    }


    public static StockPosition getStockPosition() {
        StockPosition sp = new StockPosition();
        sp.setLots(List.of(getStockLot()));
        return sp;
    }

    public static StockLot getStockLot() {
        StockLot sl = new StockLot();
        sl.setStock(getStock());
        return sl;
    }
}
