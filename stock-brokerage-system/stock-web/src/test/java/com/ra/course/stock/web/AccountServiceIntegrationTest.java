package com.ra.course.stock.web;

import com.ra.course.model.entity.enums.AccountStatus;
import com.ra.course.model.entity.enums.Role;
import com.ra.course.model.entity.stock.Stock;
import com.ra.course.model.entity.stock.StockInventory;
import com.ra.course.model.entity.stock.StockPosition;
import com.ra.course.model.entity.stock.WatchList;
import com.ra.course.model.entity.user.Account;
import com.ra.course.repository.account.AccountRepository;
import com.ra.course.services.account.AccountService;
import com.ra.course.services.exceptions.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.ra.course.stock.web.AccountServiceIntegrationTestUtils.*;
import static com.ra.course.stock.web.IntegrationTestUtils.cleanAccountRepository;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = StockBrokerageSystemApplication.class)
public class AccountServiceIntegrationTest {
    @Autowired
    AccountService accountService;
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    StockInventory stockInventory;

    private Account memberAccount;
    private Account adminAccount;
    private Stock stock;
    private StockPosition stockPosition;

    @BeforeEach
    public void beforeEach() {
        cleanAccountRepository(accountRepository);
        memberAccount = getMemberAccount();
        adminAccount = getAdminAccount();
        stock = getStock();
        stockInventory.setStocks(List.of(getStock()));
        stockPosition = getStockPosition();
        accountService.createAccount(memberAccount);
        accountService.createWatchList(memberAccount, "some_name");
    }
    @AfterEach
    public void afterEach() {
        cleanAccountRepository(accountRepository);
    }

    @Test
    @DisplayName("Account create success case")
    public void createAccount_Success() {
        assertTrue(accountRepository.findByEmail(memberAccount.getEmail()).get().getId() > 0);
    }

    @Test
    @DisplayName("Account create when email already taken throws EmailAlreadyUsedException")
    public void createAccount_Fail_EmailTaken() {
        assertThrows(EmailAlreadyUsedException.class, () -> accountService.createAccount(memberAccount));
    }

    @Test
    @DisplayName("Create watchlist success case")
    public void createWatchlist_Success() {
        Account foundedAccount = accountRepository.findByEmail(memberAccount.getEmail()).get();
        assertEquals(1, foundedAccount.getWatchLists().size());
        assertEquals("some_name", foundedAccount.getWatchLists().get(0).getName());
    }

    @Test
    @DisplayName("Create watchlist when no such entity in db throws EntityNotFoundException")
    public void createWatchlist_Fail_NoSuchAccountInRepository() {
        accountRepository.delete(memberAccount);
        assertThrows(EntityNotFoundException.class, () -> accountService.createWatchList(memberAccount, "some_name"));
    }

    @Test
    @DisplayName("Create watchlist in valid account with 'admin' role throws OnlyMemberCanHaveWatchListsException")
    public void createWatchlist_Fail_AccountIsAdmin() {
        accountService.createAccount(adminAccount);
        assertThrows(OnlyMemberCanHaveWatchListsException.class, () -> accountService.createWatchList(adminAccount, "some_name"));
    }

    @Test
    @DisplayName("Create watchlist when already exists watchlist with such name throws SuchWatchListNameAlreadyUsedException")
    public void createWatchlist_Fail_NameTaken() {
        assertThrows(SuchWatchListNameAlreadyUsedException.class, () -> accountService.createWatchList(memberAccount, "some_name"));
    }

    @Test
    @DisplayName("Adding stock to watch list success case")
    public void addStockToWatchList_Success() {
        accountService.addStockToWatchList(memberAccount.getWatchLists().get(0), stock);
        assertTrue(accountRepository.findByEmail(memberAccount.getEmail()).get().getWatchLists().get(0).getStocks().contains(stock));
    }

    @Test
    @DisplayName("Adding stock to not existing watch list throws EntityNotFoundException")
    public void addStockToWatchList_Fail_MissingWatchlist() {
        assertThrows(EntityNotFoundException.class, () -> accountService.addStockToWatchList(null, stock));
    }

    @Test
    @DisplayName("Adding stock which not existed in StockInventory to watch list throws NoSuchStockAtStockInventoryException")
    public void addStockToWatchList_Fail_StockInventoryDoesNotHasSuchStock() {
        stockInventory.setStocks(Collections.emptyList());
        final WatchList watchListForAddStock = memberAccount.getWatchLists().get(0);
        assertThrows(NoSuchStockAtStockInventoryException.class, () -> accountService.addStockToWatchList(watchListForAddStock, stock));
    }

    @Test
    @DisplayName("Retrieve accounts list of StockPositions success case")
    public void retrieveStockPositions_Success() {
        memberAccount.setStockPositions(List.of(stockPosition));
        assertEquals(1, accountService.retrieveStockPositions(memberAccount).size());
    }

    @Test
    @DisplayName("Update account success case")
    public void updateAccount_Success() {
        memberAccount.setName("new_some_name");
        accountService.updateAccount(memberAccount);
        final Account accountFromDb = accountRepository.findById(memberAccount.getId()).get();
        assertEquals("new_some_name", accountFromDb.getName());
    }

    @Test
    @DisplayName("Cancel membership success case")
    public void cancelMembership_Success() {
        accountService.cancelMembership(memberAccount);
        final var accountFromDb = accountRepository.findByEmail(memberAccount.getEmail());
        assertEquals(AccountStatus.CLOSED, accountFromDb.get().getStatus());
    }

    @Test
    @DisplayName("Block member success case")
    public void blockMember_Success() {
        accountService.createAccount(adminAccount);
        accountService.blockMember(adminAccount, memberAccount);
        assertEquals(AccountStatus.BLACKLISTED, memberAccount.getStatus());
    }

    @Test
    @DisplayName("Block member when admin account not in db throws EntityNotFoundException")
    public void blockMember_Fail_AdminAbsent() {
        assertThrows(EntityNotFoundException.class, () -> accountService.blockMember(adminAccount, memberAccount));
    }

    @Test
    @DisplayName("Block member when member account not in db throws EntityNotFoundException")
    public void blockMember_Fail_MemberAbsent() {
        accountService.createAccount(adminAccount);
        accountRepository.delete(memberAccount);
        assertThrows(EntityNotFoundException.class, () -> accountService.blockMember(adminAccount, memberAccount));
    }

    @Test
    @DisplayName("Block member when admin account has not admin role throws RightsViolationException")
    public void blockMember_Fail_AdminAccountHasNotAdminRole() {
        adminAccount.setRole(Role.MEMBER);
        accountService.createAccount(adminAccount);
        assertThrows(RightsViolationException.class, () -> accountService.blockMember(adminAccount, memberAccount));
    }

    @Test
    @DisplayName("Block member when member account has admin role throws RightsViolationException")
    public void blockMember_Fail_MemberAccountHasAdminRole() {
        memberAccount.setRole(Role.ADMIN);
        accountService.createAccount(adminAccount);
        assertThrows(RightsViolationException.class, () -> accountService.blockMember(adminAccount, memberAccount));
    }
}
