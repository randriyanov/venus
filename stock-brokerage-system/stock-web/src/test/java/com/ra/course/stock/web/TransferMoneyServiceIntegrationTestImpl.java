package com.ra.course.stock.web;


import com.ra.course.model.entity.enums.AccountStatus;
import com.ra.course.model.entity.enums.Role;
import com.ra.course.model.entity.payments.Money;
import com.ra.course.model.entity.payments.TransferMoney;
import com.ra.course.model.entity.user.Account;
import com.ra.course.repository.account.AccountRepository;
import com.ra.course.repository.transfer_money.TransferMoneyRepository;
import com.ra.course.services.money.MoneyService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.ra.course.stock.web.IntegrationTestUtils.*;

@Slf4j
@SpringBootTest(classes = StockBrokerageSystemApplication.class)
class TransferMoneyServiceIntegrationTestImpl {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private TransferMoneyRepository transferMoneyRepository;

    @Autowired
    private MoneyService moneyService;

    private Account accountFrom = Account.builder()
            .availableFundsForTrading(BigDecimal.valueOf(1000))
            .email("tishakinn@gmail.com")
            .name("Nikolay")
            .role(Role.MEMBER)
            .stockPositions(Collections.emptyList())
            .status(AccountStatus.ACTIVE)
            .build();

    private Account accountTo = Account.builder()
            .availableFundsForTrading(BigDecimal.valueOf(1000))
            .email("kryshtopenko.dmytro@gmail.com")
            .name("Dmytro")
            .role(Role.MEMBER)
            .stockPositions(Collections.emptyList())
            .status(AccountStatus.ACTIVE)
            .build();

    private TransferMoney transferMoney;

    @BeforeEach
    void beforeEach() {
        cleanAccountRepository(accountRepository);
        this.accountFrom = accountRepository.insert(accountFrom);
        this.accountTo = accountRepository.insert(accountTo);
        this.transferMoney = TransferMoney
                .builder()
                .money(new Money(BigDecimal.valueOf(250)))
                .fromAccount(accountFrom.getId())
                .toAccount(accountTo.getId())
                .build();
    }
    @AfterEach
    void afterEach(){
        cleanAccountRepository(accountRepository);
    }

    @Test
    void whenUserSendsMoneyToAnotherUserTransferSuccessfullyCreates() {
        Assertions.assertDoesNotThrow(() -> moneyService.executeTransfer(transferMoney));
        Assertions.assertNotNull(transferMoneyRepository.getAll().get(0).getCreationDate()); // Check the transfer date was set
        Assertions.assertEquals(accountFrom.getAvailableFundsForTrading(), BigDecimal.valueOf(750)); // Check that account money status changed <
        Assertions.assertEquals(accountTo.getAvailableFundsForTrading(), BigDecimal.valueOf(1250)); // Check that account money status changed >
    }
}
