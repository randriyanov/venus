package com.ra.course.repository.transfer_money;

import com.ra.course.model.entity.payments.TransferMoney;
import com.ra.course.repository.CRUDRepository;

public interface TransferMoneyRepository
        extends CRUDRepository<TransferMoney> {
}
