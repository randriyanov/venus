package com.ra.course.repository.account;

import com.ra.course.model.entity.user.Account;
import com.ra.course.repository.CRUDRepository;
import lombok.NonNull;

import java.util.Optional;

public interface AccountRepository
        extends CRUDRepository<Account> {
    Optional<Account> findByEmail(@NonNull String email);
}
