package com.ra.course.repository.transfer_money;

import com.ra.course.model.entity.payments.TransferMoney;
import com.ra.course.repository.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

@Repository
public class TransferMoneyRepoImpl
        extends CRUDRepositoryImpl<TransferMoney>
        implements TransferMoneyRepository {
}
