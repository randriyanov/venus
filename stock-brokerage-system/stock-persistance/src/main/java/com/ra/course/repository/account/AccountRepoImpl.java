package com.ra.course.repository.account;

import com.ra.course.model.entity.user.Account;
import com.ra.course.repository.CRUDRepositoryImpl;
import lombok.NonNull;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public class AccountRepoImpl
        extends CRUDRepositoryImpl<Account>
        implements AccountRepository {

    @Override
    public Optional<Account> findByEmail(final @NonNull String email) {
        return repository
                .stream()
                .filter(account -> account.getEmail().equals(email))
                .findFirst();
    }
}
