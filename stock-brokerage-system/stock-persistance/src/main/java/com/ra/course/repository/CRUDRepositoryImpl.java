package com.ra.course.repository;

import com.ra.course.model.entity.BaseEntity;
import com.ra.course.repository.exceptions.BadEntityException;
import com.ra.course.repository.exceptions.EntityAlreadyExistsException;
import com.ra.course.repository.exceptions.NoSuchEntityInRepositoryException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
public abstract class CRUDRepositoryImpl<T extends BaseEntity>
        implements CRUDRepository<T> {

    protected final transient List<T> repository = new ArrayList<>();

    @Override
    public T insert(final @NonNull T entity) {
        if(entity.getId() != null)
            if (findById(entity.getId()).isPresent()) {
                throw new EntityAlreadyExistsException(String.format("Adding exists entity with id '%s'", entity.getId()));
            }
            else {
                throw new BadEntityException();
            }
        entity.setId((long) (repository.size() + 1)); // Set new ID by collection size
        repository.add(entity);
        log.info("New entity created with ID {}", entity.getId());
        return entity;
    }

    @Override
    public Optional<T> findById(final @NonNull Long entityId) {
        return repository.stream().filter(e -> e.getId().equals(entityId)).findFirst();
    }

    @Override
    public T update(final @NonNull T entity) {
        if(entity.getId() == null || findById(entity.getId()).isEmpty()) {
            throw new NoSuchEntityInRepositoryException(
                    String.format("Updating entity with id '%s' doesn't exist", entity.getId()));
        }
        repository.set(entity.getId().intValue() - 1, entity);
        log.info("Entity has been updated with ID {}", entity.getId());
        return entity;
    }

    @Override
    public void delete(final @NonNull T entity) {
        repository.remove(entity);
    }

    @Override
    public List<T> getAll() {
        return repository;
    }

}
