package com.ra.course.repository.exceptions;

public class NoSuchEntityInRepositoryException extends RuntimeException {

    private static final long serialVersionUID = 0;

    public NoSuchEntityInRepositoryException(final String message) {
        super(message);
    }
}
