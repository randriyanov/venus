package com.ra.course.repository.order;

import com.ra.course.model.entity.order.Order;
import com.ra.course.repository.CRUDRepository;

public interface OrderRepository
        extends CRUDRepository<Order> {
}
