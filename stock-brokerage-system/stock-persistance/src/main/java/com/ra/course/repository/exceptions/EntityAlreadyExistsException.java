package com.ra.course.repository.exceptions;

public class EntityAlreadyExistsException extends RuntimeException {

    private static final long serialVersionUID = 0;

    public EntityAlreadyExistsException(final String message) {
        super(message);
    }
}
