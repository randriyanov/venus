package com.ra.course.repository.order;

import com.ra.course.model.entity.order.Order;
import com.ra.course.repository.CRUDRepositoryImpl;
import org.springframework.stereotype.Repository;

@Repository
public class OrderRepoImpl
        extends CRUDRepositoryImpl<Order>
        implements OrderRepository {
}
