package com.ra.course.repository;

import com.ra.course.model.entity.BaseEntity;
import lombok.NonNull;

import java.util.List;
import java.util.Optional;

public interface CRUDRepository<T extends BaseEntity> {

    T insert(@NonNull T entity);

    Optional<T> findById(@NonNull Long entityId);

    T update(@NonNull T entity);

    void delete(@NonNull T entity);

    List<T> getAll();
}
