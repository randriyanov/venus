package com.ra.course.repository.order;

import com.ra.course.model.entity.order.LimitOrder;
import com.ra.course.repository.exceptions.NoSuchEntityInRepositoryException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OrderRepoImplIntegrationTest {

    private OrderRepository orderRepository = new OrderRepoImpl();

    // Test data
    private LimitOrder limitOrder;

    @BeforeEach
    void beforeEach() {
        orderRepository.getAll().clear();
        limitOrder = new LimitOrder();
    }

    @Test
    @DisplayName("When id is null then inserts and return order")
    void insertLimitOrderSuccess() {
        // then
        Assertions.assertEquals(limitOrder, orderRepository.insert(limitOrder));
    }

    @Test
    @DisplayName("When orders id present in repository update repository data")
    void updateSuccess() {
        // when
        limitOrder = (LimitOrder) orderRepository.insert(limitOrder);
        // then
        assertEquals(limitOrder, orderRepository.update(limitOrder));
    }

    @Test
    @DisplayName("When orders id absent in repository throws NoSuchEntityInRepositoryException")
    void updateFailNoSuchIdInRepositoryThrowNoSuchEntityInRepositoryException() {
        // when
        limitOrder.setId((long) (orderRepository.getAll().size() + 1));
        // then
        assertThrows(NoSuchEntityInRepositoryException.class, () -> orderRepository.update(limitOrder));
    }

    @Test
    @DisplayName("When such id present in repository return non empty Optional")
    void findByIdSuccess() {
        // when
        orderRepository.insert(limitOrder);
        // then
        assertTrue(orderRepository.findById(1L).isPresent());
    }

    @Test
    @DisplayName("When such id absent in repository return empty Optional")
    void findByIdFailNoSuchId() {
        // when
        orderRepository.insert(limitOrder);
        // then
        assertTrue(orderRepository.findById(2L).isEmpty());
    }

    @Test
    @DisplayName("When added two orders returned list size is two")
    void getAllSuccess() {
        // when
        orderRepository.insert(limitOrder);
        // then
        assertEquals(1, orderRepository.getAll().size());
    }
}
