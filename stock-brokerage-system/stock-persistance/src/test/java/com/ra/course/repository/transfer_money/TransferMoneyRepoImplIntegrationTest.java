package com.ra.course.repository.transfer_money;

import com.ra.course.model.entity.payments.TransferMoney;
import com.ra.course.repository.exceptions.NoSuchEntityInRepositoryException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TransferMoneyRepoImplIntegrationTest {

    private TransferMoneyRepository transferMoneyRepository = new TransferMoneyRepoImpl();

    // Test data
    private TransferMoney transferMoney;

    @BeforeEach
    void beforeEach() {
        // given
        transferMoneyRepository.getAll().clear();
        transferMoney = new TransferMoney();
    }

    @Test
    @DisplayName("When transfer id is null then insert and return account")
    void insertSuccess() {
        // then
        assertNotNull(transferMoneyRepository.insert(transferMoney).getId());
    }

    @Test
    @DisplayName("When transfer id present in repository update repository data")
    void updateSuccess() {
        // when
        transferMoney = transferMoneyRepository.insert(transferMoney);
        // then
        assertEquals(transferMoney, transferMoneyRepository.update(transferMoney));
    }

    @Test
    @DisplayName("When transfer id absent in repository throws NoSuchEntityInRepositoryException")
    void updateFailNoSuchIdInRepositoryThrowNoSuchEntityInRepositoryException() {
        // when
        transferMoney.setId(1L);
        // then
        assertThrows(NoSuchEntityInRepositoryException.class, ()->transferMoneyRepository.update(transferMoney));
    }

    @Test
    @DisplayName("When such id present in repository return non empty Optional")
    void findByIdSuccess(){
        // when
        transferMoneyRepository.insert(transferMoney);
        // then
        assertTrue(transferMoneyRepository.findById(1L).isPresent());
    }

    @Test
    @DisplayName("When such id absent in repository return empty Optional")
    void findByIdFailNoSuchId(){
        // when
        transferMoneyRepository.insert(transferMoney);
        // then
        assertTrue(transferMoneyRepository.findById(2L).isEmpty());
    }

    @Test
    @DisplayName("When added two transfers returned list size is two")
    void getAllSuccess(){
        // when
        transferMoneyRepository.insert(transferMoney);
        // then
        assertEquals(1, transferMoneyRepository.getAll().size());
    }
}
