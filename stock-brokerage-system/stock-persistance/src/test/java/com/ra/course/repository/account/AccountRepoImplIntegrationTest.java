package com.ra.course.repository.account;

import com.ra.course.model.entity.user.Account;
import com.ra.course.repository.exceptions.BadEntityException;
import com.ra.course.repository.exceptions.EntityAlreadyExistsException;
import com.ra.course.repository.exceptions.NoSuchEntityInRepositoryException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountRepoImplIntegrationTest {

    private AccountRepository accountRepository = new AccountRepoImpl();

    // Test data
    private Account account;

    @BeforeEach
    void beforeEach() {
        accountRepository.getAll().clear();
        account = new Account();
    }

    @Test
    @DisplayName("When account id is null then insert and return account")
    void insertSuccess() {
        assertNotNull(accountRepository.insert(account).getId());
    }

    @Test
    @DisplayName("When accounts id present in repository update repository data")
    void updateSuccess() {
        account = accountRepository.insert(account);
        assertEquals(account, accountRepository.update(account));
    }

    @Test
    @DisplayName("When update account with id absent in repository throws exception")
    void updateAccountWithWrongId() {
        account.setId(1L);
        assertThrows(NoSuchEntityInRepositoryException.class, () -> accountRepository.update(account));
    }

    @Test
    @DisplayName("When create account with id absent in repository throws exception")
    void createAccountWithExistsId() {
        account.setId(1L);
        assertThrows(BadEntityException.class, () -> accountRepository.insert(account));
    }

    @Test
    @DisplayName("When create duplicate account with id absent in repository throws exception")
    void createAccountWithWrongId() {
        accountRepository.insert(account);
        assertThrows(EntityAlreadyExistsException.class, () -> accountRepository.insert(account));
    }

    @Test
    @DisplayName("When such id present in repository return non empty Optional")
    void findByIdSuccess() {
        accountRepository.insert(account);
        assertTrue(accountRepository.findById(1L).isPresent());
    }

    @Test
    @DisplayName("When add exists element - throw exception")
    void addDuplicateWrong() {
        accountRepository.insert(account);
        assertThrows(EntityAlreadyExistsException.class, () -> accountRepository.insert(account));
    }

    @Test
    @DisplayName("When such id absent in repository return empty Optional")
    void findByIdFailNoSuchId() {
        accountRepository.insert(account);
        assertTrue(accountRepository.findById(2L).isEmpty());
    }

    @Test
    @DisplayName("When added two accounts returned list size is one")
    void getAllSuccess() {
        accountRepository.insert(account);
        assertEquals(1, accountRepository.getAll().size());
    }

    @Test
    @DisplayName("When searching account by existing email returns Optional of account")
    void findByEmail_Success() {
        account.setEmail("good_email");
        account = accountRepository.insert(account);
        assertTrue(accountRepository.findByEmail("good_email").isPresent());
    }

    @Test
    @DisplayName("When searching account by absent email returns empty Optional")
    void findByEmail_Fail_NoSuchEmail() {
        account.setEmail("good_email");
        accountRepository.insert(account);
        assertTrue(accountRepository.findByEmail("bad_email").isEmpty());
    }

    @Test
    @DisplayName("When searching account by null email return")
    void findByEmailByNull() {
        account.setEmail("good_email");
        accountRepository.insert(account);
        assertThrows(NullPointerException.class, () -> accountRepository.findByEmail(null));
    }

    @Test
    @DisplayName("When removing account by existing id returns Optional of account")
    void removeById_Success() {
        accountRepository.insert(account);
        assertDoesNotThrow(() -> accountRepository.delete(account));
    }

    @Test
    @DisplayName("When removing account by NULL id throws exception")
    void removeByNullUnsuccessfully() {
        assertThrows(NullPointerException.class, () -> accountRepository.delete(null));
    }

    @Test
    @DisplayName("When getting account by NULL id throws exception")
    void findByNullUnsuccessfully() {
        assertThrows(NullPointerException.class, () -> accountRepository.findById(null));
    }

    @Test
    @DisplayName("When updating NULL throws exception")
    void updateAccountNullUnsuccessfully() {
        assertThrows(NullPointerException.class, () -> accountRepository.update(null));
    }

    @Test
    @DisplayName("When creating NULL throws exception")
    void createAccountNullUnsuccessfully() {
        assertThrows(NullPointerException.class, () -> accountRepository.insert(null));
    }
}
