package com.ra.course.services.account;

import com.ra.course.model.entity.enums.Role;
import com.ra.course.model.entity.stock.Stock;
import com.ra.course.model.entity.stock.StockInventory;
import com.ra.course.model.entity.stock.StockPosition;
import com.ra.course.model.entity.stock.WatchList;
import com.ra.course.model.entity.user.Account;
import com.ra.course.repository.account.AccountRepoImpl;
import com.ra.course.repository.account.AccountRepository;
import com.ra.course.services.exceptions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AccountServiceImplMockTest {
    private final StockInventory stockInventoryMock = mock(StockInventory.class);
    private final AccountRepository accountRepositoryMock = mock(AccountRepoImpl.class);
    private AccountService accountService;
    private final Account accountMemberMock = mock(Account.class);
    private final Account accountAdminMock = mock(Account.class);
    private final WatchList watchListMock = mock(WatchList.class);
    private final StockPosition stockPositionMock = mock(StockPosition.class);
    private final Stock stockMock = mock(Stock.class);


    @BeforeEach
    private void beforeEach() {
        accountService = new AccountServiceImpl(accountRepositoryMock, stockInventoryMock);
        when(accountRepositoryMock.update(accountMemberMock)).thenReturn(accountMemberMock);
        when(accountAdminMock.getId()).thenReturn(1L);
        when(accountAdminMock.getEmail()).thenReturn("adminEmail");
        when(accountMemberMock.getId()).thenReturn(2L);
        when(accountMemberMock.getEmail()).thenReturn("memberEmail");
    }

    @Test
    @DisplayName("createAccount - Email already used throws EmailAlreadyUsedException")
    public void createAccountFailEmailTaken() {
        //given mocked repo which returns same account as parameter account
        when(accountRepositoryMock.getAll()).thenReturn(List.of(accountMemberMock));
        when(accountMemberMock.getEmail()).thenReturn("test");
        //throws EmailAlreadyUsedException because their emails are equal
        assertThrows(EmailAlreadyUsedException.class, () -> accountService.createAccount(accountMemberMock));
    }

    @Test
    @DisplayName("createAccount - Success")
    public void createAccountSuccess() {
        //given mocked repo which returns empty list of accounts on getAll() and returns same account on insert()
        when(accountRepositoryMock.getAll()).thenReturn(Collections.emptyList());
        when(accountRepositoryMock.insert(accountMemberMock)).thenReturn(accountMemberMock);
        //returns account
        assertEquals(accountService.createAccount(accountMemberMock), accountMemberMock);
    }

    @Test
    @DisplayName("createWatchList - When id and email of account parameters not match with any account from repository then throws AccountNotFoundException")
    public void createWatchListFailAccountNotFromRepository() {
        //given mocked repo which returns empty list of accounts on getAll() and returns same account on insert()
        when(accountRepositoryMock.getAll()).thenReturn(Collections.emptyList());
        //throws AccountNotFoundException
        assertThrows(EntityNotFoundException.class, () -> accountService.createWatchList(accountMemberMock, ""));
    }

    @Test
    @DisplayName("createWatchList - When field role of parameter account not equals 'member' then throws OnlyMemberCanHaveWatchListsException")
    public void createWatchListFailNotMember() {
        //given mocked repo which returns list of one admin account which present in repo
        when(accountMemberMock.getRole()).thenReturn(Role.ADMIN);
        when(accountMemberMock.getEmail()).thenReturn("test");
        when(accountRepositoryMock.getAll()).thenReturn(List.of(accountMemberMock));
        //throws OnlyMemberCanHaveWatchListsException
        assertThrows(OnlyMemberCanHaveWatchListsException.class, () -> accountService.createWatchList(accountMemberMock, ""));
    }

    @Test
    @DisplayName("createWatchList - When account is admin and is present in repo but already has a watchlist with specified name throws SuchWatchListNameAlreadyUsedException")
    public void createWatchListFailSuchWatchlistNameIsUsed() {
        //given mocked repo which returns list of one member account which present in repo and already has watchlist with specified name
        when(accountMemberMock.getRole()).thenReturn(Role.MEMBER);
        when(accountRepositoryMock.getAll()).thenReturn(List.of(accountMemberMock));
        when(accountMemberMock.getWatchLists()).thenReturn(List.of(watchListMock));
        when(watchListMock.getName()).thenReturn("UsedName");
        //throws OnlyMemberCanHaveWatchListsException
        assertThrows(SuchWatchListNameAlreadyUsedException.class, () -> accountService.createWatchList(accountMemberMock, "UsedName"));
    }

    @Test
    @DisplayName("createWatchList - When account and name for watchlist is ok then return watchlist")
    public void createWatchListSuccess() {
        //given mocked repo which returns list of one member account which present in repo and already has watchlist with specified name
        when(accountMemberMock.getRole()).thenReturn(Role.MEMBER);
        when(accountRepositoryMock.getAll()).thenReturn(List.of(accountMemberMock));
        when(accountMemberMock.getWatchLists()).thenReturn(List.of(watchListMock));
        when(watchListMock.getName()).thenReturn("UsedName");
        //return watchlist with specified name
        assertEquals(accountService.createWatchList(accountMemberMock, "VacantName").getName(), "VacantName");
    }

    @Test
    @DisplayName("refreshWatchList - return refreshed watchlist")
    public void refreshWatchListSuccess() {
        assertEquals(accountService.refreshWatchList(watchListMock), watchListMock);
    }

    @Test
    @DisplayName("retrieveStockPositions - return List of stockPositions of account")
    public void retrieveStockPositionsSuccess() {
        when(accountMemberMock.getStockPositions()).thenReturn(List.of(stockPositionMock));
        assertEquals(accountService.retrieveStockPositions(accountMemberMock), List.of(stockPositionMock));
    }

    @Test
    @DisplayName("updateAccount - return account")
    public void updateAccountSuccess() {
        assertEquals(accountService.updateAccount(accountMemberMock), accountMemberMock);
    }

    @Test
    @DisplayName("cancelMembership - after update returns boolean true")
    public void cancelMembershipSuccess() {
        assertDoesNotThrow(() -> accountService.cancelMembership(accountMemberMock));
    }

    @Test
    @DisplayName("blockMember - accounts are not in repo throws EntityNotFoundException")
    public void blockMemberFailBothOfAccountsNotPresentInRepo() {
        //when repo not contains accounts
        when(accountRepositoryMock.getAll()).thenReturn(Collections.emptyList());
        //then throws EntityNotFoundException
        assertThrows(EntityNotFoundException.class, () -> accountService.blockMember(accountAdminMock, accountMemberMock));
    }

    @Test
    @DisplayName("blockMember - accountMember is not in repo throws EntityNotFoundException")
    public void blockMemberFailAdminPresentButMemberNot() {
        //when repo contains accountAdmin but missing accountMember
        when(accountRepositoryMock.getAll()).thenReturn(List.of(accountAdminMock));
        //then throws EntityNotFoundException
        assertThrows(EntityNotFoundException.class, () -> accountService.blockMember(accountAdminMock, accountMemberMock));
    }

    @Test
    @DisplayName("blockMember - accountAdmin is not in repo throws EntityNotFoundException")
    public void blockMemberFailMemberPresentButAdminNot() {
        //when repo contains accountMember but missing accountAdmin
        when(accountRepositoryMock.getAll()).thenReturn(List.of(accountMemberMock));
        //then throws EntityNotFoundException
        assertThrows(EntityNotFoundException.class, () -> accountService.blockMember(accountAdminMock, accountMemberMock));
    }

    @Test
    @DisplayName("blockMember - adminAccount field is 'MEMBER' then throws RightsViolationException")
    public void blockMemberFailAdminNotAdmin() {
        //when field role in accountAdmin is Role.MEMBER
        when(accountRepositoryMock.getAll()).thenReturn(List.of(accountAdminMock, accountMemberMock));
        when(accountAdminMock.getRole()).thenReturn(Role.MEMBER);
        //then throws RightsViolationException
        assertThrows(RightsViolationException.class, () -> accountService.blockMember(accountAdminMock, accountMemberMock));
    }

    @Test
    @DisplayName("blockMember - memberAccount field is 'ADMIN' then throws RightsViolationException")
    public void blockMemberFailMemberIsAdmin() {
        //when field role in accountMember is Role.ADMIN
        when(accountRepositoryMock.getAll()).thenReturn(List.of(accountAdminMock, accountMemberMock));
        when(accountAdminMock.getRole()).thenReturn(Role.ADMIN);
        when(accountMemberMock.getRole()).thenReturn(Role.ADMIN);
        //then throws RightsViolationException
        assertThrows(RightsViolationException.class, () -> accountService.blockMember(accountAdminMock, accountMemberMock));
    }

    @Test
    @DisplayName("blockMember - accounts are ok return true")
    public void blockMemberSuccess() {
        //when accountAdmin role field is Role.ADMIN, accountMember role field is Role.MEMBER, both of them in repo
        when(accountRepositoryMock.getAll()).thenReturn(List.of(accountAdminMock, accountMemberMock));
        when(accountAdminMock.getRole()).thenReturn(Role.ADMIN);
        when(accountMemberMock.getRole()).thenReturn(Role.MEMBER);
        //then return true
        assertDoesNotThrow(() -> accountService.blockMember(accountAdminMock, accountMemberMock));
    }

    @Test
    @DisplayName("unblockMember - accounts are not in repo throws EntityNotFoundException")
    public void unblockMemberFailBothOfAccountsNotPresentInRepo() {
        //when repo not contains accounts
        when(accountRepositoryMock.getAll()).thenReturn(Collections.emptyList());
        //then throws EntityNotFoundException
        assertThrows(EntityNotFoundException.class, () -> accountService.unblockMember(accountAdminMock, accountMemberMock));
    }

    @Test
    @DisplayName("unblockMember - accountMember is not in repo throws EntityNotFoundException")
    public void unblockMemberFailAdminPresentButMemberNot() {
        //when repo contains accountAdmin but missing accountMember
        when(accountRepositoryMock.getAll()).thenReturn(List.of(accountAdminMock));
        //then throws EntityNotFoundException
        assertThrows(EntityNotFoundException.class, () -> accountService.unblockMember(accountAdminMock, accountMemberMock));
    }

    @Test
    @DisplayName("unblockMember - accountAdmin is not in repo throws EntityNotFoundException")
    public void unblockMemberFailMemberPresentButAdminNot() {
        //when repo contains accountMember but missing accountAdmin
        when(accountRepositoryMock.getAll()).thenReturn(List.of(accountMemberMock));
        //then throws EntityNotFoundException
        assertThrows(EntityNotFoundException.class, () -> accountService.unblockMember(accountAdminMock, accountMemberMock));
    }

    @Test
    @DisplayName("unblockMember - adminAccount field is 'MEMBER' then throws RightsViolationException")
    public void unblockMemberFailAdminNotAdmin() {
        //when field role in accountAdmin is Role.MEMBER
        when(accountRepositoryMock.getAll()).thenReturn(List.of(accountAdminMock, accountMemberMock));
        when(accountAdminMock.getRole()).thenReturn(Role.MEMBER);
        //then throws RightsViolationException
        assertThrows(RightsViolationException.class, () -> accountService.unblockMember(accountAdminMock, accountMemberMock));
    }

    @Test
    @DisplayName("unblockMember - memberAccount field is 'ADMIN' then throws RightsViolationException")
    public void unblockMemberFailMemberIsAdmin() {
        //when field role in accountMember is Role.ADMIN
        when(accountRepositoryMock.getAll()).thenReturn(List.of(accountAdminMock, accountMemberMock));
        when(accountAdminMock.getRole()).thenReturn(Role.ADMIN);
        when(accountMemberMock.getRole()).thenReturn(Role.ADMIN);
        //then throws RightsViolationException
        assertThrows(RightsViolationException.class, () -> accountService.unblockMember(accountAdminMock, accountMemberMock));
    }

    @Test
    @DisplayName("unblockMember - accounts are ok return true")
    public void unblockMemberSuccess() {
        //when accountAdmin role field is Role.ADMIN, accountMember role field is Role.MEMBER, both of them in repo
        when(accountRepositoryMock.getAll()).thenReturn(List.of(accountAdminMock, accountMemberMock));
        when(accountAdminMock.getRole()).thenReturn(Role.ADMIN);
        when(accountMemberMock.getRole()).thenReturn(Role.MEMBER);
        //then return true
        assertDoesNotThrow( ()-> accountService.unblockMember(accountAdminMock, accountMemberMock));
    }

    @Test
    @DisplayName("addStockToWatchList - no user has such watchlist throw EntityNotFoundException")
    public void addStockToWatchListNoSuchWatchList() {
        //when no user has such watchList
        when(accountRepositoryMock.getAll()).thenReturn(Collections.emptyList());
        //then throws EntityNotFoundException
        assertThrows(EntityNotFoundException.class, () -> accountService.addStockToWatchList(watchListMock, stockMock));
    }

    @Test
    @DisplayName("addStockToWatchList - no stock in stock inventory throws NoSuchStockAtStockInventoryException")
    public void addStockToWatchListNoSuchStock() {
        //when account contains specified watchlist and stockInventory did not contains specified stock
        when(accountRepositoryMock.getAll()).thenReturn(List.of(accountMemberMock));
        when(accountMemberMock.getWatchLists()).thenReturn(List.of(watchListMock));
        when(stockInventoryMock.getStocks()).thenReturn(Collections.emptyList());
        //then throws NoSuchStockAtStockInventoryException
        assertThrows(NoSuchStockAtStockInventoryException.class, () -> accountService.addStockToWatchList(watchListMock, stockMock));
    }

    @Test
    @DisplayName("addStockToWatchList - success")
    public void addStockToWatchListSuccess() {
        //when account contains specified watchlist and stockInventory contains specified stock
        when(accountMemberMock.getWatchLists()).thenReturn(List.of(watchListMock));
        when(accountRepositoryMock.getAll()).thenReturn(List.of(accountMemberMock));
        when(stockInventoryMock.getStocks()).thenReturn(List.of(stockMock));
        when(stockMock.getSymbol()).thenReturn("symbol");
        //then returns stock
        assertEquals(stockMock, accountService.addStockToWatchList(watchListMock, stockMock));
    }

}
