package com.ra.course.services.money;

import com.ra.course.model.entity.payments.Money;
import com.ra.course.model.entity.payments.TransferMoney;
import com.ra.course.model.entity.user.Account;
import com.ra.course.repository.account.AccountRepoImpl;
import com.ra.course.repository.account.AccountRepository;
import com.ra.course.repository.transfer_money.TransferMoneyRepoImpl;
import com.ra.course.repository.transfer_money.TransferMoneyRepository;
import com.ra.course.services.exceptions.EntityNotFoundException;
import com.ra.course.services.exceptions.NotEnoughFundsToExecuteTransfer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MoneyServiceImplMockTest {
    private MoneyService moneyService;
    private final AccountRepository accountRepoMocked = mock(AccountRepoImpl.class);
    private TransferMoneyRepository repositoryTransfer = mock(TransferMoneyRepoImpl.class);
    private Account from;
    private Long idFrom = 1L;
    private Account to;
    private Long idTo = 2L;
    private Money money;
    private TransferMoney transaction;
    @BeforeEach
    public void beforeEach() {
      moneyService = new MoneyServiceImpl(accountRepoMocked,repositoryTransfer);
      transaction = new TransferMoney();
      money = new Money(new BigDecimal(20));
      from  = Account.builder().availableFundsForTrading(new BigDecimal(20)).build();
      to = Account.builder().availableFundsForTrading(new BigDecimal(0)).build();

      from.setId(idFrom);
      to.setId(idTo);
      transaction.setFromAccount(idFrom);
      transaction.setToAccount(idTo);
      transaction.setMoney(money);

      when(accountRepoMocked.findById(idFrom)).thenReturn(Optional.of(from));
      when(accountRepoMocked.findById(idTo)).thenReturn(Optional.of(to));

    }
    @Test
    @DisplayName("Throws an exception if account where money comes from doesnt exist")
    public void ifFromAccountDoesntExistThrowsException() {
        when(accountRepoMocked.findById(idFrom)).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class,() -> moneyService.executeTransfer(transaction));
    }
    @Test
    @DisplayName("Throws an exception if account where money comes to doesnt exist")
    public void ifToAccountDoesntExistThrowsException() {
        when(accountRepoMocked.findById(idTo)).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class,() -> moneyService.executeTransfer(transaction));
    }
    @Test
    @DisplayName("Throws an exception if account where money comes from doesnt have enough funds")
    public void ifAccountWhereMoneyComesFromDoesntHaveEnoughFundsExceptionThrown() {
        from.setAvailableFundsForTrading(BigDecimal.valueOf(0));
        assertThrows(NotEnoughFundsToExecuteTransfer.class,
                () -> moneyService.executeTransfer(transaction));
    }
    @Test
    @DisplayName("If all is okey than From account has to have transfered money less then before transaction")
    public void ifTransactionDoneThenFromAccHasHisFundsLoweredOnTransferedMoneyValue() {
       BigDecimal fundsBefore = from.getAvailableFundsForTrading();
       moneyService.executeTransfer(transaction);
       assertEquals(fundsBefore.subtract(transaction.getMoney().getValue()),
               from.getAvailableFundsForTrading());
    }
    @Test
    @DisplayName("If all is okey than To account has to have transfered money more then before transaction")
    public void ifTransactionDoneThenToAccHasHisFundsAddedOnTransferedMoneyValue() {
        BigDecimal fundsBefore = to.getAvailableFundsForTrading();
        moneyService.executeTransfer(transaction);
        assertEquals(fundsBefore.add(transaction.getMoney().getValue()),
                to.getAvailableFundsForTrading());
    }
}
