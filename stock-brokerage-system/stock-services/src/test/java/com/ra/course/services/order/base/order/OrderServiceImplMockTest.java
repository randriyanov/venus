package com.ra.course.services.order.base.order;

import com.ra.course.model.entity.order.MarketOrder;
import com.ra.course.repository.order.OrderRepoImpl;
import com.ra.course.repository.order.OrderRepository;
import com.ra.course.services.exceptions.EntityNotFoundException;
import com.ra.course.services.order.market.MarketOrderServiceImpl;
import com.ra.course.services.validator.RequestValidatorService;
import com.ra.course.services.validator.RequestValidatorServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class OrderServiceImplMockTest {

    private MarketOrderServiceImpl orderService;
    private final RequestValidatorService validatorMocked = mock(RequestValidatorServiceImpl.class);
    private final OrderRepository orderRepositoryMocked = mock(OrderRepoImpl.class);

    @BeforeEach
    void beforeEach() {
        orderService = new MarketOrderServiceImpl(validatorMocked, orderRepositoryMocked);
    }

    @Test
    @DisplayName("Returns false if order is not in the repository")
    void testCancelOrderMethodOrderDosntExist(){
        //given
        final var id = -1L;
        //expected that
        assertThrows(EntityNotFoundException.class, () -> orderService.cancelOrder(id));
    }

    @Test
    @DisplayName("Returns true if order is in the repository")
    void testCancelOrderMethodOrderExist() {
        // given
        final var id = 1L;
        MarketOrder order = new MarketOrder();
        order.setId(id);
        // when
        when(orderRepositoryMocked.findById(id)).thenReturn(Optional.of(order));
        // then
        assertDoesNotThrow(() -> orderService.cancelOrder(id));
    }

}
