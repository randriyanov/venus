package com.ra.course.services.validator;

import com.ra.course.model.entity.enums.AccountStatus;
import com.ra.course.model.entity.order.OrderPart;
import com.ra.course.model.entity.stock.*;
import com.ra.course.model.entity.user.Account;
import com.ra.course.repository.account.AccountRepoImpl;
import com.ra.course.repository.account.AccountRepository;
import com.ra.course.services.exceptions.BadAccountStatusException;
import com.ra.course.services.exceptions.EntityNotFoundException;
import com.ra.course.services.exceptions.NoSuchStockAtStockInventoryException;
import com.ra.course.services.exceptions.NotEnoughOrderPartsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.EnumSource.Mode.EXCLUDE;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RequestValidatorServiceImplMockTest {

    private RequestValidatorServiceImpl validatorService;

    private AccountRepository memberSpecificRepository = mock(AccountRepoImpl.class);

    private StockInventory stockInventory = mock(StockInventory.class);

    // Test data
    private String existsEmail = "exists@gmail.com";
    private Account member;
    private String stockSymbol = "I'm symbol";
    private BigDecimal stockPrice = BigDecimal.valueOf(15.5);
    private Stock stock;
    private StockLot stockLot;
    private List<OrderPart> parts;
    private OrderPart part;

    private ArrayList<Stock> stockArrayList = new ArrayList<>();
    private BuyStocksRequest buyReq;
    private SellStocksRequest sellReq;
    @BeforeEach
    void setUp() {
        validatorService = new RequestValidatorServiceImpl();
        validatorService.setAccountRepository(memberSpecificRepository);
        validatorService.setStockInventory(stockInventory);
        stock = new Stock(stockSymbol, stockPrice);
        stockArrayList.add(new Stock(stock.getSymbol(), stock.getPrice()));
        member = Account.builder()
                .email(existsEmail)
                .availableFundsForTrading(stock.getPrice())
                .status(AccountStatus.ACTIVE)
                .build();
        partsInit();
        stockLotInit();
        sellRequestBoxing();
        buyingRequestBoxing();
    }

    @Test
    @Tag("memberValidation")
    @DisplayName("When member is not provided, exception throws")
    void whenMemberIsNotProvidedThrowException() {
        assertThrows(EntityNotFoundException.class,
                () -> validatorService.memberValidation(member));
    }

    @ParameterizedTest
    @DisplayName("When not active member is provided, then throws exception")
    @Tag("memberValidation")
    @EnumSource(value = AccountStatus.class, mode = EXCLUDE, names = { "ACTIVE" })
    void whenCancelledMemberIsProvidedThrowException(AccountStatus accountStatus) {
        member.setStatus(accountStatus);
        when(memberSpecificRepository.findById(member.getId())).thenReturn(Optional.of(member));
        assertThrows(BadAccountStatusException.class,
                () -> validatorService.memberValidation(member));
    }

    @Test
    @Tag("memberValidation")
    @DisplayName("When active member is provided, then it's ok")
    void whenMemberIsProvidedThenItsSuccessfullyReturned() {
        when(memberSpecificRepository.findById(member.getId())).thenReturn(Optional.of(member));
        assertDoesNotThrow(
                () -> validatorService.memberValidation(member));
    }

    @Test
    @Tag("stockValidation")
    @DisplayName("When stock can't be found by symbol, then throws exception")
    void whenStockCantBeFoundBySymbolThenThrowsException() {
        assertThrows(NoSuchStockAtStockInventoryException.class,
                () -> validatorService.stockValidation(stock));
    }

    @Test
    @Tag("stockValidation")
    @DisplayName("When stock can be found by symbol, then it's ok")
    void whenStockCanBeFoundBySymbolThenItsSuccessfullyReturned() {
        when(stockInventory.getStocks()).thenReturn(stockArrayList);
        assertDoesNotThrow(
                () -> validatorService.stockValidation(stock));
    }

    @Test
    @Tag("orderPartsValidation")
    @DisplayName("When order parts list is empty, then throws exception")
    void whenOrderPartIsEmptyThenThrowsException() {
        assertThrows(NotEnoughOrderPartsException.class,
                () -> validatorService.orderPartsValidation(new ArrayList<>()));
    }

    @Test
    @Tag("orderPartsValidation")
    @DisplayName("When order parts list is not empty, then it's ok")
    void whenOrderPartsIsNotEmptyThenItsOk() {
        assertDoesNotThrow(
                () -> validatorService.orderPartsValidation(Collections.singletonList(new OrderPart())));
    }
    @Test
    @Tag("memberFundsValidation")
    @DisplayName("When founds enough for purchasing stocks, then it's ok")
    void whenFoundEnoughItsOk() {
        assertDoesNotThrow(() -> validatorService.memberFundsValidation(
                Collections.singletonList(OrderPart.builder().quantity(1).build()), member, stock));
    }
    @Test
    @DisplayName("Get stockInventory must return earlier given  stockInventory")
    void getValidatorServiceMustReturnGivenEarlierValidator() {
        assertEquals(stockInventory,validatorService.getStockInventory());
    }
    @Test
    @DisplayName("Get memberSpecificRepository  must return earlier given  repo")
    void getAccountRepoMustReturnGivenEarlierRepo() {
        assertEquals(memberSpecificRepository,validatorService.getAccountRepository());
    }
    @Test
    @DisplayName("When given buyingRequest is ok than method doesnt throw anything")
    void whenGivenBuyingRequestIsOkThatMethodDoesntThrowAnything() {
        when(memberSpecificRepository.findById(member.getId())).thenReturn(Optional.of(member));
        when(stockInventory.getStocks()).thenReturn(stockArrayList);
        assertDoesNotThrow(() -> validatorService.validateBuyRequest(buyReq));
    }
    @Test
    @DisplayName("When given sellingRequest is ok than method doesnt throw anything")
    void whenGivenSellingRequestIsOkThatMethodDoesntThrowAnything() {
        when(memberSpecificRepository.findById(member.getId())).thenReturn(Optional.of(member));
        when(stockInventory.getStocks()).thenReturn(stockArrayList);
        assertDoesNotThrow(() -> validatorService.validateSellingRequest(sellReq));
    }
    private void sellRequestBoxing() {
        sellReq = new SellStocksRequest();
        sellReq.setMember(member);
        sellReq.setParts(parts);
        sellReq.setStockLot(stockLot);
        sellReq.setBuyingRequest(false);
    }
    private void buyingRequestBoxing() {
        buyReq = new BuyStocksRequest();
        buyReq.setMember(member);
        buyReq.setParts(parts);
        buyReq.setStock(stock);
        buyReq.setBuyingRequest(true);
    }
    private void stockLotInit() {
        stock = new Stock(stockSymbol, stockPrice);
        stockLot = new StockLot();
        stockLot.setQuantity(100);
        stockLot.setStock(stock);
    }

    private void partsInit() {
        parts = new ArrayList<>();
        part = new OrderPart();
        part.setQuantity(1);
        parts.add(part);
    }
}
