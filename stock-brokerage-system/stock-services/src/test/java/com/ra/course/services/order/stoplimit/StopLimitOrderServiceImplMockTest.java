package com.ra.course.services.order.stoplimit;

import com.ra.course.model.entity.enums.AccountStatus;
import com.ra.course.model.entity.enums.OrderStatus;
import com.ra.course.model.entity.order.OrderPart;
import com.ra.course.model.entity.order.StopLimitOrder;
import com.ra.course.model.entity.stock.BuyStocksRequest;
import com.ra.course.model.entity.stock.SellStocksRequest;
import com.ra.course.model.entity.stock.Stock;
import com.ra.course.model.entity.stock.StockLot;
import com.ra.course.model.entity.user.Account;
import com.ra.course.repository.order.OrderRepoImpl;
import com.ra.course.repository.order.OrderRepository;
import com.ra.course.services.exceptions.NotSupportedOperationCalledException;
import com.ra.course.services.validator.RequestValidatorServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class StopLimitOrderServiceImplMockTest {
    private StopLimitOrderServiceImpl orderService;
    private RequestValidatorServiceImpl validatorMocked = mock(RequestValidatorServiceImpl.class);
    private OrderRepository orderRepositoryMocked = mock(OrderRepoImpl.class);


    private BuyStocksRequest buyReq;
    private SellStocksRequest sellReq;

    private Account member;
    private StockLot stockLot;
    private List<OrderPart> parts;
    private Stock stock;
    private String stockSymbol = "I'm symbol";
    private BigDecimal stockPrice = BigDecimal.valueOf(15.5);
    private String existEmail = "super@mail.ru";
    private OrderPart part;
    private StopLimitOrder expectedSellingOrder;
    private StopLimitOrder expecteBuyingOrder;

    @BeforeEach
    void beforeEach() {
        orderService = new StopLimitOrderServiceImpl(validatorMocked,orderRepositoryMocked);
        partsInit();
        stockLotInit();
        member = Account.builder()
                .email(existEmail)
                .availableFundsForTrading(stock.getPrice())
                .status(AccountStatus.ACTIVE)
                .build();
        sellRequestBoxing();
        sellingOrderBoxing();
        buyingRequestBoxing();
        buyingOrderBoxing();
    }



    @Test
    @DisplayName("StopLimitOrder cant represent selling request , when placeOrder gets invoked with sell request exception has to be thrown")
    void whenSellRequestTryingToBeExecutedAtStopLimitOrderServiceExceptionThrown() {
        assertThrows(NotSupportedOperationCalledException.class,
                () -> orderService.placeOrder(sellReq));
    }
    @Test
    @DisplayName("Checks does returned order equal to expected buying order")
    void  whenBuyRequestGetExecutedBuyOrderGetsReturned() {
        //given
        when(orderRepositoryMocked.insert(any())).thenReturn(expecteBuyingOrder);
        //expecting that
        assertEquals(expecteBuyingOrder,
                orderService.placeOrder(buyReq));
    }
    private void buyingOrderBoxing() {
        expecteBuyingOrder = new StopLimitOrder();
        expecteBuyingOrder.setOrderStatus(OrderStatus.OPEN);
        expecteBuyingOrder.setOrderParts(parts);
        expecteBuyingOrder.setStock(stock);
        expecteBuyingOrder.setToBuyOrder(true);
        expecteBuyingOrder.setPriceLimit(stock.getPrice());
    }

    private void buyingRequestBoxing() {
        buyReq = new BuyStocksRequest();
        buyReq.setMember(member);
        buyReq.setParts(parts);
        buyReq.setStock(stock);
        buyReq.setBuyingRequest(true);
    }

    private void sellingOrderBoxing() {
        expectedSellingOrder = new StopLimitOrder();
        expectedSellingOrder.setOrderStatus(OrderStatus.OPEN);
        expectedSellingOrder.setOrderParts(parts);
        expectedSellingOrder.setStock(stock);
        expectedSellingOrder.setToBuyOrder(false);
        expectedSellingOrder.setPriceLimit(stock.getPrice());
    }

    private void sellRequestBoxing() {
        sellReq = new SellStocksRequest();
        sellReq.setMember(member);
        sellReq.setParts(parts);
        sellReq.setStockLot(stockLot);
        sellReq.setBuyingRequest(false);
    }

    private void stockLotInit() {
        stock = new Stock(stockSymbol, stockPrice);
        stockLot = new StockLot();
        stockLot.setQuantity(100);
        stockLot.setStock(stock);
    }

    private void partsInit() {
        parts = new ArrayList<>();
        part = new OrderPart();
        part.setQuantity(10);
        parts.add(part);
    }
}
