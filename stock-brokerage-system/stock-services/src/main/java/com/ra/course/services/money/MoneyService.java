package com.ra.course.services.money;

import com.ra.course.model.entity.payments.TransferMoney;

public interface MoneyService {
    void executeTransfer(TransferMoney transfer);
}
