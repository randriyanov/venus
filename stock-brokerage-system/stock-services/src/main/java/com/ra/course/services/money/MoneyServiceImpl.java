package com.ra.course.services.money;

import com.ra.course.model.entity.payments.TransferMoney;
import com.ra.course.repository.account.AccountRepository;
import com.ra.course.repository.transfer_money.TransferMoneyRepository;
import com.ra.course.services.exceptions.EntityNotFoundException;
import com.ra.course.services.exceptions.NotEnoughFundsToExecuteTransfer;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MoneyServiceImpl implements MoneyService {
    private final transient AccountRepository accountRepo;
    private final transient TransferMoneyRepository transactionRepo;

    @Override
    public void executeTransfer(final TransferMoney transfer) {
        final var accountFrom = accountRepo.findById(transfer.getFromAccount()).orElseThrow(EntityNotFoundException::new);

        if(accountFrom.getAvailableFundsForTrading().compareTo(transfer.getMoney().getValue()) < 0) {
            throw new NotEnoughFundsToExecuteTransfer();
        }

        accountFrom.setAvailableFundsForTrading(
                accountFrom.getAvailableFundsForTrading()
                        .subtract(transfer.getMoney().getValue()));

        final var accountTo = accountRepo.findById(transfer.getToAccount()).orElseThrow(EntityNotFoundException::new);

        accountTo.setAvailableFundsForTrading(
                accountTo.getAvailableFundsForTrading()
                        .add(transfer.getMoney().getValue()));

        accountRepo.update(accountFrom);
        accountRepo.update(accountTo);
        transfer.setCreationDate(System.currentTimeMillis());
        transactionRepo.insert(transfer);
    }
}
