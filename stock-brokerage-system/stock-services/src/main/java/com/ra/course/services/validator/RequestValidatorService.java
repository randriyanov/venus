package com.ra.course.services.validator;

import com.ra.course.model.entity.stock.BuyStocksRequest;
import com.ra.course.model.entity.stock.SellStocksRequest;

public interface RequestValidatorService {

    void validateSellingRequest(SellStocksRequest req);

    void validateBuyRequest(BuyStocksRequest req);

}
