package com.ra.course.services.validator;

import com.ra.course.model.entity.enums.AccountStatus;
import com.ra.course.model.entity.order.OrderPart;
import com.ra.course.model.entity.stock.BuyStocksRequest;
import com.ra.course.model.entity.stock.SellStocksRequest;
import com.ra.course.model.entity.stock.Stock;
import com.ra.course.model.entity.stock.StockInventory;
import com.ra.course.model.entity.user.Account;
import com.ra.course.repository.account.AccountRepository;
import com.ra.course.services.exceptions.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
@Getter
@Setter
public class RequestValidatorServiceImpl implements RequestValidatorService {
    @Autowired
    private transient AccountRepository accountRepository;
    @Autowired
    private transient  StockInventory stockInventory;

    @Override
    public void validateSellingRequest(final SellStocksRequest req) {
        memberValidation(req.getMember());
        orderPartsValidation(req.getParts());
    }

    @Override
    public void validateBuyRequest(final BuyStocksRequest req) {
        memberValidation(req.getMember());
        stockValidation(req.getStock());
        orderPartsValidation(req.getParts());
        memberFundsValidation(req.getParts(),req.getMember(),req.getStock());
    }

    public void memberValidation(final Account member) {
        final Optional<Account> foundMember = accountRepository.findById(member.getId());
        if (foundMember.isEmpty()) {
            throw new EntityNotFoundException();
        }
        if (foundMember.get().getStatus() != AccountStatus.ACTIVE) {
            throw new BadAccountStatusException();
        }
    }
    public void stockValidation(final Stock stock) {
        if (stockInventory.getStocks().stream().noneMatch(s -> s.getSymbol().equals(stock.getSymbol()))) {
            throw new NoSuchStockAtStockInventoryException();
        }
    }

    public void orderPartsValidation(final List<OrderPart> parts) {
        if (parts.isEmpty()) {
            throw new NotEnoughOrderPartsException();
        }
    }

    public void memberFundsValidation(final List<OrderPart> parts,
                                      final Account member,
                                      final Stock stock) {
        final int amountQuantity = parts.stream().map(OrderPart::getQuantity).reduce(0, Integer::sum);
        final BigDecimal availableFunds = member.getAvailableFundsForTrading();
        final BigDecimal needsFunds = stock.getPrice().multiply(BigDecimal.valueOf(amountQuantity));
        if (availableFunds.compareTo(needsFunds) < 0) {
            throw new NotEnoughFundsForTrading();
        }
    }
}
