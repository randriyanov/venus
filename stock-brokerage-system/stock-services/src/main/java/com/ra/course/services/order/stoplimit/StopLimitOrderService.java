package com.ra.course.services.order.stoplimit;

import com.ra.course.model.entity.order.StopLimitOrder;
import com.ra.course.services.order.base.order.OrderService;

public interface StopLimitOrderService extends OrderService<StopLimitOrder> {
}
