package com.ra.course.services.order.stoploss;

import com.ra.course.model.entity.order.StopLossOrder;
import com.ra.course.model.entity.stock.SellStocksRequest;
import com.ra.course.repository.order.OrderRepository;
import com.ra.course.services.order.base.order.OrderServiceImpl;
import com.ra.course.services.validator.RequestValidatorService;
import org.springframework.stereotype.Service;

@Service
public class StopLossOrderServiceImpl
        extends OrderServiceImpl<StopLossOrder>
        implements StopLossOrderService {

    public StopLossOrderServiceImpl(final RequestValidatorService reqValidService,
                                   final OrderRepository orderRepo) {
        super(reqValidService, orderRepo);
    }

    @Override
    public StopLossOrder placeOrder(final SellStocksRequest req) {
        reqValidService.validateSellingRequest(req);
        bindToLot(req.getStockLot(), req.getParts());
        final StopLossOrder order = new StopLossOrder();
        baseOrderInitialization(order, req);
        order.setPriceLimit(req.getPriceLimit());
        return (StopLossOrder) orderRepo.insert(order);
    }
}
