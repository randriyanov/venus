package com.ra.course.services.order.base.order;

import com.ra.course.model.entity.order.Order;
import com.ra.course.model.entity.stock.BuyStocksRequest;
import com.ra.course.model.entity.stock.SellStocksRequest;
import com.ra.course.services.exceptions.NotSupportedOperationCalledException;

public interface OrderService<T extends Order> {
    default T placeOrder(final SellStocksRequest req) {
        throw new NotSupportedOperationCalledException();
    }
    default T placeOrder(final BuyStocksRequest req) {
        throw new NotSupportedOperationCalledException();
    }
    void cancelOrder(final Long idOrder);
}