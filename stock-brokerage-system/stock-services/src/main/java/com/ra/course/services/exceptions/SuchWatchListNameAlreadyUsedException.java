package com.ra.course.services.exceptions;

public class SuchWatchListNameAlreadyUsedException extends RuntimeException {
    private static final long serialVersionUID = 0;
}
