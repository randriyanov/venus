package com.ra.course.services.order.market;

import com.ra.course.model.entity.order.MarketOrder;
import com.ra.course.model.entity.stock.BuyStocksRequest;
import com.ra.course.model.entity.stock.SellStocksRequest;
import com.ra.course.repository.order.OrderRepository;
import com.ra.course.services.order.base.order.OrderServiceImpl;
import com.ra.course.services.validator.RequestValidatorService;
import org.springframework.stereotype.Service;

@Service
public class MarketOrderServiceImpl
        extends OrderServiceImpl<MarketOrder>
        implements MarketOrderService {

    public MarketOrderServiceImpl(final RequestValidatorService reqValidService,
                                 final OrderRepository orderRepo) {
        super(reqValidService, orderRepo);
    }

    @Override
    public MarketOrder placeOrder(final BuyStocksRequest req) {
        reqValidService.validateBuyRequest(req);
        final MarketOrder order = new MarketOrder();
        baseOrderInitialization(order,req);
        return (MarketOrder) orderRepo.insert(order);
    }

    @Override
    public MarketOrder placeOrder(final SellStocksRequest req) {
        reqValidService.validateSellingRequest(req);
        bindToLot(req.getStockLot(),req.getParts());
        final MarketOrder order = new MarketOrder();
        baseOrderInitialization(order,req);
        return (MarketOrder) orderRepo.insert(order);
    }
}
