package com.ra.course.services.account;

import com.ra.course.model.entity.enums.AccountStatus;
import com.ra.course.model.entity.enums.Role;
import com.ra.course.model.entity.stock.Stock;
import com.ra.course.model.entity.stock.StockInventory;
import com.ra.course.model.entity.stock.StockPosition;
import com.ra.course.model.entity.stock.WatchList;
import com.ra.course.model.entity.user.Account;
import com.ra.course.repository.account.AccountRepository;
import com.ra.course.services.exceptions.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class AccountServiceImpl implements AccountService {
    private final transient AccountRepository accountRepo;
    private final transient StockInventory stockInventory;

    @Override
    public Account createAccount(final Account account) {
        if (isEmailAlreadyUsed(account.getEmail())) {
            throw new EmailAlreadyUsedException();
        }
        return accountRepo.insert(account);
    }

    @Override
    public WatchList createWatchList(final Account account, final String name) {
        if (isAccountNotInRepository(account)) {
            throw new EntityNotFoundException();
        }
        if (!account.getRole().equals(Role.MEMBER)){
            throw new OnlyMemberCanHaveWatchListsException();
        }
        if (account.getWatchLists()
                .stream()
                .anyMatch(watchList -> watchList.getName().equals(name))) {
            throw new SuchWatchListNameAlreadyUsedException();
        }
        final var watchList = new WatchList(name);
        final var newWatchLists = new ArrayList<>(account.getWatchLists());
        newWatchLists.add(watchList);
        account.setWatchLists(newWatchLists);
        updateAccount(account);
        return watchList;
    }

    @Override
    public WatchList refreshWatchList(final WatchList watchList) {
        final var newStockList = watchList
                .getStocks()
                .stream()
                .filter(stock -> stockInventory.getStocks().contains(stock))
                .collect(Collectors.toList());
        watchList.setStocks(newStockList);
        return watchList;
    }

    @Override
    public Stock addStockToWatchList(final WatchList watchList, final Stock stock) {
        if (!isWatchListValid(watchList)) {
            throw new EntityNotFoundException();
        }
        if (!isStockInStockInventory(stock)) {
            throw new NoSuchStockAtStockInventoryException();
        }
        final var newStocksList = new ArrayList<>(watchList.getStocks());
        newStocksList.add(stock);
        watchList.setStocks(newStocksList);
        return stock;
    }

    @Override
    public List<StockPosition> retrieveStockPositions(final Account account) {
        return account.getStockPositions();
    }

    @Override
    public Account updateAccount(final Account account) {
        return accountRepo.update(account);
    }

    @Override
    public void cancelMembership(final Account account) {
        account.setStatus(AccountStatus.CLOSED);
        updateAccount(account);
    }

    @Override
    public void blockMember(final Account adminAccount, final Account memberAccount) {
        if (isAccountNotInRepository(adminAccount) || isAccountNotInRepository(memberAccount)) {
            throw new EntityNotFoundException();
        }
        if (!adminAccount.getRole().equals(Role.ADMIN) || memberAccount.getRole().equals(Role.ADMIN)) {
            throw new RightsViolationException();
        }
        memberAccount.setStatus(AccountStatus.BLACKLISTED);
        updateAccount(memberAccount);
    }

    @Override
    public void unblockMember(final Account adminAccount, final Account memberAccount) {
        if (isAccountNotInRepository(adminAccount) || isAccountNotInRepository(memberAccount)) {
            throw new EntityNotFoundException();
        }
        if (!adminAccount.getRole().equals(Role.ADMIN) || memberAccount.getRole().equals(Role.ADMIN)) {
            throw new RightsViolationException();
        }
        memberAccount.setStatus(AccountStatus.ACTIVE);
        updateAccount(memberAccount);
    }

    private boolean isEmailAlreadyUsed(final String email) {
        return accountRepo.getAll().stream()
                .anyMatch(repoAccount -> repoAccount.getEmail()
                        .equals(email));
    }

    private boolean isAccountNotInRepository(final Account account) {
        return accountRepo.getAll().stream()
                .noneMatch(a -> a.getId()
                        .equals(account.getId()) && a.getEmail()
                        .equals(account.getEmail()));
    }

    private boolean isWatchListValid(final WatchList watchList) {
        return accountRepo.getAll()
                .stream()
                .anyMatch(account -> account.getWatchLists()
                        .contains(watchList));
    }

    private boolean isStockInStockInventory(final Stock stock) {
        return stockInventory
                .getStocks()
                .stream()
                .anyMatch(s -> s.getSymbol()
                        .equals(stock.getSymbol()));
    }
}