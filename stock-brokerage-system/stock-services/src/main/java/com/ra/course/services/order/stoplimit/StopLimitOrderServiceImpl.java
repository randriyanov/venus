package com.ra.course.services.order.stoplimit;

import com.ra.course.model.entity.order.StopLimitOrder;
import com.ra.course.model.entity.stock.BuyStocksRequest;
import com.ra.course.repository.order.OrderRepository;
import com.ra.course.services.order.base.order.OrderServiceImpl;
import com.ra.course.services.validator.RequestValidatorService;
import org.springframework.stereotype.Service;

@Service
public class StopLimitOrderServiceImpl
        extends OrderServiceImpl<StopLimitOrder>
        implements StopLimitOrderService {

    public StopLimitOrderServiceImpl(final RequestValidatorService reqValidService,
                                   final OrderRepository orderRepo) {
        super(reqValidService, orderRepo);
    }

    @Override
    public StopLimitOrder placeOrder(final BuyStocksRequest req) {
        reqValidService.validateBuyRequest(req);
        final StopLimitOrder order = new StopLimitOrder();
        baseOrderInitialization(order,req);
        order.setPriceLimit(req.getPriceLimit());
        return (StopLimitOrder) orderRepo.insert(order);
    }
}
