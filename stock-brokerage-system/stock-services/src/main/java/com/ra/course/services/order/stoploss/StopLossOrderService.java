package com.ra.course.services.order.stoploss;

import com.ra.course.model.entity.order.StopLossOrder;
import com.ra.course.services.order.base.order.OrderService;

public interface StopLossOrderService extends OrderService<StopLossOrder> {

}
