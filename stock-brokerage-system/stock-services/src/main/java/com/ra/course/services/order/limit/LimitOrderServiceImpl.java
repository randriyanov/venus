package com.ra.course.services.order.limit;

import com.ra.course.model.entity.order.LimitOrder;
import com.ra.course.model.entity.stock.BuyStocksRequest;
import com.ra.course.model.entity.stock.SellStocksRequest;
import com.ra.course.repository.order.OrderRepository;
import com.ra.course.services.order.base.order.OrderServiceImpl;
import com.ra.course.services.validator.RequestValidatorService;
import org.springframework.stereotype.Service;

@Service
public class LimitOrderServiceImpl
        extends OrderServiceImpl<LimitOrder>
        implements LimitOrderService {
    public LimitOrderServiceImpl(final RequestValidatorService reqValidService, final OrderRepository orderRepo) {
        super(reqValidService, orderRepo);
    }

    @Override
    public LimitOrder placeOrder(final BuyStocksRequest req) {
        reqValidService.validateBuyRequest(req);
        final LimitOrder order = new LimitOrder();
        baseOrderInitialization(order,req);
        order.setPriceLimit(req.getPriceLimit());
        return (LimitOrder)orderRepo.insert(order);
    }

    @Override
    public LimitOrder placeOrder(final SellStocksRequest req) {
        reqValidService.validateSellingRequest(req);
        bindToLot(req.getStockLot(),req.getParts());
        final LimitOrder order = new LimitOrder();
        baseOrderInitialization(order,req);
        order.setPriceLimit(req.getPriceLimit());
        return (LimitOrder)orderRepo.insert(order);
    }
}
