package com.ra.course.services.exceptions;

public class OnlyMemberCanHaveWatchListsException extends RuntimeException {
    private static final long serialVersionUID = 0;
}
