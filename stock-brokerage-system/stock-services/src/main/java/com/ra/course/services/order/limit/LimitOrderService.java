package com.ra.course.services.order.limit;

import com.ra.course.model.entity.order.LimitOrder;
import com.ra.course.services.order.base.order.OrderService;

public interface LimitOrderService extends OrderService<LimitOrder> {
}
