package com.ra.course.services.order.market;

import com.ra.course.model.entity.order.MarketOrder;
import com.ra.course.services.order.base.order.OrderService;

public interface MarketOrderService extends OrderService<MarketOrder> {
}
