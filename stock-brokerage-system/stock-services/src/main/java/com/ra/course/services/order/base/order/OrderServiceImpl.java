package com.ra.course.services.order.base.order;

import com.ra.course.model.entity.enums.OrderStatus;
import com.ra.course.model.entity.order.Order;
import com.ra.course.model.entity.order.OrderPart;
import com.ra.course.model.entity.stock.Request;
import com.ra.course.model.entity.stock.Stock;
import com.ra.course.model.entity.stock.StockLot;
import com.ra.course.repository.order.OrderRepository;
import com.ra.course.services.exceptions.EntityNotFoundException;
import com.ra.course.services.exceptions.NotEnoughStocksAtStockLotException;
import com.ra.course.services.validator.RequestValidatorService;
import lombok.AllArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.Optional;


@AllArgsConstructor
public abstract class OrderServiceImpl<T extends Order> implements OrderService<T> {
    protected transient RequestValidatorService reqValidService;
    protected transient OrderRepository orderRepo;
    @Override
    public void cancelOrder(final Long idOrder) {
        final Optional<Order> orderById = orderRepo.findById(idOrder);
        if (orderById.isPresent()){
            orderById.get().setOrderStatus(OrderStatus.CANCELLED);
            orderRepo.update(orderById.get());
        } else {
            throw new EntityNotFoundException();
        }
    }
    protected void bindToLot(final StockLot stockLot, final List<OrderPart> parts) {
        final int amountQuantity = parts.stream()
                .map(OrderPart::getQuantity)
                .reduce(0, Integer::sum);
        final int diff = stockLot.getQuantity() - amountQuantity;
        if(diff < 0) {
            throw new NotEnoughStocksAtStockLotException();
        }
        parts.forEach(part -> part.setLotToSell(stockLot));
    }

    protected void baseOrderInitialization(final Order order,
                                           final Request req) {
        order.setCreationTime(new Date().getTime());
        final Stock stock = req.getBuyingRequest()
                ? req.getStock() : req.getStockLot().getStock();
        order.setStock(stock);
        order.setToBuyOrder(req.getBuyingRequest());
        order.setOrderParts(req.getParts());
        order.setOrderStatus(OrderStatus.OPEN);
        order.setOwnerId(req.getMember().getId());
    }
}
