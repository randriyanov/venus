package com.ra.course.services.account;

import com.ra.course.model.entity.stock.Stock;
import com.ra.course.model.entity.stock.StockPosition;
import com.ra.course.model.entity.stock.WatchList;
import com.ra.course.model.entity.user.Account;

import java.util.List;

public interface AccountService {
    Account createAccount(Account account);
    WatchList createWatchList(Account account, String name);
    WatchList refreshWatchList(WatchList watchList);
    Stock addStockToWatchList(WatchList watchList, Stock stock);
    List<StockPosition> retrieveStockPositions(Account account);
    Account updateAccount(Account account);
    void cancelMembership(Account account);
    void blockMember(Account admin, Account member);
    void unblockMember(Account admin, Account member);
}
