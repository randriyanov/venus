package com.ra.course.model.entity.enums;

public enum OrderStatus {
    OPEN,
    FILLED,
    PARTIALLY_FILLED,
    CANCELLED
}
