package com.ra.course.model.entity.statement;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class YearlyTaxStatement extends Statement {
    private Integer year;

    @Builder
    public YearlyTaxStatement(String name, String description, Integer year) {
        super(name, description);
        this.year = year;
    }
}
