package com.ra.course.model.entity.stock;

import com.ra.course.model.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WatchList extends BaseEntity {
    private List<Stock> stocks;
    private String name;

    public WatchList(String name) {
        this.name = name;
        this.stocks = new ArrayList<>();
    }
}
