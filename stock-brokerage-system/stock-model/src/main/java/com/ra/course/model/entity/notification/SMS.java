package com.ra.course.model.entity.notification;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SMS extends Notification {
    private String phone;

    @Builder
    public SMS(Integer notificationId, Long createdOn, String content, String phone) {
        super(notificationId, createdOn, content);
        this.phone = phone;
    }
}
