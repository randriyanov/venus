package com.ra.course.model.entity.payments;

import com.ra.course.model.entity.BaseEntity;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransferMoney extends BaseEntity {
    private Long creationDate;
    private Money money;
    private Long fromAccount;
    private Long toAccount;
}
