package com.ra.course.model.entity.user;

import com.ra.course.model.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Location extends BaseEntity {
    private String streetAddress;
    private String city;
    private String state;
    private String zipCode;
    private String country;
}
