package com.ra.course.model.entity.stock;

import com.ra.course.model.entity.BaseEntity;
import com.ra.course.model.entity.order.Order;
import com.ra.course.model.entity.order.OrderPart;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockLot extends BaseEntity {
    private Stock stock;
    private Integer quantity;
    private Order buyingOrder;
    private OrderPart executedPart;
}
