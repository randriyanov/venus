package com.ra.course.model.entity.notification;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class PushNotification extends Notification {
    private String email;

    @Builder
    public PushNotification(Integer notificationId, Long createdOn, String content, String email) {
        super(notificationId, createdOn, content);
        this.email = email;
    }
}
