package com.ra.course.model.entity.enums;

public enum AccountStatus {
    ACTIVE,
    CLOSED,
    BLACKLISTED
}
