package com.ra.course.model.entity.order;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
public class StopLimitOrder extends Order {
    private BigDecimal priceLimit;
}
