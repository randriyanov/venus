package com.ra.course.model.entity.stock;


import com.ra.course.model.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.stereotype.Component;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Component
public class StockInventory extends BaseEntity {
    private static StockInventory inventoryInstance;
    private Long lastUpdated;
    private List<Stock> stocks;

    private static StockInventory instance = null;

    private StockInventory() {
    }

    public static StockInventory getInstance() {
        if (inventoryInstance == null)
            inventoryInstance = new StockInventory();
        return instance;
    }
}
