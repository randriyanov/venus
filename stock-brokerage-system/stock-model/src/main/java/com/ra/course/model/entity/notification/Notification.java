package com.ra.course.model.entity.notification;

import com.ra.course.model.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Notification extends BaseEntity {
    private Integer notificationId;
    private Long createdOn;
    private String content;
}
