package com.ra.course.model.entity.statement;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class QuarterlyStatement extends Statement {
    private Integer quarterNumber;
    private Integer year;

    @Builder
    public QuarterlyStatement(String name, String description, Integer quarterNumber, Integer year) {
        super(name, description);
        this.quarterNumber = quarterNumber;
        this.year = year;
    }
}
