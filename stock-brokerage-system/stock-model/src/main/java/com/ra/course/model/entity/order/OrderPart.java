package com.ra.course.model.entity.order;

import com.ra.course.model.entity.BaseEntity;
import com.ra.course.model.entity.stock.StockLot;
import lombok.*;

import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
public class OrderPart extends BaseEntity {
    private Integer quantity;
    private Long executionDate;
    private BigDecimal price;
    private StockLot lotToSell;
}
