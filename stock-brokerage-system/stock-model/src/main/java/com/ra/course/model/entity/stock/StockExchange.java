package com.ra.course.model.entity.stock;

import com.ra.course.model.entity.BaseEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StockExchange extends BaseEntity {

    private static StockExchange instance = null;

    public static StockExchange getInstance() {
        if (instance == null)
            instance = new StockExchange();
        return instance;
    }
}
