package com.ra.course.model.entity.user;

import com.ra.course.model.entity.BaseEntity;
import com.ra.course.model.entity.enums.AccountStatus;
import com.ra.course.model.entity.enums.Role;
import com.ra.course.model.entity.statement.Statement;
import com.ra.course.model.entity.stock.StockPosition;
import com.ra.course.model.entity.stock.WatchList;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Account extends BaseEntity {
    private String password;
    private String name;
    private String email;
    private AccountStatus status;
    private Location address;
    private String phone;
    private BigDecimal availableFundsForTrading;
    private Long dateOfMembership;
    private List<StockPosition> stockPositions;
    private List<WatchList> watchLists;
    private List<Statement> statements;
    private Role role;
}
