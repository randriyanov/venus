package com.ra.course.model.entity.stock;

import com.ra.course.model.entity.BaseEntity;
import com.ra.course.model.entity.order.OrderPart;
import com.ra.course.model.entity.user.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Request extends BaseEntity {
    private List<OrderPart> parts;
    private BigDecimal priceLimit;
    private Account member;
    private Boolean buyingRequest;
    private StockLot stockLot;
    private Stock stock;
}
