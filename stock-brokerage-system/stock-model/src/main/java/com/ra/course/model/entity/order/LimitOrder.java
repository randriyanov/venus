package com.ra.course.model.entity.order;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
public class LimitOrder extends Order {
    private BigDecimal priceLimit;
}