package com.ra.course.model.entity.enums;

public enum Role {
    ADMIN,
    MEMBER
}
