package com.ra.course.model.entity.order;

import com.ra.course.model.entity.BaseEntity;
import com.ra.course.model.entity.enums.OrderStatus;
import com.ra.course.model.entity.stock.Stock;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order extends BaseEntity {
    private Stock stock;
    private Boolean toBuyOrder;
    private OrderStatus orderStatus;
    private Long creationTime;
    private List<OrderPart> orderParts;
    private Long ownerId;
}
