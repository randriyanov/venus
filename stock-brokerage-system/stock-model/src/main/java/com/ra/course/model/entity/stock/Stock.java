package com.ra.course.model.entity.stock;

import com.ra.course.model.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Stock extends BaseEntity {
    private String symbol;
    private BigDecimal price;
}
