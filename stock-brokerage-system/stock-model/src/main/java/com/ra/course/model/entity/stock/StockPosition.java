package com.ra.course.model.entity.stock;

import com.ra.course.model.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockPosition extends BaseEntity {
    private Stock stock;
    private List<StockLot> lots;
}
