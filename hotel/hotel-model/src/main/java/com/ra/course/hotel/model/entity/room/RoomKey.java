package com.ra.course.hotel.model.entity.room;

import com.ra.course.hotel.model.entity.BaseEntity;
import lombok.*;

import java.time.LocalDateTime;

@NoArgsConstructor
@Getter
@Setter
public class RoomKey extends BaseEntity {
    private String keyId;
    private String barcode;
    private LocalDateTime issuedAt;
    private boolean active;
    private boolean masterKey;

    public RoomKey(String keyId, String barcode, boolean masterKey) {
        this.keyId = keyId;
        this.barcode = barcode;
        this.active = false;
        this.masterKey = masterKey;
    }
}
