package com.ra.course.hotel.model.entity.account;

public enum AccountType {
    MEMBER,
    GUEST, //non-member
    MANAGER,
    RECEPTIONIST,
    HOUSEKEEPER
}
