package com.ra.course.hotel.model.entity.room;

public enum RoomStyle {
    STANDARD,
    DELUXE,
    FAMILY_SUITE,
    BUSINESS_SUITE
}
