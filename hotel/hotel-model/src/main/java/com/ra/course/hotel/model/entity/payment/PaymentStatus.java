package com.ra.course.hotel.model.entity.payment;

public enum PaymentStatus {
    UNPAID,
    PENDING,
    COMPLETED,
    FAILED,
    DECLINED,
    CANCELLED,
    ABANDONED,
    SETTLING,
    SETTLED,
    REFUNDED
}
