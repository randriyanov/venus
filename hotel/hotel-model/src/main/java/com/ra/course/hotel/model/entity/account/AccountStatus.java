package com.ra.course.hotel.model.entity.account;

public enum AccountStatus {
    ACTIVE,
    CLOSED,
    CANCELED,
    BLACKLISTED
}
