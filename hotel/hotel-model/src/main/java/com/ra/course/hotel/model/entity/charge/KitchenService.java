package com.ra.course.hotel.model.entity.charge;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
public class KitchenService extends RoomCharge {
    private String description;

    public KitchenService(LocalDate issuedAt, String description) {
        super(issuedAt);
        this.description = description;
    }
}
