package com.ra.course.hotel.model.entity.hotel;

import com.ra.course.hotel.model.entity.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
@Setter
public class Hotel extends BaseEntity {
    private String name;
    private List<HotelLocation> locations;

    public Hotel() {
        this.locations = new CopyOnWriteArrayList<>();
    }

    public Hotel(String name) {
        this.name = name;
        this.locations = new CopyOnWriteArrayList<>();
    }
}
