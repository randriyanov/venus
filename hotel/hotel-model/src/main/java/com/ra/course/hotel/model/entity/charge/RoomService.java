package com.ra.course.hotel.model.entity.charge;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
public class RoomService extends RoomCharge {
    private boolean chargable;
    private LocalDate requestTime;

    public RoomService(LocalDate issuedAt, boolean chargable, LocalDate requestTime) {
        super(issuedAt);
        this.chargable = chargable;
        this.requestTime = requestTime;
    }
}
