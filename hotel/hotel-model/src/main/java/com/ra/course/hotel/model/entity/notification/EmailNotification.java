package com.ra.course.hotel.model.entity.notification;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class EmailNotification extends Notification {
    private String email;

    public EmailNotification(String content, String email) {
        super(content);
        this.email = email;
    }
}
