package com.ra.course.hotel.model.entity.persons;

import com.ra.course.hotel.model.entity.common.Address;
import com.ra.course.hotel.model.entity.account.Account;
import com.ra.course.hotel.model.entity.account.AccountType;

public class Server extends Person {
    public Server() {
        this.accountType = AccountType.MANAGER;
    }

    public Server(String name, Address address, String email, String phone, Account account) {
        super(name, address, email, phone, account);
        this.accountType = AccountType.MANAGER;
    }
}
