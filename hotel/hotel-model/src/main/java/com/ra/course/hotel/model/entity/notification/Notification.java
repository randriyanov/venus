package com.ra.course.hotel.model.entity.notification;

import com.ra.course.hotel.model.entity.BaseEntity;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public abstract class Notification extends BaseEntity {
    private LocalDateTime createdOn;
    private String content;
    private RoomBooking booking;

    public Notification() {
        this.createdOn = LocalDateTime.now();
    }

    public Notification(String content) {
        this.createdOn = LocalDateTime.now();
        this.content = content;
    }
}
