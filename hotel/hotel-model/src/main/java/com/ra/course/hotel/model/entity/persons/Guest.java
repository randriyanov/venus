package com.ra.course.hotel.model.entity.persons;

import com.ra.course.hotel.model.entity.common.Address;
import com.ra.course.hotel.model.entity.account.Account;
import com.ra.course.hotel.model.entity.account.AccountType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Guest extends BookingPerson {
    private int totalRoomsCheckedIn;

    public Guest() {
        this.accountType = AccountType.GUEST;
    }

    public Guest(String name, Address address, String email, String phone, int totalRoomsCheckedIn,
                 Account account, AccountType accountType) {
        super(name, address, email, phone, account);
        this.accountType = AccountType.GUEST;
        this.totalRoomsCheckedIn = totalRoomsCheckedIn;
    }
}
