package com.ra.course.hotel.model.entity.account;

import com.ra.course.hotel.model.entity.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class Account extends BaseEntity {
    private String login;
    private String password;
    private AccountStatus status;

    private static final AccountStatus DEFAULT_ACCOUNT_STATUS = AccountStatus.CLOSED;

    public Account(String login, String password) {
        this.login = login;
        this.password = password;
        this.status = DEFAULT_ACCOUNT_STATUS;
    }
}
