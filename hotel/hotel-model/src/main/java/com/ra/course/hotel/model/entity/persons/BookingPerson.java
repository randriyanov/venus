package com.ra.course.hotel.model.entity.persons;

import com.ra.course.hotel.model.entity.account.Account;
import com.ra.course.hotel.model.entity.account.AccountType;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.common.Address;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
@Setter
public abstract class BookingPerson extends Person {
    private List<RoomBooking> bookings;

    public BookingPerson() {
        this.bookings = new CopyOnWriteArrayList<>();
    }

    public BookingPerson(String name, Address address, String email, String phone,
                         Account account) {
        super(name, address, email, phone, account);
        this.bookings = new CopyOnWriteArrayList<>();
    }
}
