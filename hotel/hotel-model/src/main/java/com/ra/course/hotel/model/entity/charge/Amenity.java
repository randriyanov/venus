package com.ra.course.hotel.model.entity.charge;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
public class Amenity extends RoomCharge {
    private String name;
    private String description;

    public Amenity(LocalDate issuedAt, String name, String description) {
        super(issuedAt);
        this.name = name;
        this.description = description;
    }
}
