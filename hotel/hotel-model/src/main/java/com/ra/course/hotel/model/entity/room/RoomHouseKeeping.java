package com.ra.course.hotel.model.entity.room;

import com.ra.course.hotel.model.entity.BaseEntity;
import com.ra.course.hotel.model.entity.common.MinuteInterval;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RoomHouseKeeping extends BaseEntity {
    private String description;
    private MinuteInterval interval;
}
