package com.ra.course.hotel.model.entity.common;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
public class DayInterval extends Interval {
    private LocalDate startDate;

    public DayInterval(LocalDate startDate, int duration) {
        super(duration);
        this.startDate = startDate;
    }
}
