package com.ra.course.hotel.model.entity.persons;

import com.ra.course.hotel.model.entity.common.Address;
import com.ra.course.hotel.model.entity.account.Account;
import com.ra.course.hotel.model.entity.account.AccountType;
import com.ra.course.hotel.model.entity.room.Room;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HouseKeeper extends Person {
    private Room room;

    public HouseKeeper() {
        this.accountType = AccountType.HOUSEKEEPER;
    }

    public HouseKeeper(String name, Address address, String email, String phone,  Account account) {
        super(name, address, email, phone, account);
        this.accountType = AccountType.HOUSEKEEPER;
    }
}
