package com.ra.course.hotel.model.entity.room;

import com.ra.course.hotel.model.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
@Setter
public class Room extends BaseEntity {
    private String roomNumber;
    private RoomStyle style;
    private RoomStatus roomStatus;
    private double bookingPrice;
    private boolean smokingRoom;
    private List<RoomHouseKeeping> houseKeepings;
    private List<RoomKey> keys;

    private static final RoomStatus DEFAULT_ROOM_STATUS = RoomStatus.AVAILABLE;

    public Room() {
        this.houseKeepings = new CopyOnWriteArrayList<>();
        this.keys = new CopyOnWriteArrayList<>();
    }

    public Room(String roomNumber, RoomStyle style, double bookingPrice, boolean smokingRoom) {
        this.roomNumber = roomNumber;
        this.style = style;
        this.roomStatus = DEFAULT_ROOM_STATUS;
        this.bookingPrice = bookingPrice;
        this.smokingRoom = smokingRoom;
        this.houseKeepings = new CopyOnWriteArrayList<>();
        this.keys = new CopyOnWriteArrayList<>();
    }
}
