package com.ra.course.hotel.model.entity.hotel;

import com.ra.course.hotel.model.entity.common.Address;
import com.ra.course.hotel.model.entity.BaseEntity;
import com.ra.course.hotel.model.entity.room.Room;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
@Setter
public class HotelLocation extends BaseEntity {
    private String name;
    private Address location;
    private List<Room> rooms;

    public HotelLocation() {
        this.rooms = new CopyOnWriteArrayList<>();
    }

    public HotelLocation(String name, Address location) {
        this.name = name;
        this.location = location;
        this.rooms = new CopyOnWriteArrayList<>();
    }
}
