package com.ra.course.hotel.model.entity.charge;

import com.ra.course.hotel.model.entity.BaseEntity;
import com.ra.course.hotel.model.entity.invoice.InvoiceItem;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@NoArgsConstructor
@Getter
@Setter
public class RoomCharge extends BaseEntity {
    private LocalDate issuedAt;
    private InvoiceItem invoiceItem;

    public RoomCharge(LocalDate issuedAt) {
        this.issuedAt = issuedAt;
    }
}
