package com.ra.course.hotel.model.entity.invoice;

import com.ra.course.hotel.model.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class InvoiceItem extends BaseEntity {
    private String description;
    private double amount;
}
