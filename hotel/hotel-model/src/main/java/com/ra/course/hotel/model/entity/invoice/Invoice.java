package com.ra.course.hotel.model.entity.invoice;

import com.ra.course.hotel.model.entity.BaseEntity;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.payment.BillTransaction;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
@Setter
public class Invoice extends BaseEntity {
    private double totalAmount;
    private RoomBooking booking;
    private List<InvoiceItem> invoiceItems;
    private BillTransaction transaction;

    public Invoice() {
        this.invoiceItems = new CopyOnWriteArrayList<>();
    }

    public Invoice(double totalAmount, RoomBooking booking) {
        this.totalAmount = totalAmount;
        this.booking = booking;
        this.invoiceItems = new CopyOnWriteArrayList<>();
    }
}
