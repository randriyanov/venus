package com.ra.course.hotel.model.entity.booking;

import com.ra.course.hotel.model.entity.BaseEntity;
import com.ra.course.hotel.model.entity.common.DayInterval;
import com.ra.course.hotel.model.entity.invoice.Invoice;
import com.ra.course.hotel.model.entity.invoice.InvoiceItem;
import com.ra.course.hotel.model.entity.notification.Notification;
import com.ra.course.hotel.model.entity.room.Room;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
@Setter
public class RoomBooking extends BaseEntity {
    private String reservationNumber;
    private DayInterval interval;
    private BookingStatus status;
    private LocalDateTime checkin;
    private LocalDateTime checkout;
    private Room room;
    private List<Invoice> invoices;
    private List<Notification> notifications;

    private static final BookingStatus DEFAULT_BOOKING_STATUS = BookingStatus.REQUESTED;

    public RoomBooking() {
        this.invoices = new CopyOnWriteArrayList<>();
        this.notifications = new CopyOnWriteArrayList<>();
    }

    public RoomBooking(String reservationNumber, DayInterval interval, Room room) {
        this.reservationNumber = reservationNumber;
        this.interval = interval;
        this.status = DEFAULT_BOOKING_STATUS;
        this.room = room;
        this.invoices = new CopyOnWriteArrayList<>();
        this.notifications = new CopyOnWriteArrayList<>();
    }
}
