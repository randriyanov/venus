package com.ra.course.hotel.model.entity.booking;

public enum BookingStatus {
    REQUESTED,
    PENDING,
    CONFIRMED,
    CHECKED_IN,
    CHECKED_OUT,
    CANCELED,
    ABANDONED
}
