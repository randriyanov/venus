package com.ra.course.hotel.model.entity.notification;

import com.ra.course.hotel.model.entity.common.Address;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@Getter
@Setter
public class PostalNotification extends Notification {
    private Address address;

    public PostalNotification(String content, Address address) {
        super(content);
        this.address = address;
    }
}
