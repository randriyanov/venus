package com.ra.course.hotel.model.entity.common;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@Getter
@Setter
public class MinuteInterval extends Interval {
    private LocalDateTime startDatetime;

    public MinuteInterval(LocalDateTime startDatetime, int duration) {
        super(duration);
        this.startDatetime = startDatetime;
    }
}
