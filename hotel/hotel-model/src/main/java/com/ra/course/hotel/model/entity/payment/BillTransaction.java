package com.ra.course.hotel.model.entity.payment;

import com.ra.course.hotel.model.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public abstract class BillTransaction extends BaseEntity {
    private LocalDateTime creationDate;
    private double amount;
    private PaymentStatus status;

    private static final PaymentStatus DEFAULT_PAYMENT_STATUS = PaymentStatus.UNPAID;

    public BillTransaction() {
        this.creationDate = LocalDateTime.now();
    }

    public BillTransaction(double amount) {
        this.creationDate = LocalDateTime.now();
        this.amount = amount;
        this.status = DEFAULT_PAYMENT_STATUS;
    }
}
