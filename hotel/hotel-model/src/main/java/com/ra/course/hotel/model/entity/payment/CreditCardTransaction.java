package com.ra.course.hotel.model.entity.payment;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class CreditCardTransaction extends BillTransaction {
    private String nameOnCard;
    private String zipCode;

    public CreditCardTransaction(double amount, String nameOnCard, String zipCode) {
        super(amount);
        this.nameOnCard = nameOnCard;
        this.zipCode = zipCode;
    }
}
