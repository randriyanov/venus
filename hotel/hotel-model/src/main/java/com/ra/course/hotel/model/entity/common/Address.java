package com.ra.course.hotel.model.entity.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Address {
    private String streetAddress;
    private String city;
    private String state;
    private String zipcode;
    private String country;
}
