package com.ra.course.hotel.model.entity.persons;

import com.ra.course.hotel.model.entity.common.Address;
import com.ra.course.hotel.model.entity.account.Account;
import com.ra.course.hotel.model.entity.account.AccountType;

public class Receptionist extends BookingPerson {
    public Receptionist() {
        this.accountType = AccountType.RECEPTIONIST;
    }

    public Receptionist(String name, Address address, String email, String phone, Account account) {
        super(name, address, email, phone, account);
        this.accountType = AccountType.RECEPTIONIST;
    }
}
