package com.ra.course.hotel.model.entity.persons;

import com.ra.course.hotel.model.entity.BaseEntity;
import com.ra.course.hotel.model.entity.account.Account;
import com.ra.course.hotel.model.entity.account.AccountType;
import com.ra.course.hotel.model.entity.common.Address;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public abstract class Person extends BaseEntity {
    protected String name;
    protected Address address;
    protected String email;
    protected String phone;
    protected Account account;
    protected AccountType accountType;

    public Person(String name, Address address, String email, String phone, Account account) {
        this.name = name;
        this.address = address;
        this.email = email;
        this.phone = phone;
        this.account = account;
    }
}
