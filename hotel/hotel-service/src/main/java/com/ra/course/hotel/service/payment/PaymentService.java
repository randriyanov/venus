package com.ra.course.hotel.service.payment;

import com.ra.course.hotel.model.entity.invoice.Invoice;
import com.ra.course.hotel.model.entity.payment.*;

public interface PaymentService {
    CreditCardTransaction createCreditCardTransaction(String nameOnCard, String zipCode, Invoice invoice);
    CheckTransaction createCheckTransaction(String bankName, String checkNumber, Invoice invoice);
    CashTransaction createCashTransaction(double cashTendered, Invoice invoice);
    void updatePaymentStatus(BillTransaction transaction, PaymentStatus status);
}
