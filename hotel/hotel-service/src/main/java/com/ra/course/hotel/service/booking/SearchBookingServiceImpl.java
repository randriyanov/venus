package com.ra.course.hotel.service.booking;

import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.persistence.booking.RoomBookingRepo;
import com.ra.course.hotel.service.exception.EntityNotFoundException;

import java.time.LocalDate;
import java.util.Optional;

public class SearchBookingServiceImpl implements  SearchBookingService {
    private final transient RoomBookingRepo bookingRepo;

    public SearchBookingServiceImpl(final RoomBookingRepo bookingRepo) {
        this.bookingRepo = bookingRepo;
    }

    @Override
    public RoomBooking searchByReservNumber(final String reservNumber) {
        return bookingRepo.getByReservNumber(reservNumber)
                .orElseThrow(() -> new EntityNotFoundException("Room booking number "+ reservNumber));
    }

    @Override
    public Optional<RoomBooking> searchCurrentByRoom(final Room room) {
        return bookingRepo.getAllActiveByRoom(room).stream()
                .filter(b -> b.getInterval().getStartDate().isBefore(LocalDate.now()))
                .filter(b -> b.getInterval().getStartDate().plusDays(b.getInterval().getDuration())
                        .isAfter(LocalDate.now())).findFirst();
    }
}
