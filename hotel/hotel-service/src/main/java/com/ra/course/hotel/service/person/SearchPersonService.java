package com.ra.course.hotel.service.person;

import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.model.entity.persons.Receptionist;
import com.ra.course.hotel.model.entity.room.Room;

import java.util.Optional;

public interface SearchPersonService {
    Guest searchGuestById(long guestId);
    Guest searchGuestByName(String guestName);
    Guest searchGuestByBooking(RoomBooking booking);
    Optional<Guest> searchCurrentGuestByRoom(Room room);
    Receptionist searchReceptionistById(long receptionistId);
}
