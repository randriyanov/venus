package com.ra.course.hotel.service.exception;

public class EntityNotFoundException extends RuntimeException {
    private static final long serialVersionUID = 1;

    public EntityNotFoundException(final String message) {
        super(message + " not found.");
    }
}
