package com.ra.course.hotel.service.payment;

import com.ra.course.hotel.model.entity.invoice.Invoice;
import com.ra.course.hotel.model.entity.payment.*;
import com.ra.course.hotel.persistence.payment.CashTransactionRepo;
import com.ra.course.hotel.persistence.payment.CheckTransactionRepo;
import com.ra.course.hotel.persistence.payment.CreditCardTransactionRepo;

public class PaymentServiceImpl implements PaymentService {
    private final transient CreditCardTransactionRepo cardPayRepo;
    private final transient CheckTransactionRepo checkPayRepo;
    private final transient CashTransactionRepo cashPayRepo;

    public PaymentServiceImpl(final CreditCardTransactionRepo cardPayRepo, final CheckTransactionRepo checkPayRepo,
                              final CashTransactionRepo cashPayRepo) {
        this.cardPayRepo = cardPayRepo;
        this.checkPayRepo = checkPayRepo;
        this.cashPayRepo = cashPayRepo;
    }

    @Override
    public CreditCardTransaction createCreditCardTransaction(final String nameOnCard, final String zipCode,
                                                             final Invoice invoice) {
        final CreditCardTransaction transaction = new CreditCardTransaction(invoice.getTotalAmount(), nameOnCard, zipCode);
        final CreditCardTransaction transactionWithId = cardPayRepo.add(transaction);
        invoice.setTransaction(transactionWithId);
        return transactionWithId;
    }

    @Override
    public CheckTransaction createCheckTransaction(final String bankName, final String checkNumber,
                                                   final Invoice invoice) {
        final CheckTransaction transaction = new CheckTransaction(invoice.getTotalAmount(), bankName, checkNumber);
        final CheckTransaction transactionWithId = checkPayRepo.add(transaction);
        invoice.setTransaction(transactionWithId);
        return transactionWithId;
    }

    @Override
    public CashTransaction createCashTransaction(final double cashTendered, final Invoice invoice) {
        final CashTransaction transaction = new CashTransaction(invoice.getTotalAmount(), cashTendered);
        final CashTransaction transactionWithId = cashPayRepo.add(transaction);
        invoice.setTransaction(transactionWithId);
        return transactionWithId;
    }

    @Override
    public void updatePaymentStatus(final BillTransaction transaction, final PaymentStatus status) {
        transaction.setStatus(status);
    }
}
