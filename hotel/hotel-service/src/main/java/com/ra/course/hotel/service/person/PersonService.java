package com.ra.course.hotel.service.person;

import com.ra.course.hotel.model.entity.persons.HouseKeeper;
import com.ra.course.hotel.model.entity.persons.Person;
import com.ra.course.hotel.model.entity.room.Room;

public interface PersonService {
    Person addPerson(Person person);
    void updatePerson(Person person);

    void assignHouseKeeperToRoom(HouseKeeper houseKeeper, Room room);
}
