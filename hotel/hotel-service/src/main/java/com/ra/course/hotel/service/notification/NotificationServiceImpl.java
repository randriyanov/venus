package com.ra.course.hotel.service.notification;

import com.ra.course.hotel.model.entity.booking.BookingStatus;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.common.Address;
import com.ra.course.hotel.model.entity.notification.EmailNotification;
import com.ra.course.hotel.model.entity.notification.Notification;
import com.ra.course.hotel.model.entity.notification.PostalNotification;
import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.persistence.booking.RoomBookingRepo;
import com.ra.course.hotel.persistence.notification.EmailNotificationRepo;
import com.ra.course.hotel.persistence.notification.PostalNotificationRepo;
import com.ra.course.hotel.service.person.SearchPersonService;
import lombok.Builder;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

@Builder
public class NotificationServiceImpl implements NotificationService {
    private final transient PostalNotificationRepo postalNotifRepo;
    private final transient EmailNotificationRepo emailNotifRepo;
    private final transient RoomBookingRepo bookingRepo;
    private final transient SearchPersonService sPersonService;

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private static final String DEAR_STRING = "Dear ";

    @Override
    public PostalNotification createPostalNotification(final String content, final long bookingId,
                                                       final Address address) {
        final PostalNotification notification = new PostalNotification(content, address);
        final PostalNotification notifWithId = postalNotifRepo.add(notification);
        assignBooking(notifWithId, bookingId);
        return notifWithId;
    }

    @Override
    public EmailNotification createEmailNotification(final String content, final long bookingId,
                                                     final String email) {
        final EmailNotification notification = new EmailNotification(content, email);
        final EmailNotification notifWithId = emailNotifRepo.add(notification);
        assignBooking(notifWithId, bookingId);
        return notifWithId;
    }

    @Override
    public List<EmailNotification> prepareCheckInNotifications(final int daysBefore) {
        final var list = new CopyOnWriteArrayList<EmailNotification>();

        bookingRepo.getAllByStatus(BookingStatus.CONFIRMED).stream()
                .filter(b -> LocalDate.now().plusDays(daysBefore).isEqual(b.getInterval().getStartDate()))
                .forEach((RoomBooking b) -> {
                        final Guest guest = sPersonService.searchGuestByBooking(b);
                        final String content = DEAR_STRING + guest.getName() + "!" + System.lineSeparator()
                                + "Your check in date is " + b.getInterval().getStartDate().toString()
                                + System.lineSeparator()  + "See you soon :) ...";
                        list.add(createEmailNotification(content, b.getId(), guest.getEmail()));
                 });

        return list;
    }

    @Override
    public List<EmailNotification> prepareCheckOutNotifications(final int daysBefore) {
        final var list = new CopyOnWriteArrayList<EmailNotification>();
        bookingRepo.getAllByStatus(BookingStatus.CHECKED_IN).stream()
                .filter(b -> LocalDate.now().plusDays(daysBefore)
                        .isEqual(b.getInterval().getStartDate().plusDays(b.getInterval().getDuration())))
                .forEach((RoomBooking b) -> {
                    final Guest guest = sPersonService.searchGuestByBooking(b);
                    final String content = DEAR_STRING  + guest.getName() + "!" + System.lineSeparator()
                            + "Your check out date is " + b.getInterval().getStartDate().format(formatter)
                            + System.lineSeparator() + "Please pay all invoices and return all keys before it :) ...";
                    list.add(createEmailNotification(content, b.getId(), guest.getEmail()));
                });
        return list;
    }

    private void assignBooking (final Notification notification, final long bookingId){
        final Optional<RoomBooking> booking = bookingRepo.getById(bookingId);
        if (booking.isPresent()) {
            notification.setBooking(booking.get());
            booking.get().getNotifications().add(notification);
        }
    }
}
