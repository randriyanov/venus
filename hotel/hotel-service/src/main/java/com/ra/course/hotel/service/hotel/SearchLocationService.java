package com.ra.course.hotel.service.hotel;

import com.ra.course.hotel.model.entity.hotel.HotelLocation;
import com.ra.course.hotel.model.entity.room.Room;

public interface SearchLocationService {
    HotelLocation searchById(long id);
    HotelLocation searchByName(String name);
    HotelLocation searchByRoom(Room room);
}
