package com.ra.course.hotel.service.person;

import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.model.entity.persons.HouseKeeper;
import com.ra.course.hotel.model.entity.persons.Person;
import com.ra.course.hotel.model.entity.persons.Receptionist;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.persistence.person.GuestRepo;
import com.ra.course.hotel.persistence.person.HouseKeeperRepo;
import com.ra.course.hotel.persistence.person.PersonRepo;
import com.ra.course.hotel.persistence.person.ReceptionistRepo;
import lombok.Builder;

@Builder
public class PersonServiceImpl implements PersonService {
    private final transient PersonRepo personRepo;
    private final transient GuestRepo guestRepo;
    private final transient ReceptionistRepo receptionistRepo;
    private final transient HouseKeeperRepo houseKeeperRepo;

    @Override
    public Person addPerson(final Person person) {
        final Person personWithId = personRepo.add(person);
        switch (personWithId.getAccountType()) {
            case RECEPTIONIST: receptionistRepo.add((Receptionist) personWithId); break;
            case HOUSEKEEPER: houseKeeperRepo.add((HouseKeeper) personWithId); break;
            default: guestRepo.add((Guest) personWithId); break;
        }

        return personWithId;
    }

    @Override
    public void updatePerson(final Person person) {
        personRepo.update(person);
    }

    @Override
    public void assignHouseKeeperToRoom(final HouseKeeper houseKeeper, final Room room) {
        houseKeeper.setRoom(room);
    }
}
