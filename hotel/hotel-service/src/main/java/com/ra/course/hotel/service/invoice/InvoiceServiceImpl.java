package com.ra.course.hotel.service.invoice;

import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.charge.RoomCharge;
import com.ra.course.hotel.model.entity.invoice.Invoice;
import com.ra.course.hotel.model.entity.invoice.InvoiceItem;
import com.ra.course.hotel.persistence.invoice.InvoiceItemRepo;
import com.ra.course.hotel.persistence.invoice.InvoiceRepo;

public class InvoiceServiceImpl implements InvoiceService {
    private final transient InvoiceRepo invoiceRepo;
    private final transient InvoiceItemRepo invoiceItemRepo;
    private final transient SearchInvoiceService sInvoiceService;

    public InvoiceServiceImpl(final InvoiceRepo invoiceRepo, final InvoiceItemRepo invoiceItemRepo,
                              final SearchInvoiceService sInvoiceService) {
        this.invoiceRepo = invoiceRepo;
        this.invoiceItemRepo = invoiceItemRepo;
        this.sInvoiceService = sInvoiceService;
    }

    @Override
    public Invoice createInvoice(final Invoice invoice) {
        final Invoice invoiceWithId = invoiceRepo.add(invoice);
        if (invoice.getBooking() != null) {
            addInvoiceToBooking(invoiceWithId);
        }
        return invoiceWithId;
    }

    @Override
    public void updateInvoice(final Invoice invoice) {
        final RoomBooking oldBooking = sInvoiceService.searchInvoiceById(invoice.getId()).getBooking();
        final RoomBooking newBooking = invoice.getBooking();

        invoiceRepo.update(invoice);
        if (oldBooking == null && newBooking != null) {
            addInvoiceToBooking(invoice);
        }
        else {
            if (oldBooking != null && newBooking == null) {
                removeInvoiceFromBooking(oldBooking, invoice.getId());
            }
        }
    }

    @Override
    public void removeInvoice(final Invoice invoice) {
        removeInvoiceFromBooking(invoice.getBooking(), invoice.getId());
        invoiceRepo.remove(invoice);
    }

    @Override
    public InvoiceItem addInvoiceItem(final InvoiceItem item, final Invoice invoice, final RoomCharge charge) {
        final InvoiceItem itemWithId = invoiceItemRepo.add(item);

        invoice.getInvoiceItems().add(itemWithId);
        updateInvoiceAmount(invoice);

        charge.setInvoiceItem(itemWithId);

        return itemWithId;
    }

    @Override
    public void updateInvoiceItem(final InvoiceItem item) {
        invoiceItemRepo.update(item);

        final Invoice invoice = sInvoiceService.searchInvoiceByItem(item);
        updateInvoiceAmount(invoice);
    }

    @Override
    public void removeInvoiceItem(final InvoiceItem item) {
        final Invoice invoice = sInvoiceService.searchInvoiceByItem(item);
        invoice.getInvoiceItems().remove(item);
        updateInvoiceAmount(invoice);

        invoiceItemRepo.remove(item);
    }

    private void addInvoiceToBooking(final Invoice invoice) {
        invoice.getBooking().getInvoices().add(invoice);
    }

    private void removeInvoiceFromBooking(final RoomBooking booking, final long invoiceId) {
        final var invoiceInBooking = booking.getInvoices().stream()
                .filter(inv -> inv.getId() == invoiceId).findFirst().orElseThrow();
        booking.getInvoices().remove(invoiceInBooking);
    }

    private void updateInvoiceAmount(final Invoice invoice) {
        invoice.setTotalAmount(invoice.getInvoiceItems().stream().mapToDouble(InvoiceItem::getAmount).sum());
    }
}