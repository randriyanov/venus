package com.ra.course.hotel.service.person;

import com.ra.course.hotel.model.entity.account.Account;
import com.ra.course.hotel.model.entity.account.AccountStatus;
import com.ra.course.hotel.model.entity.persons.Person;
import com.ra.course.hotel.persistence.person.AccountRepo;
import com.ra.course.hotel.persistence.person.PersonRepo;
import com.ra.course.hotel.service.exception.EntityNotFoundException;
import com.ra.course.hotel.service.notification.NotificationService;

import java.util.Optional;
import java.util.Random;

public class AccountServiceImpl implements AccountService {
    private final transient AccountRepo accountRepo;
    private final transient PersonRepo personRepo;
    private final transient NotificationService notifService;

    public AccountServiceImpl(final AccountRepo accountRepo, final PersonRepo personRepo,
                              final NotificationService notifService) {
        this.accountRepo = accountRepo;
        this.personRepo = personRepo;
        this.notifService = notifService;
    }

    @Override
    public Account createAccount(final Account account) {
        return accountRepo.add(account);
    }

    @Override
    public void updatePassword(final Account account, final String password) {
        account.setPassword(password);
    }

    @Override
    public void resetPassword(final String email) {
        final Optional<Person> person = personRepo.getByEmail(email);
        if (person.isEmpty() || person.get().getAccount() == null) {
            throw new EntityNotFoundException("Account for email " + email);
        }

        final String newPassword = generatePassword();
        person.get().getAccount().setPassword(newPassword);
        notifService.createEmailNotification("Your temporary password is " + newPassword,0, email);
    }

    @Override
    public Account login(final String login, final String password) {
        final Account account = accountRepo.getByCredentials(login, password)
                .orElseThrow(() -> new EntityNotFoundException("Account"));
        account.setStatus(AccountStatus.ACTIVE);
        return account;
    }

    @Override
    public void logout(final Account account) {
        account.setStatus(AccountStatus.CLOSED);
    }

    private String generatePassword() {
        final int leftBigLetLim = 65; // letter 'A'
        final int rightBigLetLim = 90; // letter 'Z'

        final int leftSmallLetLim = 97; // letter 'a'
        final int rightSmallLetLim = 122; // letter 'z'

        final int leftDigitLim = 48; // numeral '0'
        final int rightDigitLim = 57; // numeral '9'

        final Random random = new Random();

        return random.ints(leftBigLetLim, rightBigLetLim + 1)
                .limit(2).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString() +
                random.ints(leftSmallLetLim, rightSmallLetLim + 1)
                        .limit(4).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                        .toString() +
                random.ints(leftDigitLim, rightDigitLim + 1)
                        .limit(2).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                        .toString();
    }
}
