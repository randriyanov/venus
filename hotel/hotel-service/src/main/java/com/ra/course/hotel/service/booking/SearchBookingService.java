package com.ra.course.hotel.service.booking;

import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.room.Room;

import java.util.Optional;

public interface SearchBookingService {
    RoomBooking searchByReservNumber(String reservNumber);
    Optional<RoomBooking> searchCurrentByRoom(Room room);
}
