package com.ra.course.hotel.service.search;

import com.ra.course.hotel.model.entity.common.DayInterval;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomStyle;

import java.util.List;

public interface SearchAvailableService {
    boolean isRoomAvailable(String roomNumber, DayInterval interval);
    List<Room> searchRoomsByLocation(long locationId, RoomStyle style, DayInterval interval);
    List<Room> searchRooms(RoomStyle style, DayInterval interval);
}
