package com.ra.course.hotel.service.room;

import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomHouseKeeping;
import com.ra.course.hotel.model.entity.room.RoomStatus;
import com.ra.course.hotel.persistence.room.HouseKeepingRepo;
import com.ra.course.hotel.service.booking.SearchBookingService;
import com.ra.course.hotel.service.exception.EntityNotFoundException;

import java.util.Optional;

public class HouseKeepingServiceImpl implements HouseKeepingService {
    private final transient HouseKeepingRepo houseKeepingRepo;
    private final transient SearchRoomService sRoomService;
    private final transient SearchBookingService sBookingService;

    public HouseKeepingServiceImpl(final HouseKeepingRepo houseKeepingRepo, final SearchRoomService sRoomService,
                                   final SearchBookingService sBookingService) {
        this.houseKeepingRepo = houseKeepingRepo;
        this.sRoomService = sRoomService;
        this.sBookingService = sBookingService;
    }

    @Override
    public RoomHouseKeeping addHouseKeeping(final long roomId, final RoomHouseKeeping houseKeeping) {
        final Room room = sRoomService.searchById(roomId);

        final RoomHouseKeeping hkWithId = houseKeepingRepo.add(houseKeeping);
        room.getHouseKeepings().add(hkWithId);
        if (houseKeeping.getInterval().getDuration() == 0) {
            room.setRoomStatus(RoomStatus.BEING_SERVICED);
        }

        return hkWithId;
    }

    @Override
    public void updateHouseKeeping(final RoomHouseKeeping houseKeeping) {
        final int oldDuration = searchById(houseKeeping.getId()).getInterval().getDuration();

        houseKeepingRepo.update(houseKeeping);

        if (oldDuration == 0 && houseKeeping.getInterval().getDuration() > 0) {
            final Room room = sRoomService.searchByHouseKeeping(houseKeeping);

            final Optional<RoomBooking> booking = sBookingService.searchCurrentByRoom(room);
            if (booking.isPresent()) {
                switch(booking.get().getStatus()) {
                    case CONFIRMED: room.setRoomStatus(RoomStatus.RESERVED); break;
                    case CHECKED_IN: room.setRoomStatus(RoomStatus.OCCUPIED); break;
                    default: room.setRoomStatus(RoomStatus.AVAILABLE); break;
                }
            }
            else {
                room.setRoomStatus(RoomStatus.AVAILABLE);
            }
        }
    }

    @Override
    public RoomHouseKeeping searchById(final long id) {
        return houseKeepingRepo.getById(id)
                .orElseThrow(() -> new EntityNotFoundException("Room housekeeping with id = " + id));
    }
}
