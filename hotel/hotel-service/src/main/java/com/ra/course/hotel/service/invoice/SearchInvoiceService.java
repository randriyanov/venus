package com.ra.course.hotel.service.invoice;

import com.ra.course.hotel.model.entity.invoice.Invoice;
import com.ra.course.hotel.model.entity.invoice.InvoiceItem;

public interface SearchInvoiceService {
    Invoice searchInvoiceById(long id);
    Invoice searchInvoiceByItem(InvoiceItem item);
    InvoiceItem searchInvoiceItemById(long id);
}
