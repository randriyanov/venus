package com.ra.course.hotel.service.room;

import com.ra.course.hotel.model.entity.room.RoomHouseKeeping;

public interface HouseKeepingService {
    RoomHouseKeeping addHouseKeeping(long roomId, RoomHouseKeeping houseKeeping);
    void updateHouseKeeping(RoomHouseKeeping houseKeeping);
    RoomHouseKeeping searchById(long id);
}
