package com.ra.course.hotel.service.room;

import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomKey;
import com.ra.course.hotel.persistence.room.RoomKeyRepo;
import com.ra.course.hotel.service.exception.EntityNotFoundException;

import java.time.LocalDateTime;

public class RoomKeyServiceImpl implements RoomKeyService {
    private final transient RoomKeyRepo roomKeyRepo;
    private final transient SearchRoomService sRoomService;

    public RoomKeyServiceImpl(final RoomKeyRepo roomKeyRepo, final SearchRoomService searchRoomService) {
        this.roomKeyRepo = roomKeyRepo;
        this.sRoomService = searchRoomService;
    }

    @Override
    public RoomKey issueKey(final long roomId) {
        final Room room = sRoomService.searchById(roomId);

        final RoomKey key = room.getKeys().stream().filter(k -> !k.isActive()).findAny()
                .orElseThrow(() -> new EntityNotFoundException("Inactive key for room number " + room.getRoomNumber()));
        key.setIssuedAt(LocalDateTime.now());
        key.setActive(true);

        return key;
    }

    @Override
    public RoomKey searchByKeyId(final String keyId) {
        return roomKeyRepo.getByKeyId(keyId).orElseThrow(() -> new EntityNotFoundException("Room key " + keyId));
    }

    @Override
    public void receiveKey(final String keyId) {
        final RoomKey key = searchByKeyId(keyId);
        key.setIssuedAt(null);
        key.setActive(false);
    }
}
