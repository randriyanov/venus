package com.ra.course.hotel.service.booking;

import com.ra.course.hotel.model.entity.room.RoomKey;

public interface CheckInOutService {
    RoomKey checkIn(String reservNumber);
    void checkOut(String reservNumber);
}
