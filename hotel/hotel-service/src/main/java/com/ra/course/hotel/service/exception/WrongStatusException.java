package com.ra.course.hotel.service.exception;

public class WrongStatusException extends RuntimeException {
    private static final long serialVersionUID = 1;

    public WrongStatusException(final String message) {
        super("Wrong status for " + message);
    }
}
