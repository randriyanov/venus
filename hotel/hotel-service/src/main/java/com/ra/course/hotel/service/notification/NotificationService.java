package com.ra.course.hotel.service.notification;

import com.ra.course.hotel.model.entity.common.Address;
import com.ra.course.hotel.model.entity.notification.EmailNotification;
import com.ra.course.hotel.model.entity.notification.PostalNotification;

import java.util.List;

public interface NotificationService {
    PostalNotification createPostalNotification(String content, long bookingId, Address address);
    EmailNotification createEmailNotification(String content, long bookingId, String email);
    List<EmailNotification> prepareCheckInNotifications(int daysBefore);
    List<EmailNotification> prepareCheckOutNotifications(int daysBefore);
}
