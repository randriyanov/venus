package com.ra.course.hotel.service.room;

import com.ra.course.hotel.model.entity.room.Room;

public interface RoomService {
    Room addRoom(long locationId, Room room);
    void editRoom(Room room);
    void removeRoom(long roomId);
}
