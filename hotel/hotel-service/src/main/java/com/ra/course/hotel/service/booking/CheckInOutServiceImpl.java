package com.ra.course.hotel.service.booking;

import com.ra.course.hotel.model.entity.account.AccountType;
import com.ra.course.hotel.model.entity.booking.BookingStatus;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.invoice.Invoice;
import com.ra.course.hotel.model.entity.payment.PaymentStatus;
import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomKey;
import com.ra.course.hotel.model.entity.room.RoomStatus;
import com.ra.course.hotel.service.exception.WrongStatusException;
import com.ra.course.hotel.service.person.SearchPersonService;
import com.ra.course.hotel.service.room.RoomKeyService;

import java.time.LocalDateTime;
import java.util.Arrays;

public class CheckInOutServiceImpl implements CheckInOutService {
    private final transient RoomKeyService roomKeyService;
    private final transient SearchBookingService sBookingService;
    private final transient SearchPersonService sPersonService;

    private static final int MIN_MEMBER_ROOMS = 1;

    public CheckInOutServiceImpl(final RoomKeyService roomKeyService, final SearchBookingService sBookingService,
                                 final SearchPersonService sPersonService) {
        this.roomKeyService = roomKeyService;
        this.sBookingService = sBookingService;
        this.sPersonService = sPersonService;
    }

    @Override
    public RoomKey checkIn(final String reservNumber) {
        final RoomBooking booking = sBookingService.searchByReservNumber(reservNumber);
        if (booking.getStatus() != BookingStatus.CONFIRMED) {
            throw new WrongStatusException("check-in (BookingStatus." + booking.getStatus() + ")");
        }

        final Room room = booking.getRoom();
        final double payment = booking.getInvoices().stream().filter(inv -> inv.getTransaction().getStatus() == PaymentStatus.COMPLETED)
                .mapToDouble(inv -> inv.getTransaction().getAmount()).sum();
        if (payment < room.getBookingPrice()) {
            throw new WrongStatusException("check-in (your payment = " + payment  + ", minimal payment = " + room.getBookingPrice() +")");
        }

        if (!Arrays.asList(RoomStatus.AVAILABLE, RoomStatus.RESERVED).contains(room.getRoomStatus())) {
            throw new WrongStatusException("check-in (RoomStatus." + room.getRoomStatus() + ")");
        }

        final RoomKey key = roomKeyService.issueKey(room.getId());
        room.setRoomStatus(RoomStatus.OCCUPIED);
        booking.setStatus(BookingStatus.CHECKED_IN);
        booking.setCheckin(LocalDateTime.now());

        final Guest guest = sPersonService.searchGuestByBooking(booking);
        guest.setTotalRoomsCheckedIn(guest.getTotalRoomsCheckedIn() + 1);
        if (guest.getTotalRoomsCheckedIn() == MIN_MEMBER_ROOMS) {
            guest.setAccountType(AccountType.MEMBER);
        }

        return key;
    }

    @Override
    public void checkOut(final String reservNumber) {
        final RoomBooking booking = sBookingService.searchByReservNumber(reservNumber);

        if (booking.getStatus() != BookingStatus.CHECKED_IN) {
            throw new WrongStatusException("check-out (BookingStatus." + booking.getStatus() + ")");
        }

        final double bookingAmount = booking.getInvoices().stream().mapToDouble(Invoice::getTotalAmount).sum();
        final double bookingPayment = getTotalPayment(booking);
        if (bookingPayment < bookingAmount) {
            throw new WrongStatusException("check-out (your payment = " + bookingPayment
                    + ", invoice total amount = " + bookingAmount +")");
        }

        booking.getRoom().getKeys().forEach(k -> roomKeyService.receiveKey(k.getKeyId()));
        booking.getRoom().setRoomStatus(RoomStatus.NOT_AVAILABLE);
        booking.setStatus(BookingStatus.CHECKED_OUT);
        booking.setCheckout(LocalDateTime.now());
    }

    private double getTotalPayment(final RoomBooking booking) {
        return booking.getInvoices().stream()
                .filter(inv -> inv.getTransaction().getStatus() == PaymentStatus.COMPLETED)
                .mapToDouble(inv -> inv.getTransaction().getAmount()).sum();
    }
}
