package com.ra.course.hotel.service.person;

import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.model.entity.persons.Receptionist;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.persistence.person.GuestRepo;
import com.ra.course.hotel.persistence.person.ReceptionistRepo;
import com.ra.course.hotel.service.booking.SearchBookingService;
import com.ra.course.hotel.service.exception.EntityNotFoundException;

import java.util.Optional;

public class SearchPersonServiceImpl implements SearchPersonService {
    private final transient GuestRepo guestRepo;
    private final transient ReceptionistRepo receptionistRepo;
    private final transient SearchBookingService sBookingService;

    public SearchPersonServiceImpl(final GuestRepo guestRepo, final ReceptionistRepo receptionistRepo,
                                   final SearchBookingService sBookingService) {
        this.guestRepo = guestRepo;
        this.receptionistRepo = receptionistRepo;
        this.sBookingService = sBookingService;
    }

    @Override
    public Guest searchGuestById(final long guestId) {
        return guestRepo.getById(guestId).orElseThrow(() -> new EntityNotFoundException("Guest with id = " + guestId));
    }

    @Override
    public Guest searchGuestByName(final String guestName) {
        return guestRepo.getByName(guestName).orElseThrow(() -> new EntityNotFoundException("Guest " + guestName));
    }

    @Override
    public Guest searchGuestByBooking(final RoomBooking booking) {
        return guestRepo.getByBooking(booking)
                .orElseThrow(() -> new EntityNotFoundException("Guest with room booking "
                + booking.getReservationNumber()));
    }

    @Override
    public Optional<Guest> searchCurrentGuestByRoom(final Room room) {
        final Optional<RoomBooking> booking = sBookingService.searchCurrentByRoom(room);
        return booking.flatMap(guestRepo::getByBooking);
    }

    @Override
    public Receptionist searchReceptionistById(final long receptionistId) {
        return receptionistRepo.getById(receptionistId)
                .orElseThrow(() -> new EntityNotFoundException("Receptionist with id = " + receptionistId));
    }
}
