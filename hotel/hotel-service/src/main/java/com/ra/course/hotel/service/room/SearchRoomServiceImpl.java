package com.ra.course.hotel.service.room;

import com.ra.course.hotel.model.entity.booking.BookingStatus;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomHouseKeeping;
import com.ra.course.hotel.persistence.room.RoomRepo;
import com.ra.course.hotel.service.exception.EntityNotFoundException;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class SearchRoomServiceImpl implements SearchRoomService {
    private final transient RoomRepo roomRepo;

    private static List<BookingStatus> activeStatuses = Arrays.asList
            (BookingStatus.PENDING, BookingStatus.CONFIRMED, BookingStatus.CHECKED_IN);

    public SearchRoomServiceImpl(final RoomRepo roomRepo) {
        this.roomRepo = roomRepo;
    }

    public Room searchById(final long roomId) {
        return roomRepo.getById(roomId)
                .orElseThrow(() -> new EntityNotFoundException("Room with id = " + roomId));
    }

    public Room searchByNumber(final String roomNumber) {
        return roomRepo.getByNumber(roomNumber)
                .orElseThrow(() -> new EntityNotFoundException("Room number " + roomNumber));
    }

    @Override
    public Room searchByHouseKeeping(final RoomHouseKeeping houseKeeping) {
        return roomRepo.getByHouseKeeping(houseKeeping)
                .orElseThrow(() -> new EntityNotFoundException("Room with housekeeping " + houseKeeping.getDescription()));
    }

    @Override
    public List<Room> searchAllWithActiveBookingsByGuest(final Guest guest) {
        final var list = new CopyOnWriteArrayList<Room>();
        guest.getBookings().stream().filter(b -> activeStatuses.contains(b.getStatus()))
                .forEach((RoomBooking b) -> list.addIfAbsent(b.getRoom()));

        return  list;
    }
}
