package com.ra.course.hotel.service.hotel;

import com.ra.course.hotel.model.entity.hotel.HotelLocation;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.persistence.hotel.HotelLocationRepo;
import com.ra.course.hotel.service.exception.EntityNotFoundException;

public class SearchLocationServiceImpl implements SearchLocationService {
    private final transient HotelLocationRepo locationRepo;

    public SearchLocationServiceImpl(final HotelLocationRepo locationRepo) {
        this.locationRepo = locationRepo;
    }

    public HotelLocation searchById(final long id) {
        return locationRepo.getById(id).orElseThrow(() -> new EntityNotFoundException("Hotel with id = " + id));
    }

    @Override
    public HotelLocation searchByName(final String name) {
        return locationRepo.getByName(name)
                .orElseThrow(() -> new EntityNotFoundException("Hotel " + name));
    }

    @Override
    public HotelLocation searchByRoom(final Room room) {
        return locationRepo.getByRoom(room)
                .orElseThrow(() -> new EntityNotFoundException("Hotel with room " + room.getRoomNumber()));
    }
}
