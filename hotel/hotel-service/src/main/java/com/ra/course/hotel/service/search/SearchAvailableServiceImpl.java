package com.ra.course.hotel.service.search;

import com.ra.course.hotel.model.entity.common.DayInterval;
import com.ra.course.hotel.model.entity.hotel.HotelLocation;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomStyle;
import com.ra.course.hotel.persistence.booking.RoomBookingRepo;
import com.ra.course.hotel.persistence.hotel.HotelRepo;
import com.ra.course.hotel.service.hotel.SearchLocationService;
import com.ra.course.hotel.service.room.SearchRoomService;
import lombok.Builder;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

@Builder
public class SearchAvailableServiceImpl implements SearchAvailableService {
    private final transient HotelRepo hotelRepo;
    private final transient RoomBookingRepo bookingRepo;
    private final transient SearchLocationService sLocationService;
    private final transient SearchRoomService sRoomService;

    @Override
    public boolean isRoomAvailable(final String roomNumber, final DayInterval interval) {
        final Room room = sRoomService.searchByNumber(roomNumber);

        // a - c = existing interval, b - d = new interval, (a < d) and (b < c) - intersection
        return bookingRepo.getAllActiveByRoom(room).stream()
                .noneMatch(rb -> rb.getInterval().getStartDate()
                .isBefore(interval.getStartDate().plusDays(interval.getDuration())) // a < d
                && interval.getStartDate()
                .isBefore(rb.getInterval().getStartDate().plusDays(rb.getInterval().getDuration()))); // b < c
    }

    @Override
    public List<Room> searchRoomsByLocation(final long locationId, final RoomStyle style, final DayInterval interval) {
        final HotelLocation location = sLocationService.searchById(locationId);

        return location.getRooms().stream().filter(r -> r.getStyle().equals(style))
                .filter(r -> isRoomAvailable(r.getRoomNumber(), interval)).collect(Collectors.toList());
    }

    @Override
    public List<Room> searchRooms(final RoomStyle style, final DayInterval interval) {
        final var list = new CopyOnWriteArrayList<Room>();

        hotelRepo.getHotel().getLocations().forEach(l -> list.addAll(searchRoomsByLocation(l.getId(), style, interval)));

        return list;
    }
}