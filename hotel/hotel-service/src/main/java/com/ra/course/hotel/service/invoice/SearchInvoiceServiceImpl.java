package com.ra.course.hotel.service.invoice;

import com.ra.course.hotel.model.entity.invoice.Invoice;
import com.ra.course.hotel.model.entity.invoice.InvoiceItem;
import com.ra.course.hotel.persistence.invoice.InvoiceItemRepo;
import com.ra.course.hotel.persistence.invoice.InvoiceRepo;
import com.ra.course.hotel.service.exception.EntityNotFoundException;

public class SearchInvoiceServiceImpl implements SearchInvoiceService {
    private final transient InvoiceRepo invoiceRepo;
    private final transient InvoiceItemRepo invoiceItemRepo;

    public SearchInvoiceServiceImpl(final InvoiceRepo invoiceRepo, final InvoiceItemRepo invoiceItemRepo) {
        this.invoiceRepo = invoiceRepo;
        this.invoiceItemRepo = invoiceItemRepo;
    }

    @Override
    public Invoice searchInvoiceById(final long id) {
        return invoiceRepo.getById(id).orElseThrow(() -> new EntityNotFoundException("Invoice with id = " + id));
    }

    @Override
    public Invoice searchInvoiceByItem(final InvoiceItem item) {
        return invoiceRepo.getInvoiceByItem(item)
                .orElseThrow(() -> new EntityNotFoundException("Invoice with item " + item.getDescription()));
    }

    @Override
    public InvoiceItem searchInvoiceItemById(final long id) {
        return invoiceItemRepo.getById(id).orElseThrow(() -> new EntityNotFoundException("Invoice item with id = " + id));
    }
}
