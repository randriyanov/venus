package com.ra.course.hotel.service.room;

import com.ra.course.hotel.model.entity.hotel.HotelLocation;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.persistence.room.HouseKeepingRepo;
import com.ra.course.hotel.persistence.room.RoomKeyRepo;
import com.ra.course.hotel.persistence.room.RoomRepo;
import com.ra.course.hotel.service.hotel.SearchLocationService;
import lombok.Builder;

@Builder
public class RoomServiceImpl implements RoomService {
    private final transient RoomRepo roomRepo;
    private final transient RoomKeyRepo roomKeyRepo;
    private final transient HouseKeepingRepo houseKeepingRepo;
    private final transient SearchLocationService sLocationService;
    private final transient SearchRoomService sRoomService;

    @Override
    public Room addRoom(final long locationId, final Room room) {
        final HotelLocation location = sLocationService.searchById(locationId);

        final Room roomWithId = roomRepo.add(room);
        location.getRooms().add(roomWithId);

        return roomWithId;
    }

    @Override
    public void editRoom(final Room room) {
        roomRepo.update(room);
    }

    @Override
    public void removeRoom(final long roomId) {
        final Room room = sRoomService.searchById(roomId);

        sLocationService.searchByRoom(room).getRooms().remove(room);
        room.getKeys().forEach(roomKeyRepo::remove);
        room.getHouseKeepings().forEach(houseKeepingRepo::remove);
        roomRepo.remove(room);
    }
}
