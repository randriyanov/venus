package com.ra.course.hotel.service.booking;

import com.ra.course.hotel.model.entity.booking.RoomBooking;

public interface BookingService {
    RoomBooking createBooking(RoomBooking booking, long guestId, long receptionistId);
    void assignReceptionist(long receptionistId, RoomBooking booking);
    void updateBooking(RoomBooking booking);
    RoomBooking cancelBooking(final String reservNumber);
}
