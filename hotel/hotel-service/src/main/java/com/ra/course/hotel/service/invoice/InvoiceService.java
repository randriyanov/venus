package com.ra.course.hotel.service.invoice;

import com.ra.course.hotel.model.entity.charge.RoomCharge;
import com.ra.course.hotel.model.entity.invoice.Invoice;
import com.ra.course.hotel.model.entity.invoice.InvoiceItem;

public interface InvoiceService {
    Invoice createInvoice(Invoice invoice);
    void updateInvoice(Invoice invoice);
    void removeInvoice(Invoice invoice);
    InvoiceItem addInvoiceItem(InvoiceItem item, Invoice invoice, RoomCharge charge);
    void updateInvoiceItem(InvoiceItem item);
    void removeInvoiceItem(InvoiceItem item);

}