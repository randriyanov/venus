package com.ra.course.hotel.service.person;

import com.ra.course.hotel.model.entity.account.Account;

public interface AccountService {
    Account createAccount(Account account);
    void updatePassword(Account account, String password);
    void resetPassword(String email);
    Account login(String login, String password);
    void logout(Account account);
}
