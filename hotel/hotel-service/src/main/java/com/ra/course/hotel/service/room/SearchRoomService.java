package com.ra.course.hotel.service.room;

import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomHouseKeeping;

import java.util.List;

public interface SearchRoomService {
    Room searchById(long roomId);
    Room searchByNumber(String roomNumber);
    Room searchByHouseKeeping(RoomHouseKeeping houseKeeping);
    List<Room> searchAllWithActiveBookingsByGuest(Guest guest);
}
