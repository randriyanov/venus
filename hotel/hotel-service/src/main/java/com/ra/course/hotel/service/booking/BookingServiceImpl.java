package com.ra.course.hotel.service.booking;

import com.ra.course.hotel.model.entity.booking.BookingStatus;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.payment.PaymentStatus;
import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.model.entity.persons.Receptionist;
import com.ra.course.hotel.persistence.booking.RoomBookingRepo;
import com.ra.course.hotel.service.exception.WrongStatusException;
import com.ra.course.hotel.service.person.SearchPersonService;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

public class BookingServiceImpl implements BookingService {
    private final transient RoomBookingRepo bookingRepo;
    private final transient SearchBookingService sBookingService;
    private final transient SearchPersonService sPersonService;

    private static List<BookingStatus> statusesToCancel = Arrays.asList
            (BookingStatus.REQUESTED, BookingStatus.PENDING, BookingStatus.CONFIRMED, BookingStatus.ABANDONED);

    public BookingServiceImpl(final RoomBookingRepo bookingRepo, final SearchBookingService sBookingService,
                              final SearchPersonService sPersonService) {
        this.bookingRepo = bookingRepo;
        this.sBookingService = sBookingService;
        this.sPersonService = sPersonService;
    }
    @Override
    public RoomBooking createBooking(final RoomBooking booking, final long guestId, final long receptionistId) {
        final Guest guest = sPersonService.searchGuestById(guestId);

        final RoomBooking bookingWithId = bookingRepo.add(booking);
        bookingWithId.setStatus(BookingStatus.CONFIRMED);
        guest.getBookings().add(bookingWithId);

        if (receptionistId > 0) {
            assignReceptionist(receptionistId, bookingWithId);
        }

        return bookingWithId;
    }

    @Override
    public void assignReceptionist(final long receptionistId, final RoomBooking booking) {
        final Receptionist receptionist = sPersonService.searchReceptionistById(receptionistId);
        receptionist.getBookings().add(booking);
    }

    @Override
    public void updateBooking(final RoomBooking booking) {
        bookingRepo.update(booking);
    }

    @Override
    public RoomBooking cancelBooking(final String reservNumber) {
        final RoomBooking booking = sBookingService.searchByReservNumber(reservNumber);

        if (!statusesToCancel.contains(booking.getStatus())) {
            throw new WrongStatusException("cancel (BookingStatus." + booking.getStatus() + ")");
        }

        booking.setStatus(BookingStatus.CANCELED);

        if (LocalDateTime.now().plusDays(1).isBefore(booking.getInterval().getStartDate().atTime(14, 0))) {
            booking.getInvoices().stream().filter(inv -> inv.getTransaction() != null)
                    .forEach(inv -> inv.getTransaction().setStatus(PaymentStatus.REFUNDED));
        }

        return booking;
    }
}
