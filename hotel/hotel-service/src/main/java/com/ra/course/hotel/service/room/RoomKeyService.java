package com.ra.course.hotel.service.room;

import com.ra.course.hotel.model.entity.room.RoomKey;

public interface RoomKeyService {
    RoomKey issueKey(long roomId);
    RoomKey searchByKeyId(String keyId);
    void receiveKey(String keyId);
}
