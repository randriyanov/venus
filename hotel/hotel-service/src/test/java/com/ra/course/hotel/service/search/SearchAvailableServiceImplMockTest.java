package com.ra.course.hotel.service.search;

import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.common.DayInterval;
import com.ra.course.hotel.model.entity.hotel.Hotel;
import com.ra.course.hotel.model.entity.hotel.HotelLocation;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomStyle;
import com.ra.course.hotel.persistence.booking.RoomBookingRepo;
import com.ra.course.hotel.persistence.hotel.HotelRepo;
import com.ra.course.hotel.service.hotel.SearchLocationService;
import com.ra.course.hotel.service.room.SearchRoomService;
import org.junit.jupiter.api.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class SearchAvailableServiceImplMockTest {
    private SearchAvailableService searchAvailableService;
    private HotelRepo mockHotelRepo = mock(HotelRepo.class);
    private RoomBookingRepo mockBookingRepo = mock(RoomBookingRepo.class);
    private SearchLocationService mockSearchLocationService =  mock(SearchLocationService.class);
    private SearchRoomService mockSearchRoomService = mock(SearchRoomService.class);

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private static DayInterval bookingInterval1, bookingInterval2,
                               testIntervalBefore, testIntervalAfter, testIntervalInter;

    private static final int TEST_LOCATION_ID = 1;
    private static final String TEST_ROOM_NUMBER_PREFIX= "RO-";

    private static final String FREE_STANDARD_ROOM_NUMBER = "201";
    private static final String BUSY_BUSINESS_ROOM_NUMBER = "202";
    private static final String FREE_BUSINESS_ROOM_NUMBER = "204";

    public SearchAvailableServiceImplMockTest() {
        searchAvailableService = SearchAvailableServiceImpl.builder()
                .hotelRepo(mockHotelRepo).bookingRepo(mockBookingRepo)
                .sLocationService(mockSearchLocationService).sRoomService(mockSearchRoomService).build();
    }

    @BeforeAll
    public static void configureTestIntervals() {
        bookingInterval1 = new DayInterval(LocalDate.parse("01.07.2020", formatter), 5);
        bookingInterval2 = new DayInterval(LocalDate.parse("08.07.2020", formatter), 14);

        testIntervalBefore = new DayInterval(LocalDate.parse("29.06.2020", formatter), 2);
        testIntervalAfter = new DayInterval(LocalDate.parse("12.08.2020", formatter), 10);
        testIntervalInter = new DayInterval(LocalDate.parse("04.07.2020", formatter), 7);
    }

    @Test
    @DisplayName("When check room's availability with null RoomKeeping list then true is returned")
    public void testIsRoomAvailableWithNullRoomKeepingList() {
        //given
        var testRoomNumber = configureSearchRoom(FREE_STANDARD_ROOM_NUMBER, RoomStyle.STANDARD, false);
        var testInterval = testIntervalInter;
        //when
        boolean roomAvailable = searchAvailableService.isRoomAvailable(testRoomNumber, testInterval);
        //then
        assertTrue(roomAvailable);
    }

    @Test
    @DisplayName("When check room's availability with interval before all existing ones then true is returned")
    public void testIsRoomAvailableWithIntervalBeforeExisting() {
        //given
        var testRoomNumber = configureSearchRoom(BUSY_BUSINESS_ROOM_NUMBER, RoomStyle.BUSINESS_SUITE, true);
        var testInterval = testIntervalBefore;
        //when Predicate1 = False, Predicate2 = True
        boolean roomAvailable = searchAvailableService.isRoomAvailable(testRoomNumber, testInterval);
        //then
        assertTrue(roomAvailable);
    }

    @Test
    @DisplayName("When check room's availability with interval after all existing ones then true is returned")
    public void testIsRoomAvailableWithIntervalAfterExisting() {
        //given
        var testRoomNumber = configureSearchRoom(BUSY_BUSINESS_ROOM_NUMBER, RoomStyle.BUSINESS_SUITE, true);
        var testInterval = testIntervalAfter;
        //when Predicate1 = True, Predicate2 = False
        boolean roomAvailable = searchAvailableService.isRoomAvailable(testRoomNumber, testInterval);
        //then
        assertTrue(roomAvailable);
    }

    @Test
    @DisplayName("When check room's availability with interval intersecting with existing ones then false is returned")
    public void testIsRoomAvailableWithIntervalIntersectingExisting() {
        //given
        var testRoomNumber = configureSearchRoom(BUSY_BUSINESS_ROOM_NUMBER, RoomStyle.BUSINESS_SUITE, true);
        var testInterval = testIntervalInter;
        //when Predicate1 = True, Predicate2 = True
        boolean roomAvailable = searchAvailableService.isRoomAvailable(testRoomNumber, testInterval);
        //then
        assertFalse(roomAvailable);
    }

    @Test
    @DisplayName("When search room in hotel location with given RoomStyle rooms then result list contains such available rooms")
    public void testSearchRoomsByLocationWhenStyleExists() {
        //given
        getTestHotelLocation(TEST_LOCATION_ID, "IrisOdessa", "RO-");
        var testRoomStyle = RoomStyle.BUSINESS_SUITE;
        var testInterval = testIntervalInter;
        //when
        List<Room> list = searchAvailableService.searchRoomsByLocation(TEST_LOCATION_ID, testRoomStyle, testInterval);
        //then
        assertTrue(list.stream().allMatch(r -> r.getStyle() == testRoomStyle));
    }

    @Test
    @DisplayName("When search room in hotel location without given RoomStyle rooms then empty list is returned ")
    public void testSearchRoomsByLocationWhenStyleNotExists() {
        //given
        getTestHotelLocation(TEST_LOCATION_ID, "IrisOdessa", "RO-");
        var testRoomStyle = RoomStyle.FAMILY_SUITE;
        var testInterval = testIntervalInter;
        //when
        List<Room> list = searchAvailableService.searchRoomsByLocation(TEST_LOCATION_ID, testRoomStyle, testInterval);
        //then
        assertEquals(0, list.size());
    }

    @Test
    @DisplayName("When search room then result list contains available rooms with given RoomStyle in all hotel locations")
    public void testSearchRooms() {
        //given
        configureSearchTest();
        var testRoomStyle = RoomStyle.BUSINESS_SUITE;
        var testInterval = testIntervalInter;
        //when
        List<Room> list = searchAvailableService.searchRooms(testRoomStyle, testInterval);
        //then
        assertEquals(2, list.size());
    }

    private String configureSearchRoom(String number, RoomStyle style, boolean hasBookings) {
        var roomNumber = TEST_ROOM_NUMBER_PREFIX + number;
        getTestRoom(roomNumber, style, hasBookings);
        return roomNumber;
    }

    private Room getTestRoom(String roomNumber, RoomStyle style, boolean hasBookings) {
        var room = new Room();
        room.setRoomNumber(roomNumber);
        room.setStyle(style);
        when(mockSearchRoomService.searchByNumber(roomNumber)).thenReturn(room);
        if (hasBookings) {
            when(mockBookingRepo.getAllActiveByRoom(room)).thenReturn(getTestBookingList(room));
        }
        return room;
    }

    private List<RoomBooking> getTestBookingList(Room room) {
        var list = new CopyOnWriteArrayList<RoomBooking>();
        list.add(new RoomBooking("RB-111", bookingInterval1, room));
        list.add(new RoomBooking("RB-112", bookingInterval2, room));
        return list;
    }

    private HotelLocation getTestHotelLocation(long locationId, String name, String roomNumberPrefix) {
        var location = new HotelLocation();
        location.setId(locationId);
        location.setName(name);
        location.setRooms(getTestRoomList(roomNumberPrefix));
        when(mockSearchLocationService.searchById(locationId)).thenReturn(location);
        return location;
    }

    private List<Room> getTestRoomList(String roomNumberPrefix) {
        var list = new CopyOnWriteArrayList<Room>();
        list.add(getTestRoom(roomNumberPrefix + FREE_STANDARD_ROOM_NUMBER, RoomStyle.STANDARD, false));
        list.add(getTestRoom(roomNumberPrefix + BUSY_BUSINESS_ROOM_NUMBER, RoomStyle.BUSINESS_SUITE, true));
        list.add(getTestRoom(roomNumberPrefix + FREE_BUSINESS_ROOM_NUMBER, RoomStyle.BUSINESS_SUITE, false));
        return list;
    }

    private void configureSearchTest() {
        Hotel hotel = new Hotel("Iris");
        hotel.setLocations(getTestHotelLocationList());
        when(mockHotelRepo.getHotel()).thenReturn(hotel);
    }

    private List<HotelLocation> getTestHotelLocationList() {
        var list = new CopyOnWriteArrayList<HotelLocation>();
        list.add(getTestHotelLocation(1, "Iris Odessa", "RO-"));
        list.add(getTestHotelLocation(2, "Iris Kiev", "RK-"));
        return list;
    }
}
