package com.ra.course.hotel.service.invoice;

import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.charge.KitchenService;
import com.ra.course.hotel.model.entity.charge.RoomCharge;
import com.ra.course.hotel.model.entity.invoice.Invoice;
import com.ra.course.hotel.model.entity.invoice.InvoiceItem;
import com.ra.course.hotel.persistence.invoice.InvoiceItemRepo;
import com.ra.course.hotel.persistence.invoice.InvoiceRepo;
import org.junit.jupiter.api.*;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class InvoiceServiceImplMockTest {
    private InvoiceService invoiceService;
    private InvoiceRepo mockInvoiceRepo = mock(InvoiceRepo.class);
    private InvoiceItemRepo mockInvoiceItemRepo = mock(InvoiceItemRepo.class);
    private SearchInvoiceService mockSearchInvoiceService = mock(SearchInvoiceService.class);

    private static RoomBooking booking;
    private static final int BOOKING_ID = 1;
    private static Invoice bookingInvoice;
    private static final int BOOKING_INVOICE_ID = 101;
    private static final int EXISTING_INVOICE_ID = 102;
    private static final int NEW_INVOICE_ID = 103;
    private static InvoiceItem roomCharge;
    private static final int ROOM_CHARGE_ITEM_ID = 1001;
    private static final int NEW_INVOICE_ITEM_ID = 1002;

    private static RoomCharge breakfastCharge;

    public InvoiceServiceImplMockTest() {
        invoiceService = new InvoiceServiceImpl(mockInvoiceRepo, mockInvoiceItemRepo, mockSearchInvoiceService);
    }

    @BeforeAll
    public static void configureBooking() {
        booking = new RoomBooking();
        booking.setId(BOOKING_ID);

        bookingInvoice = new Invoice(1400, booking);
        bookingInvoice.setId(BOOKING_INVOICE_ID);
        roomCharge = new InvoiceItem("Room charge", 1400);
        roomCharge.setId(ROOM_CHARGE_ITEM_ID);
        bookingInvoice.getInvoiceItems().add(roomCharge);
        booking.getInvoices().add(bookingInvoice);

        breakfastCharge = new KitchenService(LocalDate.now(), "Breakfast (classic)");
    }

    @Test
    @Order(1)
    @DisplayName("When create invoice with booking then this booking contains it")
    public void testCreateInvoiceWithBooking() {
        //given
        var testInvoice = getInvoice(0, 120, true);
        when(mockInvoiceRepo.add(testInvoice)).thenReturn(getInvoice(NEW_INVOICE_ID, 120, true));
        //when
        Invoice invoice = invoiceService.createInvoice(testInvoice);
        //then
        assertTrue(booking.getInvoices().contains(invoice));
    }

    @Test
    @Order(2)
    @DisplayName("When update invoice with booking to change total amount then update method for invoice repository is called")
    public void testUpdateInvoiceWithBookingChangeAmount() {
        //given
        var testInvoice = booking.getInvoices().stream().filter(inv -> inv.getId() == NEW_INVOICE_ID).findFirst().orElseThrow();
        when(mockSearchInvoiceService.searchInvoiceById(NEW_INVOICE_ID)).thenReturn(testInvoice);
        //when
        testInvoice.setTotalAmount(130);
        invoiceService.updateInvoice(testInvoice);
        //then
        verify(mockInvoiceRepo).update(testInvoice);
    }

    @Test
    @Order(3)
    @DisplayName("When update invoice without booking to change total amount then update method for invoice repository is called")
    public void testUpdateInvoiceChangeAmount() {
        //given
        var testInvoice = getInvoice(EXISTING_INVOICE_ID, 220, false);
        when(mockSearchInvoiceService.searchInvoiceById(EXISTING_INVOICE_ID))
                .thenReturn(getInvoice(EXISTING_INVOICE_ID, 250,false));
        //when
        invoiceService.updateInvoice(testInvoice);
        //then
        verify(mockInvoiceRepo).update(testInvoice);
    }

    @Test
    @Order(4)
    @DisplayName("When update invoice to add booking then update method for invoice repository is called and this booking contains test invoice")
    public void testUpdateInvoiceAddBooking() {
        //given
        var testInvoice = getInvoice(EXISTING_INVOICE_ID, 250,true);
        when(mockSearchInvoiceService.searchInvoiceById(EXISTING_INVOICE_ID))
                .thenReturn(getInvoice(EXISTING_INVOICE_ID,250,false));
        //when
        invoiceService.updateInvoice(testInvoice);
        //then
        verify(mockInvoiceRepo).update(testInvoice);
        assertTrue(booking.getInvoices().contains(testInvoice));
    }

    @Test
    @Order(5)
    @DisplayName("When update invoice to remove booking then update method for invoice repository is called and this booking contains test invoice")
    public void testUpdateInvoiceRemoveBooking() {
        //given
        var testInvoice = getInvoice(EXISTING_INVOICE_ID, 250,false);
        when(mockSearchInvoiceService.searchInvoiceById(EXISTING_INVOICE_ID))
                .thenReturn(booking.getInvoices().stream().filter(inv -> inv.getId() == EXISTING_INVOICE_ID).findFirst().orElseThrow());
        //when
        invoiceService.updateInvoice(testInvoice);
        //then
        verify(mockInvoiceRepo).update(testInvoice);
        assertFalse(booking.getInvoices().contains(testInvoice));
    }

    @Test
    @Order(6)
    @DisplayName("When remove invoice with booking then this booking doesn't contain it and remove method for invoice repository is called")
    public void testRemoveInvoiceWithBooking() {
        //given
        var testInvoice = booking.getInvoices().stream().filter(inv -> inv.getId() == NEW_INVOICE_ID).findFirst().orElseThrow();
        //when
        invoiceService.removeInvoice(testInvoice);
        //then
        assertFalse(booking.getInvoices().contains(testInvoice));
        verify(mockInvoiceRepo).remove(testInvoice);
    }

    @Test
    @Order(7)
    @DisplayName("When add invoice item then invoice contains it and room charge refers to it")
    public void testAddInvoiceItem() {
        //given
        var testInvoiceItem = getInvoiceItem(0);
        when(mockInvoiceItemRepo.add(testInvoiceItem)).thenReturn(getInvoiceItem(NEW_INVOICE_ITEM_ID));
        //when
        InvoiceItem invoiceItem = invoiceService.addInvoiceItem(testInvoiceItem, bookingInvoice, breakfastCharge);
        //then
        checkAddInvoiceUtem(invoiceItem);
    }

    @Test
    @Order(8)
    @DisplayName("When update invoice item then update method for invoice item repository is called and invoice total amount is recounted")
    public void testUpdateInvoiceItem() {
        //given
        when(mockSearchInvoiceService.searchInvoiceByItem(roomCharge)).thenReturn(bookingInvoice);
        //when
        roomCharge.setAmount(2000);
        invoiceService.updateInvoiceItem(roomCharge);
        //then
        verify(mockInvoiceItemRepo).update(roomCharge);
        assertEquals(bookingInvoice.getInvoiceItems().stream().mapToDouble(InvoiceItem::getAmount).sum(), bookingInvoice.getTotalAmount());
    }

    @Test
    @Order(9)
    @DisplayName("When remove invoice item then this invoice doesn't contain it and remove method for invoice repository is called")
    public void testRemoveInvoiceItem() {
        //given
        var testInvoiceItem = bookingInvoice.getInvoiceItems().stream()
                .filter(item -> item.getId() == NEW_INVOICE_ITEM_ID).findFirst().orElseThrow();
        when(mockSearchInvoiceService.searchInvoiceByItem(testInvoiceItem)).thenReturn(bookingInvoice);
        //when
        invoiceService.removeInvoiceItem(testInvoiceItem);
        //then
        checkRemoveInvoiceItem(testInvoiceItem);
    }

    public Invoice getInvoice(long id, double amount, boolean hasBooking) {
        var invoice = new Invoice(amount, null);
        invoice.setId(id);
        if (hasBooking) {
            invoice.setBooking(booking);
        }
        return invoice;
    }

    public InvoiceItem getInvoiceItem(long id) {
        var item = new InvoiceItem("Breakfast", 100);
        item.setId(id);
        return item;
    }

    public void checkAddInvoiceUtem(InvoiceItem item) {
        assertTrue(bookingInvoice.getInvoiceItems().contains(item));
        assertEquals(bookingInvoice.getInvoiceItems().stream().mapToDouble(InvoiceItem::getAmount).sum(), bookingInvoice.getTotalAmount());
        assertEquals(item, breakfastCharge.getInvoiceItem());
    }

    public void checkRemoveInvoiceItem(InvoiceItem item) {
        assertFalse(bookingInvoice.getInvoiceItems().contains(item));
        assertEquals(bookingInvoice.getInvoiceItems().stream().mapToDouble(InvoiceItem::getAmount).sum(), bookingInvoice.getTotalAmount());
        verify(mockInvoiceItemRepo).remove(item);
    }
}
