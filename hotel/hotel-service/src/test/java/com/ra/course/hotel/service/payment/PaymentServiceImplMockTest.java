package com.ra.course.hotel.service.payment;

import com.ra.course.hotel.model.entity.invoice.Invoice;
import com.ra.course.hotel.model.entity.payment.BillTransaction;
import com.ra.course.hotel.model.entity.payment.CashTransaction;
import com.ra.course.hotel.model.entity.payment.CheckTransaction;
import com.ra.course.hotel.model.entity.payment.CreditCardTransaction;
import com.ra.course.hotel.persistence.payment.CashTransactionRepo;
import com.ra.course.hotel.persistence.payment.CheckTransactionRepo;
import com.ra.course.hotel.persistence.payment.CreditCardTransactionRepo;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class PaymentServiceImplMockTest {
    private PaymentService paymentService;
    private CreditCardTransactionRepo mockCardTransactionRepo = mock(CreditCardTransactionRepo.class);
    private CheckTransactionRepo mockCheckTransactionRepo = mock(CheckTransactionRepo.class);
    private CashTransactionRepo mockCashTransactionRepo = mock(CashTransactionRepo.class);

    private static Invoice testInvoice;
    private static final int EXISTING_TRANSACTION_ID = 1;

    public PaymentServiceImplMockTest() {
        paymentService = new PaymentServiceImpl(mockCardTransactionRepo, mockCheckTransactionRepo, mockCashTransactionRepo);
    }

    @BeforeAll
    public static void configureInvoice() {
        testInvoice = new Invoice(300, null);
        testInvoice.setId(1);
    }

    @Test
    @DisplayName("When create credit card transaction for invoice then this invoice refers to it")
    public void testCreateCreditCardTransaction() {
        //given
        var nameOnCard = "Peter Kovalenko";
        var zipCode = "65001";
        when(mockCardTransactionRepo.add(any(CreditCardTransaction.class)))
                .thenReturn(getTestCardTransaction(EXISTING_TRANSACTION_ID, nameOnCard, zipCode));
        //when
        CreditCardTransaction transaction = paymentService.createCreditCardTransaction(nameOnCard, zipCode, testInvoice);
        //then
        assertEquals(transaction, testInvoice.getTransaction());
    }

    @Test
    @DisplayName("When create check transaction for invoice then this invoice refers to it")
    public void testCreateCheckTransaction() {
        //given
        var bankName = "Alfa";
        var checkNumber = "DF-12458936";
        when(mockCheckTransactionRepo.add(any(CheckTransaction.class)))
                .thenReturn(getTestCheckTransaction(EXISTING_TRANSACTION_ID, bankName, checkNumber));
        //when
        CheckTransaction transaction = paymentService.createCheckTransaction(bankName, checkNumber, testInvoice);
        //then
        assertEquals(transaction, testInvoice.getTransaction());
    }

    @Test
    @DisplayName("When create cash transaction for invoice then this invoice refers to it")
    public void testCreateCashTransaction() {
        //given
        var cashTendered = 320;
        when(mockCashTransactionRepo.add(any(CashTransaction.class)))
                .thenReturn(getTestCashTransaction(EXISTING_TRANSACTION_ID, cashTendered));
        //when
        CashTransaction transaction = paymentService.createCashTransaction(cashTendered, testInvoice);
        //then
        assertEquals(transaction, testInvoice.getTransaction());
    }

    public CreditCardTransaction getTestCardTransaction(long id, String name, String zipCode) {
        var transaction = new CreditCardTransaction(testInvoice.getTotalAmount(), name, zipCode);
        transaction.setId(id);
        return transaction;
    }

    public CheckTransaction getTestCheckTransaction(long id, String bankName, String checkNumber) {
        var transaction = new CheckTransaction(testInvoice.getTotalAmount(), bankName, checkNumber);
        transaction.setId(id);
        return transaction;
    }

    public CashTransaction getTestCashTransaction(long id, double cashTendered) {
        var transaction = new CashTransaction(testInvoice.getTotalAmount(), cashTendered);
        transaction.setId(id);
        return transaction;
    }
}
