package com.ra.course.hotel.service.booking;

import com.ra.course.hotel.model.entity.account.AccountType;
import com.ra.course.hotel.model.entity.booking.BookingStatus;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.common.DayInterval;
import com.ra.course.hotel.model.entity.invoice.Invoice;
import com.ra.course.hotel.model.entity.payment.CreditCardTransaction;
import com.ra.course.hotel.model.entity.payment.PaymentStatus;
import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomKey;
import com.ra.course.hotel.model.entity.room.RoomStatus;
import com.ra.course.hotel.model.entity.room.RoomStyle;
import com.ra.course.hotel.service.exception.WrongStatusException;
import com.ra.course.hotel.service.person.SearchPersonService;
import com.ra.course.hotel.service.room.RoomKeyService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.function.Executable;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class CheckInOutServiceImplMockTest {
    private CheckInOutService checkInOutService;
    private RoomKeyService mockRoomKeyService = mock(RoomKeyService.class);
    private SearchBookingService mockSearchBookingService = mock(SearchBookingService.class);
    private SearchPersonService mockSearchPersonService = mock(SearchPersonService.class);

    private static final int TEST_BOOKING_ID = 111;
    private static final String TEST_RESERV_NUMBER = "RB-111";
    private static final int TEST_ROOM_ID = 11;

    private static final int TEST_GUEST_ID = 3;
    private static final String TEST_GUEST_NANE = "Peter Kovalenko";

    public CheckInOutServiceImplMockTest() {
        checkInOutService = new CheckInOutServiceImpl(mockRoomKeyService, mockSearchBookingService, mockSearchPersonService);
    }

    @Test
    @DisplayName("When check-in not confirmed booking then WrongStatusException is thrown")
    public void testCheckInNotConfirmedBooking() {
        //given
        var testBooking = getTestBooking(BookingStatus.REQUESTED, PaymentStatus.UNPAID, RoomStatus.AVAILABLE);
        //when
        Executable executable = () -> checkInOutService.checkIn(TEST_RESERV_NUMBER);
        //then
        Throwable exception = assertThrows(WrongStatusException.class, executable);
        assertEquals(exception.getMessage(),"Wrong status for check-in (BookingStatus." + testBooking.getStatus() + ")");
    }

    @Test
    @DisplayName("When check-in unpaid room then WrongStatusException is thrown")
    public void testCheckInUnpaidRoom() {
        //given
        var testBooking = getTestBooking(BookingStatus.CONFIRMED, PaymentStatus.FAILED, RoomStatus.RESERVED);
        //when
        Executable executable = () -> checkInOutService.checkIn(TEST_RESERV_NUMBER);
        //then
        Throwable exception = assertThrows(WrongStatusException.class, executable);
        assertEquals(exception.getMessage(),"Wrong status for check-in (your payment = 100.0, minimal payment = "
                + testBooking.getRoom().getBookingPrice() +")");
    }

    @Test
    @DisplayName("When check-in not ready room then WrongStatusException is thrown")
    public void testCheckInNotReadyRoom() {
        //given
        var testBooking = getTestBooking(BookingStatus.CONFIRMED, PaymentStatus.COMPLETED, RoomStatus.BEING_SERVICED);
        //when
        Executable executable = () -> checkInOutService.checkIn(TEST_RESERV_NUMBER);
        //then
        Throwable exception = assertThrows(WrongStatusException.class, executable);
        assertEquals(exception.getMessage(),"Wrong status for check-in (RoomStatus." + testBooking.getRoom().getRoomStatus() + ")");
    }

    @Test
    @DisplayName("When check-in confirmed booking for member then room key is issued and booking, room, member (not account type) fields are changed")
    public void testCheckInConfirmedBookingForMember() {
        //given
        var testBooking = getTestBooking(BookingStatus.CONFIRMED, PaymentStatus.COMPLETED, RoomStatus.RESERVED);
        var testGuest = getTestGuest(true);
        when(mockSearchPersonService.searchGuestByBooking(testBooking)).thenReturn(testGuest);
        //when
        checkInOutService.checkIn(TEST_RESERV_NUMBER);
        //then
        testSuccessfulCheckIn(testBooking, testGuest,true);
    }

    @Test
    @DisplayName("When check-in confirmed booking for guest then room key is issued and booking, room, guest (incl. account type) fields are changed")
    public void testCheckInConfirmedBookingForGuest() {
        //given
        var testBooking = getTestBooking(BookingStatus.CONFIRMED, PaymentStatus.COMPLETED, RoomStatus.RESERVED);
        var testGuest = getTestGuest(false);
        when(mockSearchPersonService.searchGuestByBooking(testBooking)).thenReturn(testGuest);
        //when
        checkInOutService.checkIn(TEST_RESERV_NUMBER);
        //then
        testSuccessfulCheckIn(testBooking, testGuest,false);
    }

    @Test
    @DisplayName("When check-out not checked-in booking then WrongStatusException is thrown")
    public void testCheckOutNotCheckedInBooking() {
        //given
        var testBooking = getTestBooking(BookingStatus.REQUESTED, PaymentStatus.UNPAID, RoomStatus.AVAILABLE);
        //when
        Executable executable = () -> checkInOutService.checkOut(TEST_RESERV_NUMBER);
        //then
        Throwable exception = assertThrows(WrongStatusException.class, executable);
        assertEquals(exception.getMessage(),"Wrong status for check-out (BookingStatus." + testBooking.getStatus() + ")");
    }

    @Test
    @DisplayName("When check-out unpaid booking then then WrongStatusException is thrown")
    public void testCheckOutUnpaidBooking() {
        //given
        var testBooking = getTestBooking(BookingStatus.CHECKED_IN, PaymentStatus.UNPAID, RoomStatus.OCCUPIED);
        //when
        Executable executable = () -> checkInOutService.checkOut(TEST_RESERV_NUMBER);
        //then
        Throwable exception = assertThrows(WrongStatusException.class, executable);
        assertEquals(exception.getMessage(),"Wrong status for check-out (your payment = 100.0, invoice total amount = "
                + (testBooking.getRoom().getBookingPrice() + 100) + ")");
    }

    @Test
    @DisplayName("When check-out paid booking then all room keys are received and booking, room fields are changed")
    public void testCheckOutPaidBooking() {
        //given
        var testBooking = getTestBooking(BookingStatus.CHECKED_IN, PaymentStatus.COMPLETED, RoomStatus.OCCUPIED);
        //when
        checkInOutService.checkOut(TEST_RESERV_NUMBER);
        //then
        testSuccessfulCheckOut(testBooking);
    }

    public RoomBooking getTestBooking(BookingStatus bookingStatus, PaymentStatus paymentStatus, RoomStatus roomStatus) {
        var interval = new DayInterval(LocalDate.now().minusDays(1), 3);
        var booking = new RoomBooking(TEST_RESERV_NUMBER, interval, getTestRoom(roomStatus));
        booking.setId(TEST_BOOKING_ID);
        booking.setStatus(bookingStatus);

        Invoice invoice1 = new Invoice(booking.getRoom().getBookingPrice(), booking);
        invoice1.setTransaction(new CreditCardTransaction(invoice1.getTotalAmount(), TEST_GUEST_NANE, "65001"));
        invoice1.getTransaction().setStatus(paymentStatus);
        booking.getInvoices().add(invoice1);
        Invoice invoice2 = new Invoice(100, booking);
        invoice2.setTransaction(new CreditCardTransaction(invoice2.getTotalAmount(), TEST_GUEST_NANE, "65001"));
        invoice2.getTransaction().setStatus(PaymentStatus.COMPLETED);
        booking.getInvoices().add(invoice2);

        when(mockSearchBookingService.searchByReservNumber(TEST_RESERV_NUMBER)).thenReturn(booking);

        return booking;
    }

    private Room getTestRoom(RoomStatus status) {
        var room = new Room("201", RoomStyle.STANDARD, 700, false);
        room.setId(TEST_ROOM_ID);
        room.setRoomStatus(status);
        room.getKeys().add(new RoomKey("K1", "123456789", true));
        room.getKeys().add(new RoomKey("K2", "987654321", false));
        return room;
    }

    public Guest getTestGuest(boolean isMember) {
        var guest = new Guest();
        guest.setId(TEST_GUEST_ID);
        guest.setName("Peter Kovalenko");
        if (isMember) {
            guest.setAccountType(AccountType.MEMBER);
            guest.setTotalRoomsCheckedIn(1);
        }
        return guest;
    }

    public void testSuccessfulCheckIn(RoomBooking booking, Guest guest, boolean wasMember) {
        verify(mockRoomKeyService).issueKey(booking.getRoom().getId());
        assertEquals(BookingStatus.CHECKED_IN, booking.getStatus());
        assertEquals(RoomStatus.OCCUPIED, booking.getRoom().getRoomStatus());
        assertNotNull(booking.getCheckin());
        if (wasMember) {
            assertEquals(2, guest.getTotalRoomsCheckedIn());
        }
        else {
            assertEquals(1, guest.getTotalRoomsCheckedIn());
            assertEquals(AccountType.MEMBER, guest.getAccountType());
        }
    }

    public void testSuccessfulCheckOut(RoomBooking booking) {
        verify(mockRoomKeyService, times(booking.getRoom().getKeys().size())).receiveKey(any(String.class));
        assertEquals(BookingStatus.CHECKED_OUT, booking.getStatus());
        assertEquals(RoomStatus.NOT_AVAILABLE, booking.getRoom().getRoomStatus());
        assertNotNull(booking.getCheckout());
    }
}
