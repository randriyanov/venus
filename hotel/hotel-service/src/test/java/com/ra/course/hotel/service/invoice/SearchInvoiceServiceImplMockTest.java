package com.ra.course.hotel.service.invoice;

import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.invoice.Invoice;
import com.ra.course.hotel.model.entity.invoice.InvoiceItem;
import com.ra.course.hotel.persistence.invoice.InvoiceItemRepo;
import com.ra.course.hotel.persistence.invoice.InvoiceRepo;
import com.ra.course.hotel.service.exception.EntityNotFoundException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class SearchInvoiceServiceImplMockTest {
    private SearchInvoiceService searchInvoiceService;
    private InvoiceRepo mockInvoiceRepo = mock(InvoiceRepo.class);
    private InvoiceItemRepo mockInvoiceItemRepo = mock(InvoiceItemRepo.class);

    private static final int EXISTING_INVOICE_ID = 1;
    private static final int ROOM_CHARGE_ITEM_ID = 11;
    private static final int BREAKFAST_ITEM_ID = 12;

    public SearchInvoiceServiceImplMockTest() {
        searchInvoiceService = new SearchInvoiceServiceImpl(mockInvoiceRepo, mockInvoiceItemRepo);
    }

    @Test
    @DisplayName("When search invoice by existing id then invoice with such id is returned")
    public void testSearchInvoiceByExistingId() {
        //given
        var testInvoiceId = EXISTING_INVOICE_ID;
        when(mockInvoiceRepo.getById(testInvoiceId)).thenReturn(Optional.of(getTestInvoice(null)));
        //when
        Invoice invoice = searchInvoiceService.searchInvoiceById(testInvoiceId);
        //then
        verify(mockInvoiceRepo).getById(testInvoiceId);
        assertNotNull(invoice);
    }

    @Test
    @DisplayName("When search invoice by missing id then EntityNotFoundException is thrown")
    public void testSearchInvoiceByMissingId() {
        //given
        var testInvoiceId = 0;
        //when
        Executable executable = () -> searchInvoiceService.searchInvoiceById(testInvoiceId);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Invoice with id = " + testInvoiceId + " not found.");
    }

    @Test
    @DisplayName("When search invoice by existing item then invoice with such item is returned")
    public void testSearchInvoiceByExistingItem() {
        //given
        var testInvoiceItem = getTestInvoiceItem(BREAKFAST_ITEM_ID, "Breakfast", 90);
        when(mockInvoiceRepo.getInvoiceByItem(testInvoiceItem)).thenReturn(Optional.of(getTestInvoice(testInvoiceItem)));
        //when
        Invoice invoice = searchInvoiceService.searchInvoiceByItem(testInvoiceItem);
        //then
        verify(mockInvoiceRepo).getInvoiceByItem(testInvoiceItem);
        assertNotNull(invoice);
    }

    @Test
    @DisplayName("When search invoice by missing item then EntityNotFoundException is thrown")
    public void testSearchInvoiceByMissingItem() {
        //given
        var testInvoiceItem = getTestInvoiceItem(BREAKFAST_ITEM_ID, "Breakfast", 100);
        //when
        Executable executable = () -> searchInvoiceService.searchInvoiceByItem(testInvoiceItem);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Invoice with item " + testInvoiceItem.getDescription() + " not found.");
    }

    @Test
    @DisplayName("When search invoice item by existing id then invoice item with such id is returned")
    public void testSearchInvoiceItemByExistingId() {
        //given
        var testInvoiceItemId = ROOM_CHARGE_ITEM_ID;
        when(mockInvoiceItemRepo.getById(testInvoiceItemId))
                .thenReturn(Optional.of(getTestInvoiceItem(ROOM_CHARGE_ITEM_ID, "Room charge", 700)));
        //when
        InvoiceItem invoiceItem = searchInvoiceService.searchInvoiceItemById(testInvoiceItemId);
        //then
        verify(mockInvoiceItemRepo).getById(testInvoiceItemId);
        assertNotNull(invoiceItem);
    }

    @Test
    @DisplayName("When search invoice item by missing id then EntityNotFoundException is thrown")
    public void testSearchInvoiceItemByMissingId() {
        //given
        var testInvoiceItemId = 0;
        //when
        Executable executable = () -> searchInvoiceService.searchInvoiceItemById(testInvoiceItemId);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Invoice item with id = " + testInvoiceItemId + " not found.");
    }

    public Invoice getTestInvoice(InvoiceItem additionalItem) {
        var invoice = new Invoice();
        invoice.setId(EXISTING_INVOICE_ID);
        invoice.setBooking(new RoomBooking());

        var mainItem = getTestInvoiceItem( ROOM_CHARGE_ITEM_ID, "Room charge", 700);
        invoice.getInvoiceItems().add(mainItem);

        if (additionalItem != null) {
            invoice.getInvoiceItems().add(additionalItem);
            invoice.setTotalAmount(mainItem.getAmount() + additionalItem.getAmount());
        }

        return invoice;
    }

    public InvoiceItem getTestInvoiceItem(long id, String description, double amount) {
        var item = new InvoiceItem(description, amount);
        item.setId(id);
        return item;
    }
}
