package com.ra.course.hotel.service.room;

import com.ra.course.hotel.model.entity.booking.BookingStatus;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.common.DayInterval;
import com.ra.course.hotel.model.entity.common.MinuteInterval;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomHouseKeeping;
import com.ra.course.hotel.model.entity.room.RoomStatus;
import com.ra.course.hotel.model.entity.room.RoomStyle;
import com.ra.course.hotel.persistence.room.HouseKeepingRepo;
import com.ra.course.hotel.service.booking.SearchBookingService;
import com.ra.course.hotel.service.exception.EntityNotFoundException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class HouseKeepingServiceImplMockTest {
    private HouseKeepingService houseKeepingService;
    private HouseKeepingRepo mockHouseKeepingRepo = mock(HouseKeepingRepo.class);
    private SearchRoomService mockSearchRoomService = mock(SearchRoomService.class);
    private SearchBookingService mockSearchBookingService = mock(SearchBookingService.class);

    private static final int TEST_ROOM_ID = 11;
    private static final int TEST_HOUSEKEEPING_ID = 1;

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    public HouseKeepingServiceImplMockTest() {
        houseKeepingService = new HouseKeepingServiceImpl(mockHouseKeepingRepo, mockSearchRoomService,
                mockSearchBookingService);
    }

    @Test
    @DisplayName("When add house keeping with null duration then room contains it and room status is BEING_SERVICED")
    public void testAddHouseKeepingNew() {
        //given
        var testRoom = configureRoomSearch(null);
        var testHouseKeeping = configureAddHouseKeeping(0);
        //when
        RoomHouseKeeping houseKeeping = houseKeepingService.addHouseKeeping(TEST_ROOM_ID, testHouseKeeping);
        //then
        assertTrue(testRoom.getHouseKeepings().contains(houseKeeping));
        assertEquals(RoomStatus.BEING_SERVICED, testRoom.getRoomStatus());
    }

    @Test
    @DisplayName("When add house keeping with not null duration then room contains it")
    public void testAddHouseKeepingFinished() {
        //given
        var testRoom = configureRoomSearch(null);
        var testHouseKeeping = configureAddHouseKeeping(30);
        //when
        RoomHouseKeeping houseKeeping = houseKeepingService.addHouseKeeping(TEST_ROOM_ID, testHouseKeeping);
        //then
        assertTrue(testRoom.getHouseKeepings().contains(houseKeeping));
        assertNotEquals(RoomStatus.BEING_SERVICED, testRoom.getRoomStatus());
    }

    @Test
    @DisplayName("When update house keeping (not its duration) then update method for housekeeping repository is called")
    public void testUpdateHouseKeepingNotDuration() {
        //given
        var testHouseKeeping = getTestHouseKeeping(TEST_HOUSEKEEPING_ID, 0);
        testHouseKeeping.setDescription("Cleaning bathroom");
        when(mockHouseKeepingRepo.getById(TEST_HOUSEKEEPING_ID))
                .thenReturn(Optional.of(getTestHouseKeeping(TEST_HOUSEKEEPING_ID, 0)));
        //when
        houseKeepingService.updateHouseKeeping(testHouseKeeping);
        //then
        verify(mockHouseKeepingRepo).update(testHouseKeeping);
    }

    @Test
    @DisplayName("When update house keeping for confirmed booking then room status becomes reserved")
    public void testUpdateHouseKeepingForConfirmedBooking() {
        //given
        var testHouseKeeping = getTestHouseKeeping(TEST_HOUSEKEEPING_ID, 30);
        var testRoom = configureUpdateHouseKeepingTest(testHouseKeeping, BookingStatus.CONFIRMED);
        //when
        houseKeepingService.updateHouseKeeping(testHouseKeeping);
        //then
        verify(mockHouseKeepingRepo).update(testHouseKeeping);
        assertEquals(RoomStatus.RESERVED, testRoom.getRoomStatus());
    }

    @Test
    @DisplayName("When update house keeping for checked-in booking then room status becomes occupied")
    public void testUpdateHouseKeepingForCheckedInBooking() {
        //given
        var testHouseKeeping = getTestHouseKeeping(TEST_HOUSEKEEPING_ID, 30);
        var testRoom = configureUpdateHouseKeepingTest(testHouseKeeping, BookingStatus.CHECKED_IN);
        //when
        houseKeepingService.updateHouseKeeping(testHouseKeeping);
        //then
        verify(mockHouseKeepingRepo).update(testHouseKeeping);
        assertEquals(RoomStatus.OCCUPIED, testRoom.getRoomStatus());
    }

    @Test
    @DisplayName("When update house keeping for free room then room status becomes available")
    public void testUpdateHouseKeepingForFreeRoom() {
        //given
        var testHouseKeeping = getTestHouseKeeping(TEST_HOUSEKEEPING_ID, 30);
        var testRoom = configureUpdateHouseKeepingTest(testHouseKeeping, BookingStatus.CHECKED_OUT);
        //when
        houseKeepingService.updateHouseKeeping(testHouseKeeping);
        //then
        verify(mockHouseKeepingRepo).update(testHouseKeeping);
        assertEquals(RoomStatus.AVAILABLE, testRoom.getRoomStatus());
    }

    @Test
    @DisplayName("When update house keeping for room without current bookings then room status becomes available")
    public void testUpdateHouseKeepingNoBooking() {
        //given
        var testHouseKeeping = getTestHouseKeeping(TEST_HOUSEKEEPING_ID, 30);
        var testRoom = configureUpdateHouseKeepingTest(testHouseKeeping, null);
        //when
        houseKeepingService.updateHouseKeeping(testHouseKeeping);
        //then
        verify(mockHouseKeepingRepo).update(testHouseKeeping);
        assertEquals(RoomStatus.AVAILABLE, testRoom.getRoomStatus());
    }

    @Test
    @DisplayName("When search housekeeping by missing id then EntityNotFoundException is thrown")
    public void testSearchHouseKeepingByMissingId() {
        //given
        var testHouseKeepingId = 0;
        //when
        Executable executable = () -> houseKeepingService.searchById(testHouseKeepingId);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Room housekeeping with id = " + testHouseKeepingId + " not found.");
    }

    private Room getTestRoom(RoomHouseKeeping houseKeeping) {
        var room = new Room("202", RoomStyle.BUSINESS_SUITE, 850, false);
        room.setId(TEST_ROOM_ID);

        if (houseKeeping != null) {
            room.getHouseKeepings().add(houseKeeping);
            room.setRoomStatus(RoomStatus.BEING_SERVICED);
        }

        return room;
    }

    private Room configureRoomSearch(RoomHouseKeeping houseKeeping) {
        var room = getTestRoom(houseKeeping);
        when(mockSearchRoomService.searchById(TEST_ROOM_ID)).thenReturn(room);
        return room;
    }

    private RoomHouseKeeping getTestHouseKeeping(long id, int duration) {
        var houseKeeping =
                new RoomHouseKeeping("Test housekeeping", new MinuteInterval(LocalDateTime.now(), duration));
        if (id > 0) {
            houseKeeping.setId(id);
        }
        return houseKeeping;
    }

    private RoomHouseKeeping configureAddHouseKeeping(int duration) {
        var houseKeeping = getTestHouseKeeping(0, duration);
        when(mockHouseKeepingRepo.add(houseKeeping)).thenReturn(getTestHouseKeeping(TEST_HOUSEKEEPING_ID, duration));
        return houseKeeping;
    }

    private Room configureUpdateHouseKeepingTest(RoomHouseKeeping houseKeeping, BookingStatus status) {
        when(mockHouseKeepingRepo.getById(TEST_HOUSEKEEPING_ID))
                .thenReturn(Optional.of(getTestHouseKeeping(TEST_HOUSEKEEPING_ID, 0)));

        var room = getTestRoom(houseKeeping);
        when(mockSearchRoomService.searchByHouseKeeping(houseKeeping)).thenReturn(room);
        if (status != null) {
            var interval = new DayInterval(LocalDate.parse("01.07.2020", formatter), 3);
            var booking = new RoomBooking("RB-111", interval, room);
            booking.setStatus(status);
            when(mockSearchBookingService.searchCurrentByRoom(room)).thenReturn(Optional.of(booking));
        }

        return room;
    }
}
