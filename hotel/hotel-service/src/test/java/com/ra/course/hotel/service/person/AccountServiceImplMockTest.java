package com.ra.course.hotel.service.person;

import com.ra.course.hotel.model.entity.account.Account;
import com.ra.course.hotel.model.entity.account.AccountStatus;
import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.model.entity.persons.Person;
import com.ra.course.hotel.persistence.person.AccountRepo;
import com.ra.course.hotel.persistence.person.PersonRepo;
import com.ra.course.hotel.service.exception.EntityNotFoundException;
import com.ra.course.hotel.service.notification.NotificationService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AccountServiceImplMockTest {
    private AccountService accountService;
    private AccountRepo mockAccountRepo = mock(AccountRepo.class);
    private PersonRepo mockPersonRepo = mock(PersonRepo.class);
    private NotificationService mockNotificationService = mock(NotificationService.class);

    private static final String TEST_LOGIN = "peter_kovalenko";
    private static final String TEST_PASSWORD = "BlueBird_357";
    private static final String TEST_EMAIL = "peter_kovalenko@gmail.com";

    public AccountServiceImplMockTest() {
        accountService = new AccountServiceImpl(mockAccountRepo, mockPersonRepo, mockNotificationService);
    }

    @Test
    @DisplayName("When create account then add method for account repository is called")
    public void testCreateAccount() {
        //given
        var testAccount = getTestAccount(AccountStatus.CLOSED);
        //when
        accountService.createAccount(testAccount);
        //then
        verify(mockAccountRepo).add(testAccount);
    }

    @Test
    @DisplayName("When update password for account then password for this account is updated")
    public void testUpdatePassword() {
        //given
        var testAccount = getTestAccount(AccountStatus.ACTIVE);
        var newPassword = "BlueBird_124";
        //when
        accountService.updatePassword(testAccount, newPassword);
        //then
        assertEquals(newPassword, testAccount.getPassword());
    }

    @Test
    @DisplayName("When reset password for missing email then EntityNotFoundException is thrown")
    public void testResetPasswordMissingEmail() {
        //given
        var missingEmail = "bla@gmail.com";
        //when
        Executable executable = () -> accountService.resetPassword(missingEmail);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Account for email " + missingEmail + " not found.");
    }

    @Test
    @DisplayName("When reset password for person without account then EntityNotFoundException is thrown")
    public void testResetPasswordPersonWithoutAccount() {
        //given
        when(mockPersonRepo.getByEmail(TEST_EMAIL)).thenReturn(Optional.of(getTestGuest(false)));
        //when
        Executable executable = () -> accountService.resetPassword(TEST_EMAIL);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Account for email " + TEST_EMAIL + " not found.");
    }

    @Test
    @DisplayName("When reset password for existing email then temporary password is generated and notification to this email is created")
    public void testResetPasswordExistingEmail() {
        //given
        var testGuest = getTestGuest(true);
        when(mockPersonRepo.getByEmail(TEST_EMAIL)).thenReturn(Optional.of(testGuest));
        //when
        accountService.resetPassword(TEST_EMAIL);
        //then
        assertNotEquals(TEST_PASSWORD, testGuest.getAccount().getPassword());
        verify(mockNotificationService).createEmailNotification
                ("Your temporary password is " + testGuest.getAccount().getPassword(), 0, TEST_EMAIL);
    }

    @Test
    @DisplayName("When login with right credentials then active account is returned")
    public void testLoginRightCredentials() {
        //given
        when(mockAccountRepo.getByCredentials(TEST_LOGIN, TEST_PASSWORD)).thenReturn(Optional.of(getTestAccount(AccountStatus.CLOSED)));
        //when
        Account account = accountService.login(TEST_LOGIN, TEST_PASSWORD);
        //then
        assertEquals(AccountStatus.ACTIVE, account.getStatus());
    }

    @Test
    @DisplayName("When login with wrong credentials then EntityNotFoundException is thrown")
    public void testLoginWrongCredentials() {
        //when
        Executable executable = () -> accountService.login(TEST_LOGIN, "bla_bla_bla");
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Account not found.");
    }

    @Test
    @DisplayName("When logout then account status is closed ")
    public void testLogout() {
        //given
        var testAccount = getTestAccount(AccountStatus.ACTIVE);
        //when
        accountService.logout(testAccount);
        //then
        assertEquals(AccountStatus.CLOSED, testAccount.getStatus());
    }

    public Account getTestAccount(AccountStatus status) {
        var account = new Account(TEST_LOGIN, TEST_PASSWORD);
        account.setStatus(status);
        return account;
    }

    public Guest getTestGuest(boolean hasAccount) {
        var guest = new Guest();
        guest.setName("Peter Kovalenko");
        guest.setEmail(TEST_EMAIL);
        if (hasAccount) {
            guest.setAccount(getTestAccount(AccountStatus.CLOSED));
        }

        return guest;
    }
}
