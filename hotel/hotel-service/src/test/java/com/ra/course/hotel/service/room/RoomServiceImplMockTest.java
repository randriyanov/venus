package com.ra.course.hotel.service.room;

import com.ra.course.hotel.model.entity.common.MinuteInterval;
import com.ra.course.hotel.model.entity.hotel.HotelLocation;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomHouseKeeping;
import com.ra.course.hotel.model.entity.room.RoomKey;
import com.ra.course.hotel.model.entity.room.RoomStyle;
import com.ra.course.hotel.persistence.room.HouseKeepingRepo;
import com.ra.course.hotel.persistence.room.RoomKeyRepo;
import com.ra.course.hotel.persistence.room.RoomRepo;
import com.ra.course.hotel.service.hotel.SearchLocationService;
import org.junit.jupiter.api.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class RoomServiceImplMockTest {
    private RoomService roomService;
    private RoomRepo mockRoomRepo = mock(RoomRepo.class);
    private RoomKeyRepo mockRoomKeyRepo = mock(RoomKeyRepo.class);
    private HouseKeepingRepo mockHouseKeepingRepo = mock(HouseKeepingRepo.class);
    private static SearchLocationService mockSearchLocationService = mock(SearchLocationService.class);
    private SearchRoomService mockSearchRoomService = mock(SearchRoomService.class);

    private static HotelLocation location;
    private static final int TEST_LOCATION_ID = 1;
    private static final int TEST_ROOM_ID = 11;

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

    public RoomServiceImplMockTest() {
       roomService = RoomServiceImpl.builder()
               .roomRepo(mockRoomRepo).roomKeyRepo(mockRoomKeyRepo).houseKeepingRepo(mockHouseKeepingRepo)
               .sLocationService(mockSearchLocationService).sRoomService(mockSearchRoomService).build();
    }

    @BeforeAll
    public static void configureTestLocation() {
        location = new HotelLocation();
        location.setId(TEST_LOCATION_ID);
        location.setName("Iris Odessa");

        var room = new Room("201", RoomStyle.STANDARD, 700, false);
        room.setId(10);
        location.getRooms().add(room);

        when(mockSearchLocationService.searchById(TEST_LOCATION_ID)).thenReturn(location);
    }

    @Test
    @Order(1)
    @DisplayName("When add new room then hotel location contains it")
    public void testAddRoom() {
        //given
        var newRoom = getNewRoom(0);
        when(mockRoomRepo.add(newRoom)).thenReturn(getNewRoom(TEST_ROOM_ID));
        //when
        Room room = roomService.addRoom(TEST_LOCATION_ID, newRoom);
        //then
        assertTrue(location.getRooms().contains(room));
    }

    @Test
    @Order(2)
    @DisplayName("When edit room then update method for room repository is called")
    public void testEditRoom() {
        //given
        var testRoom = getExistingRoom(TEST_ROOM_ID, false);
        //when
        roomService.editRoom(testRoom);
        //then
        verify(mockRoomRepo).update(testRoom);
    }

    @Test
    @Order(3)
    @DisplayName("When remove room then location doesn't contain it, all its keys and housekeepings are removed too")
    public void testRemoveRoom() {
        //given
        var testRoomId = TEST_ROOM_ID;
        var testRoom = configureRemoveTest(testRoomId);
        //when
        roomService.removeRoom(testRoomId);
        //then
        checkRoomConnections(testRoom);
        verify(mockRoomRepo).remove(testRoom);
    }

    private RoomKey getTestKey(String keyId, boolean isMaster) {
        var key = new RoomKey();
        key.setKeyId(keyId);
        key.setMasterKey(isMaster);
        return  key;
    }

    private Room getNewRoom(long roomId) {
        var room = new Room("208", RoomStyle.FAMILY_SUITE, 1030, false);
        if (roomId > 0) {
            room.setId(roomId);
        }

        room.getKeys().add(getTestKey("K1", true));
        room.getKeys().add(getTestKey("K2", false));

        return room;
    }

    private Room getExistingRoom(long roomId, boolean hasHouseKeepings) {
        var room = location.getRooms().stream().filter(r -> r.getId() == roomId).findFirst().orElseThrow();

        if (hasHouseKeepings) {
            room.getHouseKeepings().add(new RoomHouseKeeping("Changing linens",
                    new MinuteInterval(LocalDateTime.parse("01.06.2020 10:00", formatter), 10)));
            room.getHouseKeepings().add(new RoomHouseKeeping("Cleaning room",
                    new MinuteInterval(LocalDateTime.parse("01.06.2020 10:10", formatter), 30)));

        }

        return room;
    }

    private Room configureRemoveTest(long roomId) {
        var testRoom = getExistingRoom(roomId, true);

        when(mockSearchRoomService.searchById(roomId)).thenReturn(testRoom);
        when(mockSearchLocationService.searchByRoom(testRoom)).thenReturn(location);

        return testRoom;
    }

    public void checkRoomConnections(Room room) {
        assertFalse(location.getRooms().contains(room));
        verify(mockRoomKeyRepo, times(room.getKeys().size())).remove(any(RoomKey.class));
        verify(mockHouseKeepingRepo, times(room.getHouseKeepings().size())).remove(any(RoomHouseKeeping.class));
    }
}
