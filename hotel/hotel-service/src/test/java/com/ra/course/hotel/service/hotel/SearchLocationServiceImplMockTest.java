package com.ra.course.hotel.service.hotel;

import com.ra.course.hotel.model.entity.hotel.HotelLocation;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomStyle;
import com.ra.course.hotel.persistence.hotel.HotelLocationRepo;
import com.ra.course.hotel.service.exception.EntityNotFoundException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class SearchLocationServiceImplMockTest {
    private SearchLocationService searchLocationService;
    private HotelLocationRepo mockHotelLocationRepo = mock(HotelLocationRepo.class);

    private static final int EXISTING_LOCATION_ID = 1;

    private static final String EXISTING_LOCATION_NAME = "Iris Odessa";
    private static final String MISSING_LOCATION_NAME = "Iris Lviv";

    public SearchLocationServiceImplMockTest() {
        searchLocationService = new SearchLocationServiceImpl(mockHotelLocationRepo);
    }

    @Test
    @DisplayName("When search hotel location by existing id then hotel location with such id is returned")
    public void testSearchHotelByExistingId() {
        //given
        var testLocationId = EXISTING_LOCATION_ID;
        when(mockHotelLocationRepo.getById(testLocationId)).thenReturn(Optional.of(getTestHotelLocation(null)));
        //when
        HotelLocation location = searchLocationService.searchById(testLocationId);
        //then
        verify(mockHotelLocationRepo).getById(testLocationId);
        assertNotNull(location);
    }

    @Test
    @DisplayName("When search hotel location by missing id then EntityNotFoundException is thrown")
    public void testSearchHotelByMissingId() {
        //given
        var testLocationId = 0;
        //when
        Executable executable = () -> searchLocationService.searchById(testLocationId);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Hotel with id = " + testLocationId + " not found.");
    }

    @Test
    @DisplayName("When search hotel location by existing name then hotel location with such name is returned")
    public void testSearchHotelByExistingName() {
        //given
        var testLocationName = EXISTING_LOCATION_NAME;
        when(mockHotelLocationRepo.getByName(testLocationName)).thenReturn(Optional.of(getTestHotelLocation(null)));
        //when
        HotelLocation location = searchLocationService.searchByName(testLocationName);
        //then
        verify(mockHotelLocationRepo).getByName(testLocationName);
        assertNotNull(location);
    }

    @Test
    @DisplayName("When search hotel location by missing name then EntityNotFoundException is thrown")
    public void testSearchHotelByMissingName() {
        //given
        var testLocationName = MISSING_LOCATION_NAME;
        //when
        Executable executable = () -> searchLocationService.searchByName(testLocationName);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Hotel " + testLocationName + " not found.");
    }

    @Test
    @DisplayName("When search hotel location by existing room then hotel location containing such room is returned")
    public void testSearchHotelByExistingRoom() {
        //given
        var testRoom = getTestRoom();
        when(mockHotelLocationRepo.getByRoom(testRoom)).thenReturn(Optional.of(getTestHotelLocation(testRoom)));
        //when
        HotelLocation location = searchLocationService.searchByRoom(testRoom);
        //then
        verify(mockHotelLocationRepo).getByRoom(testRoom);
        assertNotNull(location);
    }

    @Test
    @DisplayName("When search hotel location by missing room then EntityNotFoundException is thrown")
    public void testSearchHotelByMissingRoom() {
        //given
        var testRoom = getTestRoom();
        //when
        Executable executable = () -> searchLocationService.searchByRoom(testRoom);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Hotel with room " + testRoom.getRoomNumber() + " not found.");
    }

    private HotelLocation getTestHotelLocation(Room room) {
        var location = new HotelLocation();

        location.setId(EXISTING_LOCATION_ID);
        location.setName(EXISTING_LOCATION_NAME);

        if (room != null) {
            location.getRooms().add(room);
        }

        return location;
    }

    private Room getTestRoom() {
        return new Room("202", RoomStyle.BUSINESS_SUITE, 850, false);
    }
}

