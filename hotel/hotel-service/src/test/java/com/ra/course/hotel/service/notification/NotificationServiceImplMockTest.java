package com.ra.course.hotel.service.notification;

import com.ra.course.hotel.model.entity.booking.BookingStatus;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.common.Address;
import com.ra.course.hotel.model.entity.common.DayInterval;
import com.ra.course.hotel.model.entity.notification.EmailNotification;
import com.ra.course.hotel.model.entity.notification.PostalNotification;
import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomStyle;
import com.ra.course.hotel.persistence.booking.RoomBookingRepo;
import com.ra.course.hotel.persistence.notification.EmailNotificationRepo;
import com.ra.course.hotel.persistence.notification.PostalNotificationRepo;
import com.ra.course.hotel.service.person.SearchPersonService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class NotificationServiceImplMockTest {
    private NotificationService notificationService;
    private PostalNotificationRepo mockPostalNotifRepo = mock(PostalNotificationRepo.class);
    private EmailNotificationRepo mockEmailNotifRepo = mock(EmailNotificationRepo.class);
    private RoomBookingRepo mockBookingRepo = mock(RoomBookingRepo.class);
    private SearchPersonService mockSearchPersonService = mock(SearchPersonService.class);

    private final Address testAddress = new Address
            ("Sadovaya, 5", "Odessa", "Odesskaya", "65001", "Ukraine");
    private static final String TEST_CONTENT = "Hello! It's only test notification.";
    private static final int TEST_BOOKING_ID = 111;
    private static final String TEST_GUEST_NAME = "Peter Kovalenko";
    private static final String TEST_EMAIL = "peter_kovalenko@gmail.com";
    private static final int TEST_EMAIL_NOTIFICATION_ID = 1001;
    private static final int TEST_POSTAL_NOTIFICATION_ID = 1002;
    private static final int TEST_DAYS_BEFORE = 3;

    private static RoomBooking booking;
    private static List<RoomBooking> confirmedBookingsList, checkedinBookingsList;
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private static Guest guest;
    private static String testCheckInContent, testCheckOutContent;

    public NotificationServiceImplMockTest() {
        notificationService = NotificationServiceImpl.builder()
                .postalNotifRepo(mockPostalNotifRepo).emailNotifRepo(mockEmailNotifRepo)
                .bookingRepo(mockBookingRepo).sPersonService(mockSearchPersonService).build();
    }

    @BeforeAll
    public static void configureBookings() {
        var room1 = new Room("201", RoomStyle.STANDARD, 700, false);
        var room2 = new Room("202", RoomStyle.BUSINESS_SUITE, 850, false);
        var room3 = new Room("204", RoomStyle.BUSINESS_SUITE, 900, true);

        var bookingInterval = new DayInterval(LocalDate.now().plusDays(1), 2);
        booking = new RoomBooking("RB-120", bookingInterval, room2);
        booking.setId(TEST_BOOKING_ID);

        booking.setStatus(BookingStatus.CONFIRMED);

        var bookingInterval1 = new DayInterval(LocalDate.now().minusDays(2), 5);
        var bookingInterval2 = new DayInterval(LocalDate.now().minusDays(6), 9);
        var bookingInterval3 = new DayInterval(LocalDate.now().minusDays(14), 15);

        var bookingInterval4 = new DayInterval(LocalDate.now().plusDays(TEST_DAYS_BEFORE), 5);
        var bookingInterval5 = new DayInterval(LocalDate.now().plusDays(TEST_DAYS_BEFORE), 10);

        RoomBooking booking1, booking2, booking3, booking4, booking5;
        booking1 = new RoomBooking("RB-112", bookingInterval1, room1);
        booking1.setStatus(BookingStatus.CHECKED_IN);
        booking2 = new RoomBooking("RB-114", bookingInterval2, room2);
        booking2.setStatus(BookingStatus.CHECKED_IN);
        booking3 = new RoomBooking("RB-115", bookingInterval3, room3);
        booking3.setStatus(BookingStatus.CHECKED_IN);
        booking4 = new RoomBooking("RB-121", bookingInterval4, room2);
        booking4.setStatus(BookingStatus.CONFIRMED);
        booking5 = new RoomBooking("RB-124", bookingInterval5, room1);
        booking5.setStatus(BookingStatus.CONFIRMED);

        confirmedBookingsList = new CopyOnWriteArrayList<>(Arrays.asList(booking, booking4, booking5));
        checkedinBookingsList = new CopyOnWriteArrayList<>(Arrays.asList(booking1, booking2, booking3));

        guest = new Guest();
        guest.setName(TEST_GUEST_NAME);
        guest.setPhone("+380671234567");
        guest.setEmail(TEST_EMAIL);

        testCheckInContent = "Dear " + TEST_GUEST_NAME + "!" + System.lineSeparator()
                + "Your check in date is " + LocalDate.now().plusDays(TEST_DAYS_BEFORE).format(formatter)
                + System.lineSeparator() + "See you soon :) ...";

        testCheckOutContent = "Dear " + TEST_GUEST_NAME + "!" + System.lineSeparator()
                + "Your check out date is " + LocalDate.now().plusDays(TEST_DAYS_BEFORE).format(formatter)
                + System.lineSeparator() + "Please pay all invoices and return all keys before it :) ...";
    }

    @Test
    @DisplayName("When create postal notification with not null booking id then it has reference to booking with this id and booking contains it")
    public void testCreatePostalNotification() {
        //given
        when(mockPostalNotifRepo.add(any(PostalNotification.class))).thenReturn(getTestPostalNotification(testAddress));
        when(mockBookingRepo.getById(TEST_BOOKING_ID)).thenReturn(Optional.of(booking));
        //when
        PostalNotification notification = notificationService.createPostalNotification(TEST_CONTENT, TEST_BOOKING_ID, testAddress);
        //then
        assertEquals(TEST_BOOKING_ID, notification.getBooking().getId());
        assertTrue(booking.getNotifications().contains(notification));
    }

    @Test
    @DisplayName("When create email notification with not null booking id then it has reference to booking with this id and booking contains it")
    public void testCreateEmailNotification() {
        //given
        when(mockEmailNotifRepo.add(any(EmailNotification.class))).thenReturn(getTestEmailNotification(TEST_CONTENT));
        when(mockBookingRepo.getById(TEST_BOOKING_ID)).thenReturn(Optional.of(booking));
        //when
        EmailNotification notification = notificationService.createEmailNotification(TEST_CONTENT, TEST_BOOKING_ID, TEST_EMAIL);
        //then
        assertEquals(TEST_BOOKING_ID, notification.getBooking().getId());
        assertTrue(booking.getNotifications().contains(notification));
    }

    @Test
    @DisplayName("When prepare check in notifications then email notifications for confirmed bookings with suitable start dates are created")
    public void testPrepareCheckInNotifications() {
        //given
        when(mockBookingRepo.getAllByStatus(BookingStatus.CONFIRMED)).thenReturn(confirmedBookingsList);
        when(mockSearchPersonService.searchGuestByBooking(any(RoomBooking.class))).thenReturn(guest);
        when(mockEmailNotifRepo.add(any(EmailNotification.class))).thenReturn(getTestEmailNotification(testCheckInContent));
        //when
        List<EmailNotification> notifications = notificationService.prepareCheckInNotifications(TEST_DAYS_BEFORE);
        //then
        assertEquals(2, notifications.stream().filter(n -> n.getContent().equals(testCheckInContent))
                .filter(n -> n.getEmail().equals(TEST_EMAIL)).count());
    }

    @Test
    @DisplayName("When prepare check out notifications then email notifications for checked-in bookings with suitable end dates are created")
    public void testPrepareCheckOutNotifications() {
        //given
        when(mockBookingRepo.getAllByStatus(BookingStatus.CHECKED_IN)).thenReturn(checkedinBookingsList);
        when(mockSearchPersonService.searchGuestByBooking(any(RoomBooking.class))).thenReturn(guest);
        when(mockEmailNotifRepo.add(any(EmailNotification.class))).thenReturn(getTestEmailNotification(testCheckOutContent));
        //when
        List<EmailNotification> notifications = notificationService.prepareCheckOutNotifications(TEST_DAYS_BEFORE);
        //then
        assertEquals(2, notifications.stream().filter(n -> n.getContent().equals(testCheckOutContent))
                .filter(n -> n.getEmail().equals(TEST_EMAIL)).count());
    }

    public PostalNotification getTestPostalNotification(Address address) {
        var notification = new PostalNotification(TEST_CONTENT, address);
        notification.setId(TEST_POSTAL_NOTIFICATION_ID);
        return notification;
    }

    public EmailNotification getTestEmailNotification(String content) {
        var notification = new EmailNotification(content, TEST_EMAIL);
        notification.setId(TEST_EMAIL_NOTIFICATION_ID);
        return notification;
    }
}
