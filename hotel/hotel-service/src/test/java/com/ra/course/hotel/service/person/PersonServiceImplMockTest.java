package com.ra.course.hotel.service.person;

import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.model.entity.persons.HouseKeeper;
import com.ra.course.hotel.model.entity.persons.Person;
import com.ra.course.hotel.model.entity.persons.Receptionist;
import com.ra.course.hotel.persistence.person.GuestRepo;
import com.ra.course.hotel.persistence.person.HouseKeeperRepo;
import com.ra.course.hotel.persistence.person.PersonRepo;
import com.ra.course.hotel.persistence.person.ReceptionistRepo;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class PersonServiceImplMockTest {
    private PersonService personService;
    private PersonRepo mockPersonRepo = mock(PersonRepo.class);
    private GuestRepo mockGuestRepo = mock(GuestRepo.class);
    private ReceptionistRepo mockReceptionistRepo = mock(ReceptionistRepo.class);
    private HouseKeeperRepo mockHouseKeeperRepo = mock(HouseKeeperRepo.class);

    private static final int TEST_GUEST_ID = 5;
    private static final int TEST_RECEPTIONIST_ID = 1;
    private static final int TEST_HOUSEKEEPER_ID = 3;

    public PersonServiceImplMockTest() {
        personService = PersonServiceImpl.builder().personRepo(mockPersonRepo).guestRepo(mockGuestRepo)
                .receptionistRepo(mockReceptionistRepo).houseKeeperRepo(mockHouseKeeperRepo).build();
    }

    @Test
    @DisplayName("When add guest person then add method for guest repository is called")
    public void testAddPersonGuest() {
        //given
        var newGuest = getTestGuest(0);
        var testGuest = getTestGuest(TEST_GUEST_ID);
        when(mockPersonRepo.add(newGuest)).thenReturn(testGuest);
        //when
        Person person = personService.addPerson(newGuest);
        //then
        verify(mockGuestRepo).add(testGuest);
    }

    @Test
    @DisplayName("When add receptionist person then add method for receptionist repository is called")
    public void testAddPersonReceptionist() {
        //given
        var newReceptionist = getTestReceptionist(0);
        var testReceptionist = getTestReceptionist(TEST_RECEPTIONIST_ID);
        when(mockPersonRepo.add(newReceptionist)).thenReturn(testReceptionist);
        //when
        Person person = personService.addPerson(newReceptionist);
        //then
        verify(mockReceptionistRepo).add(testReceptionist);
    }

    @Test
    @DisplayName("When add house keeper person then add method for house keeper repository is called")
    public void testAddPersonHouseKeeper() {
        //given
        var newHouseKeeper = getTestHouseKeeper(0);
        var testHouseKeeper = getTestHouseKeeper(TEST_HOUSEKEEPER_ID);
        when(mockPersonRepo.add(newHouseKeeper)).thenReturn(testHouseKeeper);
        //when
        Person person = personService.addPerson(newHouseKeeper);
        //then
        verify(mockHouseKeeperRepo).add(testHouseKeeper);
    }

    @Test
    @DisplayName("When update person then update method for person repository is called")
    public void testUpdatePerson() {
        //given
        var testPerson = getTestGuest(TEST_GUEST_ID);
        //when
        personService.updatePerson(testPerson);
        //then
        verify(mockPersonRepo).update(testPerson);
    }

    public Guest getTestGuest(long id) {
        var guest = new Guest();
        guest.setId(id);
        guest.setName("Peter Kovalenko");
        guest.setPhone("+380671234567");
        guest.setEmail("peter_kovalenko@gmail.com");
        return guest;
    }

    public Receptionist getTestReceptionist(long id) {
        var receptionist = new Receptionist();
        receptionist.setId(TEST_RECEPTIONIST_ID);
        receptionist.setName("Olga Zayceva");
        receptionist.setPhone("+38063987654321");
        receptionist.setEmail("olga_zayceva@ukr.net");
        return receptionist;
    }

    public HouseKeeper getTestHouseKeeper(long id) {
        var houseKeeper = new HouseKeeper();
        houseKeeper.setId(TEST_HOUSEKEEPER_ID);
        houseKeeper.setName("Klava Ivanova");
        houseKeeper.setPhone("+380631928375");
        return houseKeeper;
    }
}
