package com.ra.course.hotel.service.room;

import com.ra.course.hotel.model.entity.booking.BookingStatus;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.common.DayInterval;
import com.ra.course.hotel.model.entity.common.MinuteInterval;
import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomHouseKeeping;
import com.ra.course.hotel.model.entity.room.RoomStyle;
import com.ra.course.hotel.persistence.room.RoomRepo;
import com.ra.course.hotel.service.exception.EntityNotFoundException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class SearchRoomServiceImplMockTest {
    private SearchRoomService searchRoomService;
    private RoomRepo mockRoomRepo = mock(RoomRepo.class);

    private static final int EXISTING_ROOM_ID = 11;

    private static final String EXISTING_ROOM_NUMBER = "202";
    private static final String MISSING_ROOM_NUMBER = "205";

    public SearchRoomServiceImplMockTest() {
        this.searchRoomService = new SearchRoomServiceImpl(mockRoomRepo);
    }

    @Test
    @DisplayName("When search room by existing id then room with such id is returned")
    public void testSearchRoomByExistingId() {
        //given
        var testRoomId = EXISTING_ROOM_ID;
        when(mockRoomRepo.getById(testRoomId)).thenReturn(Optional.of(getTestRoom(null)));
        //when
        Room room = searchRoomService.searchById(testRoomId);
        //then
        verify(mockRoomRepo).getById(testRoomId);
        assertNotNull(room);
    }

    @Test
    @DisplayName("When search room by missing id then EntityNotFoundException is thrown")
    public void testSearchRoomByMissingId() {
        //given
        var testRoomId = 0;
        //when
        Executable executable = () -> searchRoomService.searchById(testRoomId);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Room with id = " + testRoomId + " not found.");
    }

    @Test
    @DisplayName("When search room by existing room number then room with such room number is returned")
    public void testSearchRoomByExistingNumber() {
        //given
        var testRoomNumber = EXISTING_ROOM_NUMBER;
        when(mockRoomRepo.getByNumber(testRoomNumber)).thenReturn(Optional.of(getTestRoom(null)));
        //when
        Room room = searchRoomService.searchByNumber(testRoomNumber);
        //then
        verify(mockRoomRepo).getByNumber(testRoomNumber);
        assertNotNull(room);
    }

    @Test
    @DisplayName("When search room by missing number then EntityNotFoundException is thrown")
    public void testSearchRoomByMissingNumber() {
        //given
        var testRoomNumber = MISSING_ROOM_NUMBER;
        //when
        Executable executable = () -> searchRoomService.searchByNumber(testRoomNumber);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Room number " + testRoomNumber + " not found.");
    }

    @Test
    @DisplayName("When search room by existing housekeeping then room containing such housekeeping is returned")
    public void testSearchRoomByExistingHouseKeeping() {
        //given
        var testHouseKeeping = getTestHouseKeeping();
        when(mockRoomRepo.getByHouseKeeping(testHouseKeeping)).thenReturn(Optional.of(getTestRoom(testHouseKeeping)));
        //when
        Room room = searchRoomService.searchByHouseKeeping(testHouseKeeping);
        //then
        verify(mockRoomRepo).getByHouseKeeping(testHouseKeeping);
        assertNotNull(room);
    }

    @Test
    @DisplayName("When search room by missing housekeeping then EntityNotFoundException is thrown")
    public void testSearchRoomByMissingHouseKeeping() {
        //given
        var testHouseKeeping = getTestHouseKeeping();
        //when
        Executable executable = () -> searchRoomService.searchByHouseKeeping(testHouseKeeping);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(),
                "Room with housekeeping " + testHouseKeeping.getDescription() + " not found.");
    }

    @Test
    @DisplayName("When search all rooms with active bookings by guest then list of all rooms with active bookings made by this guest is returned")
    public void testSearchAllWithActiveBookingsByGuest() {
        //given
        var testGuest = getTestGuest();
        //when
        List<Room> rooms = searchRoomService.searchAllWithActiveBookingsByGuest(testGuest);
        //then
        assertEquals(3, rooms.size());
    }

    private Room getTestRoom(RoomHouseKeeping houseKeeping) {
        var room = new Room(EXISTING_ROOM_NUMBER, RoomStyle.BUSINESS_SUITE, 850, false);
        room.setId(EXISTING_ROOM_ID);

        if (houseKeeping != null) {
            room.getHouseKeepings().add(houseKeeping);
        }

        return room;
    }

    private RoomHouseKeeping getTestHouseKeeping() {
        return new RoomHouseKeeping("Test housekeeping", new MinuteInterval(LocalDateTime.now(), 30));
    }

    public Guest getTestGuest() {
        var guest = new Guest();
        guest.setId(1);
        guest.setName("Peter Kovalenko");
        guest.setPhone("+380671234567");
        guest.setEmail("peter_kovalenko@gmail.com");

        var room1 = new Room("201", RoomStyle.STANDARD, 700,false);
        var room2 = getTestRoom(null);
        var room3 = new Room("204", RoomStyle.BUSINESS_SUITE, 900, true);

        RoomBooking booking1 = new RoomBooking("RB-111",
                new DayInterval(LocalDate.now().minusDays(1), 3), room1);
        booking1.setStatus(BookingStatus.CONFIRMED);

        RoomBooking booking2 = new RoomBooking("RB-112",
                new DayInterval(LocalDate.now().minusDays(2), 4), room2);
        booking2.setStatus(BookingStatus.CHECKED_IN);

        RoomBooking booking3 = new RoomBooking("RB-115",
                new DayInterval(LocalDate.now().plusDays(5), 2), room2);
        booking3.setStatus(BookingStatus.CONFIRMED);

        RoomBooking booking4 = new RoomBooking("RB-116",
                new DayInterval(LocalDate.now(),1), room3);
        booking4.setStatus(BookingStatus.PENDING);

        RoomBooking booking5 = new RoomBooking("RB-117",
                new DayInterval(LocalDate.now().minusDays(1),1), room3);
        booking5.setStatus(BookingStatus.CANCELED);

        guest.setBookings(Arrays.asList(booking1, booking2, booking3, booking4, booking5));

        return guest;
    }
}
