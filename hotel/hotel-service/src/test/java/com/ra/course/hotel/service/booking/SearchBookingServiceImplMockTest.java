package com.ra.course.hotel.service.booking;

import com.ra.course.hotel.model.entity.booking.BookingStatus;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.common.DayInterval;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomStyle;
import com.ra.course.hotel.persistence.booking.RoomBookingRepo;
import com.ra.course.hotel.service.exception.EntityNotFoundException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class SearchBookingServiceImplMockTest {
    private SearchBookingService searchBookingService;
    private RoomBookingRepo mockRoomBookingRepo = mock(RoomBookingRepo.class);

    private static Room room1, room2;
    private static RoomBooking booking1, booking2, curBooking;
    private static List<RoomBooking> bookingList1, bookingList2;

    private static final String EXISTING_RESERV_NUMBER = "RB-111";
    private static final String MISSING_RESERV_NUMBER = "RB-115";

    public SearchBookingServiceImplMockTest() {
        searchBookingService = new SearchBookingServiceImpl(mockRoomBookingRepo);
    }

    @BeforeAll
    public static void configureRooms() {
        room1 = new Room("201", RoomStyle.STANDARD, 700, false);
        var bookingInterval1 = new DayInterval(LocalDate.now().plusDays(30), 14);
        booking1 = new RoomBooking(EXISTING_RESERV_NUMBER, bookingInterval1, room1);
        booking1.setStatus(BookingStatus.PENDING);
        bookingList1 = new CopyOnWriteArrayList<>();
        bookingList1.add(booking1);

        room2 = new Room("202", RoomStyle.BUSINESS_SUITE, 850, false);
        var bookingInterval2 = new DayInterval(LocalDate.now().plusDays(7), 5);
        booking2 = new RoomBooking("RB-112", bookingInterval2, room2);
        booking2.setStatus(BookingStatus.CONFIRMED);
        var curBookingInterval = new DayInterval(LocalDate.now().minusDays(1), 3);
        curBooking = new RoomBooking("RB-114", curBookingInterval, room2);
        curBooking.setStatus(BookingStatus.CHECKED_IN);
        bookingList2 = new CopyOnWriteArrayList<>();
        bookingList2.add(booking2);
        bookingList2.add(curBooking);
    }

    @Test
    @DisplayName("When search room booking by existing reservation number then booking with such number is returned")
    public void testSearchByExistingReservNumber() {
        //given
        var testReservNumber = EXISTING_RESERV_NUMBER;
        when(mockRoomBookingRepo.getByReservNumber(testReservNumber)).thenReturn(Optional.of(booking1));
        //when
        RoomBooking booking = searchBookingService.searchByReservNumber(testReservNumber);
        //then
        verify(mockRoomBookingRepo).getByReservNumber(testReservNumber);
        assertNotNull(booking);
    }

    @Test
    @DisplayName("When search room booking by missing reservation number then EntityNotFoundException is thrown")
    public void testSearchByMissingReservNumber() {
        //given
        var testReservNumber = MISSING_RESERV_NUMBER;
        //when
        Executable executable = () -> searchBookingService.searchByReservNumber(testReservNumber);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Room booking number " + testReservNumber + " not found.");
    }

    @Test
    @DisplayName("When search current room booking by busy now room then Optional with current booking for this room is returned")
    public void testSearchCurrentByBusyRoom() {
        //given
        var testRoom = room2;
        when(mockRoomBookingRepo.getAllActiveByRoom(testRoom)).thenReturn(bookingList2);
        //when
        Optional<RoomBooking> booking = searchBookingService.searchCurrentByRoom(testRoom);
        //then
        assertTrue(booking.isPresent());
        assertTrue(booking.get().getInterval().getStartDate().isBefore(LocalDate.now())
                && booking.get().getInterval().getStartDate().plusDays(booking.get().getInterval().getDuration()).isAfter(LocalDate.now()));
    }

    @Test
    @DisplayName("When search current room booking by free now room then empty Optional is returned")
    public void testSearchCurrentByFreeRoom() {
        //given
        var testRoom = room1;
        when(mockRoomBookingRepo.getAllActiveByRoom(testRoom)).thenReturn(bookingList1);
        //when
        Optional<RoomBooking> booking = searchBookingService.searchCurrentByRoom(testRoom);
        //then
        assertTrue(booking.isEmpty());
    }
}
