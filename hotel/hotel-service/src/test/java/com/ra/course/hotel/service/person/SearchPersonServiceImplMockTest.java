package com.ra.course.hotel.service.person;

import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.common.DayInterval;
import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.model.entity.persons.Receptionist;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.persistence.person.GuestRepo;
import com.ra.course.hotel.persistence.person.ReceptionistRepo;
import com.ra.course.hotel.service.booking.SearchBookingService;
import com.ra.course.hotel.service.exception.EntityNotFoundException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class SearchPersonServiceImplMockTest {
    private SearchPersonService searchPersonService;
    private GuestRepo mockGuestRepo = mock(GuestRepo.class);
    private ReceptionistRepo mockReceptionistRepo = mock(ReceptionistRepo.class);
    private SearchBookingService mockSearchBookingService = mock(SearchBookingService.class);

    private static final int EXISTING_GUEST_ID = 3;
    private static final String EXISTING_GUEST_NAME = "Peter Kovalenko";
    private static final String MISSING_GUEST_NAME = "Ivan Basov";
    private static final String TEST_RESERV_NUMBER = "RB-111";
    private static final int EXISTING_RECEPTIONIST_ID = 1;

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    public SearchPersonServiceImplMockTest() {
        searchPersonService = new SearchPersonServiceImpl(mockGuestRepo, mockReceptionistRepo, mockSearchBookingService);
    }

    @Test
    @DisplayName("When search guest by existing id then guest with such id is returned")
    public void testSearchGuestByExistngId() {
        //given
        when(mockGuestRepo.getById(EXISTING_GUEST_ID)).thenReturn(Optional.of(getTestGuest(null)));
        //when
        Guest guest = searchPersonService.searchGuestById(EXISTING_GUEST_ID);
        //then
        verify(mockGuestRepo).getById(EXISTING_GUEST_ID);
        assertNotNull(guest);
    }

    @Test
    @DisplayName("When search guest by missing id then EntityNotFoundException is thrown")
    public void testSearchGuestByMissingId() {
        //when
        Executable executable = () -> searchPersonService.searchGuestById(0);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Guest with id = 0 not found.");
    }

    @Test
    @DisplayName("When search guest by existing name then guest with such name is returned")
    public void testSearchGuestByExistingName() {
        //given
        when(mockGuestRepo.getByName(EXISTING_GUEST_NAME)).thenReturn(Optional.of(getTestGuest(null)));
        //when
        Guest guest = searchPersonService.searchGuestByName(EXISTING_GUEST_NAME);
        //then
        verify(mockGuestRepo).getByName(EXISTING_GUEST_NAME);
        assertNotNull(guest);
    }

    @Test
    @DisplayName("When search guest by missing name then EntityNotFoundException is thrown")
    public void testSearchGuestByMissingName() {
        //when
        Executable executable = () -> searchPersonService.searchGuestByName(MISSING_GUEST_NAME);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Guest " + MISSING_GUEST_NAME + " not found.");
    }

    @Test
    @DisplayName("When search guest by existing booking then guest is returned")
    public void testSearchGuestByExistingBooking() {
        //given
        var testBooking = getTestBooking(getTestRoom());
        when(mockGuestRepo.getByBooking(testBooking)).thenReturn(Optional.of(getTestGuest(testBooking)));
        //when
        Guest guest = searchPersonService.searchGuestByBooking(testBooking);
        //then
        verify(mockGuestRepo).getByBooking(testBooking);
        assertNotNull(guest);
    }

    @Test
    @DisplayName("When search guest by missing booking then EntityNotFoundException is thrown")
    public void testSearchGuestByMissingBooking() {
        //given
        var testBooking = getTestBooking(getTestRoom());
        //when
        Executable executable = () -> searchPersonService.searchGuestByBooking(testBooking);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(),
                "Guest with room booking " + TEST_RESERV_NUMBER + " not found.");
    }

    @Test
    @DisplayName("When search guest by room with current booking then not empty Optional is returned")
    public void testSearchCurrentGuestByRoomWithCurrentBooking() {
        //given
        var testRoom = getTestRoom();
        configureSearchGuestByRoom(testRoom, true);
        //when
        Optional<Guest> guest = searchPersonService.searchCurrentGuestByRoom(testRoom);
        //then
        assertTrue(guest.isPresent());
    }

    @Test
    @DisplayName("When search guest by room without current booking then empty Optional is returned")
    public void testSearchCurrentGuestByRoomWithoutCurrentBooking() {
        //given
        var testRoom = getTestRoom();
        configureSearchGuestByRoom(testRoom, false);
        //when
        Optional<Guest> guest = searchPersonService.searchCurrentGuestByRoom(testRoom);
        //then
        assertTrue(guest.isEmpty());
    }

    @Test
    @DisplayName("When search receptionist by existing id then receptionist with such id is returned")
    public void testSearchReceptionistByExistngId() {
        //given
        when(mockReceptionistRepo.getById(EXISTING_RECEPTIONIST_ID)).thenReturn(Optional.of(getTestReceptionist()));
        //when
        Receptionist receptionist = searchPersonService.searchReceptionistById(EXISTING_RECEPTIONIST_ID);
        //then
        assertEquals(EXISTING_RECEPTIONIST_ID, receptionist.getId());
    }

    @Test
    @DisplayName("When search receptionist by missing id then EntityNotFoundException is thrown")
    public void testSearchReceptionistByMissingId() {
        //when
        Executable executable = () -> searchPersonService.searchReceptionistById(0);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Receptionist with id = 0 not found.");
    }

    public Guest getTestGuest(RoomBooking booking) {
        var guest = new Guest();
        guest.setId(EXISTING_GUEST_ID);
        guest.setName(EXISTING_GUEST_NAME);
        guest.setPhone("+380671234567");
        guest.setEmail("peter_kovalenko@gmail.com");
        if (booking != null) {
            guest.getBookings().add(booking);
        }
        return guest;
    }

    public RoomBooking getTestBooking(Room room) {
        var interval = new DayInterval(LocalDate.parse("01.07.2020", formatter), 3);
        return new RoomBooking(TEST_RESERV_NUMBER, interval, room);
    }

    public Room getTestRoom() {
        var room = new Room();
        room.setRoomNumber("201");
        return room;
    }

    public void configureSearchGuestByRoom(Room room, boolean roomHasBooking) {
        RoomBooking booking = null;
        if (roomHasBooking) {
            booking = getTestBooking(room);
            when(mockSearchBookingService.searchCurrentByRoom(room)).thenReturn(Optional.of(booking));
        }
        when(mockGuestRepo.getByBooking(booking)).thenReturn(Optional.of(getTestGuest(booking)));
    }

    public Receptionist getTestReceptionist() {
        var receptionist = new Receptionist();
        receptionist.setId(EXISTING_RECEPTIONIST_ID);
        receptionist.setName("Olga Zayceva");
        receptionist.setPhone("+38063987654321");
        receptionist.setEmail("olga_zayceva@ukr.net");
        return receptionist;
    }
}