package com.ra.course.hotel.service.booking;

import com.ra.course.hotel.model.entity.booking.BookingStatus;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.common.DayInterval;
import com.ra.course.hotel.model.entity.invoice.Invoice;
import com.ra.course.hotel.model.entity.payment.CreditCardTransaction;
import com.ra.course.hotel.model.entity.payment.PaymentStatus;
import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.model.entity.persons.Receptionist;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.persistence.booking.RoomBookingRepo;
import com.ra.course.hotel.service.exception.WrongStatusException;
import com.ra.course.hotel.service.person.SearchPersonService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class BookingServiceImplMockTest {
    private BookingService bookingService;
    private RoomBookingRepo mockBookingRepo = mock(RoomBookingRepo.class);
    private SearchBookingService mockSearchBookingService = mock(SearchBookingService.class);
    private SearchPersonService mockSearchPersonService = mock(SearchPersonService.class);

    private static Guest guest;
    private static final int TEST_GUEST_ID = 3;
    private static final String TEST_GUEST_NAME = "Peter Kovalenko";
    private static Receptionist receptionist;
    private static final int TEST_RECEPTIONIST_ID = 1;

    private static final int TEST_BOOKING_ID = 111;
    private static final String TEST_RESERV_NUMBER = "RB-111";
    private static final int TEST_ROOM_ID = 11;

    public BookingServiceImplMockTest() {
        bookingService = new BookingServiceImpl(mockBookingRepo, mockSearchBookingService, mockSearchPersonService);
    }

    @BeforeAll
    public static void configurePersons() {
        guest = new Guest();
        guest.setId(TEST_GUEST_ID);
        guest.setName(TEST_GUEST_NAME);

        receptionist = new Receptionist();
        receptionist.setId(TEST_RECEPTIONIST_ID);
        receptionist.setName("Olga Zayceva");
    }

    @Test
    @DisplayName("When create booking with guest (without receptionist) then guest has this booking")
    public void testCreateBookingWithGuest() {
        // given
        var testBooking = configureCreateBooking(TEST_GUEST_ID, 0);
        //when
        RoomBooking booking = bookingService.createBooking(testBooking, TEST_GUEST_ID, 0);
        //then
        assertEquals(BookingStatus.CONFIRMED, booking.getStatus());
        assertTrue(guest.getBookings().contains(booking));
    }

    @Test
    @DisplayName("When create booking with guest and receptionist then guest and receptionist have this booking")
    public void testCreateBookingWithGuestAndReceptionist() {
        // given
        var testBooking = configureCreateBooking(TEST_GUEST_ID, TEST_RECEPTIONIST_ID);
        //when
        RoomBooking booking = bookingService.createBooking(testBooking, TEST_GUEST_ID, TEST_RECEPTIONIST_ID);
        //then
        assertEquals(BookingStatus.CONFIRMED, booking.getStatus());
        assertTrue(guest.getBookings().contains(booking));
        assertTrue(receptionist.getBookings().contains(booking));
    }

    @Test
    @DisplayName("When update room booking then update method for room booking repository is called")
    public void testUpdateBooking() {
        //given
        var testBooking = getTestBooking(TEST_BOOKING_ID, LocalDate.now(), BookingStatus.REQUESTED, false);
        //when
        bookingService.updateBooking(testBooking);
        //then
        verify(mockBookingRepo).update(testBooking);
    }

    @Test
    @DisplayName("When cancel room booking with wrong booking status then WrongStatusException is thrown")
    public void testCancelWrongStatusBooking() {
        //given
        var testBooking = getTestBooking(TEST_BOOKING_ID, LocalDate.now(), BookingStatus.CHECKED_OUT,false);
        when(mockSearchBookingService.searchByReservNumber(TEST_RESERV_NUMBER)).thenReturn(testBooking);
        //when
        Executable executable = () -> bookingService.cancelBooking(TEST_RESERV_NUMBER);
        //then
        Throwable exception = assertThrows(WrongStatusException.class, executable);
        assertEquals(exception.getMessage(),
                "Wrong status for cancel (BookingStatus." + testBooking.getStatus() + ")");
    }

    @Test
    @DisplayName("When cancel room booking on the last day then booking is canceled without full refund")
    public void testCancelBookingOnLastDay() {
        //given
        var testBooking = getTestBooking(TEST_BOOKING_ID, LocalDate.now(), BookingStatus.CONFIRMED, true);
        when(mockSearchBookingService.searchByReservNumber(TEST_RESERV_NUMBER)).thenReturn(testBooking);
        //when
        RoomBooking booking = bookingService.cancelBooking(TEST_RESERV_NUMBER);
        //then
        assertEquals(BookingStatus.CANCELED, booking.getStatus());
        assertNotEquals(Optional.empty(), booking.getInvoices().stream().filter(inv -> inv.getTransaction() != null)
                .filter(inv -> inv.getTransaction().getStatus() != PaymentStatus.REFUNDED).findAny());
    }

    @Test
    @DisplayName("When cancel room booking before last day then booking is canceled with full refund")
    public void testCancelBookingBeforeLastDay() {
        //given
        var testBooking = getTestBooking(TEST_BOOKING_ID, LocalDate.now().plusDays(2), BookingStatus.CONFIRMED, true);
        when(mockSearchBookingService.searchByReservNumber(TEST_RESERV_NUMBER)).thenReturn(testBooking);
        //when
        RoomBooking booking = bookingService.cancelBooking(TEST_RESERV_NUMBER);
        //then
        assertEquals(BookingStatus.CANCELED, booking.getStatus());
        assertEquals(Optional.empty(), booking.getInvoices().stream().filter(inv -> inv.getTransaction() != null)
                .filter(inv -> inv.getTransaction().getStatus() != PaymentStatus.REFUNDED).findAny());
    }

    public RoomBooking configureCreateBooking(long guestId, long receptionistId) {
        var startDate = LocalDate.now().plusDays(7);
        var booking = getTestBooking(0, startDate, BookingStatus.PENDING, false);
        when(mockSearchPersonService.searchGuestById(guestId)).thenReturn(guest);
        if (receptionistId > 0) {
            when(mockSearchPersonService.searchReceptionistById(receptionistId)).thenReturn(receptionist);
        }
        when(mockBookingRepo.add(booking))
                .thenReturn(getTestBooking(TEST_BOOKING_ID, startDate, BookingStatus.PENDING,false));
        return booking;
    }

    private RoomBooking getTestBooking(long bookingId, LocalDate date, BookingStatus bookingStatus, boolean hasPayment) {
        var interval = new DayInterval(date, 3);
        var booking = new RoomBooking("RB-111", interval, new Room());
        if (bookingId > 0) {
            booking.setId(bookingId);
        }
        booking.setStatus(bookingStatus);

        if (hasPayment) {
            Invoice invoice = new Invoice(booking.getRoom().getBookingPrice(), booking);
            invoice.setTransaction(new CreditCardTransaction(invoice.getTotalAmount(), TEST_GUEST_NAME, "65001"));
            invoice.getTransaction().setStatus(PaymentStatus.COMPLETED);
            booking.getInvoices().add(invoice);

            booking.getInvoices().add(new Invoice(100, booking));
        }

        return booking;
    }
}
