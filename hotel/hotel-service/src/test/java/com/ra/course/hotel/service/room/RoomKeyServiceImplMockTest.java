package com.ra.course.hotel.service.room;

import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomKey;
import com.ra.course.hotel.model.entity.room.RoomStyle;
import com.ra.course.hotel.persistence.room.RoomKeyRepo;
import com.ra.course.hotel.service.exception.EntityNotFoundException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class RoomKeyServiceImplMockTest {
    private RoomKeyService roomKeyService;
    private RoomKeyRepo mockRoomKeyRepo = mock(RoomKeyRepo.class);
    private SearchRoomService mockSearchRoomService = mock(SearchRoomService.class);

    private static final int TEST_ROOM_ID = 11;

    private static final String ACTIVE_KEY_ID = "K1";
    private static final String INACTIVE_KEY_ID = "K2";
    private static final String MISSING_KEY_ID = "K99";

    public RoomKeyServiceImplMockTest() {
        roomKeyService = new RoomKeyServiceImpl(mockRoomKeyRepo, mockSearchRoomService);
    }

    @Test
    @DisplayName("When issue key for room with inactive keys then active room key is returned")
    public void testIssueKeyForRoomWithInactiveKeys() {
        //given
        var testRoomId = TEST_ROOM_ID;
        when(mockSearchRoomService.searchById(testRoomId)).thenReturn(getTestRoom(true));
        //when
        RoomKey key = roomKeyService.issueKey(testRoomId);
        //then
        assertTrue(mockSearchRoomService.searchById(testRoomId).getKeys().contains(key));
        assertTrue(key.isActive());
    }

    @Test
    @DisplayName("When issue key for room without inactive keys then EntityNotFoundException is thrown")
    public void testIssueKeyForRoomWithoutInactiveKeys() {
        //given
        var testRoomId = TEST_ROOM_ID;
        when(mockSearchRoomService.searchById(testRoomId)).thenReturn(getTestRoom(false));
        //when
        Executable executable = () -> roomKeyService.issueKey(testRoomId);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Inactive key for room number "
                + mockSearchRoomService.searchById(testRoomId).getRoomNumber() + " not found.");
    }

    @Test
    @DisplayName("When search room key by existing keyId then key with such keyId is returned")
    public void testSearchRoomKeyByExistngKeyId() {
        //given
        var testKeyId = INACTIVE_KEY_ID;
        when(mockRoomKeyRepo.getByKeyId(testKeyId)).thenReturn(Optional.of(getTestKey(INACTIVE_KEY_ID, false)));
        //when
        RoomKey key = roomKeyService.searchByKeyId(testKeyId);
        //then
        verify(mockRoomKeyRepo).getByKeyId(testKeyId);
        assertNotNull(key);
    }

    @Test
    @DisplayName("When search room key by missing keyId then EntityNotFoundException is thrown")
    public void testSearchRoomKeyByMissingKeyId() {
        //given
        var testKeyId = MISSING_KEY_ID;
        //when
        Executable executable = () -> roomKeyService.searchByKeyId(testKeyId);
        //then
        Throwable exception = assertThrows(EntityNotFoundException.class, executable);
        assertEquals(exception.getMessage(), "Room key " + testKeyId + " not found.");
    }

    @Test
    @DisplayName("When receive room key then it becomes inactive with null IssuedAt field")
    public void testReceiveKey() {
        //given
        var testKeyId = ACTIVE_KEY_ID;
        when(mockRoomKeyRepo.getByKeyId(testKeyId)).thenReturn(Optional.of(getTestKey(testKeyId, true)));
        //when
        roomKeyService.receiveKey(ACTIVE_KEY_ID);
        //then
        assertNull(roomKeyService.searchByKeyId(testKeyId).getIssuedAt());
        assertFalse(roomKeyService.searchByKeyId(testKeyId).isActive());
    }

    private Room getTestRoom(boolean hasKeys) {
        var room = new Room("202", RoomStyle.BUSINESS_SUITE, 850, false);
        room.setId(TEST_ROOM_ID);

        if (hasKeys) {
            room.setKeys(getTestKeyList());
        }

        return room;
    }

    private RoomKey getTestKey(String keyId, boolean isActive) {
        var key = new RoomKey();
        key.setKeyId(keyId);
        key.setActive(isActive);
        if (isActive) {
            key.setIssuedAt(LocalDateTime.now().minusDays(1));
        }
        return key;
    }

    private List<RoomKey> getTestKeyList() {
        var list = new CopyOnWriteArrayList<RoomKey>();
        list.add(getTestKey(ACTIVE_KEY_ID, true));
        list.add(getTestKey(INACTIVE_KEY_ID, false));
        return  list;
    }
}
