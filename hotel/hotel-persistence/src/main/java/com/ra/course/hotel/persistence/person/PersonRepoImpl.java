package com.ra.course.hotel.persistence.person;

import com.ra.course.hotel.model.entity.persons.Person;
import com.ra.course.hotel.persistence.BaseRepoImpl;

import java.util.Optional;

public class PersonRepoImpl extends BaseRepoImpl<Person> implements PersonRepo {
    @Override
    public Optional<Person> getByEmail(final String email) {
        return repo.stream().filter(p -> p.getEmail().equals(email)).findFirst();
    }
}