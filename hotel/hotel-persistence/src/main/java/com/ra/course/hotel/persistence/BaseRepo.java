package com.ra.course.hotel.persistence;

import com.ra.course.hotel.model.entity.BaseEntity;

import java.util.List;
import java.util.Optional;

public interface BaseRepo<T extends BaseEntity> {
    T add(T entity);
    void update(T entity);
    void remove(T entity);
    List<T> getAll();
    Optional<T> getById(long id);
}
