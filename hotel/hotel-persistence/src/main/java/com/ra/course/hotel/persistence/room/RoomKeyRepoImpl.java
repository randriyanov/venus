package com.ra.course.hotel.persistence.room;

import com.ra.course.hotel.model.entity.room.RoomKey;
import com.ra.course.hotel.persistence.BaseRepoImpl;

import java.util.Optional;

public class RoomKeyRepoImpl extends BaseRepoImpl<RoomKey> implements RoomKeyRepo {
    @Override
    public Optional<RoomKey> getByKeyId(final String keyId) {
        return repo.stream().filter(k -> k.getKeyId().equals(keyId)).findFirst();
    }
}
