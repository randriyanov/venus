package com.ra.course.hotel.persistence.room;

import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomHouseKeeping;
import com.ra.course.hotel.persistence.BaseRepoImpl;

import java.util.Optional;

public class RoomRepoImpl extends BaseRepoImpl<Room> implements RoomRepo {
    @Override
    public Optional<Room> getByNumber(final String roomNumber) {
        return repo.stream().filter(r -> r.getRoomNumber().equals(roomNumber)).findFirst();
    }

    @Override
    public Optional<Room> getByHouseKeeping(final RoomHouseKeeping houseKeeping) {
        return repo.stream().filter(r -> r.getHouseKeepings().contains(houseKeeping)).findFirst();
    }
}

