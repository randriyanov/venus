package com.ra.course.hotel.persistence.hotel;

import com.ra.course.hotel.model.entity.hotel.HotelLocation;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.persistence.BaseRepo;

import java.util.Optional;

public interface HotelLocationRepo extends BaseRepo<HotelLocation> {
    Optional<HotelLocation> getByName(String name);
    Optional<HotelLocation> getByRoom(Room room);
}
