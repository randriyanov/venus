package com.ra.course.hotel.persistence.invoice;

import com.ra.course.hotel.model.entity.invoice.Invoice;
import com.ra.course.hotel.model.entity.invoice.InvoiceItem;
import com.ra.course.hotel.persistence.BaseRepo;

import java.util.Optional;

public interface InvoiceRepo extends BaseRepo<Invoice> {
    Optional<Invoice> getInvoiceByItem(InvoiceItem item);
}
