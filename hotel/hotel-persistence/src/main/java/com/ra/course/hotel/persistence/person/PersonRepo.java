package com.ra.course.hotel.persistence.person;

import com.ra.course.hotel.model.entity.persons.Person;
import com.ra.course.hotel.persistence.BaseRepo;

import java.util.Optional;

public interface PersonRepo extends BaseRepo<Person> {
    Optional<Person> getByEmail(String email);
}
