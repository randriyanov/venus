package com.ra.course.hotel.persistence.person;

import com.ra.course.hotel.model.entity.persons.Receptionist;
import com.ra.course.hotel.persistence.BaseRepo;

public interface ReceptionistRepo extends BaseRepo<Receptionist> {
}
