package com.ra.course.hotel.persistence.hotel;

import com.ra.course.hotel.model.entity.hotel.Hotel;

public class HotelRepoImpl implements HotelRepo {
    final private Hotel hotel;

    public HotelRepoImpl() {
        this.hotel = new Hotel("Hotel");
    }

    @Override
    public void updateName(final String name) {
        hotel.setName(name);
    }

    public Hotel getHotel() {
        return hotel;
    }
}
