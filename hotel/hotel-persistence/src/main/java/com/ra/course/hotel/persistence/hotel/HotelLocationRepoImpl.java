package com.ra.course.hotel.persistence.hotel;

import com.ra.course.hotel.model.entity.hotel.HotelLocation;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.persistence.BaseRepoImpl;

import java.util.Optional;

public class HotelLocationRepoImpl extends BaseRepoImpl<HotelLocation> implements HotelLocationRepo {
    @Override
    public Optional<HotelLocation> getByName(final String name) {
        return repo.stream().filter(l -> l.getName().equals(name)).findAny();
    }

    @Override
    public Optional<HotelLocation> getByRoom(final Room room) {
        return repo.stream().filter(l -> l.getRooms().contains(room)).findFirst();
    }
}
