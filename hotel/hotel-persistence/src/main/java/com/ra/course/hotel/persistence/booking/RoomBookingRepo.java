package com.ra.course.hotel.persistence.booking;

import com.ra.course.hotel.model.entity.booking.BookingStatus;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.persistence.BaseRepo;

import java.util.List;
import java.util.Optional;

public interface RoomBookingRepo extends BaseRepo<RoomBooking> {
    Optional<RoomBooking> getByReservNumber(String reservNumber);
    List<RoomBooking> getAllByStatus(BookingStatus status);
    List<RoomBooking> getAllActiveByRoom(Room room);
}
