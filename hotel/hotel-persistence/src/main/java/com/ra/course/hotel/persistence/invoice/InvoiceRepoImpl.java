package com.ra.course.hotel.persistence.invoice;

import com.ra.course.hotel.model.entity.invoice.Invoice;
import com.ra.course.hotel.model.entity.invoice.InvoiceItem;
import com.ra.course.hotel.persistence.BaseRepoImpl;

import java.util.Optional;

public class InvoiceRepoImpl extends BaseRepoImpl<Invoice> implements InvoiceRepo {
    @Override
    public Optional<Invoice> getInvoiceByItem(final InvoiceItem item) {
        return repo.stream().filter(inv -> inv.getInvoiceItems().contains(item)).findFirst();
    }
}
