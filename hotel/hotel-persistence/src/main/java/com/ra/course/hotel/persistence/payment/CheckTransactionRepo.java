package com.ra.course.hotel.persistence.payment;

import com.ra.course.hotel.model.entity.payment.CheckTransaction;
import com.ra.course.hotel.persistence.BaseRepo;

public interface CheckTransactionRepo extends BaseRepo<CheckTransaction> {
}
