package com.ra.course.hotel.persistence;

import com.ra.course.hotel.model.entity.BaseEntity;
import com.ra.course.hotel.persistence.exception.AddEntityException;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

public class BaseRepoImpl<T extends BaseEntity> implements BaseRepo<T> {
    private transient long lastId;
    protected final transient CopyOnWriteArrayList<T> repo;

    public BaseRepoImpl() {
        this.repo = new CopyOnWriteArrayList<>();
    }

    @Override
    public T add(final T entity) {
        entity.setId(++lastId);
        if (!repo.addIfAbsent(entity)) {
            throw new AddEntityException(entity.getClass().getSimpleName());
        }
        return entity;
    }

    @Override
    public void update(final T entity) {
        final Optional<T> oldEntity = getById(entity.getId());
        oldEntity.ifPresent(x -> repo.set(repo.indexOf(oldEntity.get()), entity));
    }

    @Override
    public void remove(final T entity) {
        repo.remove(entity);
    }

    @Override
    public List<T> getAll() {
        return repo;
    }

    @Override
    public Optional<T> getById(final long id) {
        return repo.stream().filter(entity -> entity.getId() == id).findFirst();
    }
}
