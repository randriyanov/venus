package com.ra.course.hotel.persistence.invoice;

import com.ra.course.hotel.model.entity.invoice.InvoiceItem;
import com.ra.course.hotel.persistence.BaseRepo;

public interface InvoiceItemRepo extends BaseRepo<InvoiceItem> {
}
