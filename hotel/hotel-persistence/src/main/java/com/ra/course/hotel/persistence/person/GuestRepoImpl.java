package com.ra.course.hotel.persistence.person;

import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.persistence.BaseRepoImpl;

import java.util.Optional;

public class GuestRepoImpl extends BaseRepoImpl<Guest> implements GuestRepo {
    @Override
    public Optional<Guest> getByName(final String name) {
        return repo.stream().filter(g -> g.getName().equals(name)).findAny();
    }

    @Override
    public Optional<Guest> getByBooking(final RoomBooking booking) {
        return repo.stream().filter(g -> g.getBookings().contains(booking)).findAny();
    }
}
