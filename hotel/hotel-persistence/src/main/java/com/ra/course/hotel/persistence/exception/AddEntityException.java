package com.ra.course.hotel.persistence.exception;

public class AddEntityException extends RuntimeException {
    private static final long serialVersionUID = 1;

    public AddEntityException(final String message) {
        super(message + " wasn't added.");
    }
}
