package com.ra.course.hotel.persistence.room;

import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomHouseKeeping;
import com.ra.course.hotel.persistence.BaseRepo;

import java.util.Optional;

public interface RoomRepo extends BaseRepo<Room> {
    Optional<Room> getByNumber(String roomNumber);
    Optional<Room> getByHouseKeeping(RoomHouseKeeping houseKeeping);
}
