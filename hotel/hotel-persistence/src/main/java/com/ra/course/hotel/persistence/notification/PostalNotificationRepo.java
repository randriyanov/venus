package com.ra.course.hotel.persistence.notification;

import com.ra.course.hotel.model.entity.notification.PostalNotification;
import com.ra.course.hotel.persistence.BaseRepo;

public interface PostalNotificationRepo extends BaseRepo<PostalNotification> {
}
