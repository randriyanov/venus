package com.ra.course.hotel.persistence.person;

import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.persistence.BaseRepo;

import java.util.Optional;

public interface GuestRepo extends BaseRepo<Guest> {
    Optional<Guest> getByName(String name);
    Optional<Guest> getByBooking(RoomBooking booking);
}
