package com.ra.course.hotel.persistence.room;

import com.ra.course.hotel.model.entity.room.RoomHouseKeeping;
import com.ra.course.hotel.persistence.BaseRepo;

public interface HouseKeepingRepo extends BaseRepo<RoomHouseKeeping> {
}
