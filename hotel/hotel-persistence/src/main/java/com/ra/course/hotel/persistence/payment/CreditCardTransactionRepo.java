package com.ra.course.hotel.persistence.payment;

import com.ra.course.hotel.model.entity.payment.CreditCardTransaction;
import com.ra.course.hotel.persistence.BaseRepo;

public interface CreditCardTransactionRepo extends BaseRepo<CreditCardTransaction> {
}
