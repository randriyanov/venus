package com.ra.course.hotel.persistence.booking;

import com.ra.course.hotel.model.entity.booking.BookingStatus;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.persistence.BaseRepoImpl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class RoomBookingRepoImpl extends BaseRepoImpl<RoomBooking> implements RoomBookingRepo {
    public static final List<BookingStatus> ACTIVE_STATUSES = Arrays.asList
            (BookingStatus.PENDING, BookingStatus.CONFIRMED, BookingStatus.CHECKED_IN);

    @Override
    public Optional<RoomBooking> getByReservNumber(final String reservNumber) {
        return repo.stream().filter(b -> b.getReservationNumber().equals(reservNumber)).findFirst();
    }

    @Override
    public List<RoomBooking> getAllByStatus(final BookingStatus status) {
        return repo.stream().filter(b -> b.getStatus() == status).collect(Collectors.toList());
    }

    @Override
    public List<RoomBooking> getAllActiveByRoom(final Room room) {
        return repo.stream()
                .filter(b -> b.getRoom() == room)
                .filter(b -> ACTIVE_STATUSES.contains(b.getStatus())).collect(Collectors.toList());
    }
}
