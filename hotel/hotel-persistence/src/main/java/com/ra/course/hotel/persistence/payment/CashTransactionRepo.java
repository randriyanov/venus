package com.ra.course.hotel.persistence.payment;

import com.ra.course.hotel.model.entity.payment.CashTransaction;
import com.ra.course.hotel.persistence.BaseRepo;

public interface CashTransactionRepo extends BaseRepo<CashTransaction> {
}
