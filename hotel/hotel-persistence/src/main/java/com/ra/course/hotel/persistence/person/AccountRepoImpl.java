package com.ra.course.hotel.persistence.person;

import com.ra.course.hotel.model.entity.account.Account;
import com.ra.course.hotel.persistence.BaseRepoImpl;

import java.util.Optional;

public class AccountRepoImpl extends BaseRepoImpl<Account> implements  AccountRepo {
    @Override
    public Optional<Account> getByCredentials(final String login, final String password) {
        return repo.stream().filter(a -> a.getLogin().equals(login) && a.getPassword().equals(password)).findFirst();
    }
}
