package com.ra.course.hotel.persistence.hotel;

import com.ra.course.hotel.model.entity.hotel.Hotel;

public interface HotelRepo {
    void updateName (String name);
    Hotel getHotel();
}
