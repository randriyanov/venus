package com.ra.course.hotel.persistence.notification;

import com.ra.course.hotel.model.entity.notification.EmailNotification;
import com.ra.course.hotel.persistence.BaseRepo;

public interface EmailNotificationRepo extends BaseRepo<EmailNotification> {
}
