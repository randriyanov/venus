package com.ra.course.hotel.persistence.person;

import com.ra.course.hotel.model.entity.persons.HouseKeeper;
import com.ra.course.hotel.persistence.BaseRepo;

public interface HouseKeeperRepo extends BaseRepo<HouseKeeper> {
}
