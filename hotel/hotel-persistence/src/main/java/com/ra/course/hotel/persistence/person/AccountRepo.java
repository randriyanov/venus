package com.ra.course.hotel.persistence.person;

import com.ra.course.hotel.model.entity.account.Account;
import com.ra.course.hotel.persistence.BaseRepo;

import java.util.Optional;

public interface AccountRepo extends BaseRepo<Account> {
    Optional<Account> getByCredentials(String login, String password);
}
