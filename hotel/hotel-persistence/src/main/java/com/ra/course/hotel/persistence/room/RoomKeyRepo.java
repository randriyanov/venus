package com.ra.course.hotel.persistence.room;

import com.ra.course.hotel.model.entity.room.RoomKey;
import com.ra.course.hotel.persistence.BaseRepo;

import java.util.Optional;

public interface RoomKeyRepo extends BaseRepo<RoomKey> {
    Optional<RoomKey> getByKeyId(String keyId);
}
