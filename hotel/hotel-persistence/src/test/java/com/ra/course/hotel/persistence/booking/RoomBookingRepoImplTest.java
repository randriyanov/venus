package com.ra.course.hotel.persistence.booking;

import com.ra.course.hotel.model.entity.booking.BookingStatus;
import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.common.DayInterval;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomStyle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RoomBookingRepoImplTest {
    private RoomBookingRepo bookingRepository;
    private Room room1, room2;

    private static final String EXISTING_RESERV_NUMBER = "RB-111";
    private static final String MISSING_RESERV_NUMBER = "RB-115";

    @BeforeEach
    public void configureRepo() {
        bookingRepository = new RoomBookingRepoImpl();

        room1 = new Room("201", RoomStyle.STANDARD, 700, false);
        room2 = new Room("202", RoomStyle.BUSINESS_SUITE, 850, false);

        var bookingInterval1 = new DayInterval(LocalDate.now(), 2);
        var bookingInterval2 = new DayInterval(LocalDate.now().minusDays(10), 5);
        var bookingInterval3 = new DayInterval(LocalDate.now().plusDays(20), 3);
        var bookingInterval4 = new DayInterval(LocalDate.now().plusDays(7), 4);

        RoomBooking booking1, booking2, booking3, booking4, booking5;

        booking1 = new RoomBooking("RB-111", bookingInterval1, room2);
        booking1.setStatus(BookingStatus.CHECKED_IN);
        booking2 = new RoomBooking("RB-112", bookingInterval2, room2);
        booking2.setStatus(BookingStatus.CHECKED_OUT);
        booking3 = new RoomBooking("RB-114", bookingInterval3, room2);
        booking3.setStatus(BookingStatus.PENDING);
        booking4 = new RoomBooking("RB-118", bookingInterval4, room2);
        booking4.setStatus(BookingStatus.CONFIRMED);
        booking5 = new RoomBooking("RB-119", bookingInterval2, room1);
        booking5.setStatus(BookingStatus.CHECKED_OUT);

        bookingRepository.add(booking1);
        bookingRepository.add(booking2);
        bookingRepository.add(booking3);
        bookingRepository.add(booking4);
        bookingRepository.add(booking5);
    }

    @Test
    @DisplayName("When get room booking by existing reservation number then Optional containing booking with such number is returned")
    public void testGetByExistingReservNumber() {
        //given
        var testReservNumber = EXISTING_RESERV_NUMBER;
        //when
        Optional<RoomBooking> booking = bookingRepository.getByReservNumber(testReservNumber);
        //then
        assertTrue(booking.isPresent());
        assertEquals(testReservNumber, booking.get().getReservationNumber());
    }

    @Test
    @DisplayName("When get room booking by missing reservation number then empty Optional is returned")
    public void testGetByMissingReservNumber() {
        //when
        Optional<RoomBooking> booking = bookingRepository.getByReservNumber(MISSING_RESERV_NUMBER);
        //then
        assertTrue(booking.isEmpty());
    }

    @Test
    @DisplayName("When get room bookings by existing status then list with all bookings having such status is returned")
    public void testGetAllByExistingStatus() {
        //given
        var testStatus = BookingStatus.CHECKED_OUT;
        //when
        List<RoomBooking> bookings = bookingRepository.getAllByStatus(testStatus);
        //then
        assertEquals(2,bookings.size());
        assertTrue(bookings.stream().allMatch(b -> b.getStatus() == testStatus));
    }

    @Test
    @DisplayName("When get room bookings by missing status then empty list is returned")
    public void testGetAllByMissingStatus() {
        //given
        var testStatus = BookingStatus.CANCELED;
        //when
        List<RoomBooking> bookings = bookingRepository.getAllByStatus(testStatus);
        //then
        assertEquals(0, bookings.size());
    }


    @Test
    @DisplayName("When get room bookings by busy room then list with all active bookings for this room is returned")
    public void testGetAllActiveByBusyRoom() {
        //given
        var testRoom = room2;
        //when
        List<RoomBooking> bookings = bookingRepository.getAllActiveByRoom(testRoom);
        //then
        assertEquals(3, bookings.size());
        assertTrue(bookings.stream().allMatch(b -> b.getRoom() == testRoom));
        assertTrue(bookings.stream().allMatch(b -> RoomBookingRepoImpl.ACTIVE_STATUSES.contains(b.getStatus())));
    }

    @Test
    @DisplayName("When get room bookings by free room then empty list is returned")
    public void testGetAllActiveByFreeRoom() {
        //given
        var testRoom = room1;
        //when
        List<RoomBooking> bookings = bookingRepository.getAllActiveByRoom(testRoom);
        //then
        assertEquals(0, bookings.size());
    }
}
