package com.ra.course.hotel.persistence.room;

import com.ra.course.hotel.model.entity.room.RoomKey;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class RoomKeyRepoImplTest {
    private RoomKeyRepo keyRepository;

    private static final String ACTIVE_KEY_ID = "K1";
    private static final String INACTIVE_KEY_ID = "K2";
    private static final String MISSING_KEY_ID = "K99";

    @BeforeEach
    public void configureRepo() {
        keyRepository = new RoomKeyRepoImpl();

        var key1 = new RoomKey();
        key1.setKeyId(ACTIVE_KEY_ID);
        key1.setActive(true);
        key1.setIssuedAt(LocalDateTime.now().minusDays(1));
        keyRepository.add(key1);

        var key2 = new RoomKey();
        key2.setKeyId(INACTIVE_KEY_ID);
        keyRepository.add(key2);
    }

    @Test
    @DisplayName("When get room key by existing keyId then Optional containing key with such keyId is returned")
    public void testGetByExistngKeyId() {
        //given
        var testKeyId = INACTIVE_KEY_ID;
        //when
        Optional<RoomKey> key = keyRepository.getByKeyId(testKeyId);
        //then
        assertTrue(key.isPresent());
        assertEquals(testKeyId, key.get().getKeyId());
    }

    @Test
    @DisplayName("When get room key by missing keyId then empty Optional is returned")
    public void testGetByMissingKeyId() {
        //when
        Optional<RoomKey> key = keyRepository.getByKeyId(MISSING_KEY_ID);
        //then
        assertTrue(key.isEmpty());
    }
}
