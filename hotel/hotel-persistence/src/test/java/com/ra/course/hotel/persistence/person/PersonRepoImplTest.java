package com.ra.course.hotel.persistence.person;

import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.model.entity.persons.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PersonRepoImplTest {
    private PersonRepo personRepository;

    private static final String EXISTING_EMAIL = "peter_kovalenko@gmail.com";
    private static final String MISSING_EMAIL = "bla@gmail.com";

    @BeforeEach
    public void configureRepo() {
        personRepository = new PersonRepoImpl();

        var guest1 = new Guest();
        guest1.setName("Peter Kovalenko");
        guest1.setEmail(EXISTING_EMAIL);
        personRepository.add(guest1);

        var guest2 = new Guest();
        guest2.setName("Mary Kovalenko");
        guest2.setEmail("mary_kovalenko@gmail.com");
        personRepository.add(guest2);
    }

    @Test
    @DisplayName("When get person by existing email then Optional containing person with such email is returned")
    public void testGetByExistingEmail() {
        //given
        var testEmail = EXISTING_EMAIL;
        //when
        Optional<Person> person = personRepository.getByEmail(testEmail);
        //then
        assertTrue(person.isPresent());
        assertEquals(testEmail, person.get().getEmail());
    }

    @Test
    @DisplayName("When get person by missing email then empty Optional is returned")
    public void testGetByMissingEmail() {
        //when
        Optional<Person> person = personRepository.getByEmail(MISSING_EMAIL);
        //then
        assertTrue(person.isEmpty());
    }
}