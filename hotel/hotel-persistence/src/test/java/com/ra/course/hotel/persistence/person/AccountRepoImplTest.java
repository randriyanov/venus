package com.ra.course.hotel.persistence.person;

import com.ra.course.hotel.model.entity.account.Account;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AccountRepoImplTest {
    private AccountRepo accountRepository;

    private static final String TEST_LOGIN = "peter_kovalenko";
    private static final String TEST_PASSWORD = "BlueBird_357";

    @BeforeEach
    public void configureRepo() {
        accountRepository = new AccountRepoImpl();

        accountRepository.add(new Account(TEST_LOGIN, TEST_PASSWORD));
        accountRepository.add(new Account("olga_zayceva", "RoseElephant_76786"));
    }

    @Test
    @DisplayName("When get account by existing credentials then Optional containing account with such credentials is returned")
    public void testGetByExistingCredentials() {
        //when
        Optional<Account> account = accountRepository.getByCredentials(TEST_LOGIN, TEST_PASSWORD);
        //then
        assertTrue(account.isPresent());
        assertEquals(TEST_LOGIN, account.get().getLogin());
        assertEquals(TEST_PASSWORD, account.get().getPassword());
    }

    @Test
    @DisplayName("When get account by missing credentials then empty Optional is returned")
    public void testGetByMissingCredentials() {
        //when
        Optional<Account> account = accountRepository.getByCredentials(TEST_LOGIN, "bla_bla_bla");
        //then
        assertTrue(account.isEmpty());
    }
}
