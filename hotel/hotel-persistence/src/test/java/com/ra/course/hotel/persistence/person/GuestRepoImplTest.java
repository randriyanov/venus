package com.ra.course.hotel.persistence.person;

import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.common.DayInterval;
import com.ra.course.hotel.model.entity.persons.Guest;
import com.ra.course.hotel.model.entity.room.Room;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GuestRepoImplTest {
    private GuestRepo guestRepository;
    private RoomBooking booking;

    private static final String EXISTING_GUEST_NAME = "Peter Kovalenko";
    private static final String MISSING_GUEST_NAME = "Ivan Basov";

    @BeforeEach
    public void configureRepo() {
        guestRepository = new GuestRepoImpl();

        var guest1 = new Guest();
        guest1.setName(EXISTING_GUEST_NAME);
        guest1.setPhone("+380671234567");
        guest1.setEmail("peter_kovalenko@gmail.com");
        guestRepository.add(guest1);

        var guest2 = new Guest();
        guest2.setName("Julia Kovalenko");
        guestRepository.add(guest2);

        var interval = new DayInterval(LocalDate.now(), 3);
        var room = new Room();
        room.setRoomNumber("201");
        booking = new RoomBooking("RB-111", interval, room);
        guest1.getBookings().add(booking);
    }

    @Test
    @DisplayName("When get guest by existing name then Optional containing guest with such name is returned")
    public void testGetByExistingName() {
        //given
        var testName = EXISTING_GUEST_NAME;
        //when
        Optional<Guest> guest = guestRepository.getByName(testName);
        //then
        assertTrue(guest.isPresent());
        assertEquals(testName, guest.get().getName());
    }

    @Test
    @DisplayName("When get guest by missing name then empty Optional is returned")
    public void testGetByMissingName() {
        //when
        Optional<Guest> guest = guestRepository.getByName(MISSING_GUEST_NAME);
        //then
        assertTrue(guest.isEmpty());
    }

    @Test
    @DisplayName("When get guest by existing booking then Optional containing guest with such booking is returned")
    public void testGetByExistingBooking() {
        //when
        Optional<Guest> guest = guestRepository.getByBooking(booking);
        //then
        assertTrue(guest.isPresent());
        assertTrue(guest.get().getBookings().contains(booking));
    }

    @Test
    @DisplayName("When get guest by missing booking then empty Optional is returned")
    public void testGetByMissingBooking() {
        //when
        Optional<Guest> guest = guestRepository.getByBooking(null);
        //then
        assertTrue(guest.isEmpty());
    }
}
