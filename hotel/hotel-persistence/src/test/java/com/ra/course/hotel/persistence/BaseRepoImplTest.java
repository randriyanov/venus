package com.ra.course.hotel.persistence;

import com.ra.course.hotel.model.entity.BaseEntity;
import com.ra.course.hotel.persistence.exception.AddEntityException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.function.Executable;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class BaseRepoImplTest {
    private BaseRepo<BaseEntity> baseRepository;

    private RealBaseEntity testEntity;
    private static final long TEST_ENTITY_ID = 1;
    private static final String CHANGED_ENTITY_NAME = "BigTable";

    public BaseRepoImplTest() {
        baseRepository = new BaseRepoImpl<>();
    }

    @BeforeEach
    public void createEntity() {
        testEntity = new RealBaseEntity("TestEntity");
    }

    @Test
    @DisplayName("When add new entity then repository contains it and entity with id is returned")
    public void testAddNewEntity() {
        //when
        BaseEntity entity = baseRepository.add(testEntity);
        //then
        assertEquals(entity, testEntity);
        assertTrue(baseRepository.getAll().contains(entity));
        assertEquals(1, entity.getId());
    }

    @Test
    @DisplayName("When add duplicate entity then AddEntityException is thrown")
    public void testAddDuplicateEntity() {
        //given
        baseRepository.add(testEntity);
        //when
        Executable executable = () -> baseRepository.add(testEntity);
        //then
        Throwable exception = assertThrows(AddEntityException.class, executable);
        assertEquals(exception.getMessage(), testEntity.getClass().getSimpleName() + " wasn't added.");
    }

    @Test
    @DisplayName("When update entity then it's updated in the repository")
    public void testUpdateEntity() {
        //given
        baseRepository.add(testEntity);
        var changedEntity = new RealBaseEntity(CHANGED_ENTITY_NAME);
        changedEntity.setId(TEST_ENTITY_ID);
        //when
        baseRepository.update(changedEntity);
        //then
        assertTrue(baseRepository.getById(TEST_ENTITY_ID).isPresent() &&
                ((RealBaseEntity) baseRepository.getById(TEST_ENTITY_ID).get()).getName().equals(CHANGED_ENTITY_NAME));
    }

    @Test
    @DisplayName("When remove entity then repository doesn't contain it")
    public void testRemoveEntity() {
        //given
        baseRepository.add(testEntity);
        //when
        baseRepository.remove(testEntity);
        //then
        assertFalse(baseRepository.getAll().contains(testEntity));
    }

    @Test
    @DisplayName("When get entity by existing id then entity with this id is returned")
    public void testGetByExistingId() {
        //given
        baseRepository.add(testEntity);
        baseRepository.add(new RealBaseEntity("Chair"));
        //when
        Optional<BaseEntity> entity = baseRepository.getById(TEST_ENTITY_ID);
        //then
        assertTrue(entity.isPresent() && entity.get().getId() == TEST_ENTITY_ID);
    }

    @Test
    @DisplayName("When get entity by missing id then empty Optional is returned")
    public void testGetByMissingId() {
        //given
        baseRepository.add(testEntity);
        //when
        Optional<BaseEntity> entity = baseRepository.getById(0);
        //then
        assertTrue(entity.isEmpty());
    }

    private class RealBaseEntity extends BaseEntity{
        String name;

        public RealBaseEntity() {
        }

        public RealBaseEntity(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
