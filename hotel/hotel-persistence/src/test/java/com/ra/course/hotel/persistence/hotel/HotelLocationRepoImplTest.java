package com.ra.course.hotel.persistence.hotel;

import com.ra.course.hotel.model.entity.hotel.HotelLocation;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomStyle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class HotelLocationRepoImplTest {
    private HotelLocationRepo locationRepository;
    private Room testRoom;

    private static final String EXISTING_LOCATION_NAME = "Iris Odessa";
    private static final String MISSING_LOCATION_NAME = "Iris Lviv";

    @BeforeEach
    public void configureRepo() {
        locationRepository = new HotelLocationRepoImpl();

        var location1 = new HotelLocation();
        location1.setName(EXISTING_LOCATION_NAME);
        testRoom = new Room("202", RoomStyle.BUSINESS_SUITE, 850, false);
        location1.getRooms().add(testRoom);
        locationRepository.add(location1);

        var location2 = new HotelLocation();
        location2.setName("Iris Kiev");
        locationRepository.add(location2);
    }

    @Test
    @DisplayName("When get hotel location by existing name then Optional containing location with such name is returned")
    public void testGetHotelByExistingName() {
        //given
        var testLocationName = EXISTING_LOCATION_NAME;
        //when
        Optional<HotelLocation> location = locationRepository.getByName(testLocationName);
        //then
        assertTrue(location.isPresent());
        assertEquals(testLocationName, location.get().getName());
    }

    @Test
    @DisplayName("When get hotel location by missing name then empty Optional is returned")
    public void testGetHotelByMissingName() {
        //when
        Optional<HotelLocation> location = locationRepository.getByName(MISSING_LOCATION_NAME);
        //then
        assertTrue(location.isEmpty());
    }

    @Test
    @DisplayName("When get hotel location by existing room then Optional with location containing such room is returned")
    public void testGetHotelByExistingRoom() {
        //when
        Optional<HotelLocation> location = locationRepository.getByRoom(testRoom);
        //then
        assertTrue(location.isPresent());
        assertTrue(location.get().getRooms().contains(testRoom));
    }

    @Test
    @DisplayName("When get hotel location by missing room then empty Optional is returned")
    public void testGetHotelByMissingRoom() {
        //when
        Optional<HotelLocation> location = locationRepository.getByRoom(null);
        //then
        assertTrue(location.isEmpty());
    }
}
