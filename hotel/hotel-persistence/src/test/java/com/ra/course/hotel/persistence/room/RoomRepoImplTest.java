package com.ra.course.hotel.persistence.room;

import com.ra.course.hotel.model.entity.common.MinuteInterval;
import com.ra.course.hotel.model.entity.room.Room;
import com.ra.course.hotel.model.entity.room.RoomHouseKeeping;
import com.ra.course.hotel.model.entity.room.RoomStyle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class RoomRepoImplTest {
    private RoomRepo roomRepository;
    private RoomHouseKeeping testHouseKeeping;

    private static final String EXISTING_ROOM_NUMBER = "201";
    private static final String MISSING_ROOM_NUMBER = "205";

    @BeforeEach
    public void configureRepo() {
        roomRepository = new RoomRepoImpl();

        roomRepository.add(new Room(EXISTING_ROOM_NUMBER, RoomStyle.STANDARD, 700,false));
        var room = new Room("202", RoomStyle.BUSINESS_SUITE, 850, false);
        roomRepository.add(room);
        testHouseKeeping = new RoomHouseKeeping("Test housekeeping", new MinuteInterval(LocalDateTime.now(), 30));
        room.getHouseKeepings().add(testHouseKeeping);
        roomRepository.add(new Room("204", RoomStyle.BUSINESS_SUITE, 900, true));
    }

    @Test
    @DisplayName("When get room by existing room number then Optional containing room with such number is returned")
    public void testGetByExistingNumber() {
        //given
        var testRoomNumber = EXISTING_ROOM_NUMBER;
        //when
        Optional<Room> room = roomRepository.getByNumber(testRoomNumber);
        //then
        assertTrue(room.isPresent());
        assertEquals(testRoomNumber, room.get().getRoomNumber());
    }

    @Test
    @DisplayName("When get room by missing room number then empty Optional is returned")
    public void testGetByMissingNumber() {
        //when
        Optional<Room> room = roomRepository.getByNumber(MISSING_ROOM_NUMBER);
        //then
        assertTrue(room.isEmpty());
    }

    @Test
    @DisplayName("When get room by existing housekeeping then Optional containing room with such housekeeping is returned")
    public void testGetByExistingHouseKeeping() {
        //when
        Optional<Room> room = roomRepository.getByHouseKeeping(testHouseKeeping);
        //then
        assertTrue(room.isPresent());
        assertTrue(room.get().getHouseKeepings().contains(testHouseKeeping));
    }

    @Test
    @DisplayName("When get room by missing housekeeping then empty Optional is returned")
    public void testGetByMissingHouseKeeping() {
        //when
        Optional<Room> room = roomRepository.getByHouseKeeping(null);
        //then
        assertTrue(room.isEmpty());
    }
}
