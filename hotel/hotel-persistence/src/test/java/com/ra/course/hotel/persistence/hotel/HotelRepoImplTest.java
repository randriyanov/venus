package com.ra.course.hotel.persistence.hotel;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HotelRepoImplTest {
    private HotelRepo hotelRepository;

    public HotelRepoImplTest() {
        hotelRepository = new HotelRepoImpl();
    }

    @Test
    @DisplayName("When update hotel name then hotel has new name")
    public void testUpdateName() {
        //given
        var testName="Iris";
        //when
        hotelRepository.updateName(testName);
        //then
        assertEquals(testName, hotelRepository.getHotel().getName());
    }
}
