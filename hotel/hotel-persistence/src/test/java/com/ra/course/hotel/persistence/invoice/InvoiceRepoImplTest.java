package com.ra.course.hotel.persistence.invoice;

import com.ra.course.hotel.model.entity.booking.RoomBooking;
import com.ra.course.hotel.model.entity.invoice.Invoice;
import com.ra.course.hotel.model.entity.invoice.InvoiceItem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class InvoiceRepoImplTest {
    private InvoiceRepo invoiceRepository;
    private InvoiceItem invoiceItem1, invoiceItem2;

    @BeforeEach
    public void configureRepo() {
        invoiceRepository = new InvoiceRepoImpl();

        var invoice1 = new Invoice();
        invoice1.setBooking(new RoomBooking());
        var mainItem = new InvoiceItem("Room charge", 700);
        invoice1.getInvoiceItems().add(mainItem);
        invoiceItem1 = new InvoiceItem("Breakfast", 90);
        invoice1.getInvoiceItems().add(invoiceItem1);
        invoice1.setTotalAmount(mainItem.getAmount() + invoiceItem1.getAmount());
        invoiceRepository.add(invoice1);

        var invoice2 = new Invoice();
        invoice2.setBooking(new RoomBooking());
        invoiceItem2 = new InvoiceItem("Laundry", 120);
        invoice2.getInvoiceItems().add(invoiceItem2);
        invoice2.setTotalAmount(invoiceItem2.getAmount());
        invoiceRepository.add(invoice2);
    }

    @Test
    @DisplayName("When get invoice by existing invoice item then Optional containing invoice with such item is returned")
    public void testGetInvoiceByExistingItem() {
        //given
        var testInvoiceItem = invoiceItem1;
        //when
        Optional<Invoice> invoice = invoiceRepository.getInvoiceByItem(testInvoiceItem);
        //then
        assertTrue(invoice.isPresent());
        assertTrue(invoice.get().getInvoiceItems().contains(testInvoiceItem));
    }

    @Test
    @DisplayName("When get invoice by missing invoice item then empty Optional is returned")
    public void testGetInvoiceByMissingItem() {
        //when
        Optional<Invoice> invoice = invoiceRepository.getInvoiceByItem(null);
        //then
        assertTrue(invoice.isEmpty());
    }
}
