package com.ra.course.linkedin.model.entity.other;

import com.ra.course.linkedin.model.entity.BaseEntity;
import com.ra.course.linkedin.model.entity.person.Member;
import lombok.*;

import java.sql.Blob;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Message extends BaseEntity {
    private List<Member> sendTo;
    private String messageBody;
    private Blob[] media;
    private Member author;

    public Message(List<Member> sendTo, String messageBody, Member author) {
        this.sendTo = sendTo;
        this.messageBody = messageBody;
        this.author = author;
    }
}
